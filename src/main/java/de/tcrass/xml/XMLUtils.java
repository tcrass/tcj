package de.tcrass.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import de.tcrass.io.FileUtils;
import de.tcrass.util.SysUtils;

public class XMLUtils {

    public static final String XMLNS_URI             = "http://www.w3.org/2000/xmlns/";

    public static final String XSI_NAMESPACE_URI     = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String DEFAULT_XSI_NS_PREFIX = "xsi";

    public static final int    INDENTATION           = 2;

    public static DocumentBuilder createDocumentBuilder() {
        DocumentBuilder db = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            db = dbf.newDocumentBuilder();
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
        return db;
    }

    public static Document createDocument() {
        Document d = null;
        try {
            DocumentBuilder db = createDocumentBuilder();
            d = db.newDocument();
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
        return d;
    }

    public static Element toXML(Document d, XMLable obj) {
        Element e = d.createElement(obj.getClass().getName());
        obj.toXML(e);
        return e;
    }

    public static Element listToXML(Document d, List<?> v, String name) {
        Element e = d.createElement(name);
        for (Object o : v) {
            if (o instanceof XMLable) {
                Element item = toXML(d, (XMLable) o);
                e.appendChild(item);
            }
        }
        return e;
    }

    public static Element arrayToXML(Document d, Object[] a, String name) {
        return listToXML(d, new ArrayList<Object>(Arrays.asList(a)), name);
    }

    public static XMLable fromXML(Document d) {
        return fromXML(d.getDocumentElement());
    }

    public static XMLable fromXML(Element e) {
        XMLable o = null;
        try {
            XMLable obj = (XMLable) Class.forName(e.getNodeName()).newInstance();
            if (obj.fromXML(e)) {
                o = obj;
            }
        }
        catch (Exception ex) {
            SysUtils.reportException(ex);
        }
        return o;
    }

    public static List<?> listFromXML(Element e) {
        List<Object> v = new ArrayList<Object>();
        List<Element> c = XMLUtils.getChildren(e);
        for (Element element : c) {
            Object o = XMLUtils.fromXML(element);
            if (o != null) {
                v.add(o);
            }
        }
        return v;
    }

    public static void writeToFile(Document d, File file) throws IOException,
    TransformerException {
        FileOutputStream fs = new FileOutputStream(file.getCanonicalPath());
        writeToStream(d, fs);
        fs.close();
    }

    public static void writeToFile(Document d, String filename) throws IOException,
    TransformerException {
        writeToFile(d, new File(filename));
    }

    public static void writeToStream(Document d, OutputStream s) throws IOException,
    TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setAttribute("indent-number", INDENTATION);
        Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        Source src = new DOMSource(d);
        Result out = new StreamResult(new OutputStreamWriter(s, "utf-8"));
        t.transform(src, out);
    }

    public static Document readFromStream(InputStream is) throws IOException, SAXException {
        Document d = null;
        d = readFromStream(is, createDocumentBuilder());
        return d;
    }

    public static Document readFromStream(InputStream is, DocumentBuilder db)
    throws IOException, SAXException {
        Document d = null;
        d = db.parse(is);
        return d;
    }

    public static Document readFromFile(File file, DocumentBuilder db) throws IOException,
    SAXException {
        Document d = null;
        FileInputStream fs = new FileInputStream(file.getCanonicalPath());
        d = readFromStream(fs, db);
        fs.close();
        return d;
    }

    public static Document readFromFile(String filename, DocumentBuilder db)
    throws IOException, SAXException {
        return readFromFile(new File(filename), db);
    }

    public static Document readFromFile(File file) throws IOException, SAXException {
        Document d = null;
        d = readFromFile(file, createDocumentBuilder());
        return d;
    }

    public static Document readFromFile(String filename) throws IOException, SAXException {
        return readFromFile(new File(filename));
    }

    public static Document readXMLResource(String path, String name) throws IOException {
        Document d = null;
        try {
            InputStream is = FileUtils.createResourceInputStream(path, name);
            d = readFromStream(is);
            is.close();
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
        return d;
    }

    public static void setAttribute(Element e, String key, String val) {
        e.setAttribute(key, val);
    }

    public static void setAttribute(Element e, String key, int val) {
        e.setAttribute(key, String.valueOf(val));
    }

    public static void setAttribute(Element e, String key, long val) {
        e.setAttribute(key, String.valueOf(val));
    }

    public static void setAttribute(Element e, String key, float val) {
        e.setAttribute(key, String.valueOf(val));
    }

    public static void setAttribute(Element e, String key, double val) {
        e.setAttribute(key, String.valueOf(val));
    }

    public static void setAttribute(Element e, String key, boolean val) {
        e.setAttribute(key, String.valueOf(val));
    }

    public static void setAttributeNS(Element e, String ns, String key, String val) {
        e.setAttributeNS(ns, key, val);
    }

    public static void setAttributeNS(Element e, String ns, String key, int val) {
        e.setAttributeNS(ns, key, String.valueOf(val));
    }

    public static void setAttributeNS(Element e, String ns, String key, long val) {
        e.setAttributeNS(ns, key, String.valueOf(val));
    }

    public static void setAttributeNS(Element e, String ns, String key, float val) {
        e.setAttributeNS(ns, key, String.valueOf(val));
    }

    public static void setAttributeNS(Element e, String ns, String key, double val) {
        e.setAttributeNS(ns, key, String.valueOf(val));
    }

    public static void setAttributeNS(Element e, String ns, String key, boolean val) {
        e.setAttributeNS(ns, key, String.valueOf(val));
    }

    public static String getStringAttribute(Element e, String key) {
        String a = e.getAttribute(key);
        if (a == null) {
            a = "";
        }
        return a;
    }

    public static int getIntAttribute(Element e, String key) {
        int i = 0;
        String a = e.getAttribute(key);
        if (!a.equals("")) {
            i = Integer.parseInt(a);
        }
        return i;
    }

    public static long getLongAttribute(Element e, String key) {
        long l = 0;
        String a = e.getAttribute(key);
        if (!a.equals("")) {
            l = Long.parseLong(a);
        }
        return l;
    }

    public static float getFloatAttribute(Element e, String key) {
        float f = 0f;
        String a = e.getAttribute(key);
        if (!a.equals("")) {
            f = Float.parseFloat(a);
        }
        return f;
    }

    public static double getDoubleAttribute(Element e, String key) {
        double d = 0;
        String a = e.getAttribute(key);
        if (!a.equals("")) {
            d = Double.parseDouble(a);
        }
        return d;
    }

    public static boolean getBooleanAttribute(Element e, String key) {
        boolean b = false;
        String a = e.getAttribute(key);
        if (a.equals("")) {
            b = false;
        }
        else if (a.equals("0")) {
            b = false;
        }
        else {
            try {
                b = Boolean.valueOf(a.toLowerCase()).booleanValue();
            }
            catch (Exception ex) {
                b = true;
            }
        }
        return b;
    }

    public static List<String> getAttributeNames(Element e) {
        List<String> names = new ArrayList<String>();
        NamedNodeMap attribs = e.getAttributes();
        for (int i = 0; i < attribs.getLength(); i++) {
            names.add(attribs.item(i).getNodeName());
        }
        return names;
    }

    public static void addText(Element e, String text) {
        Text textNode = e.getOwnerDocument().createTextNode(text);
        e.appendChild(textNode);
    }

    public static String getText(Element e) {
        String text = "";
        NodeList children = e.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node instanceof Text) {
                text += ((Text) node).getNodeValue();
            }
        }
        return text;
    }

    public static List<Element> getChildren(Element e) {
        List<Element> c = new ArrayList<Element>();
        NodeList children = e.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node instanceof Element) {
                c.add((Element) node);
            }
        }
        return c;
    }

    public static Element getChildByName(Element e, String name) {
        Element c = null;
        NodeList children = e.getChildNodes();
        int i = 0;
        boolean found = false;
        while ((!found) && (i < children.getLength())) {
            Node node = children.item(i);
            if ((node instanceof Element) && ((Element) node).getNodeName().equals(name)) {
                c = (Element) node;
                found = true;
            }
            i++;
        }
        return c;
    }

    public static List<Element> getChildrenByName(Element e, String name) {
        List<Element> c = new ArrayList<Element>();
        NodeList children = e.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if ((node instanceof Element) && ((Element) node).getNodeName().equals(name)) {
                c.add((Element) node);
            }
        }
        return c;
    }

}

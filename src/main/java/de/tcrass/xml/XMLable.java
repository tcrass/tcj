package de.tcrass.xml;

import org.w3c.dom.Element;

public interface XMLable {

    void toXML(Element e);

    boolean fromXML(Element e);

}

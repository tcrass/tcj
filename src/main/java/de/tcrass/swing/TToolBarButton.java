/*
 * Created on 03.02.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import de.tcrass.awt.AWTUtils;
import de.tcrass.swing.plaf.basic.ToolbarButtonUI;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TToolBarButton extends TRaisedButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Dimension         dim              = new Dimension(32, 32);

    // public TToolBarButton (String label, String command) {
    // this(label, null, command);
    // }

    public TToolBarButton(Icon icon, String command) {
        super(null, icon);
        setActionCommand(command);
        setRolloverEnabled(true);
        setFocusable(false);
        setContentAreaFilled(false);
        setUI(ToolbarButtonUI.createUI(this));
    }

    // public TToolBarButton (Icon icon, String command) {
    // this(null, icon, command);
    // }

    @Override
    public Dimension getMinimumSize() {
        return dim;
    }

    @Override
    public Dimension getPreferredSize() {
        return dim;
    }

    @Override
    public Dimension getMaximumSize() {
        return dim;
    }

    @Override
    public void setSize(int sizeX, int sizeY) {
        dim = new Dimension(sizeX, sizeY);
        super.setSize(sizeX, sizeY);
    }

    public static TToolBarButton createIconButton(String command, String hint, String iconPath) {
        TToolBarButton tbb = null;
        try {
            tbb = new TToolBarButton(new ImageIcon(AWTUtils.readImageResource(iconPath,
                command + ".png", new Component() {

                    /**
                     * 
                     */
                    private static final long serialVersionUID = 1L;
                })), command);
            tbb.setToolTipText(hint);
        }
        catch (IOException e) {
            SysUtils.reportException(e);
        }
        return tbb;
    }

}

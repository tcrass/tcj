package de.tcrass.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;

import de.tcrass.awt.dnd.DnDManager;

public class DragSelectionManager implements MouseListener, MouseMotionListener {

    private Point                 dragStart, dragNow;
    private JComponent            component;
    private DragSelectionListener listener;
    private int                   modifiers;

    public DragSelectionManager(JComponent c, DragSelectionListener l) {
        component = c;
        listener = l;
        c.addMouseListener(this);
        c.addMouseMotionListener(this);
    }

    private Rectangle getDragRect(Point a, Point b) {
        int x1 = (a.x < b.x ? a.x : b.x);
        int y1 = (a.y < b.y ? a.y : b.y);
        int x2 = (a.x < b.x ? b.x : a.x);
        int y2 = (a.y < b.y ? b.y : a.y);
        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }

    public void dispose() {
        component.removeMouseListener(this);
        component.removeMouseMotionListener(this);
    }

    public void paintSelectionRect(Graphics g) {
        if (dragNow != null) {
            Color c = SystemColor.textHighlight;
            Rectangle dragRect = getDragRect(dragStart, dragNow);
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 64));
            g2.fillRect(dragRect.x, dragRect.y, dragRect.width, dragRect.height);
            g2.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 128));
            g2.drawRect(dragRect.x, dragRect.y, dragRect.width, dragRect.height);
        }
    }

    // --- Implementation of MouseListener

    public void mousePressed(MouseEvent e) {
        modifiers = e.getModifiers();
        dragStart = e.getPoint();
    }

    public void mouseReleased(MouseEvent e) {
        if (dragNow != null) {
            dragNow = e.getPoint();
            listener.dragSelectionCompleted(getDragRect(dragStart, dragNow), modifiers);
        }
        component.repaint();
        dragStart = null;
        dragNow = null;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        Point p = e.getPoint();
        if (dragNow == null) {
            if ((Math.abs(p.x - dragStart.x) + Math.abs(p.y - dragStart.y)) > DnDManager.DEFAULT_MIN_DRAG_DELTA) {
                dragNow = p;
                listener.dragSelectionStarted(dragStart, e.getModifiers());
            }
        }
        if (dragNow != null) {
            dragNow = p;
            component.scrollRectToVisible(new Rectangle(dragNow.x, dragNow.y, 1, 1));
            component.repaint();
        }
    }

}

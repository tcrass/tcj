package de.tcrass.swing;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.AWTEventListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;

import de.tcrass.awt.AWTUtils;
import de.tcrass.awt.dnd.DnDManager;
import de.tcrass.awt.dnd.DnDMedium;

public class ZPanel extends JPanel
implements AWTEventListener, DragSelectionListener, DnDMedium {

    /**
     * 
     */
    private static final long    serialVersionUID           = 1L;

    public static final int      SELECT_ON_ADD              = 1;

    public static final int      SELECTION_MANDATORY        = 2;

    public static final int      MULTIPLE_SELECTION_ALLOWED = 4;

    public static final int      SELECT_BY_DRAGGING         = 8;

    public static final int      BORDER_SELECT_MASK         = 65535;

    private static final int     MULTIPLE_SELECTION_MASK    = MULTIPLE_SELECTION_ALLOWED |
                                                              SELECT_BY_DRAGGING;
    private ArrayList<Component> selectedChildren;

    private int                  selectPolicy               = SELECT_ON_ADD |
                                                              MULTIPLE_SELECTION_ALLOWED;

    private boolean              updating                   = false;

    private boolean              dragSelecting              = false;
    private Point                dragStart, dragNow;

    private Component            lastClicked;

    private DragSelectionManager dsm;

    public ZPanel() {
        super();
        getToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK);
        dsm = new DragSelectionManager(this, this);
        selectedChildren = new ArrayList<Component>();
    }

    // --- Private methods

    private Component findSelectable(Container c, MouseEvent event) {
        Component s = null;
        Object source = event.getSource();
        if (source instanceof Component) {
            Point p = AWTUtils.convertCoords(event.getPoint(), (Component) event.getSource(), c);
            for (int i = 0; i < c.getComponents().length; i++) {
                Component child = c.getComponents()[i];
                if (child instanceof ZSelectable) {
                    ZSelectable z = (ZSelectable) child;
                    if (child.getBounds().contains(p)) {
                        if ((z.getZSelectProperties() & BORDER_SELECT_MASK) != 0) {
                            int border = z.getZSelectProperties() & 65535;
                            Rectangle r = new Rectangle(child.getLocation().x + border,
                                child.getLocation().y + border, child.getSize().width - 2 *
                                                                border, child.getSize().height -
                                                                        2 * border);
                            if (r.contains(p) && (child instanceof Container)) {
                                s = findSelectable((Container) child, event);
                            }
                            else {
                                s = child;
                            }
                        }
                        else {
                            s = child;
                        }
                    }
                }
                if (s != null) {
                    break;
                }
            }
        }
        return s;
    }

    // private Component findSelectable(Component child) {
    // Component sel = null;
    // if (child != this) {
    // if (child.getParent() != this) {
    // sel = findSelectable(child.getParent());
    // }
    // if ((sel == null) && (child instanceof ZSelectable)) {
    // sel = child;
    // }
    // }
    // return sel;
    // }

    private void mouseDown(MouseEvent event) {
        // Component child = (Component) event.getSource();
        lastClicked = findSelectable(this, event);
    }

    private void mouseUp(MouseEvent event) {
        // Point p = event.getPoint();
        // Component source = (Component)event.getSource();
        // p = AWTUtils.convertCoords(p, source, this);
        // Component child = findComponentAt(p);
        // Component c = null;
        // if (child != null) {
        Component c = findSelectable(this, event);
        // }

        if (!dragSelecting && (dragStart == null) && (c == lastClicked)) {
            if (c == null) {
                if ((selectPolicy & SELECTION_MANDATORY) == 0) {
                    deselectAll();
                }
            }
            else {
                if (event.isShiftDown()) {
                    select(c);
                }
                else if (event.isControlDown()) {
                    invertSelection(c);
                }
                else {
                    selectOnly(c);
                }
            }
        }
        dragStart = null;
    }

    private void updateChildren(ArrayList<Component> children) {
        updating = true;
        removeAll();
        for (Component c : children) {
            add(c);
        }
        updating = false;
    }

    private boolean canBeSelected(Component c) {
        boolean can = false;
        if ((c instanceof ZSelectable) && !isSelected(c)) {
            ArrayList<Component> children = new ArrayList<Component>(
                Arrays.asList(getComponents()));
            if (children.contains(c)) {
                can = (((ZSelectable) c).getZSelectProperties() & ZSelectable.IS_SELECTABLE) != 0;
            }
        }
        return can;
    }

    // --- Public methods

    public void setSelectPolicy(int p) {
        selectPolicy = p;
        if ((selectPolicy & SELECT_BY_DRAGGING) != 0) {
            if (dsm == null) {
                dsm = new DragSelectionManager(this, this);
            }
        }
        else {
            if (dsm != null) {
                dsm.dispose();
            }
            dsm = null;
        }
    }

    public java.util.List<Component> getSelectedChildren() {
        return selectedChildren;
    }

    public boolean isSelected(Component c) {
        return selectedChildren.contains(c);
    }

    public Component getLastSelected() {
        if (selectedChildren.size() > 0) {
            return selectedChildren.get(selectedChildren.size() - 1);
        }
        else {
            return null;
        }
    }

    public boolean isLastSelected(Component c) {
        return getLastSelected() == c;
    }

    public void clearSelection() {
        for (int i = selectedChildren.size() - 1; i >= 0; i--) {
            Component c = selectedChildren.remove(i);
            ZSelectable s = (ZSelectable) c;
            s.onDeselection();
            c.repaint();
        }
    }

    public void addToSelection(Component c) {
        if (canBeSelected(c)) {
            ZSelectable s = (ZSelectable) c;
            if (getLastSelected() != null) {
                ((ZSelectable) getLastSelected()).staySelected();
                getLastSelected().repaint();
            }
            selectedChildren.add(c);
            if ((s.getZSelectProperties() & ZSelectable.RAISE_ON_SELECTION) != 0) {
                moveToFront(c);
            }
            s.onSelection();
            c.repaint();
        }
    }

    public void ensureMandatorySelection() {
        if ((selectPolicy & SELECTION_MANDATORY) != 0) {
            if (selectedChildren.size() == 0) {
                Component[] children = getComponents();
                boolean found = false;
                int i = children.length;
                while (!found && (i > 0)) {
                    i--;
                    Component c = children[i];
                    if (c instanceof ZSelectable) {
                        select(c);
                        found = true;
                    }
                }
            }
        }
    }

    public void removeFromSelection(Component c) {
        if (isSelected(c)) {
            ZSelectable s = (ZSelectable) c;
            selectedChildren.remove(c);
            s.onDeselection();
            c.repaint();
            Component lc = getLastSelected();
            if (lc != null) {
                ((ZSelectable) lc).onSelection();
                lc.repaint();
            }
        }
    }

    public void select(Component c) {
        if (canBeSelected(c)) {
            ZSelectable s = (ZSelectable) c;
            if ((selectPolicy & MULTIPLE_SELECTION_ALLOWED) == 0) {
                clearSelection();
            }
            else {
                Component lc = getLastSelected();
                if (lc != null) {
                    ((ZSelectable) lc).staySelected();
                    lc.repaint();
                }
            }
            addToSelection(c);
        }
    }

    public void select(java.util.List<Component> components) {
        for (Component c : components) {
            select(c);
        }
    }

    public void selectOnly(Component c) {
        deselectAll();
        select(c);
    }

    public void deselect(Component c) {
        if (isSelected(c)) {
            removeFromSelection(c);
        }
    }

    public void deselect(java.util.List<Component> components) {
        for (Component c : components) {
            deselect(c);
        }
    }

    public void deselectAll() {
        for (int i = selectedChildren.size() - 1; i >= 0; i--) {
            Component c = selectedChildren.get(i);
            deselect(c);
        }
    }

    public void invertSelection(Component c) {
        if (isSelected(c)) {
            deselect(c);
        }
        else {
            select(c);
        }
    }

    public void invertSelection(java.util.List<Component> components) {
        for (Component c : components) {
            invertSelection(c);
        }
    }

    public void moveToFront(Component comp) {
        ArrayList<Component> children = new ArrayList<Component>(Arrays.asList(getComponents()));
        if (children.contains(comp)) {
            ArrayList<Component> v = new ArrayList<Component>();
            v.add(comp);
            for (Component c : children) {
                if (c != comp) {
                    v.add(c);
                }
            }
            updateChildren(v);
        }
    }

    public void moveToBack(Component comp) {
        ArrayList<Component> children = new ArrayList<Component>(Arrays.asList(getComponents()));
        if (children.contains(comp)) {
            ArrayList<Component> v = new ArrayList<Component>();
            for (Component c : children) {
                if (c != comp) {
                    v.add(c);
                }
            }
            v.add(comp);
            updateChildren(v);
        }
    }

    // --- Overwritten methods

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (dsm != null) {
            dsm.paintSelectionRect(g);
        }
        if (isDraggingChildren()) {
            g.setColor(Color.BLACK);
            g.setXORMode(Color.WHITE);
            int dx = dragNow.x - dragStart.x;
            int dy = dragNow.y - dragStart.y;
            for (Component c : selectedChildren) {
                Rectangle b = c.getBounds();
                g.drawRect(b.x + dx, b.y + dy, b.width, b.height);
            }
        }
    }

    @Override
    public void addImpl(Component c, Object constraints, int index) {
        if (updating) {
            super.addImpl(c, constraints, index);
        }
        else {
            super.addImpl(c, constraints, index);
            if ((selectPolicy & SELECT_ON_ADD) != 0) {
                select(c);
            }
        }
    }

    @Override
    public void remove(int index) {
        if (updating) {
            super.remove(index);
        }
        else {
            Component c = getComponent(index);
            if (c != null) {
                remove(c);
            }
        }
    }

    @Override
    public void remove(Component comp) {
        if (updating) {
            super.remove(comp);
        }
        else {
            updating = true;
            removeFromSelection(comp);
            super.remove(comp);
            ensureMandatorySelection();
            updating = false;
        }
    }

    @Override
    public void removeAll() {
        if (!updating) {
            deselectAll();
        }
        super.removeAll();
    }

    // --- Implementation of EventListener

    public void eventDispatched(AWTEvent e) {
        if (isShowing() && (e instanceof MouseEvent)) {
            // System.out.println("Click! in ZPanel: "+e.toString());
            MouseEvent me = (MouseEvent) e;
            Object o = me.getSource();
            if (o instanceof Component) {
                Component s = (Component) o;

                if ((me.getID() == MouseEvent.MOUSE_PRESSED) ||
                    (me.getID() == MouseEvent.MOUSE_RELEASED)) {
                    if (s.isShowing() && ((s == this) || isAncestorOf(s))) {
                        if (me.getID() == MouseEvent.MOUSE_PRESSED) {
                            mouseDown(me);
                        }
                        else if (me.getID() == MouseEvent.MOUSE_RELEASED) {
                            mouseUp(me);
                        }
                    }

                }
            }
        }
    };

    // --- Implementation of DragSelectionListener

    public boolean isDragSelecting() {
        return dragSelecting;
    }

    private java.util.List<Component> findSelectablesIn(Rectangle r) {
        ArrayList<Component> selectables = new ArrayList<Component>();
        for (Component c : getComponents()) {
            if (r.contains(c.getBounds())) {
                selectables.add(c);
            }
        }
        return selectables;
    }

    public void dragSelectionStarted(Point dragStart, int modifiers) {
        dragSelecting = true;
    }

    public void dragSelectionCompleted(Rectangle dragRect, int modifiers) {
        java.util.List<Component> components = findSelectablesIn(dragRect);
        if ((modifiers & InputEvent.SHIFT_MASK) != 0) {
            select(components);
        }
        else if ((modifiers & InputEvent.CTRL_MASK) != 0) {
            invertSelection(components);
        }
        else {
            deselectAll();
            select(components);
        }
        dragSelecting = false;
    }

    // --- Implementation of DnDMedium

    protected boolean isDraggingChildren() {
        return ((dragStart != null) && (dragNow != null));
    }

    public void dragStarted(DnDManager dm, Object item, Point where) {
        if (item instanceof Component) {
            Component c = (Component) item;
            if (isSelected(c) || canBeSelected(c)) {
                if (!isSelected(c)) {
                    selectOnly(c);
                }
                dragStart = where;
            }
        }
    }

    public void itemDragged(DnDManager dm, Object item, Point currentPos) {
        if (dragStart != null) {
            dragNow = currentPos;
        }
    }

    public void itemDropped(DnDManager dm, Object item, Component target) {
        if ((dragStart != null) && (dragNow != null)) {
            int dx = dragNow.x - dragStart.x;
            int dy = dragNow.y - dragStart.y;
            for (Component c : selectedChildren) {
                c.setLocation(c.getLocation().x + dx, c.getLocation().y + dy);
            }
            // dragStart = null;
            dragNow = null;
        }
    }

}

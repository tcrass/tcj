/*
 * Created on 28.08.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TIntCluster extends TNumericCluster {

    private static final String INVALID_INPUT_TOOLTIP = "Field must contain a valid integer number!";

    public TIntCluster(String title) {
        super(title);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(0);
    }

    public TIntCluster(String title, int value) {
        super(title);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(value);
    }

    public TIntCluster(String title, long value) {
        super(title);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(value);
    }

    public TIntCluster(String title, byte value) {
        super(title);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(value);
    }

    public TIntCluster(String title, int titleWidth, int valueWidth) {
        super(title, titleWidth, valueWidth);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(0);
    }

    public TIntCluster(String title, int titleWidth, int value, int valueWidth) {
        super(title, titleWidth, valueWidth);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(value);
    }

    public TIntCluster(String title, int titleWidth, long value, int valueWidth) {
        super(title, titleWidth, valueWidth);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(value);
    }

    public TIntCluster(String title, int titleWidth, byte value, int valueWidth) {
        super(title, titleWidth, valueWidth);
        setInvalidInputTooltip(INVALID_INPUT_TOOLTIP);
        setValue(value);
    }

    public void setValue(int value) {
        super.setValue(Integer.toString(value));
    }

    public void setValue(long value) {
        super.setValue(Long.toString(value));
    }

    public void setValue(byte value) {
        super.setValue(Byte.toString(value));
    }

    public int getValueAsInt() {
        return new Long(getValue()).intValue();
    }

    public long getValueAsLong() {
        return new Long(getValue()).longValue();
    }

    public byte getValueAsByte() {
        return new Long(getValue()).byteValue();
    }

    @Override
    protected InputValidator createInputValidator() {
        return new InputValidator() {

            public boolean isValidInput(String s) {
                boolean valid = true;
                try {
                    Double.parseDouble(s);
                }
                catch (NumberFormatException e) {
                    valid = false;
                }
                return valid;
            }
        };
    }

    // protected void markAsValidInput(boolean valid) {
    // super.markAsValidInput(valid);
    // if (valid) {
    // getTextField().setToolTipText(null);
    // }
    // else {
    // getTextField().setToolTipText("Field must contain a valid integer
    // number!");
    // }
    // }

}

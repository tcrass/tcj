package de.tcrass.swing.dialog;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import de.tcrass.awt.AWTUtils;
import de.tcrass.swing.FixedSizeLabel;
import de.tcrass.swing.SwingUtils;
import de.tcrass.util.ApplicationTimer;
import de.tcrass.util.TimerEvent;
import de.tcrass.util.TimerListener;

public class TProgressDialog extends TDialog implements TimerListener {

    /**
     * 
     */
    private static final long serialVersionUID  = 1L;

    private static final int  POLLING_INTERVALL = 100;

    private Thread            launcher;
    private Thread            worker;
    private boolean           interruptable;
    private long              startTime;
    private BufferedImage     iconImage;
    private JLabel            timePanel;

    private FixedSizeLabel    text;
    private JProgressBar      bar;

    public TProgressDialog(String title, int width, boolean interruptable, Frame owner) {
        this(title, width, 0, interruptable, CLOCK_ICON, owner);
    }

    public TProgressDialog(String title, int width, int max, boolean interruptable, Frame owner) {
        this(title, width, max, interruptable, INFORMATION_ICON, owner);
    }

    public TProgressDialog(String title, int width, int max, boolean interruptable,
                           int iconType, Frame owner) {
        super(title, (interruptable ? TDialog.CANCEL_BUTTON : TDialog.NO_BUTTONS), 0,
            createImageIcon(iconType, 0, owner), owner);
        initDialog(width, max, interruptable, iconType);
    }

    public TProgressDialog(String title, int width, boolean interruptable, Dialog owner) {
        this(title, width, 0, interruptable, CLOCK_ICON, owner);
    }

    public TProgressDialog(String title, int width, int max, boolean interruptable, Dialog owner) {
        this(title, width, max, interruptable, INFORMATION_ICON, owner);
    }

    public TProgressDialog(String title, int width, int max, boolean interruptable,
                           int iconType, Dialog owner) {
        super(title, (interruptable ? TDialog.CANCEL_BUTTON : TDialog.NO_BUTTONS), 0,
            createImageIcon(iconType, 0, owner), owner);
        initDialog(width, max, interruptable, iconType);
    }

    protected void initDialog(int width, int max, boolean interruptable, int iconType) {
        this.interruptable = interruptable;

        text = new FixedSizeLabel(width);
        JPanel p = new JPanel();
        p.add(text);
        addRowComponent(p);

        bar = new JProgressBar(0, max);
        if ((max > 0) || (iconType != CLOCK_ICON)) {
            addRowComponent(bar);
        }
        if (iconType == CLOCK_ICON) {
            startTime = System.currentTimeMillis();
            scheduleClock(0);
        }
    }

    @Override
    protected void initGUI(String title, int buttonSelection, int defaultButton, Icon icon,
                           JComponent c) {
        super.initGUI(title, buttonSelection, defaultButton, icon, c);

        timePanel = new JLabel();
        timePanel.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc = AWTUtils.newGBC(0, 1, 1, 1, 1, 1,
            GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER);
        getContentPane().add(timePanel, gbc);

    }

    @Override
    protected boolean canClose(int code) {
        boolean can = false;
        if (interruptable && (launcher != null)) {
            launcher.interrupt();
            can = true;
        }
        return can;
    }

    // public void close(int code) {
    // super.close(code);
    // }

    public int showDialog(Thread t) {
        setThread(t);
        return showDialog();
    }

    public void setThread(Thread thr) {
        if (worker == null) {
            worker = thr;
            // SwingUtils.invokeLater(
            launcher = new Thread("TProgressDialog: invoking worker thread") {

                @Override
                public void run() {
                    while (!isShowing()) {
                        try {
                            Thread.sleep(POLLING_INTERVALL);
                        }
                        catch (InterruptedException e) {
                            currentThread().interrupt();
                        }
                    }
                    if (!Thread.interrupted()) {
                        worker.start();
                        try {
                            worker.join();
                        }
                        catch (InterruptedException e) {
                            worker.interrupt();
                        }
                    }
                    if (!interrupted() && isShowing()) {
                        close(0);
                    }
                }
            };
            launcher.start();
        }
    }

    public int getValue() {
        return bar.getValue();
    }

    public void setValue(int v) {
        bar.setValue(v);
    }

    public void setNextValue() {
        bar.setValue(bar.getValue() + 1);
    }

    public void setText(String s) {
        text.setText(s);
        pack();
    }

    public void setNextText(String s) {
        setNextValue();
        text.setText(s);
        pack();
    }

    public long getTimePassed() {
        return System.currentTimeMillis() - startTime;
    }

    private void scheduleClock(long intervall) {
        TimerEvent te = new TimerEvent(this, this, "adjustClock");
        ApplicationTimer.schedule(te, intervall);
    }

    public void timerAlarm(TimerEvent e) {

        long dt = getTimePassed() / 1000;
        long seconds = dt % 60;
        long minutes = (dt / 60) % 60;
        String ts = String.format("%02d:%02d", Long.valueOf(minutes), Long.valueOf(seconds));
        if (dt >= 3600) {
            long hours = (dt / 3600) % 24;
            ts = String.format("%02d:" + ts, Long.valueOf(hours));
        }
        if (dt >= 86400) {
            long days = (dt / 86400) % 3600;
            ts = days + ":" + ts;
        }
        timePanel.setText(ts);

        if (iconImage == null) {
            Image img = (createImageIcon(CLOCK_ICON, 0, this)).getImage();
            iconImage = new BufferedImage(img.getWidth(this), img.getHeight(this),
                BufferedImage.TYPE_INT_ARGB);
            Graphics g = iconImage.getGraphics();
            g.drawImage(img, 0, 0, this);
        }
        BufferedImage clockImage = new BufferedImage(iconImage.getWidth(),
            iconImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) clockImage.getGraphics();
        g.drawImage(iconImage, 0, 0, this);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int x0 = clockImage.getWidth() / 2;
        int y0 = clockImage.getHeight() / 2;

        double r = 0.45;
        double angle = ((dt * 360 / 3600) % 360 - 90) * Math.PI / 180;
        int dx = (int) (r * x0 * Math.cos(angle));
        int dy = (int) (r * x0 * Math.sin(angle));
        g.setColor(Color.BLACK);
        g.drawLine(x0, y0, x0 + dx, y0 + dy);

        r = 0.7;
        angle = ((dt * 360 / 60) % 360 - 90) * Math.PI / 180;
        dx = (int) (r * x0 * Math.cos(angle));
        dy = (int) (r * x0 * Math.sin(angle));
        g.setColor(Color.BLUE);
        g.drawLine(x0, y0, x0 + dx, y0 + dy);

        final ImageIcon icon = new ImageIcon(clockImage);
        try {
            SwingUtils.invokeAndWait(new Runnable() {

                public void run() {
                    setIcon(icon);
                    pack();
                }
            });
        }
        catch (InterruptedException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }
        catch (InvocationTargetException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }

        scheduleClock(1000);
    }

}

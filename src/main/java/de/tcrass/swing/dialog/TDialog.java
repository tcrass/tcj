package de.tcrass.swing.dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import de.tcrass.awt.AWTUtils;
import de.tcrass.swing.panel.TGridPanel;
import de.tcrass.util.SysUtils;

public class TDialog extends JDialog implements ActionListener, WindowListener {

    /**
     * 
     */
    private static final long     serialVersionUID      = 1L;
    public static final int       NO_BUTTONS            = 256;
    public static final int       YES_BUTTON            = 1;
    public static final int       NO_BUTTON             = 2;
    public static final int       OK_BUTTON             = 4;
    public static final int       CANCEL_BUTTON         = 8;
    public static final int       CLOSE_BUTTON          = 16;

    public static final int       OK_CANCEL_BUTTONS     = OK_BUTTON | CANCEL_BUTTON;
    public static final int       YES_NO_BUTTONS        = YES_BUTTON | NO_BUTTON;
    public static final int       YES_NO_CANCEL_BUTTONS = YES_BUTTON | NO_BUTTON |
                                                          CANCEL_BUTTON;

    public static final int       NO_ICON               = 0;
    public static final int       WARNING_ICON          = 1;
    public static final int       INFORMATION_ICON      = 2;
    public static final int       QUESTION_ICON         = 3;
    public static final int       CLOCK_ICON            = 4;
    public static final int       AUTO_ICON             = 256;

    private static final String   ICON_PATH             = "de.tcrass.swing.resources.dialog";
    private static final String[] ICON_NAMES            = {
        "", "dialogWarning.png", "dialogInformation.png", "dialogQuestion.png",
        "dialogClock.png"
                                                        };

    private int                   result                = 0;
    private int                   buttons               = 0;
    private JPanel                buttonPanel;
    private JPanel                iconPanel;
    private JComponent            contentContainer;
    protected JComponent          content;

    // --- Constructors

    public TDialog(String title, int buttonSelection, int defaultButton, Frame owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, null, null);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, JPanel content,
                   Frame owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, null, content);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, Dialog owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, null, null);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, JPanel content,
                   Dialog owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, null, content);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, int iconType,
                   Frame owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, createImageIcon(iconType,
            buttonSelection, owner), null);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, int iconType,
                   JPanel content, Frame owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, createImageIcon(iconType,
            buttonSelection, owner), content);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, int iconType,
                   Dialog owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, createImageIcon(iconType,
            buttonSelection, owner), null);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, int iconType,
                   JPanel content, Dialog owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, createImageIcon(iconType,
            buttonSelection, owner), content);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, Icon icon, Frame owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, icon, null);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, Icon icon,
                   JPanel content, Frame owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, icon, content);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, Icon icon, Dialog owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, icon, null);
    }

    public TDialog(String title, int buttonSelection, int defaultButton, Icon icon,
                   JPanel content, Dialog owner) {
        super(owner);
        initGUI(title, buttonSelection, defaultButton, icon, content);
    }

    // --- Private methods

    protected void initGUI(String title, int buttonSelection, int defaultButton, Icon icon,
                           JComponent c) {
        setTitle(title);
        setModal(true);

        setResizable(false);

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(this);

        getContentPane().setLayout(new GridBagLayout());

        setIcon(icon);

        GridBagConstraints gbc = AWTUtils.newGBC(1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH,
            GridBagConstraints.NORTHEAST);
        contentContainer = createContentContainer(c);
        getContentPane().add(contentContainer, gbc);

        buttonPanel = createButtonPanel(buttonSelection, defaultButton);
        gbc = AWTUtils.newGBC(1, 1, 1, 1, 0, 0, GridBagConstraints.HORIZONTAL,
            GridBagConstraints.SOUTHEAST);
        getContentPane().add(buttonPanel, gbc);

        // updateLayout();
    }

    // --- Protected methods

    protected static ImageIcon createImageIcon(int iconType, int buttons, Component obs) {
        ImageIcon icon = null;
        if (iconType != NO_ICON) {
            Image im = null;
            int type = iconType;
            try {
                if (type == AUTO_ICON) {
                    type = INFORMATION_ICON;
                    if ((buttons & (YES_BUTTON | NO_BUTTON)) != 0) {
                        type = QUESTION_ICON;
                    }
                    if ((buttons & CANCEL_BUTTON) != 0) {
                        type = WARNING_ICON;
                    }
                }
                if ((type > 0) && (type < ICON_NAMES.length)) {
                    im = AWTUtils.readImageResource(ICON_PATH, ICON_NAMES[type], obs);
                }
            }
            catch (IOException e) {
                SysUtils.reportException(e);
            }
            if (im != null) {
                icon = new ImageIcon(im);
            }
        }
        return icon;
    }

    protected JComponent createContentContainer(JComponent c) {

        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.setBorder(BorderFactory.createRaisedBevelBorder());

        if (c != null) {
            content = c;
        }
        else {
            content = createContentPanel();
        }

        p.add(content, BorderLayout.CENTER);
        return p;
    }

    protected TGridPanel createContentPanel() {
        return new TGridPanel();
    }

    protected JPanel createButtonPanel(int buttonSelection, int defaultButton) {
        buttons = buttonSelection;
        if (buttons == 0) {
            buttons = OK_BUTTON;
        }

        JPanel bp = new JPanel();
        bp.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton b;

        if ((buttons & YES_BUTTON) != 0) {
            b = new JButton("Yes");
            b.setActionCommand(Integer.toString(YES_BUTTON));
            b.addActionListener(this);
            bp.add(b);
            if (defaultButton == YES_BUTTON) {
                getRootPane().setDefaultButton(b);
            }
        }
        if ((buttons & NO_BUTTON) != 0) {
            b = new JButton("No");
            b.setActionCommand(Integer.toString(NO_BUTTON));
            b.addActionListener(this);
            bp.add(b);
            if (defaultButton == NO_BUTTON) {
                getRootPane().setDefaultButton(b);
            }
        }
        if ((buttons & OK_BUTTON) != 0) {
            b = new JButton("OK");
            b.setActionCommand(Integer.toString(OK_BUTTON));
            b.addActionListener(this);
            bp.add(b);
            if (defaultButton == OK_BUTTON) {
                getRootPane().setDefaultButton(b);
            }
        }
        if ((buttons & CANCEL_BUTTON) != 0) {
            b = new JButton("Cancel");
            b.setActionCommand(Integer.toString(CANCEL_BUTTON));
            b.addActionListener(this);
            bp.add(b);
            if (defaultButton == CANCEL_BUTTON) {
                getRootPane().setDefaultButton(b);
            }
        }
        if ((buttons & CLOSE_BUTTON) != 0) {
            b = new JButton("Close");
            b.setActionCommand(Integer.toString(CLOSE_BUTTON));
            b.addActionListener(this);
            bp.add(b);
            if (defaultButton == CLOSE_BUTTON) {
                getRootPane().setDefaultButton(b);
            }
        }
        return bp;
    }

    protected boolean isValidInput() {
        boolean valid = true;
        if (content instanceof ValidatingInputComponent) {
            valid = ((ValidatingInputComponent) content).isValidInput();
        }
        return valid;
    }

    protected boolean canClose(int code) {
        return true;
    }

    // --- Public methods

    public void setContent(JComponent c) {
        getContentPane().remove(contentContainer);
        GridBagConstraints gbc = AWTUtils.newGBC(1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH,
            GridBagConstraints.NORTHEAST);
        contentContainer = createContentContainer(c);
        getContentPane().add(contentContainer, gbc);
    }

    public void setIcon(Icon icon) {
        if (iconPanel != null) {
            getContentPane().remove(iconPanel);
        }
        if (icon != null) {
            iconPanel = new JPanel();
            iconPanel.setLayout(new BorderLayout());
            iconPanel.add(new JLabel(icon), BorderLayout.NORTH);
            int b = TGridPanel.DEFAULT_BORDER_WIDTH;
            iconPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createRaisedBevelBorder(), BorderFactory.createEmptyBorder(b, b,
                    b, b)));
            GridBagConstraints gbc = AWTUtils.newGBC(0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH,
                GridBagConstraints.NORTHWEST);
            getContentPane().add(iconPanel, gbc);
        }
    }

    public void setIcon(int iconType) {
        setIcon(createImageIcon(iconType, buttons, this));
    }

    public void prependButton(JButton button) {
        Component[] buttons = buttonPanel.getComponents();
        buttonPanel.removeAll();
        buttonPanel.add(button);
        for (Component element : buttons) {
            buttonPanel.add(element);
        }
        pack();
    }

    public void appendButton(JButton button) {
        buttonPanel.add(button);
        pack();
    }

    // public void centerOnScreen() {
    // setLocation((int) (getToolkit().getScreenSize().width -
    // getWidth()) / 2,
    // (int) (getToolkit().getScreenSize().height - getHeight()) / 2);
    // }

    public void close(int code) {
        result = code;
        setVisible(false);
    }

    public int showDialog() {
        result = 0;
        pack();
        AWTUtils.centerOnScreen(this);
        setVisible(true);
        return result;
    }

    public void addCluster(TComponentCluster c) {
        ((TGridPanel) content).addCluster(c);
        pack();
    }

    public void addComponent(JComponent c, GridBagConstraints ori) {
        ((TGridPanel) content).addComponent(c, ori);
    }

    public void addLeftComponent(JComponent c, int width) {
        ((TGridPanel) content).addLeftComponent(c, width);
        pack();
    }

    public void addRightComponent(JComponent c, int width) {
        ((TGridPanel) content).addRightComponent(c, width);
        pack();
    }

    public void addLeftMaxComponent(JComponent c, int width) {
        ((TGridPanel) content).addLeftMaxComponent(c, width);
        pack();
    }

    public void addRightMaxComponent(JComponent c, int width) {
        ((TGridPanel) content).addRightMaxComponent(c, width);
        pack();
    }

    public void addRowComponent(JComponent c) {
        ((TGridPanel) content).addRowComponent(c);
        pack();
    }

    public void addBlockComponent(JComponent c) {
        ((TGridPanel) content).addBlockComponent(c);
        pack();
    }

    public void addShyComponent(JComponent c) {
        ((TGridPanel) content).addShyComponent(c);
        pack();
    }

    public void newRow() {
        ((TGridPanel) content).newRow();
    }

    public void actionPerformed(ActionEvent e) {
        try {
            int code = Integer.parseInt(e.getActionCommand());
            if (canClose(code) && (isValidInput() || (code != OK_BUTTON))) {
                close(code);
            }
        }
        catch (RuntimeException ex) {
            SysUtils.doNothing();
        }
    }

    // --- Implementation of WindowListener

    public void windowActivated(WindowEvent e) {

    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        int r = 0;
        if (buttons == NO_BUTTONS) {
            r = CLOSE_BUTTON;
        }
        else if ((buttons & CLOSE_BUTTON) != 0) {
            r = CLOSE_BUTTON;
        }
        else if ((buttons & CANCEL_BUTTON) != 0) {
            r = CANCEL_BUTTON;
        }
        else if ((buttons == OK_BUTTON) && isValidInput()) {
            r = OK_BUTTON;
        }
        if (r != 0) {
            if (canClose(r)) {
                result = r;
                setVisible(false);
            }
        }
    }

    public void windowDeactivated(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowOpened(WindowEvent e) {

    }

}

/*
 * Created on 28.10.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.border.Border;

import de.tcrass.swing.TFileExtensionFilter;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TURICluster extends TComponentCluster implements ValidatingInputComponent {

    private static final int DEFAULT_COLS = 32;

    private JTextField       uriField;
    private URI              uri;
    private JButton          selectButton;

    private Border           border;

    String[]                 extensions;
    int                      chooserType  = JFileChooser.OPEN_DIALOG;
    int                      chooserMode  = JFileChooser.FILES_AND_DIRECTORIES;

    // public TFileCluster() {
    // super();
    // initGUI("", new String[] {"*"}, "");
    // }

    public TURICluster(String title) {
        super(title, 2);
        initCluster(null, "", JFileChooser.FILES_ONLY);
    }

    public TURICluster(String title, String uriStr) {
        super(title, 2);
        initCluster(null, uriStr, JFileChooser.FILES_ONLY);
    }

    public TURICluster(String title, String[] extensions) {
        super(title, 2);
        initCluster(extensions, "", JFileChooser.FILES_ONLY);
    }

    public TURICluster(String title, String[] extensions, String uriStr) {
        super(title, 2);
        initCluster(extensions, uriStr, JFileChooser.FILES_ONLY);
    }

    public TURICluster(String title, URI uri) {
        super(title, 2);
        String uriStr = "";
        if (uri != null) {
            uriStr = uri.toString();
        }
        initCluster(null, uriStr, JFileChooser.FILES_ONLY);
    }

    public TURICluster(String title, String[] extensions, URI uri) {
        super(title, 2);
        String uriStr = "";
        if (uri != null) {
            uriStr = uri.toString();
        }
        initCluster(null, uriStr, JFileChooser.FILES_ONLY);
    }

    public TURICluster(String title, int chooserMode) {
        super(title, 2);
        initCluster(null, "", chooserMode);
    }

    public TURICluster(String title, String uriStr, int chooserMode) {
        super(title, 2);
        initCluster(null, uriStr, chooserMode);
    }

    public TURICluster(String title, String[] extensions, int chooserMode) {
        super(title, 2);
        initCluster(extensions, "", chooserMode);
    }

    public TURICluster(String title, String[] extensions, String uriStr, int chooserMode) {
        super(title, 2);
        initCluster(extensions, uriStr, chooserMode);
    }

    public TURICluster(String title, URI uri, int chooserMode) {
        super(title, 2);
        String uriStr = "";
        if (uri != null) {
            uriStr = uri.toString();
        }
        initCluster(null, uriStr, chooserMode);
    }

    public TURICluster(String title, String[] extensions, URI uri, int chooserMode) {
        super(title, 2);
        String uriStr = "";
        initCluster(null, uriStr, chooserMode);
    }

    // public TFileCluster(String[] extensions, String filename) {
    // super(title, 2);
    // initCluster(extensions, filename);
    // }

    private void initCluster(String[] extensions, String uriStr, int chooserMode) {
        this.extensions = extensions;
        this.chooserMode = chooserMode;
        uriField = new JTextField(DEFAULT_COLS);
        border = uriField.getBorder();
        setURI(uriStr);

        selectButton = new JButton("...");
        selectButton.setPreferredSize(new Dimension(selectButton.getMinimumSize().width,
            uriField.getPreferredSize().height));
        selectButton.addActionListener(new SelectButtonListener());

        addRightMaxComponent(uriField, 3);
        addLeftComponent(selectButton, 1);
    }

    // private void setParent(Frame parent) {
    // this.parent = parent;
    // }

    public JTextField getURIField() {
        return uriField;
    }

    protected void updateTextFieldToURI(URI uri) {
        uriField.setText(uri.toString());
    }

    protected URI getURIFromTextField() {
        URI uri = null;
        try {
            uri = new URI(uriField.getText());
        }
        catch (URISyntaxException e) {
        }
        return uri;
    }

    public void setExtensions(String[] extensions) {
        this.extensions = extensions;
    }

    public String[] getExtensions() {
        return extensions;
    }

    public void setMode(int mode) {
        chooserMode = mode;
    }

    public void setType(int type) {
        chooserType = type;
    }

    public void setURI(String uriStr) {
        try {
            uri = new URI(uriStr);
            updateTextFieldToURI(uri);
        }
        catch (URISyntaxException e) {
            SysUtils.doNothing();
        }
    }

    public void setURI(URI uri) {
        setURI(uri.toString());
    }

    public URI getURI() {
        return getURIFromTextField();
    }

    public String getURIString() {
        String uriStr = "";
        URI uri = getURI();
        if (uri != null) {
            uriStr = uri.toString();
        }
        return uriStr;
    }

    @Override
    public String getValue() {
        return getURIString();
    }

    // public void setEnabled(boolean enabled) {
    // selectButton.setEnabled(enabled);
    // }
    //  
    // public boolean isEnabled() {
    // return selectButton.isEnabled();
    // }
    //
    private class SelectButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                File f = new File(getURIString());
                JFileChooser d = new JFileChooser(f);
                d.setDialogTitle("Select a file...");
                d.setDialogType(chooserType);
                d.setFileSelectionMode(chooserMode);
                if (getExtensions() != null) {
                    TFileExtensionFilter ff = new TFileExtensionFilter(getExtensions());
                    d.addChoosableFileFilter(ff);
                }
                if (d.showDialog(((JComponent) e.getSource()).getTopLevelAncestor(), "OK") == JFileChooser.APPROVE_OPTION) {
                    // d.setVisible(true);
                    if (d.getSelectedFile() != null) {
                        setURI(d.getSelectedFile().toURI());
                    }
                }
            }
            catch (Exception ex) {
                SysUtils.doNothing();
            }
        }
    }

    @Override
    protected InputValidator createInputValidator() {
        return new InputValidator() {

            public boolean isValidInput(String s) {
                boolean valid = true;
                try {
                    new URI(getURIString());
                }
                catch (URISyntaxException e) {
                    valid = false;
                }
                return valid;
            }
        };
    }

    @Override
    protected void markAsValidInput(boolean valid) {
        if (valid) {
            getURIField().setToolTipText(null);
            getURIField().setBorder(border);
        }
        else {
            getURIField().setToolTipText("Field must contain a valid URI/URL!");
            getURIField().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
        }
    }

}

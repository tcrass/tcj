/*
 * Created on 18.02.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import javax.swing.JComboBox;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TDropDownCluster extends TComponentCluster {

    private JComboBox box;

    public TDropDownCluster(String title, String[] items) {
        super(title, 4);
        initCluster(items);
    }

    private void initCluster(String[] items) {
        box = new JComboBox();

        for (String element : items) {
            box.addItem(element);
        }
        addLeftComponent(box, 2);
    }

    public JComboBox getComboBox() {
        return box;
    }

    @Override
    public String getValue() {
        return (String) box.getModel().getSelectedItem();
    }

    public void setValue(String item) {
        Object o = null;
        for (int i = 0; i < box.getItemCount(); i++) {
            if (((String) box.getItemAt(i)).equals(item)) {
                o = box.getItemAt(i);
                break;
            }
        }
        box.getModel().setSelectedItem(o);
    }

    public void setSelectedIndex(int index) {
        box.setSelectedIndex(index);
    }

    public int getSelectedIndex() {
        return getComboBox().getSelectedIndex();
    }

}

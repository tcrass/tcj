/*
 * Created on 29.08.2003 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.tcrass.swing.FixedSizeLabel;
import de.tcrass.util.StringUtils;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TSliderCluster extends TComponentCluster {

    private JSlider        slider;
    private FixedSizeLabel currentValue;
    private DecimalFormat  format;

    public TSliderCluster(String title, int min, int max) {
        super(title, 2);
        initCluster(min, max, min);
    }

    public TSliderCluster(String title, int min, int max, int value) {
        super(title, 2);
        initCluster(min, max, value);
    }

    private void initCluster(int min, int max, int value) {
        currentValue = new FixedSizeLabel(1);
        currentValue.setBorder(BorderFactory.createLoweredBevelBorder());
        currentValue.setHorizontalAlignment(SwingConstants.RIGHT);
        slider = new JSlider();
        format = createFormat();

        slider.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                setSliderPosition(getSliderPosition());
            }
        });
        addRightMaxComponent(slider, 3);

        slider.setMinimum(min);
        slider.setMaximum(max);
        setSliderPosition(value);

        addLeftComponent(currentValue, 1);

    }

    private DecimalFormat createFormat() {
        DecimalFormat format = new DecimalFormat("0");
        String s = format.format(slider.getMaximum());
        if (format.format(slider.getMaximum()).length() > s.length()) {
            s = format.format(slider.getMaximum());
        }
        String f = StringUtils.polymerize("0", s.length());
        currentValue.setWidth(f.length() + 4);
        // format = new DecimalFormat(f);
        return format;
    }

    protected String getValueAsText(int value) {
        return "  " + format.format(value) + "  ";
    }

    protected DecimalFormat getFormat() {
        return format;
    }

    public JSlider getSlider() {
        return slider;
    }

    public int getMinimum() {
        return slider.getMinimum();
    }

    public int getMaximum() {
        return slider.getMaximum();
    }

    public int getRange() {
        return getMaximum() - getMinimum();
    }

    @Override
    public String getValue() {
        return new Integer(getSliderPosition()).toString();
    }

    public int getSliderPosition() {
        return slider.getValue();
    }

    public void setMinimum(int min) {
        slider.setMinimum(min);
        format = createFormat();
    }

    public void setMaximum(int max) {
        slider.setMaximum(max);
        format = createFormat();
    }

    public double getRelativeValue() {
        return (double) (getSliderPosition() - getMinimum()) / (double) getRange();
    }

    public void setSliderPosition(int value) {
        slider.setValue(value);
        currentValue.setText(getValueAsText(value));
    }

    public void setRelativeValue(double value) {
        setSliderPosition((int) (getMinimum() + value * getRange() + 0.5));
    }

    public void setWidth(int width) {
        currentValue.setWidth(width);
    }

}

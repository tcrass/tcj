/*
 * Created on 01.02.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.swing.dialog;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.net.URL;

import de.tcrass.swing.panel.TBrowserControls;
import de.tcrass.swing.panel.TGridPanel;
import de.tcrass.swing.panel.THTMLBrowserPanel;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class TWebBrowser extends TDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TWebBrowser(String title, URL url, int rows, int cols, Frame win) {
        this(title, url, rows, cols, TBrowserControls.ALL_BUT_CLOSE_BUTTON, true, true, win);
    }

    public TWebBrowser(String title, URL url, int rows, int cols, int buttons, boolean status,
                       boolean modal, Frame win) {
        super(title, NO_BUTTONS, 0, NO_ICON, win);
        init(url, rows, cols, buttons, status, modal);
    }

    public TWebBrowser(String title, URL url, int rows, int cols, Dialog win) {
        this(title, url, rows, cols, TBrowserControls.ALL_BUT_CLOSE_BUTTON, true, true, win);
    }

    public TWebBrowser(String title, URL url, int rows, int cols, int buttons, boolean status,
                       boolean modal, Dialog win) {
        super(title, NO_BUTTONS, 0, NO_ICON, win);
        init(url, rows, cols, buttons, status, modal);
    }

    private void init(URL url, int rows, int cols, int buttons, boolean status, boolean modal) {
        getContentPane().setLayout(new BorderLayout());
        setModal(modal);

        TGridPanel p = new TGridPanel(true);

        THTMLBrowserPanel b = new THTMLBrowserPanel(url, rows, cols, buttons);
        b.setEmptyBorder(0);
        p.addRowComponent(b.getBrowserControls());
        p.addBlockComponent(b);
        if (status) {
            p.addRowComponent(b.getBrowserStatusBar());
        }

        getContentPane().add(p, BorderLayout.CENTER);
        pack();
        setLocation((getToolkit().getScreenSize().width - getWidth()) / 2,
            (getToolkit().getScreenSize().height - getHeight()) / 2);

        setResizable(true);

    }

}

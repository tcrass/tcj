/*
 * Created on 01.02.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.swing.dialog;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JFileChooser;

import de.tcrass.io.FileUtils;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class TFileCluster extends TURICluster {

    private static String file2uriStr(File f) {
        // String uriStr = "";
        // if (f != null) {
        // try {
        // URI uri =
        // FileUtils.getCurrentUserURI().relativize(f.toURI());
        // uriStr = "file:" + uri.getPath();
        // }
        // catch (Exception e) {
        // SysUtils.doNothing();
        // }
        // }
        // return uriStr;
        return f.toURI().toString();
    }

    private static String filename2uriStr(String filename) {
        return file2uriStr(new File(filename));
    }

    @Override
    protected void updateTextFieldToURI(URI uri) {
        String filename = "";
        try {
            File f = FileUtils.createRelativeFile(FileUtils.getCurrentUserDirectory(),
                new File(uri));
            filename = f.getPath();
        }
        catch (IOException e) {
            SysUtils.doNothing();
        }
        catch (IllegalArgumentException e) {
            SysUtils.doNothing();
        }
        getURIField().setText(filename);
    }

    @Override
    protected URI getURIFromTextField() {
        URI uri = null;
        try {
            uri = new URI(filename2uriStr(getURIField().getText()));
        }
        catch (URISyntaxException e) {
            SysUtils.doNothing();
        }
        return uri;
    }

    public TFileCluster(String title) {
        super(title, JFileChooser.FILES_ONLY);
    }

    public TFileCluster(String title, String filename) {
        super(title, filename2uriStr(filename));
    }

    public TFileCluster(String title, String[] extensions) {
        super(title, extensions);
    }

    public TFileCluster(String title, String[] extensions, String filename) {
        super(title, extensions, filename2uriStr(filename));
    }

    public TFileCluster(String title, File file) {
        super(title, file2uriStr(file));
    }

    public TFileCluster(String title, String[] extensions, File file) {
        super(title, file2uriStr(file));
    }

    public TFileCluster(String title, int chooserMode) {
        super(title, chooserMode);
    }

    public TFileCluster(String title, String filename, int chooserMode) {
        super(title, filename2uriStr(filename), chooserMode);
    }

    public TFileCluster(String title, String[] extensions, int chooserMode) {
        super(title, extensions, chooserMode);
    }

    public TFileCluster(String title, String[] extensions, String filename, int chooserMode) {
        super(title, extensions, filename2uriStr(filename), chooserMode);
    }

    public TFileCluster(String title, File file, int chooserMode) {
        super(title, file2uriStr(file), chooserMode);
    }

    public TFileCluster(String title, String[] extensions, File file, int chooserMode) {
        super(title, extensions, file2uriStr(file), chooserMode);
    }

    public void setFilename(String filename) {
        setURI(filename2uriStr(filename));
    }

    public void setFile(File file) {
        if (file != null) {
            setFilename(file.getPath());
        }
    }

    public String getFilename() {
        String filename = "";
        URI uri = getURI();
        try {
            File f = FileUtils.createRelativeFile(FileUtils.getCurrentUserDirectory(),
                new File(uri));
            filename = f.getPath();
        }
        catch (IOException e) {
            SysUtils.doNothing();
        }
        // try {
        // URI uri = new URI(urlStr);
        // uri = FileUtils.getCurrentUserURI().resolve(uri);
        // File f = new File(uri);
        // f =
        //FileUtils.createRelativeFile(FileUtils.getCurrentUserDirectory
        // (),
        // f);
        // filename = f.getPath();
        // }
        // catch (Exception e) {
        // SysUtils.doNothing();
        // }
        return filename;
    }

    public File getFile() {
        File f = new File(getURI());
        // if (!getFilename().equals("")) {
        // f = new File(getFilename());
        // }
        return f;
    }

}

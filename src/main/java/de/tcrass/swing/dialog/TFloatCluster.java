package de.tcrass.swing.dialog;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class TFloatCluster extends TNumericCluster {

    private static final String INVALID_INPUT_TOOLTIP = "Field must contain a valid floating point number!";

    private DecimalFormat       format                = new DecimalFormat("0.############");

    public TFloatCluster(String title) {
        super(title);
    }

    public TFloatCluster(String title, float value) {
        super(title);
        init(value);
    }

    public TFloatCluster(String title, double value) {
        super(title);
        init(value);
    }

    public TFloatCluster(String title, int titleWidth, int valueWidth) {
        super(title, titleWidth, valueWidth);
        init(0);
    }

    public TFloatCluster(String title, int titleWidth, float value, int valueWidth) {
        super(title, titleWidth, valueWidth);
        init(value);
    }

    public TFloatCluster(String title, int titleWidth, double value, int valueWidth) {
        super(title, titleWidth, valueWidth);
        init(value);
    }

    private void init(double value) {
        DecimalFormatSymbols s = new DecimalFormatSymbols();
        s.setDecimalSeparator('.');
        s.setGroupingSeparator((char) 0);
        format.setDecimalFormatSymbols(s);
        format.setDecimalSeparatorAlwaysShown(false);
        setValue(value);
    }

    public void setValue(float value) {
        super.setValue(format.format(value));
    }

    public void setValue(double value) {
        super.setValue(format.format(value));
    }

    public float getValueAsFloat() {
        return new Double(getValue()).floatValue();
    }

    public double getValueAsDouble() {
        return new Double(getValue()).doubleValue();
    }

    public String getFormat() {
        return format.toPattern();
    }

    public void setFormat(String pattern) {
        format = new DecimalFormat(pattern);
    }

    @Override
    protected InputValidator createInputValidator() {
        return new InputValidator() {

            public boolean isValidInput(String s) {
                boolean valid = true;
                try {
                    Double.parseDouble(s);
                }
                catch (NumberFormatException e) {
                    valid = false;
                }
                return valid;
            }
        };
    }

    // protected void markAsValidInput(boolean valid) {
    // super.markAsValidInput(valid);
    // if (valid) {
    // getTextField().setToolTipText(null);
    // }
    // else {
    // getTextField().setToolTipText("Field must contain a valid
    // floating point number!");
    // }
    // }

}

/*
 * Created on 15.02.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.dialog;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import de.tcrass.swing.panel.TGridPanel;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TTabbedDialog extends TDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    static String[]           tempTitles;

    private JComponent[]      content;
    private String[]          tabTitles;
    private JTabbedPane       tabs;

    public TTabbedDialog(String title, String[] tabTitles, int buttonSelection,
                         int defaultButton, Frame owner) {
        super(saveTempTitles(title, tabTitles), buttonSelection, defaultButton, owner);
    }

    public TTabbedDialog(String title, String[] tabTitles, int buttonSelection,
                         int defaultButton, Dialog owner) {
        super(saveTempTitles(title, tabTitles), buttonSelection, defaultButton, owner);
    }

    // This is *dirty*!
    private static String saveTempTitles(String title, String[] tabTitles) {
        tempTitles = tabTitles;
        return title;
    }

    @Override
    protected JComponent createContentContainer(JComponent c) {
        tabTitles = tempTitles;
        content = new JComponent[tabTitles.length];
        tabs = new JTabbedPane();
        for (int i = 0; i < tabTitles.length; i++) {
            JPanel p = new JPanel();
            p.setLayout(new BorderLayout());
            p.setBorder(BorderFactory.createRaisedBevelBorder());
            content[i] = createContentPanel();
            p.add(content[i], BorderLayout.NORTH);
            tabs.addTab(tabTitles[i], p);
        }
        return tabs;
    }

    @Override
    protected boolean isValidInput() {
        boolean valid = true;
        int tab = -1;
        for (int i = 0; i < tabTitles.length; i++) {
            valid = ((ValidatingInputComponent) content[i]).isValidInput();
            if (!valid && (tab < 0)) {
                tab = i;
                break;
            }
        }
        if (tab >= 0) {
            tabs.setSelectedIndex(tab);
        }
        return valid;
    }

    public void addCluster(int tab, TComponentCluster c) {
        ((TGridPanel) content[tab]).addCluster(c);
    }

    public void addComponent(int tab, JComponent c, GridBagConstraints gbc) {
        ((TGridPanel) content[tab]).addComponent(c, gbc);
    }

    public void addLeftComponent(int tab, JComponent c, int width) {
        ((TGridPanel) content[tab]).addLeftComponent(c, width);
    }

    public void addRightComponent(int tab, JComponent c, int width) {
        ((TGridPanel) content[tab]).addRightComponent(c, width);
    }

    public void addLeftMaxComponent(int tab, JComponent c, int width) {
        ((TGridPanel) content[tab]).addLeftMaxComponent(c, width);
    }

    public void addRightMaxComponent(int tab, JComponent c, int width) {
        ((TGridPanel) content[tab]).addRightMaxComponent(c, width);
    }

    public void addRowComponent(int tab, JComponent c) {
        ((TGridPanel) content[tab]).addRowComponent(c);
    }

    public void addBlockComponent(int tab, JComponent c) {
        ((TGridPanel) content[tab]).addBlockComponent(c);
    }

    public void addShyComponent(int tab, JComponent c) {
        ((TGridPanel) content[tab]).addShyComponent(c);
    }

    public void newRow(int tab) {
        ((TGridPanel) content[tab]).newRow();
    }

}

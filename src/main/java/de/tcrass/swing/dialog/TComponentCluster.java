package de.tcrass.swing.dialog;

import java.awt.GridBagConstraints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.text.JTextComponent;

import de.tcrass.awt.AWTUtils;
import de.tcrass.swing.panel.TGridPanel;

public abstract class TComponentCluster implements ValidatingInputComponent, FocusListener {

    public static final int                 DEFAULT_TITLE_WIDTH = 2;

    private int                             nextX               = 0;
    private int                             nextY               = 0;
    private int                             cols                = TGridPanel.DEFAULT_GRID_COLS;

    protected ArrayList<JComponent>         components          = new ArrayList<JComponent>();
    protected ArrayList<GridBagConstraints> constraints         = new ArrayList<GridBagConstraints>();
    private boolean                         enabled             = true;
    private int                             maxHeight;

    private JLabel                          label;

    private InputValidator                  inputValidator;
    private String                          invalidInputTooltip;

    public TComponentCluster() {
        // initTitle(null, 0);
    }

    public TComponentCluster(String title) {
        initLabel(title, DEFAULT_TITLE_WIDTH);
    }

    public TComponentCluster(String title, int titleSize) {
        initLabel(title, titleSize);
    }

    private void initLabel(String title, int titleSize) {
        if (title != null) {
            label = new JLabel(title + " ");
            addLeftMaxComponent(label, titleSize);
        }
        setInputValidator(createInputValidator());
    }

    protected JLabel getLabel() {
        return label;
    }

    protected InputValidator createInputValidator() {
        return new InputValidator() {

            public boolean isValidInput(String input) {
                return true;
            }
        };
    }

    protected void markAsValidInput(boolean valid) {
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public void addComponent(JComponent c, GridBagConstraints gbc) {
        components.add(c);
        constraints.add(gbc);
        if (gbc.gridy + gbc.gridheight > maxHeight) {
            maxHeight = gbc.gridy + gbc.gridheight;
        }
        nextX = gbc.gridx + gbc.gridwidth;
    }

    public void addRightComponent(JComponent c, int w) {
        GridBagConstraints gbc = AWTUtils.newGBC(nextX, nextY, w, 1, 0, 0,
            GridBagConstraints.NONE, GridBagConstraints.EAST);
        addComponent(c, gbc);
    }

    public void addLeftComponent(JComponent c, int w) {
        GridBagConstraints gbc = AWTUtils.newGBC(nextX, nextY, w, 1, 0, 0,
            GridBagConstraints.NONE, GridBagConstraints.WEST);
        addComponent(c, gbc);
    }

    public void addRightMaxComponent(JComponent c, int w) {
        GridBagConstraints gbc = AWTUtils.newGBC(nextX, nextY, w, 1, 1, 0,
            GridBagConstraints.HORIZONTAL, GridBagConstraints.EAST);
        addComponent(c, gbc);
    }

    public void addLeftMaxComponent(JComponent c, int w) {
        GridBagConstraints gbc = AWTUtils.newGBC(nextX, nextY, w, 1, 1, 0,
            GridBagConstraints.HORIZONTAL, GridBagConstraints.WEST);
        addComponent(c, gbc);
    }

    // public void addLeftComponent(JComponent c) {
    // addLeftComponent(c, 1);
    // }
    //  
    // public void addRightComponent(JComponent c) {
    // addRightComponent(c, 1);
    // }
    //  
    public void addRowComponent(JComponent c) {
        addLeftMaxComponent(c, cols);
        newRow();
    }

    public void newRow() {
        nextX = 0;
        nextY = nextY + maxHeight;
        maxHeight = 0;
    }

    public void setEnabled(boolean enabled) {
        for (JComponent c : components) {
            c.setEnabled(enabled);
            if (c instanceof JTextComponent) {
                ((JTextComponent) c).setEditable(enabled);
            }
        }
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getComponentCount() {
        return components.size();
    }

    public JComponent getComponent(int i) {
        return components.get(i);
    }

    public GridBagConstraints getComponentConstraints(int i) {
        return constraints.get(i);
    }

    // public Dimension getMaximumSize() {
    // return new Dimension(super.getMaximumSize().width,
    // getPreferredSize().height);
    // }

    public void setInputValidator(InputValidator validator) {
        inputValidator = validator;
    }

    public void setInvalidInputTooltip(String s) {
        invalidInputTooltip = s;
    }

    public String getInvalidInputTooltip() {
        return invalidInputTooltip;
    }

    public String getValue() {
        return null;
    }

    // --- Implementation of ValidatingInputComponent

    public boolean isValidInput() {
        boolean valid = inputValidator.isValidInput(getValue());
        markAsValidInput(valid);
        return valid;
    }

    // --- Implementation of FocusListener

    public void focusGained(FocusEvent e) {
        markAsValidInput(true);
    }

    public void focusLost(FocusEvent e) {
        markAsValidInput(inputValidator.isValidInput(getValue()));
    }

}

package de.tcrass.swing.plaf.basic;

import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalButtonUI;

public class ToolbarButtonUI extends MetalButtonUI {

    protected static ToolbarButtonUI buttonUI;

    public static ComponentUI createUI(JComponent c) {
        if (buttonUI == null) {
            buttonUI = new ToolbarButtonUI();
        }
        return buttonUI;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        JButton b = (JButton) c;
        if (b.getModel().isPressed()) {
            b.setBorder(BorderFactory.createLoweredBevelBorder());
        }
        else {
            if (!b.isRolloverEnabled() || b.getModel().isRollover()) {
                b.setBorder(BorderFactory.createRaisedBevelBorder());
            }
            else {
                // b.setBorder(BorderFactory.createEtchedBorder());
                b.setBorder(BorderFactory.createEmptyBorder());
            }
        }
        super.paint(g, c);
    }
}

package de.tcrass.swing.plaf.basic;

import java.awt.Graphics;

import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonUI;

import de.tcrass.swing.TGraphicalButton;

public class GraphicalButtonUI extends BasicButtonUI {

    protected static GraphicalButtonUI buttonUI;

    public static ComponentUI createUI(JComponent c) {
        if (buttonUI == null) {
            buttonUI = new GraphicalButtonUI();
        }
        return buttonUI;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        TGraphicalButton b = (TGraphicalButton) c;
        ButtonModel m = b.getModel();
        if (b.getBackgroundImage() != null) {
            g.drawImage(b.getBackgroundImage(), 0, 0, b);
        }
        if (!b.isEnabled()) {
            if (b.getDisabledImage() != null) {
                g.drawImage(b.getDisabledImage(), 0, 0, b);
            }
            // else {
            // g.drawImage(b.getEnabledImage(), 0, 0, b);
            // }
        }
        else {
            if (m.isArmed()) {
                if (b.getArmedImage() != null) {
                    g.drawImage(b.getArmedImage(), 0, 0, b);
                }
                else {
                    g.drawImage(b.getEnabledImage(), 0, 0, b);
                }
            }
            else if (m.isRollover()) {
                if (b.getRolloverImage() != null) {
                    g.drawImage(b.getRolloverImage(), 0, 0, b);
                }
                else {
                    g.drawImage(b.getEnabledImage(), 0, 0, b);
                }
            }
            else {
                g.drawImage(b.getEnabledImage(), 0, 0, b);
            }
        }
    }

}

package de.tcrass.swing.plaf.basic;

import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalButtonUI;

public class RaisedButtonUI extends MetalButtonUI {

    protected static RaisedButtonUI buttonUI;

    public static ComponentUI createUI(JComponent c) {
        if (buttonUI == null) {
            buttonUI = new RaisedButtonUI();
        }
        return buttonUI;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        JButton b = (JButton) c;
        if (b.getModel().isPressed()) {
            b.setBorder(BorderFactory.createLoweredBevelBorder());
        }
        else {
            b.setBorder(BorderFactory.createRaisedBevelBorder());
        }
        super.paint(g, c);
    }
}

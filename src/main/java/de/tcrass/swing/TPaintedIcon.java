package de.tcrass.swing;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

public class TPaintedIcon implements Icon {

    private int width  = 0;
    private int height = 0;

    public TPaintedIcon(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getIconWidth() {
        return width;
    }

    public int getIconHeight() {
        return height;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
    }

}

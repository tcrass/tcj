package de.tcrass.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;

import de.tcrass.awt.AWTUtils;
import de.tcrass.util.SysUtils;

public class TPopupButton extends TToolBarButton implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JMenu             menu;

    // public PopupButton (String label, JMenu menu, String command) {
    // super(label);
    // this.label = label;
    // this.menu = menu;
    // setActionCommand(command);
    // addActionListener(this);
    // setRolloverEnabled(true);
    // setFocusable(false);
    // setBorder(BorderFactory.createEtchedBorder());
    // }
    //
    // public TPopupButton (String label, String command, JMenu menu) {
    // super(label, command);
    // this.menu = menu;
    // addActionListener(this);
    // }

    public TPopupButton(Icon icon, String command, JMenu menu) {
        super(icon, command);
        this.menu = menu;
        addActionListener(this);
    }

    // public void paint(Graphics g) {
    // super.paint(g);
    // g.setColor(Color.black);
    // g.drawString(label, (int)SIZE/2, SIZE-4);
    // }

    public void setMenu(JMenu m) {
        menu = m;
    }

    public void actionPerformed(ActionEvent e) {
        setBorder(BorderFactory.createEtchedBorder());
        menu.getPopupMenu().show(this, getSize().width / 2, getSize().height / 2);
    }

    public static TPopupButton createPopupButton(String command, JMenu m, String hint,
                                                 String iconPath) {
        TPopupButton pub = null;
        try {
            pub = new TPopupButton(new ImageIcon(AWTUtils.readImageResource(iconPath, command +
                                                                                      ".png",
                null)), command, m);
            pub.setToolTipText(hint);
        }
        catch (IOException e) {
            SysUtils.reportException(e);
        }
        return pub;
    }

}

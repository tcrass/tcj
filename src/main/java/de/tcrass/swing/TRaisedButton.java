package de.tcrass.swing;

import javax.swing.Icon;
import javax.swing.JButton;

import de.tcrass.swing.plaf.basic.RaisedButtonUI;

public class TRaisedButton extends JButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TRaisedButton() {
        this(null, null);
    }

    public TRaisedButton(String text) {
        this(text, null);
    }

    public TRaisedButton(Icon icon) {
        this(null, icon);
    }

    public TRaisedButton(String text, Icon icon) {
        super(text, icon);
        setUI(RaisedButtonUI.createUI(this));
        setContentAreaFilled(false);
    }

}

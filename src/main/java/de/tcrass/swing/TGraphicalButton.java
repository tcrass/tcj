package de.tcrass.swing;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

import de.tcrass.swing.plaf.basic.GraphicalButtonUI;

public class TGraphicalButton extends JButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Dimension         dim;
    private BufferedImage     backgroundImage;
    private BufferedImage     disabledImage;
    private BufferedImage     enabledImage;
    private BufferedImage     rolloverImage;
    private BufferedImage     armedImage;

    public TGraphicalButton(BufferedImage backgroundImage, BufferedImage disabledImage,
                            BufferedImage enabledImage, BufferedImage rolloverImage,
                            BufferedImage armedImage) {
        super();
        dim = new Dimension();
        this.backgroundImage = backgroundImage;
        adjustSizeToImage(backgroundImage);
        this.disabledImage = disabledImage;
        adjustSizeToImage(disabledImage);
        this.enabledImage = enabledImage;
        adjustSizeToImage(enabledImage);
        this.rolloverImage = rolloverImage;
        adjustSizeToImage(rolloverImage);
        this.armedImage = armedImage;
        adjustSizeToImage(armedImage);
        setUI(GraphicalButtonUI.createUI(this));
        setBorderPainted(false);
        setFocusPainted(false);
        setRolloverEnabled(true);
        setOpaque(false);
    }

    private void adjustSizeToImage(BufferedImage i) {
        if (i != null) {
            if (i.getWidth() > dim.width) {
                dim.width = i.getWidth();
            }
            if (i.getHeight() > dim.height) {
                dim.height = i.getHeight();
            }
        }
    }

    // --- Attribute access

    public BufferedImage getBackgroundImage() {
        return backgroundImage;
    }

    public BufferedImage getDisabledImage() {
        return disabledImage;
    }

    public BufferedImage getEnabledImage() {
        return enabledImage;
    }

    public BufferedImage getRolloverImage() {
        return rolloverImage;
    }

    public BufferedImage getArmedImage() {
        return armedImage;
    }

    // --- Public methods

    @Override
    public Dimension getMinimumSize() {
        return dim;
    }

    @Override
    public Dimension getPreferredSize() {
        return dim;
    }

    @Override
    public Dimension getMaximumSize() {
        return dim;
    }

    // --- Implementation of MouseListener
    //
    // public void mouseEntered(MouseEvent e) {
    // }
    // 
    // public void mouseExited(MouseEvent e) {
    // }
    // 
    // public void mousePressed(MouseEvent e) {
    // }
    // 
    // public void mouseReleased(MouseEvent e) {
    // }
    // 
}

package de.tcrass.swing.panel;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;

import de.tcrass.swing.SwingUtils;
import de.tcrass.swing.dialog.TDialog;
import de.tcrass.swing.dialog.TURICluster;
import de.tcrass.util.HistoryList;
import de.tcrass.util.SysUtils;

public class THTMLBrowserPanel extends TTextDisplayPanel
implements HyperlinkListener, ActionListener {

    //==================================================================
    // ======

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private class BrowserHistoryItem {

        private URL            url;
        private HyperlinkEvent event;

        BrowserHistoryItem(URL u, HyperlinkEvent e) {
            try {
                url = new URL(u.toExternalForm());
            }
            catch (MalformedURLException ex) {
                SysUtils.reportException(ex);
            }
            if (e instanceof HTMLFrameHyperlinkEvent) {
                HTMLFrameHyperlinkEvent he = (HTMLFrameHyperlinkEvent) e;
                event = new HTMLFrameHyperlinkEvent(he.getSource(), he.getEventType(),
                    he.getURL(), he.getDescription(), he.getSourceElement(), he.getTarget());
            }
            else {
                event = new HyperlinkEvent(e.getSource(), e.getEventType(), e.getURL(),
                    e.getDescription(), e.getSourceElement());
            }
        }

        URL getURL() {
            return url;
        }

        HyperlinkEvent getEvent() {
            return event;
        }

    }

    //==================================================================
    // ======

    private URL                             homeURL;
    private URL                             currentURL;
    private TBrowserControls                controls;
    private HistoryList<BrowserHistoryItem> history;
    // private Container topWindow;
    private TBrowserStatusBar               status;

    // private JButton backButton;
    // private JButton forwardButton;
    // private JButton homeButton;

    // private String frameName;

    public THTMLBrowserPanel(int rows, int cols) {
        this(null, rows, cols, TBrowserControls.ALL_BUT_CLOSE_BUTTON);
    }

    public THTMLBrowserPanel(URL url, int rows, int cols) {
        this(url, rows, cols, TBrowserControls.ALL_BUT_CLOSE_BUTTON);
    }

    public THTMLBrowserPanel(URL url, int rows, int cols, int buttons) {
        super(TEXT_HTML, false, rows, cols);
        // frameName = name;
        getTextPane().addHyperlinkListener(this);
        history = new HistoryList<BrowserHistoryItem>();
        controls = new TBrowserControls(this, buttons);
        status = new TBrowserStatusBar(this, null);
        if (url != null) {
            try {
                setHomeURL(url);
                goHome();
            }
            catch (IOException e) {
                SysUtils.reportException(e);
            }
        }
        controls.updateButtons();
    }

    // --- Private methods

    private void recordInHistory(URL u, HyperlinkEvent e) {
        BrowserHistoryItem item = new BrowserHistoryItem(u, e);
        history.addItem(item);
        controls.updateButtons();
    }

    private void recordInHistory(URL u) {
        HyperlinkEvent e = new HyperlinkEvent(getTextPane(),
            HyperlinkEvent.EventType.ACTIVATED, u);
        recordInHistory(u, e);
    }

    private void recordInHistory(HyperlinkEvent e) {
        recordInHistory(currentURL, e);
        controls.updateButtons();
    }

    private void processHyperlinkEvent(HyperlinkEvent e) {
        controls.disableButtons();
        if (e instanceof HTMLFrameHyperlinkEvent) {
            HTMLFrameHyperlinkEvent he = (HTMLFrameHyperlinkEvent) e;
            HTMLDocument doc = (HTMLDocument) getTextPane().getDocument();
            doc.processHTMLFrameHyperlinkEvent(he);
        }
        else {
            try {
                setContent(e.getURL());
            }
            catch (IOException ex) {
                SysUtils.reportException(ex);
            }
        }
        controls.updateButtons();
    }

    private void restoreFromHistory(BrowserHistoryItem i) {
        final BrowserHistoryItem item = i;
        SwingUtils.invokeLater(new Thread("Restoring Browser History") {

            @Override
            public void run() {
                HyperlinkEvent e = item.getEvent();
                if (e instanceof HTMLFrameHyperlinkEvent) {
                    // HTMLFrameHyperlinkEvent he =
                    // (HTMLFrameHyperlinkEvent) e;
                    if (!currentURL.equals(item.getURL())) {
                        setContent(item.getURL().toExternalForm());
                    }
                    processHyperlinkEvent(e);
                }
                else {
                    try {
                        setContent(item.getURL());
                    }
                    catch (IOException ex) {
                        SysUtils.reportException(ex);
                    }
                }
                controls.updateButtons();
            }
        });
    }

    // --- Package-scope methods

    HistoryList<BrowserHistoryItem> getHistory() {
        return history;
    }

    // --- Public methods

    public TBrowserControls getBrowserControls() {
        return controls;
    }

    public TBrowserStatusBar getBrowserStatusBar() {
        return status;
    }

    public void setHomeURL(URL u) throws IOException {
        homeURL = new URL(u.toExternalForm());
    }

    public URL getHomeURL() {
        return homeURL;
    }

    @Override
    public void setContent(URL u) throws IOException {
        status.setText("Loading " + u.toExternalForm() + "...");
        super.setContent(u);
        currentURL = new URL(u.toExternalForm());
        status.setText(currentURL.toExternalForm());
    }

    public void goHome() {
        try {
            setContent(homeURL);
            recordInHistory(homeURL);
        }
        catch (IOException e) {
            SysUtils.reportException(e);
        }
    }

    public void goBack() {
        if (history.hasPrevious()) {
            BrowserHistoryItem item = history.getPreviousItem();
            restoreFromHistory(item);
        }
    }

    public void goForward() {
        if (history.hasNext()) {
            BrowserHistoryItem item = history.getNextItem();
            restoreFromHistory(item);
        }
    }

    public void goTo() {
        TDialog d = null;
        Container topWindow = getTopLevelAncestor();
        if (topWindow instanceof Frame) {
            d = new TDialog("Go to URL...", TDialog.OK_BUTTON + TDialog.CANCEL_BUTTON,
                TDialog.OK_BUTTON, (Frame) topWindow);
        }
        else if (topWindow instanceof Dialog) {
            d = new TDialog("Go to URL...", TDialog.OK_BUTTON + TDialog.CANCEL_BUTTON,
                TDialog.OK_BUTTON, (Dialog) topWindow);
        }
        TURICluster url = new TURICluster("URL:");
        d.addCluster(url);
        if (d.showDialog() == TDialog.OK_BUTTON) {
            if (url.getURI() != null) {
                try {
                    URI uri = url.getURI();
                    if (uri.getScheme() == null) {
                        uri = new URI("http://" + uri.getSchemeSpecificPart());
                    }
                    setContent(new URL(uri.toString()));
                    recordInHistory(currentURL);
                }
                catch (URISyntaxException e) {
                    SysUtils.doNothing();
                }
                catch (IOException e) {
                    SysUtils.reportException(e);
                }
            }
        }
    }

    public void reload() {
        try {
            setContent(currentURL);
        }
        catch (IOException e) {
            SysUtils.reportException(e);
        }
    }

    public void close() {
        Container topWindow = getTopLevelAncestor();
        if (topWindow instanceof Frame) {
            ((Frame) topWindow).setVisible(false);
        }
        else if (topWindow instanceof Dialog) {
            ((Dialog) topWindow).setVisible(false);
        }
        // topWindow.setVisible(false);
    }

    // --- Implementation of ActionListener

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("browserHome")) {
            goHome();
        }
        else if (command.equals("browserBack")) {
            goBack();
        }
        else if (command.equals("browserForward")) {
            goForward();
        }
        else if (command.equals("browserGoTo")) {
            goTo();
        }
        else if (command.equals("browserReload")) {
            reload();
        }
        else if (command.equals("browserClose")) {
            close();
        }
    }

    // --- Implementation of HyperlinkListener

    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            processHyperlinkEvent(e);
            recordInHistory(e);
        }
    }

}

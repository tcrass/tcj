package de.tcrass.swing.panel;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import de.tcrass.swing.ListDoubleClickListener;

public class TListPanel extends TGridPanel {

    private static final long            serialVersionUID = 1L;
    private JList                        list;
    private Object[]                     items;
    private MouseListener                ml;
    private Set<ListDoubleClickListener> clickListeners   = new HashSet<ListDoubleClickListener>();

    public TListPanel() {
        super(true);
    }

    public TListPanel(int w, int h) {
        super(true);
        setItems(new Object[0], new Dimension(w, h));
    }

    public TListPanel(String title, int w, int h) {
        super(title);
        setItems(new Object[0], new Dimension(w, h));
    }

    public TListPanel(Object[] items, int w, int h) {
        super(true);
        setItems(items, new Dimension(w, h));
    }

    public TListPanel(String title, Object[] items, int w, int h) {
        super(title);
        setItems(items, new Dimension(w, h));
    }

    public TListPanel(Object[] items, Dimension s) {
        super(true);
        setItems(items, s);
    }

    public TListPanel(String title, Object[] items, Dimension s) {
        super(title);
        setItems(items, s);
    }

    public void setItems(Object[] items, Dimension size) {
        this.items = items;

        removeAll();
        // setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        if (list != null) {
            list.removeMouseListener(ml);
        }

        list = new JList(items);
        list.setVisibleRowCount(-1);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    TListPanel.this.mouseDoubleClicked(me);
                }
            }
        });

        JScrollPane s = new JScrollPane(list);
        s.setPreferredSize(size);

        addBlockComponent(s);
    }

    private void mouseDoubleClicked(MouseEvent me) {
        if (list.locationToIndex(me.getPoint()) >= 0) {
            for (ListDoubleClickListener l : clickListeners) {
                l.listDoubleClicked(this);
            }
        }
    }

    public void addListDoubleClickListener(ListDoubleClickListener l) {
        clickListeners.add(l);
    }

    public void removeDoubleListClickListener(ListDoubleClickListener l) {
        clickListeners.remove(l);
    }

    public JList getList() {
        return list;
    }

    public Object[] getItems() {
        return items;
    }

    public void setValue(int selection) {
        if ((selection >= 0) && (selection < items.length)) {
            list.setSelectedIndex(selection);
            list.ensureIndexIsVisible(selection);
        }
    }

    public void setValue(Object o) {
        for (int i = 0; i < list.getModel().getSize(); i++) {
            if (list.getModel().getElementAt(i) == o) {
                setValue(i);
                break;
            }
        }
    }

    public int getValue() {
        return list.getSelectedIndex();
    }

    public Object getValueAsObject() {
        Object o = null;
        if (list.getSelectedIndex() >= 0) {
            o = list.getModel().getElementAt(list.getSelectedIndex());
        }
        return o;
    }

    public String getValueAsString() {
        String s = "";
        if (list.getSelectedIndex() >= 0) {
            s = list.getModel().getElementAt(list.getSelectedIndex()).toString();
        }
        return s;
    }

    public int getValueUnderMouse(MouseEvent e) {
        return list.locationToIndex(e.getPoint());
    }

}

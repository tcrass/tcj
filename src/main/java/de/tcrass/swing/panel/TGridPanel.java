package de.tcrass.swing.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

import de.tcrass.awt.AWTUtils;
import de.tcrass.swing.dialog.TComponentCluster;
import de.tcrass.swing.dialog.ValidatingInputComponent;

public class TGridPanel extends JPanel implements ValidatingInputComponent {

    private static final long                   serialVersionUID     = 1L;
    public static final int                     DEFAULT_BORDER_WIDTH = 8;
    public static final int                     DEFAULT_GRID_COLS    = 6;

    private int                                 nextX                = 0;
    private int                                 nextY                = 0;
    private int                                 cols                 = DEFAULT_GRID_COLS;
    private int                                 maxHeight            = 0;
    private GridBagLayout                       lm;
    private ArrayList<ValidatingInputComponent> inputValidators;

    public TGridPanel() {
        super();
        initPanel();
        setEmptyBorder(0);
    }

    public TGridPanel(String title) {
        super();
        initPanel();
        setTitledBorder(title);
    }

    public TGridPanel(boolean bordered) {
        super();
        initPanel();
        if (bordered) {
            setEtchedBorder();
        }
        else {
            setEmptyBorder(DEFAULT_BORDER_WIDTH);
        }
    }

    public TGridPanel(int borderWidth) {
        super();
        initPanel();
        setEmptyBorder(borderWidth);
    }

    private void initPanel() {
        lm = new GridBagLayout();
        setLayout(lm);
        inputValidators = new ArrayList<ValidatingInputComponent>();
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public void setEmptyBorder(int borderWidth) {
        setBorder(BorderFactory.createEmptyBorder(borderWidth, borderWidth, borderWidth,
            borderWidth));
    }

    public void setEtchedBorder() {
        setBorder(BorderFactory.createEtchedBorder());
    }

    public void setTitledBorder(String title) {
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
    }

    public void addCluster(TComponentCluster p) {
        if (nextX != 0) {
            newRow();
        }
        maxHeight = 0;
        for (int i = 0; i < p.getComponentCount(); i++) {
            JComponent c = p.getComponent(i);
            GridBagConstraints gbc = p.getComponentConstraints(i);
            nextX = 0;
            addComponent(c, gbc);
        }
        newRow();
        inputValidators.add(p);
    }

    public void addComponent(JComponent c, GridBagConstraints ori) {
        GridBagConstraints gbc = (GridBagConstraints) ori.clone();
        gbc.gridx = gbc.gridx + nextX;
        gbc.gridy = gbc.gridy + nextY;
        lm.setConstraints(c, gbc);
        add(c);
        if (ori.gridheight > maxHeight) {
            maxHeight = ori.gridheight;
        }
        nextX = gbc.gridx + gbc.gridwidth;
        if (c instanceof ValidatingInputComponent) {
            inputValidators.add((ValidatingInputComponent) c);
        }
    }

    public void addLeftComponent(JComponent c, int width) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, width, 1, 0, 0, GridBagConstraints.NONE,
            GridBagConstraints.WEST);
        addComponent(c, gbc);
    }

    public void addRightComponent(JComponent c, int width) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, width, 1, 0, 0, GridBagConstraints.NONE,
            GridBagConstraints.EAST);
        addComponent(c, gbc);
    }

    public void addLeftMaxComponent(JComponent c, int width) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, width, 1, 1, 0,
            GridBagConstraints.HORIZONTAL, GridBagConstraints.WEST);
        addComponent(c, gbc);
    }

    public void addRightMaxComponent(JComponent c, int width) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, width, 1, 1, 0,
            GridBagConstraints.HORIZONTAL, GridBagConstraints.EAST);
        addComponent(c, gbc);
    }

    public void addRowComponent(JComponent c) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, cols, 1, 1, 0,
            GridBagConstraints.HORIZONTAL, GridBagConstraints.WEST);
        addComponent(c, gbc);
        newRow();
    }

    public void addBlockComponent(JComponent c) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, cols, 1, 1, 1, GridBagConstraints.BOTH,
            GridBagConstraints.WEST);
        addComponent(c, gbc);
        newRow();
    }

    public void addShyComponent(JComponent c) {
        GridBagConstraints gbc = AWTUtils.newGBC(0, 0, cols, 1, 0, 0, GridBagConstraints.NONE,
            GridBagConstraints.WEST);
        addComponent(c, gbc);
        newRow();
    }

    public void newRow() {
        nextX = 0;
        nextY = nextY + maxHeight;
        maxHeight = 0;
    }

    // --- Implementation of InputValidator

    public boolean isValidInput() {
        boolean valid = true;
        for (int i = 0; i < inputValidators.size(); i++) {
            ValidatingInputComponent c = inputValidators.get(i);
            if (!c.isValidInput()) {
                valid = false;
                // break;
            }
        }
        return valid;
    }

}

/*
 * Created on 01.04.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.panel;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TTextPanel extends TGridPanel {

    /**
     * 
     */
    private static final long serialVersionUID     = 1L;
    public static final int   DEFAULT_COLS         = 40;
    public static final int   DEFAULT_ROWS         = 5;
    public static final int   DEFAULT_BORDER_WIDTH = 8;

    private JTextComponent    textComponent;

    public TTextPanel(String text, int rows, int cols) {
        super();
        initPanel(text, rows, cols);
        setEmptyBorder(0);
    }

    public TTextPanel(String title, String text, int rows, int cols) {
        super(title);
        initPanel(text, rows, cols);
    }

    public TTextPanel(boolean bordered, String text, int rows, int cols) {
        super(bordered);
        initPanel(text, rows, cols);
    }

    private void initPanel(String text, int rows, int cols) {
        // setLayout(new BorderLayout());
        setLayout(new BorderLayout());
        textComponent = createTextComponent(text, rows, cols);
        textComponent.setPreferredSize(new JTextArea(rows, cols).getPreferredSize());
        // textComponent.setMinimumSize(new JTextArea(rows,
        // cols).getPreferredSize());
        JScrollPane p = new JScrollPane(textComponent);
        p.setPreferredSize(textComponent.getPreferredSize());
        addBlockComponent(p);
    }

    protected JTextComponent createTextComponent(String text, int rows, int cols) {
        return new JTextComponent() {

            /**
             * 
             */
            private static final long serialVersionUID = 1L;
        };
    }

    protected JTextComponent getTextComponent() {
        return textComponent;
    }

}

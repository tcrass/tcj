package de.tcrass.swing.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.tcrass.awt.AWTUtils;

public class TImagePanel2 extends TGridPanel implements ChangeListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private double            zoomFactor       = 1;
    private double            minZoom          = 0.25;
    private double            maxZoom          = 4;
    private BufferedImage     image;

    private ImageCanvas       canvas;
    private JScrollPane       scroller;
    private ZoomControlPanel  controls;

    // ======================================================

    private class ImageCanvas extends JComponent {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private Dimension         zoomedSize       = new Dimension(0, 0);
        private BufferedImage     zoomedImage;
        private TImagePanel2      imagePanel;

        private ImageCanvas(TImagePanel2 p) {
            imagePanel = p;
        }

        private void rezoomImage() {
            if (image != null) {
                int width = (int) ((image.getWidth()) * zoomFactor);
                int height = (int) ((image.getHeight()) * zoomFactor);

                zoomedSize = new Dimension(width, height);

                System.out.println("Rezooming to " + zoomedSize.toString());

                setSize(zoomedSize);
                setMinimumSize(zoomedSize);
                setPreferredSize(zoomedSize);
                setMaximumSize(zoomedSize);
                zoomedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g = (Graphics2D) zoomedImage.getGraphics();
                if (zoomFactor != Math.floor(zoomFactor)) {
                    g.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);
                }
                else {
                    g.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_DEFAULT);
                }
                g.drawImage(image, 0, 0, width, height, 0, 0, image.getWidth(),
                    image.getHeight(), this);
                scroller.invalidate();
            }

        }

        @Override
        public void paintComponent(Graphics g) {
            if (image == null) {
                super.paintComponent(g);
            }
            else {
                g.drawImage(zoomedImage, 0, 0, this);
            }
        }

    }

    private class ZoomControlPanel extends JPanel {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private JSlider           zoomControl;
        private JLabel            factorDisplay;
        private DecimalFormat     format;
        private TImagePanel2      imagePanel;

        private ZoomControlPanel(TImagePanel2 p) {
            imagePanel = p;

            setLayout(new GridBagLayout());

            GridBagConstraints gbc = AWTUtils.newGBC(0, 0, 1, 1, 0, 1, GridBagConstraints.BOTH,
                GridBagConstraints.WEST);
            JLabel l = new JLabel("Zoom:");
            add(l, gbc);

            gbc = AWTUtils.newGBC(1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH,
                GridBagConstraints.CENTER);
            zoomControl = new JSlider();
            add(zoomControl, gbc);

            gbc = AWTUtils.newGBC(2, 0, 1, 1, 0, 1, GridBagConstraints.BOTH,
                GridBagConstraints.CENTER);
            factorDisplay = new JLabel("1");
            add(factorDisplay, gbc);

            adjustZoomControl();
            zoomControl.addChangeListener(imagePanel);

            setBorder(BorderFactory.createEtchedBorder());
        }

        private void adjustZoomControl() {
            zoomControl.setMinimum(factor2code(minZoom));
            zoomControl.setMaximum(factor2code(maxZoom));
            zoomControl.setValue(factor2code(zoomFactor));
            factorDisplay.setText(getFactorAsText(zoomFactor));
        }

        protected String getFactorAsText(double factor) {
            String s;
            if (format == null) {
                format = new DecimalFormat("0.#");
            }
            if (factor >= 0) {
                s = format.format(factor) + " : 1";
            }
            else {
                s = "1 : " + format.format(1 / factor);
            }
            return s;
        }

    }

    // ======================================================

    public TImagePanel2() {
        super();

        setLayout(new BorderLayout());

        canvas = new ImageCanvas(this);

        scroller = new JScrollPane(canvas);

        add(scroller, BorderLayout.CENTER);

        controls = new ZoomControlPanel(this);
        add(controls, BorderLayout.SOUTH);

    }

    // --- Private methods -----------------------------------

    protected static int factor2code(double factor) {
        int code = 0;
        if (factor >= 1) {
            code = (int) (10 * (factor - 1));
        }
        else {
            code = 10 - (int) (10 / (factor));
        }
        return code;
    }

    protected static double code2factor(int code) {
        double factor = 1;
        if (code >= 0) {
            factor = ((double) (code)) / 10 + 1;
        }
        else {
            factor = 1 / (1 - ((double) code) / 10);
        }
        return factor;
    }

    // --- Public methods ------------------------------------

    public ZoomControlPanel getControls() {
        return controls;
    }

    public BufferedImage getImage() {
        return image;
    }

    public BufferedImage getZoomedImage() {
        return canvas.zoomedImage;
    }

    public void setImage(BufferedImage img) {
        image = img;
        canvas.rezoomImage();
    }

    public void setZoomFactor(double f) {
        zoomFactor = f;
        controls.adjustZoomControl();
    }

    // --- Implementation of ChangeListener

    public void stateChanged(ChangeEvent e) {
        zoomFactor = code2factor(controls.zoomControl.getValue());
        controls.adjustZoomControl();
        canvas.rezoomImage();
    }

}

package de.tcrass.swing.panel;

import java.awt.GridLayout;
import java.util.Enumeration;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JRadioButton;

public class TRadioPanel extends TGridPanel {

    private static final long serialVersionUID = 1L;
    private ButtonGroup       buttons;
    private String[]          items;

    public TRadioPanel() {
        super(true);
    }

    public TRadioPanel(String[] items) {
        super(true);
        setItems(items);
    }

    public TRadioPanel(String[] items, int value) {
        super(true);
        setItems(items);
        setValue(value);
    }

    public TRadioPanel(String title) {
        super(title);
    }

    public TRadioPanel(String title, String[] items) {
        super(title);
        setItems(items);
    }

    public TRadioPanel(String title, String[] items, int value) {
        super(title);
        setItems(items);
        setValue(value);
    }

    public void setTitle(String title) {
        setTitledBorder(title);
    }

    public void setItems(String[] items) {
        removeAll();
        // setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setLayout(new GridLayout(items.length, 1));
        buttons = new ButtonGroup();
        for (String element : items) {
            JRadioButton b = new JRadioButton(element);
            buttons.add(b);
            add(b);
        }
        this.items = items;
    }

    public void setValue(int value) {
        int index = 0;
        Enumeration<?> e = buttons.getElements();
        while (e.hasMoreElements()) {
            ButtonModel m = ((JRadioButton) e.nextElement()).getModel();
            if (index == value) {
                m.setSelected(true);
                break;
            }
            index++;
        }
    }

    public int getValue() {
        int selected = -1;
        int index = 0;
        Enumeration<?> e = buttons.getElements();
        while (e.hasMoreElements()) {
            ButtonModel m = ((JRadioButton) e.nextElement()).getModel();
            if (m.isSelected()) {
                selected = index;
                break;
            }
            index++;
        }
        return selected;
    }

    public String getValueAsString() {
        String s = "";
        int selected = getValue();
        if (selected >= 0) {
            s = items[selected];
        }
        return s;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.comlink.home.tcrass.swing.TDialogPanel#createInputComponent()
     */

    // public Dimension getMaximumSize() {
    // return new Dimension(super.getMaximumSize().width,
    // getPreferredSize().height);
    // }
}

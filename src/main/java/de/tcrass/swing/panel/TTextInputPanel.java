/*
 * Created on 18.02.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.panel;

import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TTextInputPanel extends TTextPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public static final int   DEFAULT_COLS     = 40;
    public static final int   DEFAULT_ROWS     = 5;

    private JTextArea         textArea;

    public TTextInputPanel(String title) {
        super(title, DEFAULT_ROWS, DEFAULT_COLS);
    }

    public TTextInputPanel(String title, int rows, int cols) {
        super(title, rows, cols);
    }

    public TTextInputPanel(String title, String text) {
        super(title, text, DEFAULT_ROWS, DEFAULT_COLS);
    }

    public TTextInputPanel(String title, String text, int rows, int cols) {
        super(title, text, rows, cols);
    }

    @Override
    protected JTextComponent createTextComponent(String text, int rows, int cols) {
        // setLayout(new BorderLayout());
        textArea = new JTextArea(text, rows, cols);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        return textArea;
    }

    public String getText() {
        return textArea.getText();
    }

    public void setText(String text) {
        textArea.setText(text);
    }

}

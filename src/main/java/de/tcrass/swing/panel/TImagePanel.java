/*
 * Created on 06.09.2004 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.swing.panel;

import java.awt.Adjustable;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.tcrass.awt.AWTUtils;
import de.tcrass.swing.dialog.TComponentCluster;
import de.tcrass.swing.dialog.TSliderCluster;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class TImagePanel extends JPanel
implements ChangeListener, ComponentListener, AdjustmentListener {

    // =======================================================

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private class ZoomFactorSliderCluster extends TSliderCluster {

        private DecimalFormat format;

        ZoomFactorSliderCluster(int min, int max) {
            super("Zoom", min, max);
            setWidth(20);
        }

        @Override
        protected String getValueAsText(int factor) {
            String s;
            if (format == null) {
                format = new DecimalFormat("0.#");
            }
            if (factor >= 0) {
                s = format.format(zoomCode2zoomFactor(factor)) + " : 1";
            }
            else {
                s = "1 : " + format.format(1 / zoomCode2zoomFactor(factor));
            }
            return s;
        }

    }

    // =======================================================

    private class ZoomableCanvas extends JPanel {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        ZoomableCanvas() {
            super();
            setOpaque(true);
            // setBackground(Color.black);
        }

        @Override
        public void paintComponent(Graphics g) {
            if (!canZoom()) {
                super.paintComponent(g);
            }
            else {
                if (fitsX() || fitsY()) {
                    super.paintComponent(g);
                }
                double imageX;
                double imageY;
                double imageWidth;
                if (fitsX()) {
                    imageX = 0;
                    imageWidth = image.getWidth();
                }
                else {
                    imageX = (hScroller.getValue()) / zoomFactor;
                    imageWidth = (hScroller.getVisibleAmount()) / zoomFactor;
                }
                double imageHeight;
                if (fitsY()) {
                    imageY = 0;
                    imageHeight = image.getHeight();
                }
                else {
                    imageY = (vScroller.getValue()) / zoomFactor;
                    imageHeight = (vScroller.getVisibleAmount()) / zoomFactor;
                }

                double canvasX;
                double canvasY;
                double canvasWidth;
                double canvasHeight;
                if (fitsX()) {
                    canvasWidth = getZoomedImageSize().getWidth();
                    canvasX = (canvas.getWidth() - canvasWidth) / 2;
                }
                else {
                    canvasX = 0;
                    canvasWidth = getWidth();
                }
                if (fitsY()) {
                    canvasHeight = getZoomedImageSize().getHeight();
                    canvasY = (canvas.getHeight() - canvasHeight) / 2;
                }
                else {
                    canvasY = 0;
                    canvasHeight = getHeight();
                }

                Graphics2D g2 = (Graphics2D) g;
                if (antiAliasing) {
                    double log2 = Math.log(zoomFactor) / Math.log(2);
                    if ((zoomFactor < 1) || (log2 != Math.rint(log2))) {
                        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
                    }
                }
                g2.drawImage(image, (int) canvasX, (int) canvasY,
                    (int) (canvasX + canvasWidth), (int) (canvasY + canvasHeight),
                    (int) imageX, (int) imageY, (int) (imageX + imageWidth),
                    (int) (imageY + imageHeight), Color.BLACK, this);
            }
        }
    }

    // =======================================================

    private ZoomableCanvas canvas;
    private double         zoomFactor;
    private JScrollBar     hScroller, vScroller;
    private TGridPanel     controlPanel;
    private TSliderCluster zoomControl;
    private BufferedImage  image;
    private double         lastZoomFactor = 1;
    private boolean        antiAliasing   = false;

    // --- Constructors

    public TImagePanel() {
        this(null, 0.25, 4);
    }

    public TImagePanel(BufferedImage image) {
        this(image, 0.25, 4);
    }

    public TImagePanel(double minZoom, double maxZoom) {
        this(null, minZoom, maxZoom);
    }

    public TImagePanel(BufferedImage image, double minZoom, double maxZoom) {
        initPanel(image, minZoom, maxZoom);
    }

    private void initPanel(BufferedImage image, double minZoom, double maxZoom) {

        GridBagLayout lm = new GridBagLayout();
        setLayout(lm);

        GridBagConstraints gbc;

        vScroller = new JScrollBar(Adjustable.VERTICAL);
        gbc = AWTUtils.newGBC(1, 0, 1, 1, 0, 0, GridBagConstraints.VERTICAL,
            GridBagConstraints.EAST);
        lm.setConstraints(vScroller, gbc);
        add(vScroller);

        hScroller = new JScrollBar(Adjustable.HORIZONTAL);
        gbc = AWTUtils.newGBC(0, 1, 1, 1, 0, 0, GridBagConstraints.HORIZONTAL,
            GridBagConstraints.SOUTH);
        lm.setConstraints(hScroller, gbc);
        add(hScroller);

        canvas = new ZoomableCanvas();
        gbc = AWTUtils.newGBC(0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH,
            GridBagConstraints.NORTHWEST);
        lm.setConstraints(canvas, gbc);
        add(canvas);

        controlPanel = new TGridPanel();
        zoomControl = new ZoomFactorSliderCluster(zoomFactor2zoomCode(minZoom),
            zoomFactor2zoomCode(maxZoom));
        setZoomRange(minZoom, maxZoom);
        controlPanel.addCluster(zoomControl);

        setImage(image);
        setZoomFactor(1);

        vScroller.addAdjustmentListener(this);
        hScroller.addAdjustmentListener(this);
        canvas.addComponentListener(this);
        zoomControl.getSlider().addChangeListener(this);
    }

    // --- Static methods

    protected static int zoomFactor2zoomCode(double factor) {
        int code = 0;
        if (factor >= 1) {
            code = (int) (10 * (factor - 1));
        }
        else {
            code = 10 - (int) (10 / (factor));
        }
        return code;
    }

    protected static double zoomCode2zoomFactor(int code) {
        double factor = 1;
        if (code >= 0) {
            factor = ((double) (code)) / 10 + 1;
        }
        else {
            factor = 1 / (1 - ((double) code) / 10);
        }
        return factor;
    }

    // --- Attribute access methods

    public BufferedImage getImage() {
        return image;
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public TComponentCluster getControlCluster() {
        return zoomControl;
    }

    public JComponent getControlPanel() {
        return controlPanel;
    }

    public boolean isAntiAliasing() {
        return antiAliasing;
    }

    public void setAntiAliasing(boolean antiAliasing) {
        this.antiAliasing = antiAliasing;
    }

    // --- Private methods

    private boolean canZoom() {
        return (image != null) && (canvas.getWidth() > 0) && (canvas.getHeight() > 0);
    }

    private double getMinZoomFactor() {
        double f = 1;
        if (canZoom()) {
            f = ((double) canvas.getWidth()) / ((double) image.getWidth());
            if ((double) (canvas.getHeight()) / ((double) image.getHeight()) < f) {
                f = ((double) canvas.getHeight()) / ((double) image.getHeight());
            }
        }
        if (f > 1) {
            f = 1;
        }
        return f;
    }

    public double getMaxZoomFactor() {
        double f = 1;
        if (image != null) {
            f = ((double) Short.MAX_VALUE) / ((double) image.getWidth());
            if ((double) (Short.MAX_VALUE) / ((double) image.getHeight()) > f) {
                f = ((double) Short.MAX_VALUE) / ((double) image.getHeight());
            }
        }
        return f;
    }

    private Dimension getZoomedImageSize(double f) {
        Dimension d = null;
        if (image != null) {
            int w = (int) (f * image.getWidth());
            int h = (int) (f * image.getHeight());
            d = new Dimension(w, h);
        }
        else {
            d = new Dimension(1, 1);
        }
        return d;
    }

    private Dimension getZoomedImageSize() {
        return getZoomedImageSize(zoomFactor);
    }

    private boolean fitsX() {
        return getZoomedImageSize().getWidth() <= canvas.getWidth();
    }

    private boolean fitsY() {
        return getZoomedImageSize().getHeight() <= canvas.getHeight();
    }

    private void updateZoomControl() {
        if (canZoom()) {
            if (zoomControl.getSlider().getMinimum() < zoomFactor2zoomCode(getMinZoomFactor())) {
                zoomControl.getSlider().setMinimum(zoomFactor2zoomCode(getMinZoomFactor()));
            }
            if (zoomControl.getSlider().getMaximum() > zoomFactor2zoomCode(getMaxZoomFactor())) {
                zoomControl.getSlider().setMaximum(zoomFactor2zoomCode(getMaxZoomFactor()));
            }
            if (zoomFactor < getMinZoomFactor()) {
                zoomFactor = getMinZoomFactor();
            }
            if (zoomFactor > getMaxZoomFactor()) {
                zoomFactor = getMaxZoomFactor();
            }
            zoomControl.setSliderPosition(zoomFactor2zoomCode(zoomFactor));
        }
    }

    private void updateScrollers() {
        if (canZoom()) {
            Dimension dLastImage = getZoomedImageSize(lastZoomFactor);
            Dimension dImage = getZoomedImageSize();

            if (fitsX()) {
                // hScroller.setVisible(true);
                hScroller.setMaximum(canvas.getWidth());
                hScroller.setValue(0);
                hScroller.setVisibleAmount(canvas.getWidth());
            }
            else {
                // hScroller.setVisible(true);
                double change = zoomFactor / lastZoomFactor;
                double lastCorner = hScroller.getValue();
                double lastRelCorner = lastCorner - dLastImage.getWidth() / 2;
                double lastRelCenter = lastRelCorner + canvas.getWidth() / 2;
                double relCenter = lastRelCenter * change;
                double relCorner = relCenter - canvas.getWidth() / 2;
                double corner = relCorner + dImage.getWidth() / 2;
                hScroller.setMaximum(dImage.width);
                hScroller.setValue((int) corner);
                hScroller.setVisibleAmount(canvas.getWidth());
            }

            if (fitsY()) {
                // vScroller.setVisible(true);
                vScroller.setMaximum(canvas.getHeight());
                vScroller.setValue(0);
                vScroller.setVisibleAmount(canvas.getHeight());
            }
            else {
                // vScroller.setVisible(true);
                double change = zoomFactor / lastZoomFactor;
                double lastCorner = vScroller.getValue();
                double lastRelCorner = lastCorner - dLastImage.getHeight() / 2;
                double lastRelCenter = lastRelCorner + canvas.getHeight() / 2;
                double relCenter = lastRelCenter * change;
                double relCorner = relCenter - canvas.getHeight() / 2;
                double corner = relCorner + dImage.getHeight() / 2;
                vScroller.setMaximum(dImage.height);
                vScroller.setValue((int) corner);
                vScroller.setVisibleAmount(canvas.getHeight());
            }
        }
        else {
            hScroller.setMaximum(0);
            vScroller.setMaximum(0);
        }
        if (fitsX() && fitsY()) {
            lastZoomFactor = getMinZoomFactor();
        }
        else {
            lastZoomFactor = zoomFactor;
        }
        canvas.repaint();
    }

    private void doSetZoomFactor(double f) {
        zoomFactor = f;
        updateZoomControl();
        updateScrollers();
    }

    // --- Public methods ---

    public void setImage(BufferedImage image) {
        if (image != null) {
            image.flush();
        }
        this.image = image;
        updateScrollers();
        updateZoomControl();
    }

    public void setZoomRange(double minZoom, double maxZoom) {
        double min = minZoom;
        double max = maxZoom;
        if (canZoom()) {
            if (max > getMaxZoomFactor()) {
                max = getMaxZoomFactor();
            }
            if (min <= 0) {
                min = getMinZoomFactor();
            }
        }
        zoomControl.getSlider().setMinimum(zoomFactor2zoomCode(min));
        zoomControl.getSlider().setMaximum(zoomFactor2zoomCode(max));
        updateZoomControl();
    }

    public void adjustZoomRangeToImage() {
        if (canZoom()) {
            zoomControl.getSlider().setMinimum(zoomFactor2zoomCode(getMinZoomFactor()));
            zoomControl.getSlider().setMaximum(zoomFactor2zoomCode(getMaxZoomFactor()));
            updateZoomControl();
        }
    }

    public void setCanvasBackground(Color c) {
        canvas.setBackground(c);
    }

    public void setZoomFactor(double f) {
        doSetZoomFactor(f);
        zoomControl.setSliderPosition(zoomFactor2zoomCode(zoomFactor));
    }

    public double getOptimumZoomFactor() {
        double f = 1;
        if (canZoom()) {
            f = ((double) canvas.getWidth()) / ((double) image.getWidth());
            if ((double) (canvas.getHeight()) / ((double) image.getHeight()) < f) {
                f = ((double) canvas.getHeight()) / ((double) image.getHeight());
            }
        }
        return f;
    }

    public void zoomToFit() {
        setZoomFactor(getOptimumZoomFactor());
    }

    // --- Implementation of ChangeListener

    public void stateChanged(ChangeEvent e) {
        JSlider slider = zoomControl.getSlider();
        if (e.getSource() == slider) {
            doSetZoomFactor(zoomCode2zoomFactor(slider.getValue()));
        }
    }

    // --- Implementation of ComponentListener

    public void componentShown(ComponentEvent e) {
        updateScrollers();
        updateZoomControl();
    }

    public void componentHidden(ComponentEvent e) {
        updateScrollers();
        updateZoomControl();
    }

    public void componentMoved(ComponentEvent e) {
        updateScrollers();
        updateZoomControl();
    }

    public void componentResized(ComponentEvent e) {
        updateScrollers();
        updateZoomControl();
    }

    // --- Implementation of AdjustmentListener

    public void adjustmentValueChanged(AdjustmentEvent e) {
        if (e.getSource() == hScroller) {
            canvas.repaint();
        }
        else if (e.getSource() == vScroller) {
            canvas.repaint();
        }
    }
}

/*
 * Created on 01.04.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing.panel;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.JTextComponent;

import de.tcrass.util.SysUtils;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TTextDisplayPanel extends TTextPanel {

    /**
     * 
     */
    private static final long  serialVersionUID = 1L;
    public static final String TEXT_PLAIN       = "text/plain";
    public static final String TEXT_HTML        = "text/html";
    public static final String TEXT_RTF         = "text/rtf";

    public TTextDisplayPanel(String type, String title, int rows, int cols) {
        super(title, type, rows, cols);
        init(null);
    }

    public TTextDisplayPanel(String type, String title, String text, int rows, int cols) {
        super(title, type, rows, cols);
        setContent(text);
    }

    public TTextDisplayPanel(String type, boolean bordered, int rows, int cols) {
        super(bordered, type, rows, cols);
    }

    public TTextDisplayPanel(String type, boolean bordered, String text, int rows, int cols) {
        super(bordered, type, rows, cols);
        setContent(text);
    }

    private void init(String text) {
        if (text != null) {
            setContent(text);
        }
    }

    @Override
    protected JTextComponent createTextComponent(String type, int rows, int cols) {
        JTextPane t = new JTextPane();
        t.setContentType(type);
        t.setEditable(false);
        t.setPreferredSize(new JTextArea(rows, cols).getPreferredSize());
        return t;
    }

    public JTextPane getTextPane() {
        return (JTextPane) getTextComponent();
    }

    public void setContent(String s) {
        getTextPane().setText(s);
    }

    public void setContent(File f) throws IOException {
        try {
            setContent(f.toURI().toURL());
        }
        catch (MalformedURLException e) {
            SysUtils.reportException(e);
        }
    }

    public void setContent(URL u) throws IOException {
        getTextPane().setPage(u);
    }

}

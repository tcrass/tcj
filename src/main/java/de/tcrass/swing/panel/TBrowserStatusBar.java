package de.tcrass.swing.panel;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TBrowserStatusBar extends JPanel {

    private static final long serialVersionUID = 1L;
    private THTMLBrowserPanel browser;
    private JLabel            label;

    TBrowserStatusBar(THTMLBrowserPanel b, String text) {
        browser = b;
        setBorder(BorderFactory.createLoweredBevelBorder());

        setLayout(new BorderLayout());
        label = new JLabel();
        add(label, BorderLayout.CENTER);

        setText(text);
    }

    public THTMLBrowserPanel getBrowserPanel() {
        return browser;
    }

    public void setText(String text) {
        label.setText(text);
    }

}

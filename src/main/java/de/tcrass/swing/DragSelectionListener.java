package de.tcrass.swing;

import java.awt.Point;
import java.awt.Rectangle;

public interface DragSelectionListener {

    void dragSelectionStarted(Point dragStart, int modifiers);

    void dragSelectionCompleted(Rectangle dragRect, int modifiers);

}

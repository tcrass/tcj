/*
 * Created on 11.02.2005
 */
package de.tcrass.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import de.tcrass.awt.CommandManager;
import de.tcrass.awt.MouseTracker;
import de.tcrass.awt.dnd.DnDManager;
import de.tcrass.util.StringUtils;
import de.tcrass.util.SysUtils;
import de.tcrass.util.ThreadPool;

/**
 * @author tcrass
 */
public abstract class TApplication extends JFrame implements ActionListener {

    /**
     * 
     */
    private static final long   serialVersionUID = -4435738807426775610L;
    private static TApplication application;
    private static Class<?>     applicationClass;
    private static String       applicationName;

    private String              version          = "N.N.";

    private CommandManager      commandManager;
    private DnDManager          dndManager;
    private MouseTracker        mouseTracker;

    private JMenuBar            menuBar;

    private JComponent          paneContent;

    // === Static methods ======================================

    // --- Public static methods -------------------------------

    // --- Call from main method

    public static final void runApplication(String[] argv) {
        try {
            StackTraceElement[] stack = new Throwable().getStackTrace();
            StackTraceElement startup = stack[stack.length - 1];
            String startupClassName = startup.getClassName();
            applicationClass = Class.forName(startupClassName);
            application = (TApplication) applicationClass.newInstance();
            application.preInit();
            application.init(argv);
            application.postInit();
        }
        catch (Exception ex) {
            SysUtils.reportException(ex);
        }
    }

    // --- May be called from everywhere

    public static final TApplication getApplication() {
        return application;
    }

    public static final DnDManager getDnDManager() {
        return application.dndManager;
    }

    public static final MouseTracker getMouseTracker() {
        return application.mouseTracker;
    }

    public static final CommandManager getCommandManager() {
        return application.commandManager;
    }

    public static final void setWindowTitle(String title) {
        application.setTitle(applicationName + (title != null ? " - " + title : ""));
    }

    public static final void setContent(JComponent content) {
        setContent(content, true);
    }

    public static final void setContent(JComponent content, boolean scrollable) {
        if (application.paneContent != null) {
            application.getContentPane().remove(application.paneContent);
            application.paneContent = null;
        }
        if (content != null) {
            if (scrollable) {
                JScrollPane p = new JScrollPane(content);
                application.paneContent = p;
            }
            else {
                application.paneContent = content;
            }
            application.paneContent.setLocation(0, 0);
            application.paneContent.setSize(application.getContentPane().getSize());
            application.getContentPane().add(application.paneContent, BorderLayout.CENTER);
            if (scrollable) {
                content.setLocation(0, 0);
                content.setSize(application.getContentPane().getSize());
            }
        }
    }

    public static final File selectFileToOpen(String title, File dir, String name,
                                              String[] extensions) {
        File f = null;
        JFileChooser d = new JFileChooser();
        d.setDialogTitle(title);
        d.setCurrentDirectory(dir);
        if (name != null) {
            d.setSelectedFile(new File(name));
        }
        d.setDialogType(JFileChooser.OPEN_DIALOG);
        d.setFileSelectionMode(JFileChooser.FILES_ONLY);
        TFileExtensionFilter ff = null;
        if (extensions != null) {
            ff = new TFileExtensionFilter(extensions);
        }
        d.addChoosableFileFilter(ff);
        if (d.showDialog(getApplication(), "OK") == JFileChooser.APPROVE_OPTION) {
            if (d.getSelectedFile() != null) {
                f = d.getSelectedFile();
            }
        }
        return f;
    }

    public static File selectFileToOpen() {
        return selectFileToOpen("Open file", null, null, null);
    }

    public static final File selectDirectory(String title, File dir, String name) {
        File f = null;
        JFileChooser d = new JFileChooser();
        d.setDialogTitle(title);
        d.setCurrentDirectory(dir);
        if (name != null) {
            d.setSelectedFile(new File(name));
        }
        d.setDialogType(JFileChooser.OPEN_DIALOG);
        d.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (d.showDialog(getApplication(), "OK") == JFileChooser.APPROVE_OPTION) {
            if (d.getSelectedFile() != null) {
                f = d.getSelectedFile();
            }
        }
        return f;
    }

    public static File selectDirectory() {
        return selectDirectory("Select a directory...", null, null);
    }

    public static final File selectFileToSave(String title, File dir, String name,
                                              String[] extensions) {
        File f = null;
        JFileChooser d = new JFileChooser();
        d.setDialogTitle(title);
        d.setCurrentDirectory(dir);
        if (name != null) {
            d.setSelectedFile(new File(name));
        }
        d.setDialogType(JFileChooser.SAVE_DIALOG);
        d.setFileSelectionMode(JFileChooser.FILES_ONLY);
        TFileExtensionFilter ff = null;
        if (extensions != null) {
            ff = new TFileExtensionFilter(extensions);
        }
        d.addChoosableFileFilter(ff);
        if (d.showDialog(getApplication(), "OK") == JFileChooser.APPROVE_OPTION) {
            if (d.getSelectedFile() != null) {
                f = d.getSelectedFile();
            }
        }
        return f;
    }

    public static File selectFileToSave() {
        return selectFileToOpen("Save file as...", null, null, null);
    }

    // === Instance methods ====================================

    // --- Private methods -------------------------------------

    private void preInit() {
        String applicationClassName = applicationClass.getName();
        if (applicationClassName.lastIndexOf(".") >= 0) {
            applicationClassName = applicationClassName.substring(applicationClassName.lastIndexOf(".") + 1);
        }
        applicationName = applicationClassName;
        SysUtils.setApplicationName(applicationName);
        SysUtils.setApplicationWindow(this);

        Dimension screenSize = getToolkit().getScreenSize();
        setLocation(100, 100);
        setSize(screenSize.width - 200, screenSize.height - 200);
        setExtendedState(Frame.MAXIMIZED_BOTH);

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                if (canClose()) {
                    setVisible(false);
                    cleanup();
                    System.exit(0);
                }
            }
        });

        dndManager = new DnDManager(this);
        mouseTracker = new MouseTracker(this);
        commandManager = new CommandManager();

        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        getContentPane().setLayout(new BorderLayout());

        setWindowTitle(null);
    }

    private void postInit() {
        SysUtils.setApplicationVersion(version);
        SysUtils.setApplicationName(applicationName);
        updateCommands();
        setVisible(true);
    }

    private void registerMenuItem(JMenuItem menu) {
        menu.addActionListener(this);
        getCommandManager().add(menu);
        if (menu instanceof JMenu) {
            Component[] items = ((JMenu) menu).getMenuComponents();
            for (Component element : items) {
                if (element instanceof JMenuItem) {
                    registerMenuItem((JMenuItem) element);
                }
            }
        }
    }

    private String entry2caption(String entry) {
        String caption = StringUtils.replaceAll(entry, "^", "");
        return caption;
    }

    // private String entry2command(String entry) {
    // String command = "";
    //
    // Pattern p;
    // Matcher m;
    // StringBuffer b;
    //    
    // p = Pattern.compile("\\W");
    // m = p.matcher(entry);
    // b = new StringBuffer();
    // while (m.find()) {
    // m.appendReplacement(b, " ");
    // }
    // m.appendTail(b);
    // command = b.toString();
    //    
    // p = Pattern.compile("\\s([a-z])");
    // m = p.matcher(command);
    // b = new StringBuffer();
    // while (m.find()) {
    // m.appendReplacement(b, m.group(1).toUpperCase());
    // }
    // m.appendTail(b);
    // command = b.toString();
    //
    // p = Pattern.compile("\\s");
    // m = p.matcher(command);
    // b = new StringBuffer();
    // while (m.find()) {
    // m.appendReplacement(b, "");
    // }
    // m.appendTail(b);
    // command = b.toString();
    //
    // return command;
    // }

    private int entry2mnemonic(String entry) {
        int key = 0;

        Pattern p;
        Matcher m;
        p = Pattern.compile("\\^(\\w)");
        m = p.matcher(entry);
        if (m.find()) {
            key = m.group(1).charAt(0);
        }

        return key;
    }

    private void setMenuItemMnemonic(JMenuItem m, String entry) {
        int key = entry2mnemonic(entry);
        if (key != 0) {
            m.setMnemonic(key);
        }
    }

    private JMenu createMenu(String prefix, String name, String[] entries) {
        JMenu m = new JMenu(entry2caption(name));
        setMenuItemMnemonic(m, name);

        int subLevel = 0;
        String subMenuName = "";
        ArrayList<String> subMenuEntries = new ArrayList<String>();
        for (int i = 0; i < entries.length; i++) {
            String entry = entries[i];
            if ((entry == null) || entry.equals("") || entry.equals("-") || entry.equals("|")) {
                m.addSeparator();
            }
            else {
                if (entry.equals("(")) {
                    if (subLevel == 0) {
                        subMenuEntries = new ArrayList<String>();
                        if (i > 0) {
                            subMenuName = entries[i - 1];
                            Component prev = m.getMenuComponent(i - 1);
                            m.remove(prev);
                            commandManager.remove(prev);
                        }
                    }
                    else {
                        subMenuEntries.add(entry);
                    }
                    subLevel++;
                }
                else if (entry.equals(")")) {
                    if (subLevel > 0) {
                        subLevel--;
                    }
                    if ((subLevel == 0) && (subMenuEntries.size() > 0)) {
                        String[] subNames = new String[subMenuEntries.size()];
                        for (int j = 0; j < subMenuEntries.size(); j++) {
                            subNames[j] = subMenuEntries.get(j);
                        }
                        JMenu subMenu = createMenu(prefix, subMenuName, subNames);
                        m.add(subMenu);
                    }
                    else {
                        subMenuEntries.add(entry);
                    }
                }
                else {
                    if (subLevel > 0) {
                        subMenuEntries.add(entry);
                    }
                    else {
                        JMenuItem mi = new JMenuItem(entry2caption(entry));
                        setMenuItemMnemonic(mi, entry);
                        mi.setActionCommand(StringUtils.firstToLowerCase(prefix) +
                                            StringUtils.firstToUpperCase(StringUtils.toCamelBack(entry2caption(entry))));
                        getCommandManager().add(mi);
                        m.add(mi);
                    }
                }
            }
        }
        return m;
    }

    // --- Protected methods -----------------------------------

    // --- Call these from overwritten init() methods

    protected final void setApplicationName(String name) {
        applicationName = name;
        setWindowTitle(null);
    }

    protected final void setVersion(String v) {
        version = v;
    }

    protected final void addMainMenuItem(JMenu menu) {
        registerMenuItem(menu);
        menuBar.add(menu);
    }

    protected final void addMainMenuItem(String mainCaption, String[] names) {
        JMenu menu = createMenu(StringUtils.toCamelBack(mainCaption), mainCaption, names);
        registerMenuItem(menu);
        menuBar.add(menu);
    }

    // --- Must be implemented ---

    protected abstract void init(String[] args);

    protected abstract void cleanup();

    protected abstract boolean canClose();

    protected abstract void updateCommands();

    // === Interface implementations ===========================

    // --- Implementation of ActionListener --------------------

    public final void actionPerformed(ActionEvent e) {
        final ActionEvent event = e;

        Thread t = new Thread() {

            @Override
            public void run() {
                getCommandManager().disableAll();
                String command = event.getActionCommand();
                try {
                    String methodName = "on" + StringUtils.firstToUpperCase(command);
                    Method m = applicationClass.getDeclaredMethod(methodName, new Class[] {
                        ActionEvent.class
                    });
                    AccessibleObject.setAccessible(new AccessibleObject[] {
                        m
                    }, true);
                    m.invoke(TApplication.this, new Object[] {
                        event
                    });
                }
                catch (InvocationTargetException ex) {
                    SysUtils.reportException(ex.getTargetException());
                }
                catch (Exception ex) {
                    SysUtils.reportException(ex);
                }
                updateCommands();
                SwingUtils.invokeLater(new Runnable() {

                    public void run() {
                        invalidate();
                        repaint();
                    }
                });
            }

        };
        ThreadPool.getDefaultThreadPool().addTask(t);
    }

}

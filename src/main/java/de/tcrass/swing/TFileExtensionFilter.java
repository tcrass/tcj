/*
 * Created on 30.03.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.swing;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import de.tcrass.io.FileExtensionFilter;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class TFileExtensionFilter extends FileFilter {

    private FileExtensionFilter filter;

    public TFileExtensionFilter(String[] extensions) {
        filter = new FileExtensionFilter(extensions, true);
    }

    public static void addFileFilters(JFileChooser d, String[] extensions) {
        addFileFilters(d, extensions, 0);
    }

    public static void addFileFilters(JFileChooser d, String[] extensions, int defaultIndex) {
        TFileExtensionFilter defaultFilter = null;
        for (int i = 0; i < extensions.length; i++) {
            TFileExtensionFilter ff = new TFileExtensionFilter(new String[] {
                extensions[i]
            });
            d.addChoosableFileFilter(ff);
            if (i == defaultIndex) {
                defaultFilter = ff;
            }
        }
        d.setFileFilter(defaultFilter);
    }

    public String[] getExtensions() {
        return filter.getExtensions();
    }

    @Override
    public boolean accept(File f) {
        boolean acc = false;
        if (f.isDirectory()) {
            acc = true;
        }
        else {
            acc = filter.accept(f.getParentFile(), f.getName());
        }
        return acc;
    }

    @Override
    public String getDescription() {
        return filter.getFilterString();
    }

}

package de.tcrass.graph;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import de.tcrass.io.ParseException;
import de.tcrass.io.TTokenizer;

/**
 * A stream parser for graph descriptions provided in the Graphviz dot
 * language. This class is heavily made use of by the methods provided
 * by the {@link Graphviz} i/o class.
 * <p>
 * It's called a <i>simple</i> dot parser since some few aspects of the
 * dot language are neither covered by this class, nor they by the
 * {@link Graph} and {@link Edge} classes (see there for futher
 * details).
 */
public class SimpleDotParser {

    private static String[] GRAPH_TOKENS               = {
        "graph", "digraph"
                                                       };
    // private static char[] PROPERTY_TERMINATION_TOKENS = {',', ']'};
    private static char[]   EDGE_TERMINATION_TOKENS    = {
        '-', '>'
                                                       };
    private static String[] ATTRIBUTE_STATEMENT_TOKENS = {
        "graph", "node", "edge"
                                                       };
    private static String[] SUBGRAPH_STATEMENT_TOKENS  = {
        "subgraph", "{"
                                                       };
    private static char[]   PORT_STATEMENT_TOKENS      = {
        ':', '@'
                                                       };

    private class LineReconstructionReader extends PushbackReader {

        private LineReconstructionReader(Reader in) {
            super(new PushbackReader(in));
        }

        private boolean nextWasLf() throws IOException {
            boolean lf = false;
            int c = in.read();
            if (c != -1) {
                switch (c) {
                    case 10:
                        lf = true;
                        break;
                    case 13:
                        lf = true;
                        int d = in.read();
                        if ((d != -1) && (d != 10)) {
                            ((PushbackReader) in).unread(d);
                        }
                        break;
                    default:
                        ((PushbackReader) in).unread(c);
                }
            }
            return lf;
        }

        @Override
        public int read() throws IOException {
            int c = in.read();
            if ((c != -1) && (((char) c) == '\\')) {
                if (nextWasLf()) {
                    c = in.read();
                }
            }
            return c;
        }

        // public int read(char[] cbuff, int off, int len) throws
        // IOException {
        // System.out.println("read(...)");
        // int i = off;
        // int n = 0;
        // int c = in.read();
        // while ((c != -1) && (n < len)) {
        // char chr = (char)c;
        // if (chr == '\\') {
        // int d = in.read();
        // if ((d != -1) && (((char)d != '\n'))) {
        // ((PushbackReader)in).unread(d);
        // }
        // else {
        // c = d;
        // }
        // }
        // if (c != -i) {
        // cbuff[i] = chr;
        // i++;
        // n++;
        // }
        // }
        // return n;
        // }
        //    
    }

    private TTokenizer t;
    private List<Edge> currentEdgeSet;
    private String     currentPort, tailPort, headPort;

    // --- Constructor

    /**
     * Creates a new SimpleDotParser which will create a
     * {@link de.tcrass.graph.Graph} object representing the graph
     * described by the Graphviz dot language description read from the
     * given {@link java.io.Reader}.
     * 
     * @param reader
     *            the Reader from which to read the graph descriptions
     */
    public SimpleDotParser(Reader reader) throws IOException {
        t = new TTokenizer(new LineReconstructionReader(reader));
        t.dontParseNumbers();
        t.quoteChar('"');
        t.slashSlashComments(true);
        t.slashStarComments(true);
        t.wordChars('_', '_');
        t.wordChars('.', '.');

        t.nextToken();

        currentEdgeSet = new ArrayList<Edge>();
    }

    // --- Private methods

    private void parseGraph(Graph g) throws ParseException, IOException {
        boolean ok = true;

        if (t.getWord().equals("strict")) {
            t.nextToken();
        }

        if (t.wordIn(GRAPH_TOKENS)) {
            if (t.getWord().equals("graph")) {
                g.setDirected(false);
                t.nextToken();
            }
            else if (t.getWord().equals("digraph")) {
                g.setDirected(true);
                t.nextToken();
            }
        }
        else {
            ok = false;
        }

        if (ok && (t.wasWord() || t.wasQuote())) {
            g.setId(t.getString());
            t.nextToken();
        }

        if (ok) {
            parseStmtList(g);
        }

        if (!ok || !t.wasEof()) {
            throw new ParseException(t);
        }
    }

    private void parseStmtList(Graph g) throws ParseException, IOException {
        boolean ok = true;

        if (t.getCharStr().equals("{")) {
            t.nextToken();
        }
        else {
            ok = false;
        }

        List<Node> currentNodeSet = new ArrayList<Node>();
        while (ok && !t.getCharStr().equals("}")) {

            // --- parse Stmt
            if (t.wordIn(ATTRIBUTE_STATEMENT_TOKENS)) {
                // --- parse AttrStmt
                parseAttrStmt(g);
            }
            else {
                currentNodeSet = parseNodeSet(g, false);
                if (t.getCharStr().equals("-")) {
                    currentEdgeSet.clear();
                    parseEdgeStmt(g, currentNodeSet);
                }
            }

            if (t.getCharStr().equals(";")) {
                t.nextToken();
            }
        }

        t.nextToken();

        if (!ok) {
            throw new ParseException(t);
        }

    }

    private List<Node> parseNodeSet(Graph g, boolean isEdgeStmt) throws ParseException,
    IOException {
        List<Node> nodes;

        if (t.stringIn(SUBGRAPH_STATEMENT_TOKENS)) {
            Graph sg = parseSubgraph(g);
            nodes = sg.getNodes();
        }
        else {
            Node node = parseNodeStmt(g, isEdgeStmt);
            nodes = new ArrayList<Node>();
            nodes.add(node);
        }

        return nodes;
    }

    private Node parseNodeStmt(Graph g, boolean isEdgeStmt) throws ParseException, IOException {
        boolean ok = true;

        currentPort = null;
        Node n = null;
        if (t.wasWord() || t.wasQuote()) {
            String id = t.getString();
            t.nextToken();

            if (t.getCharStr().equals("=")) {
                t.nextToken();
                if (t.wasWord() || t.wasQuote()) {
                    String val = t.getString();
                    t.nextToken();
                    g.setGVAttribute(id, val);
                }
                else {
                    ok = false;
                }
            }

            else {
                Properties p;
                Node m = g.getTopLevelGraph().getNode(id);
                if (m != null) {
                    Graph supg = m.getOwner();
                    if (supg.containsSubgraph(g.getId())) {
                        g.moveNode(m);
                    }
                    n = m;
                    p = n.getGVAttributes();
                }
                else {
                    p = new Properties();
                    n = g.getTopLevelGraph().getNode(id);
                    if (n == null) {
                        n = g.createNode(id, p);
                    }
                }

                if (t.charIn(PORT_STATEMENT_TOKENS)) {
                    parsePortStmt(n);
                }

                if (!isEdgeStmt && t.getCharStr().equals("[")) {
                    parseAttrList(p);
                }
            }
        }
        else {
            ok = false;
        }

        if (!ok) {
            throw new ParseException(t);
        }

        return n;
    }

    private void parsePortStmt(Node n) throws ParseException, IOException {
        boolean ok = true;

        while (ok && t.charIn(PORT_STATEMENT_TOKENS)) {
            if (t.getCharStr().equals(":")) {
                t.nextToken();
                if (t.wasWord() || t.wasQuote()) {
                    currentPort = t.getString();
                    t.nextToken();
                }
                else {
                    ok = false;
                }
            }
            else if (t.getCharStr().equals("@")) {
                t.nextToken();
                // ==================================
                t.nextToken();
            }
            else {
                ok = false;
            }
        }

        if (!ok) {
            throw new ParseException(t);
        }
    }

    private Graph parseSubgraph(Graph g) throws ParseException, IOException {
        boolean ok = true;

        Graph sg = null;
        if (t.getWord().equals("subgraph")) {
            t.nextToken();
            if (t.wasWord() || t.wasQuote()) {
                String id = t.getString();
                t.nextToken();
                sg = g.getTopLevelGraph().getSubgraph(id);
                if (sg == null) {
                    sg = g.createSubgraph(id);
                }
            }
            else {
                if (t.getString().equals("{")) {
                    sg = g.createSubgraph(null);
                }
                else {
                    ok = false;
                }
            }
        }
        else {
            sg = g.createSubgraph(null);
        }

        if (ok && t.getCharStr().equals("{")) {

            parseStmtList(sg);

        }

        if (!ok) {
            throw new ParseException(t);
        }

        return sg;
    }

    private void parseAttrStmt(Graph g) throws ParseException, IOException {
        boolean ok = true;

        if (t.getWord().equals("graph")) {
            t.nextToken();
            parseAttrList(g.getGVAttributes());
        }
        else if (t.getWord().equals("node")) {
            t.nextToken();
            parseAttrList(g.getDefaultGVNodeAttributes());
        }
        else if (t.getWord().equals("edge")) {
            t.nextToken();
            parseAttrList(g.getDefaultGVEdgeAttributes());
        }
        else {
            ok = false;
        }

        if (!ok) {
            throw new ParseException(t);
        }
    }

    private void parseAttrList(Properties p) throws ParseException, IOException {
        boolean ok = true;

        if (t.getCharStr().equals("[")) {
            t.nextToken();
        }
        else {
            ok = false;
        }

        while (ok && !t.getCharStr().equals("]")) {
            // --- parse AList
            parseKeyVal(p);
            if (t.getCharStr().equals(",")) {
                t.nextToken();
            }
        }

        t.nextToken();

        if (!ok) {
            throw new ParseException(t);
        }
    }

    private void parseKeyVal(Properties p) throws ParseException, IOException {
        boolean ok = true;
        String key = null;

        if (t.wasWord()) {
            key = t.getWord();
            t.nextToken();
        }
        else {
            ok = false;
        }

        if (ok && t.getCharStr().equals("=")) {
            t.nextToken();
        }
        else {
            ok = false;
        }

        if (ok) {
            String val = t.getString();
            p.setProperty(key, val);
            t.nextToken();
        }

        if (!ok) {
            throw new ParseException(t);
        }
    }

    private void parseEdgeStmt(Graph g, List<Node> tailSet) throws ParseException, IOException {
        boolean ok = true;

        if (t.getCharStr().equals("-")) {
            t.nextToken();
        }
        else {
            ok = false;
        }

        if (ok && t.charIn(EDGE_TERMINATION_TOKENS)) {
            if (g.isDirected() && !t.getCharStr().equals(">")) {
                ok = false;
            }
            if (!g.isDirected() && !t.getCharStr().equals("-")) {
                ok = false;
            }
            t.nextToken();
        }
        else {
            ok = false;
        }

        if (ok) {
            tailPort = currentPort;
            List<Node> headSet = parseNodeSet(g, true);
            headPort = currentPort;

            for (Node tail : tailSet) {
                for (Node head : headSet) {
                    Edge e = g.createEdge(tail, head);
                    e.setTailPort(tailPort);
                    e.setHeadPort(headPort);
                    currentEdgeSet.add(e);
                }
            }

            // Properties p =
            // (Properties)g.getDefaultEdgeProperties().clone();
            Properties p = new Properties();
            if (t.getCharStr().equals("[")) {
                parseAttrList(p);
            }

            for (Edge e : currentEdgeSet) {
                Properties q = (Properties) p.clone();
                for (Enumeration<Object> j = e.getGVAttributes().keys(); j.hasMoreElements();) {
                    String key = (String) j.nextElement();
                    q.setProperty(key, e.getGVAttribute(key));
                }
                e.setGVAttributes(q);
            }

            if (t.getCharStr().equals("-")) {
                parseEdgeStmt(g, headSet);
            }
        }

        if (!ok) {
            throw new ParseException(t);
        }
    }

    // --- Public methods

    /**
     * Returns a {@link de.tcrass.graph.Graph} object compliant with the
     * Graphviz dot language description read from the Reader passed
     * during initialization.
     * 
     * @return the newly created graph
     */
    public Graph parse() throws IOException, ParseException {
        Graph g = new Graph();
        parseGraph(g);
        return g;
    }

}

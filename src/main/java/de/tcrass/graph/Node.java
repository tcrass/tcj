package de.tcrass.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import de.tcrass.util.DataStore;

/**
 * A class representing a {@link Graph}'s nodes.
 * <p>
 * Since Node objects always belong to a graph, they cannot be
 * instantiated directly, but only through the corresponding factory
 * methods provided by the graph class. A Node must have an id (either
 * an {@link Integer} or a {@link String}) unique within the Node's
 * owner graph's top-level graph and (recursively) all its subgraphs.
 * <p>
 * As subclass of {@link GraphElement} and {@link GraphObject}, each
 * Node features an a {@link DataStore} which holds, among any other
 * data added by the user, the Node's coloring, weight and Graphviz
 * attributes.
 */
public class Node extends GraphElement implements Cloneable {

    private ArrayList<Edge> inEdges;
    private ArrayList<Edge> outEdges;

    // --- constructors

    /**
     * Creates a new Node instance with the given id and the given
     * Properties as Graphviz attributes (if not null).
     * 
     * @param id
     *            the object to be used as the new Node's id (should be
     *            either an {@link Integer} or a {@link String})
     * @param p
     *            the Graphviz attributes to be assigned to the new Node
     *            (may be null)
     */
    Node(Object id, Properties p) {
        super(id, p);
        inEdges = new ArrayList<Edge>();
        outEdges = new ArrayList<Edge>();
    }

    // --- Private methods

    private Node createClone(Graph target, Object newId) {
        return target.cloneNode(this, newId);
    }

    private Node createConnectedClone(Graph target, Object newId) {
        Node c = createClone(target, newId);
        for (Edge e : getInEdges()) {
            Node tail = target.getTopLevelGraph().getNode(e.getTail().getId());
            if (tail != null) {
                e.createClone(target, tail, c);
            }
        }
        for (Edge e : getOutEdges()) {
            Node head = target.getTopLevelGraph().getNode(e.getHead().getId());
            if (head != null) {
                e.createClone(target, c, head);
            }
        }
        return c;
    }

    // --- Package-scope methods

    /**
     * Registers the given edge as another edge pointing towards this
     * Node.
     * 
     * @param edge
     *            the Node's new incoming edge
     */
    void addInEdge(Edge edge) {
        inEdges.add(edge);
    }

    /**
     * Removes the given edge from this Node's list of incoming edges.
     * 
     * @param edge
     *            the edge to unregister as incoming for this Node
     */
    void removeInEdge(Edge edge) {
        inEdges.remove(edge);
    }

    /**
     * Registers the given edge as another edge pointing away from this
     * Node.
     * 
     * @param edge
     *            the Node's new outgoing edge
     */
    void addOutEdge(Edge edge) {
        outEdges.add(edge);
    }

    /**
     * Removes the given edge from this Node's list of outgoing edges.
     * 
     * @param edge
     *            the edge to unregister as outgoing for this Node
     */
    void removeOutEdge(Edge edge) {
        outEdges.remove(edge);
    }

    // --- public methods

    @Override
    public String getDefaultGVAttributeKey() {
        return Graph.DATA_KEY_GRAPHVIZ_NODE_ATTRIBUTES;
    }

    /**
     * Unregisters from this Node all edges pointing towards or away
     * from it and removes them from the Node's onwer graph.
     */
    public void disconnect() {
        for (Edge e : new ArrayList<Edge>(inEdges)) {
            e.getOwner().removeEdge(e);
        }
        for (Edge e : new ArrayList<Edge>(outEdges)) {
            e.getOwner().removeEdge(e);
        }
    }

    /**
     * Returns all edges pointing towards this Node.
     * 
     * @return all this Node's incoming edges
     */
    public List<Edge> getInEdges() {
        return inEdges;
    }

    /**
     * Returns all edges pointing away from this Node.
     * 
     * @return all this Node's outgoing edges
     */
    public List<Edge> getOutEdges() {
        return outEdges;
    }

    /**
     * Returns all edges pointing towards or away from (i.e. being
     * incident to) this Node.
     * 
     * @return all edges incident to this node
     */
    public List<Edge> getEdges() {
        List<Edge> v = new ArrayList<Edge>(getInEdges());
        v.addAll(getOutEdges());
        return v;
    }

    /**
     * Returns all Nodes connected to this Node by at least one incoming
     * edge.
     * 
     * @return the Node's predecessors
     */
    public Set<Node> getPredecessors() {
        Set<Node> v = new HashSet<Node>();
        for (Edge e : inEdges) {
            v.add(e.getTail());
        }
        return v;
    }

    /**
     * Returns all Nodes connected to this Node by at least one outgoing
     * edge.
     * 
     * @return the Node's successors
     */
    public Set<Node> getSuccessors() {
        Set<Node> v = new HashSet<Node>();
        for (Edge e : outEdges) {
            v.add(e.getHead());
        }
        return v;
    }

    /**
     * Returns all Nodes adjacent to this Node.
     * 
     * @return the Node's neighbours
     */
    public Set<Node> getNeighbours() {
        Set<Node> v = getPredecessors();
        v.addAll(getSuccessors());
        return v;
    }

    /**
     * Returns all (if any) edges pointing from the given predecessor
     * Node to this Node.
     * 
     * @param pred
     *            some predecessor Node
     * @return the edges pointing from the predecessor to this Node
     */
    public List<Edge> getEdgesFrom(Node pred) {
        List<Edge> edges = new ArrayList<Edge>();
        for (Edge e : inEdges) {
            if (e.getTail().equals(pred)) {
                edges.add(e);
            }
        }
        return edges;
    }

    /**
     * Returns all (if any) edges pointing from this Node to the given
     * successor Node.
     * 
     * @param succ
     *            somme successor Node
     * @return the edges pointing from this Node to the successor
     */
    public List<Edge> getEdgesTo(Node succ) {
        List<Edge> edges = new ArrayList<Edge>();
        for (Edge e : outEdges) {
            if (e.getHead().equals(succ)) {
                edges.add(e);
            }
        }
        return edges;
    }

    /**
     * Returns all (if any) edges connecting this Node and the given
     * adjacent Node.
     * 
     * @param n
     *            some adjacent Node
     * @return the edges pointing from this Node to its neighbour or
     *         vice versa
     */
    public List<Edge> getEdgesWith(Node n) {
        List<Edge> edges = getEdgesFrom(n);
        edges.addAll(getEdgesTo(n));
        return edges;
    }

    // --- Implementation of Cloneable

    /**
     * Returns a clone of this Node; do not call directly, but use any
     * of the provided createClone methods!
     */
    @Override
    public Object clone() {
        Node c = (Node) super.clone();
        c.inEdges = new ArrayList<Edge>();
        c.outEdges = new ArrayList<Edge>();
        return c;
    }

    /**
     * Creates a copy of this Node, assigns it an identical id and adds
     * it to the given target graph, which must not be the same graph
     * this Node belongs to, nor any of its top-level graph's
     * (recursive) subgraphs.
     * 
     * @param target
     *            the graph the cloned node to add to
     * @return the new clone of this Node
     */
    public Node createClone(Graph target) {
        return createClone(target, id);
    }

    /**
     * Creates a copy of this Node, assigns it the given id and adds it
     * to the given target graph. If the given id identical to the
     * original Node's id, the target must not be the same graph this
     * Node belongs to, nor any of its top-level graph's (recursive)
     * subgraphs.
     * 
     * @param target
     *            the graph the cloned node to add to
     * @param id
     *            the id to be assigned to the new clone of this Node
     * @return the new clone of this Node
     */
    public Node createClone(Graph target, String id) {
        return createClone(target, (Object) id);
    }

    /**
     * Creates a copy of this Node, assigns it the given id and adds it
     * to the given target graph. If the given id is identical to the
     * original Node's id, the target must not be the same graph this
     * Node belongs to, nor any of its top-level graph's (recursive)
     * subgraphs.
     * 
     * @param target
     *            the graph the cloned node to add to
     * @param id
     *            the id to be assigned to the new clone of this Node
     * @return the new clone of this Node
     */
    public Node createClone(Graph target, int id) {
        return createClone(target, (Object) id);
    }

    /**
     * Creates a copy of this Node, assigns it the given id and adds it
     * to the same graph. The given id must not be identical to the
     * original Node's id, nor may a node with the same id already
     * exists in the graph's top-level graph or any of its (recursive)
     * subgraphs.
     * 
     * @param id
     *            the id to be assigned to the new clone of this Node
     * @return the new clone of this Node
     */
    public Node createClone(int id) {
        return createClone(owner, id);
    }

    /**
     * Creates a copy of this Node, assigns it the given id and adds it
     * to the same graph this Node is owned by. The given id must not be
     * identical to the original Node's id, nor may a node with the same
     * id already exists in the graph's top-level graph or any of its
     * (recursive) subgraphs.
     * 
     * @param id
     *            the id to be assigned to the new clone of this Node
     * @return the new clone of this Node
     */
    public Node createClone(String id) {
        return createClone(owner, id);
    }

    /**
     * Creates in the target Graph a clone of this node, assigns it the
     * same id and clones all edges incident to the original node so
     * that the clone is finally connected to the nodes bearing the same
     * ids as the original node's neighbours, with edge directionality
     * preserved. (If for an edge no matching tail or head node is found
     * in the target graph, the edge gets just skipped.)
     * <p>
     * No node featuring the same id as the original node must already
     * be owned by the target graph's top level graph or (recursively)
     * any of its subgraphs.
     * 
     * @param target
     *            the graph the cloned node to add to
     * @return the cloned node
     */
    public Node createConnectedClone(Graph target) {
        return createConnectedClone(target, id);
    }

    /**
     * Creates in the target Graph a clone of this node, assigns it the
     * given new id and clones all edges incident to the original node
     * so that the clone is finally connected to the nodes bearing the
     * same ids as the original node's neighbours, with edge
     * directionality preserved. (If for an edge no matching tail or
     * head node is found in the target graph, the edge gets just
     * skipped.)
     * <p>
     * No node featuring the given new id must already be owned by the
     * target graph's top level graph or (recursively) any of its
     * subgraphs.
     * 
     * @param target
     *            the graph the cloned node to add to
     * @param newId
     *            the id to be assigned to the clone
     * @return the cloned node
     */
    public Node createConnectedClone(Graph target, int newId) {
        return createConnectedClone(target, (Object) newId);
    }

    /**
     * Creates in the target Graph a clone of this node, assigns it the
     * given new id and clones all edges incident to the original node
     * so that the clone is finally connected to the nodes bearing the
     * same ids as the original node's neighbours, with edge
     * directionality preserved. (If for an edge no matching tail or
     * head node is found in the target graph, the edge gets just
     * skipped.)
     * <p>
     * No node featuring the given new id must already be owned by the
     * target graph's top level graph or (recursively) any of its
     * subgraphs.
     * 
     * @param target
     *            the graph the cloned node to add to
     * @param newId
     *            the id to be assigned to the clone
     * @return the cloned node
     */
    public Node createConnectedClone(Graph target, String newId) {
        return createConnectedClone(target, (Object) newId);
    }

    /**
     * Creates a clone of this node, assigns it the given new id, adds
     * it to the same graph and clones all edges incident to the
     * original node so that the clone is finally connected to the nodes
     * bearing the same ids as the original node's neighbours, with edge
     * directionality preserved. (If for an edge no matching tail or
     * head node is found in the target graph, the edge gets just
     * skipped.)
     * <p>
     * No node featuring the given new id must already be owned by the
     * original node's owner graph's top level graph or (recursively)
     * any of its subgraphs.
     * 
     * @param newId
     *            the id to be assigned to the clone
     * @return the cloned node
     */
    public Node createConnectedClone(int newId) {
        return createConnectedClone(owner, newId);
    }

    /**
     * Creates a clone of this node, assigns it the given new id, adds
     * it to the same graph and clones all edges incident to the
     * original node so that the clone is finally connected to the nodes
     * bearing the same ids as the original node's neighbours, with edge
     * directionality preserved. (If for an edge no matching tail or
     * head node is found in the target graph, the edge gets just
     * skipped.)
     * <p>
     * No node featuring the given new id must already be owned by the
     * original node's owner graph's top level graph or (recursively)
     * any of its subgraphs.
     * 
     * @param newId
     *            the id to be assigned to the clone
     * @return the cloned node
     */
    public Node createConnectedClone(String newId) {
        return createConnectedClone(owner, newId);
    }

}

package de.tcrass.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.tcrass.graph.condition.GraphObjectCondition;
import de.tcrass.util.DataStore;
import de.tcrass.xml.XMLUtils;

/**
 * A class representing a Graph, i.&nbsp;e.&nbsp;basically a tupel of sets G =
 * (N, E) with N being a set of {@link Node}s and E a set of {@link Edge}s
 * connecting these nodes.
 * <p>
 * Generally, graphs represented by this class feature basically the same
 * expressiveness as provided by the Graphviz dot language. In particular
 * <ul>
 * <li>A graph can be either directed or undirected as a whole; directed and
 * undirected edge cannot be mixed in one and the same graph
 * <li>Even in undirected graphs, edges do have an explicit tail and head node
 * <li>graphs can have subgraphs
 * <li>graphs and subgraphs may, but need not have explicit ids
 * <li>nodes must have ids
 * <li>edges don't have ids
 * </ul>
 * <p>
 * Subgraphs, nodes and edges cannot exist on their own; rather, they are always
 * created from and owned by a graph. However, while there is an intuitive
 * interpretation of a subgraph or a node belonging to a certain graph, there is
 * no similarly clear concept for edges since an edge may connect nodes owned by
 * different graphs (sensu different subgraphs of a common supergraph). To this
 * end, an edge by definition just belongs to the graph it has been created
 * from, no matter which nodes it connects. (This is actually also compatible
 * with Graphviz dot semantics.)
 * <p>
 * Depending on the state of the checkSanity flag, graph objects will perform
 * additional checks e.g. when creating edges in order to ensure that the
 * connected nodes do actually belong to the respective graph (or at least to a
 * subgraph of its top-level graph). If you feel you'd like to set this flag to
 * false for performance reasons, you are wellcome to do so -- to your own risk.
 * <p>
 * Since the graph, node and edge classes all inherit from {@link GraphObject},
 * they all can be loaded with almost arbitrary data through their individual
 * {@link DataStore}s.
 * <p>
 * Graph objects can be created from and serialized to GraphML documents; in
 * order to capture the richer semantics of data stored in a DataStore, as
 * compared to native GraphML data types, an extended GraphML dialect may be
 * used (which, unfortunately, is still waiting for its corresponding XML schema
 * being defined). It has to be noted that in case of plain GraphML
 * serialization all graphical information (in terms of Graphviz attributes) is
 * lost! Also, it is not advisable to use String ids and string-identical
 * numeric ids in the same graph if plain GraphML serialization is intended
 * since those nodes would become indistinguishable -- both the Java String "42"
 * and the int value 42 would refer to the id "42" in plain GraphML.
 * <p>
 * Another difference between the Graph class's and the GraphML data model is
 * that in the latter subgraphs are harboured by nodes, which may carry attached
 * data. When creating a Graph object from such a GraphML file, the data
 * directly attached to any subgraph's node will be lost. (The actual subgraph's
 * data, however, will be restored.)
 * <p>
 * Owed to their semantic origins, graph objects do well interact with the input
 * and output methods provided by the {@link Graphviz} class. In particular,
 * graph objects can be created from .dot files, preserving most of their
 * meaning, and can either be output directly as .dot file or layouted to the
 * whole menagerie of formats supported by the Graphviz tools (dot, neato,
 * circo, twopi...)
 * <p>
 * There are, however, some issues to keep in mind when moving hither and
 * thither between Graph objects and .dot files. For instance, just like
 * GraphML, the dot language only knows string identifiers for graphs and nodes;
 * hence you should avoid using String ids and string-identical numeric ids
 * within the same graph.
 * <p>
 * Further limitations arises from the fact that dot is actually a graph drawing
 * language (and not a graph representation language). To this end, data stored
 * in a GraphObject's DataStore cannot generally be represented in dot; only
 * those data accessible through methods bearing the term
 * <code>GVAttrivbute</code> within their names will end up in a .dot file,
 * namely in the form of the object's Graphviz attributes. However, while it is
 * possible in dot to alter node and edge default graphics attributes multiple
 * times within the same graph or subgraph, the Graph class features only one
 * set of each node and edge default attribute for all its corresponding
 * elements. (Subgraphs, however, may of course have their own default
 * attributes for their nodes and edges.)
 * <p>
 * See also the {@link Edge} class's description for some further details
 * regarding dot semantics.
 * <p>
 * Finally, see the description of the {@link de.tcrass.graph.Edge} class for
 * some more remarks regarding Graphviz semantics.
 */
public class Graph extends GraphObject {

    /**
     * A set containing the data keys for (default) Graphviz attributes. Can be
     * used e. g. in {@link #toGraphMLDocument} for preventing graphical
     * information to be written to a GraphML file.
     */
    public static final Set<String> GRAPHVIZ_ATTRIBUTES_BLACKLIST = new HashSet<String>();

    /**
     * Name for the DataStore} {@link Properties} element holding the graph's
     * default Graphviz node attributes
     */
    public static final String DATA_KEY_GRAPHVIZ_NODE_ATTRIBUTES = "__GRAPHVIZ_NODE_ATTRIBUTES__";

    /**
     * Name for the DataStore} {@link Properties} element holding the graph's
     * default Graphviz edge attributes
     */
    public static final String DATA_KEY_GRAPHVIZ_EDGE_ATTRIBUTES = "__GRAPHVIZ_EDGE_ATTRIBUTES__";

    /**
     * The default XML namespace prefix for extended GraphML attributes
     */
    public static final String DEFAULT_NS_PREFIX = "gr";

    /**
     * The URL of the XML schema describing GraphML extension
     */
    public static final String SCHEMA_NAME = "http://www.sybig.de/xml/schema/graph/1.0/graph.xsd";

    /**
     * Constant telling to use the edges pointing towards a node when removing
     * it for mending the resulting "hole"
     */
    public static final int KEEP_IN_EDGES = 1;
    /**
     * Constant telling to use the edges pointing away from a node when removing
     * it for mending the resulting "hole"
     */
    public static final int KEEP_OUT_EDGES = 2;
    /**
     * Constant telling to use all edges incident to a node when removing it for
     * mending the resulting "hole"
     */
    public static final int KEEP_ALL_EDGES = KEEP_IN_EDGES | KEEP_OUT_EDGES;

    private static boolean checkSanity = true;
    private static String nsPrefix = DEFAULT_NS_PREFIX;

    private Hashtable<Object, Node> nodes;
    private ArrayList<Edge> edges;
    private ArrayList<Graph> subgraphs;

    /**
     * Whether or not this is a directed graph
     */
    protected boolean directed;

    // --- Static methods

    /**
     * If set to true, all members fo this class will perform some additional
     * checks when attempting some graph manipulations. On by default.
     * 
     * @param sanity
     *            whether or not to perform additional checks
     */
    public static void setSanityChecking(boolean sanity) {
        checkSanity = sanity;
    }

    /**
     * Tells whether or not additional checks are enabled.
     * 
     * @return true, if additional checks are enabled
     */
    public static boolean isSanityChecking() {
        return checkSanity;
    }

    /**
     * Gets the XML namespace prefix used for extended GraphML.
     * 
     * @return the extended GraphML namespace prefix
     */
    public static String getNSPrefix() {
        return nsPrefix;
    }

    /**
     * Sets the XML namespace attribute to be used for extended GraphML.
     * 
     * @param prefix
     *            the new extended GraphML namespace prefix
     */
    public static void setNSPrefix(String prefix) {
        nsPrefix = prefix;
    }

    // --- Constructors ---

    static {
        GRAPHVIZ_ATTRIBUTES_BLACKLIST.add(DATA_KEY_GRAPHVIZ_ATTRIBUTES);
        GRAPHVIZ_ATTRIBUTES_BLACKLIST.add(DATA_KEY_GRAPHVIZ_NODE_ATTRIBUTES);
        GRAPHVIZ_ATTRIBUTES_BLACKLIST.add(DATA_KEY_GRAPHVIZ_EDGE_ATTRIBUTES);
    }

    /**
     * Instantiates a new graph.
     * 
     * @param id
     *            the graph's id; should be either an Integer or a String
     * @param directed
     *            whether or not this is a directed graph
     * @param p
     *            this graph's Graphviz attributes
     */
    protected Graph(Object id, boolean directed, Properties p) {
        super(id, p);
        nodes = new Hashtable<Object, Node>();
        edges = new ArrayList<Edge>();
        subgraphs = new ArrayList<Graph>();
        this.directed = directed;
        data().setCopyAction(DATA_KEY_GRAPHVIZ_NODE_ATTRIBUTES,
                DataStore.COPY_BY_VALUE);
        data().setCopyAction(DATA_KEY_GRAPHVIZ_EDGE_ATTRIBUTES,
                DataStore.COPY_BY_VALUE);
    }

    /**
     * Instantiates a new graph.
     * 
     * @param id
     *            the graph's id
     * @param directed
     *            whether or not this is a directed graph
     * @param p
     *            this Graph's graphviz attributes
     */
    public Graph(String id, boolean directed, Properties p) {
        this((Object) id, directed, p);
    }

    /**
     * Instantiates a new graph.
     * 
     * @param id
     *            the graph's id
     * @param directed
     *            whether or not this is a directed graph
     * @param p
     *            this graph's Graphviz attributes
     */
    public Graph(int id, boolean directed, Properties p) {
        this(Integer.valueOf(id), directed, p);
    }

    /**
     * Instantiates a new directed graph.
     * 
     * @param id
     *            the graph's id
     * @param p
     *            this graph's Graphviz attributes
     */
    public Graph(String id, Properties p) {
        this((Object) id, true, p);
    }

    /**
     * Instantiates a new directed graph.
     * 
     * @param id
     *            the graph's id
     * @param p
     *            this graph's Graphviz attributes
     */
    public Graph(int id, Properties p) {
        this(Integer.valueOf(id), true, p);
    }

    /**
     * Instantiates a new directed graph.
     * 
     * @param id
     *            the graph's id
     */
    public Graph(String id) {
        this(id, null);
    }

    /**
     * Instantiates a new directed graph.
     * 
     * @param id
     *            the graph's id
     */
    public Graph(int id) {
        this(Integer.valueOf(id), null);
    }

    /**
     * Instantiates a new graph.
     * 
     * @param directed
     *            whether or not this is a directed graph
     */
    public Graph(boolean directed) {
        this(null, directed, null);
    }

    /**
     * Instantiates a new directed graph.
     * 
     * @param p
     *            this graph's Graphviz attributes
     */
    public Graph(Properties p) {
        this(null, p);
    }

    /**
     * Instantiates a new directed graph.
     */
    public Graph() {
        this(null, null);
    }

    // --- Attribute access ---

    /**
     * Tells whether or not this is a directed graph.
     * 
     * @return true, if directed
     */
    public boolean isDirected() {
        return directed;
    }

    /**
     * Specifies whether or not this is a directed graph.
     * 
     * @param directed
     *            the new directed
     */
    public void setDirected(boolean directed) {
        this.directed = directed;
    }

    /**
     * Gets this graph's default Graphviz node attributes.
     * 
     * @return the default Graphviz node attributes
     */
    public Properties getDefaultGVNodeAttributes() {
        return data().getProperties(DATA_KEY_GRAPHVIZ_NODE_ATTRIBUTES);
    }

    /**
     * Sets this graph's default Graphviz node attributes.
     * 
     * @param p
     *            the new default Graphviz node attributes
     */
    public void setDefaultGVNodeAttributes(Properties p) {
        data().set(DATA_KEY_GRAPHVIZ_NODE_ATTRIBUTES, p);
    }

    /**
     * Gets this graph's default Graphviz edge attributes.
     * 
     * @return the default Graphviz edge attributes
     */
    public Properties getDefaultGVEdgeAttributes() {
        return data().getProperties(DATA_KEY_GRAPHVIZ_EDGE_ATTRIBUTES);
    }

    /**
     * Sets this graph's default Graphviz edge attributes.
     * 
     * @param p
     *            the new default Graphviz edge attributes
     */
    public void setDefaultGVEdgeAttributes(Properties p) {
        data().set(DATA_KEY_GRAPHVIZ_EDGE_ATTRIBUTES, p);
    }

    // --- Private methods ---

    /*
     * Recursively clones all nodes belonging to this Graph or (recursively) any
     * of its subgraphs
     */
    private static void cloneNodes(Graph template, Graph clone,
            GraphObjectCondition nodeCond) {
        for (Node n : template.nodes.values()) {
            if ((nodeCond == null) || nodeCond.satisfiedBy(n)) {
                n.createClone(clone);
            }
        }
        Iterator<Graph> i = template.getDirectlyOwnedSubgraphs().iterator();
        Iterator<Graph> j = clone.getDirectlyOwnedSubgraphs().iterator();
        while (i.hasNext()) {
            Graph sTemplate = i.next();
            Graph sClone = j.next();
            cloneNodes(sTemplate, sClone, nodeCond);
        }
    }

    /*
     * Recursively clones all nodes belonging to this Graph or (recursively) any
     * of its subgraphs
     */
    private static void cloneEdges(Graph template, Graph clone,
            GraphObjectCondition nodeCond, GraphObjectCondition edgeCond) {
        for (Edge templateEdge : template.edges) {
            if ((edgeCond == null) || edgeCond.satisfiedBy(templateEdge)) {
                if ((nodeCond == null)
                        || (nodeCond.satisfiedBy(templateEdge.getTail()) && nodeCond
                                .satisfiedBy(templateEdge.getHead()))) {
                    clone.cloneEdge(templateEdge, clone.getTopLevelGraph()
                            .getNode(templateEdge.getTail().getId()), clone
                            .getTopLevelGraph().getNode(
                                    templateEdge.getHead().getId()));
                }
            }
        }
        Iterator<Graph> i = template.getDirectlyOwnedSubgraphs().iterator();
        Iterator<Graph> j = clone.getDirectlyOwnedSubgraphs().iterator();
        while (i.hasNext()) {
            Graph sTemplate = i.next();
            Graph sClone = j.next();
            cloneEdges(sTemplate, sClone, nodeCond, edgeCond);
        }
    }

    /*
     * returns the Map of GraphML key-type mappings for this Graph and all its
     * components
     */
    private Map<String, String> getKeyTypeMapping() {
        Map<String, String> m = new HashMap<String, String>();
        updateKeyTypeMapping(m, data().getKeyTypeMapping());
        for (Graph sg : subgraphs) {
            updateKeyTypeMapping(m, sg.getKeyTypeMapping());
        }
        for (Node n : nodes.values()) {
            updateKeyTypeMapping(m, n.data().getKeyTypeMapping());
        }
        for (Edge k : edges) {
            updateKeyTypeMapping(m, k.data().getKeyTypeMapping());
        }
        return m;
    }

    /*
     * update the Map of GraphML key-type mappings already seen during GraphML
     * creation
     */
    private void updateKeyTypeMapping(Map<String, String> keyTypeMapping,
            Map<String, String> newKeyTypes) {
        for (String key : newKeyTypes.keySet()) {
            keyTypeMapping.put(key, newKeyTypes.get(key));
        }
    }

    /*
     * For all node elements, create corresponding Nodes
     */
    private void nodesFromXML(Element e, Map<String, Graph> subgraphsMap,
            Map<String, String> keyTypeMapping, boolean plainGraphML) {
        for (Element nodeElement : XMLUtils.getChildrenByName(e, "node")) {
            if (XMLUtils.getChildByName(nodeElement, "graph") == null) {
                // if (!XMLUtils.getBooleanAttribute(nodeElement,
                // nsPrefix +
                // ":subgraph")
                // || (plainGraphML && (nNodes == 0) && (nEdges == 0)))
                // {
                Object nid = idFromXML(nodeElement);
                Node n = new Node(nid, null);
                n.fromXML(nodeElement, keyTypeMapping);
                addNode(n);
            }
        }
        for (Element nodeElement : XMLUtils.getChildrenByName(e, "node")) {
            // int nNodes = XMLUtils.getChildrenByName(nodeElement,
            // "node").size();
            // int nEdges = XMLUtils.getChildrenByName(nodeElement,
            // "edge").size();
            if (XMLUtils.getChildByName(nodeElement, "graph") != null) {
                // if (XMLUtils.getBooleanAttribute(nodeElement,
                // nsPrefix +
                // ":subgraph")
                // || (plainGraphML && ((nNodes > 0) || (nEdges > 0))))
                // {
                Element subElement = XMLUtils.getChildByName(nodeElement,
                        "graph");
                String sidString = subElement.getAttribute("id");
                Object sid = idFromXML(subElement);
                Graph sg = new Graph();
                sg.setId(sid);
                sg.fromXML(subElement, keyTypeMapping);
                addSubgraph(sg);
                subgraphsMap.put(sidString, sg);
                sg.nodesFromXML(subElement, subgraphsMap, keyTypeMapping,
                        plainGraphML);
            }
        }
    }

    /*
     * For all edge elements, create corresponding Edges
     */
    private void edgesFromXML(Element e, Map<String, Graph> subgraphsMap,
            Map<String, String> keyTypeMapping) {
        for (Element edgeElement : XMLUtils.getChildrenByName(e, "edge")) {
            String tailIdString = edgeElement.getAttribute("source");
            Node tail;
            if (XMLUtils.getStringAttribute(edgeElement,
                    nsPrefix + ":source_id_type").equals("numeric")) {
                tail = getTopLevelGraph().getNode(
                        Integer.parseInt(tailIdString));
            } else {
                tail = getTopLevelGraph().getNode(tailIdString);
            }
            String headIdString = edgeElement.getAttribute("target");
            Node head;
            if (XMLUtils.getStringAttribute(edgeElement,
                    nsPrefix + ":target_id_type").equals("numeric")) {
                head = getTopLevelGraph().getNode(
                        Integer.parseInt(headIdString));
            } else {
                head = getTopLevelGraph().getNode(headIdString);
            }
            Edge k = createEdge(tail, head);
            k.setId(idFromXML(edgeElement));
            k.fromXML(edgeElement, keyTypeMapping);
        }
        for (Element nodeElement : XMLUtils.getChildrenByName(e, "node")) {
            if (XMLUtils.getChildByName(nodeElement, "graph") != null) {
                Element subElement = XMLUtils.getChildByName(nodeElement,
                        "graph");
                String sidString = subElement.getAttribute("id");
                Graph sg = subgraphsMap.get(sidString);
                sg.edgesFromXML(subElement, subgraphsMap, keyTypeMapping);
            }
        }
    }

    /*
     * Revert the direction of all edges belonging to this Graph; recursively do
     * the same for all its subgraphs.
     */
    private void doReverse() {
        for (Edge e : edges) {
            e.flip();
        }
        for (Graph sg : subgraphs) {
            sg.doReverse();
        }
    }

    // --- Subgraphs

    /*
     * Recursively add this Graph and all its subgraphs to the given List
     */
    private void fillSubgraphsList(List<Graph> sl) {
        for (Graph sg : subgraphs) {
            sl.add(sg);
            sg.fillSubgraphsList(sl);
        }
    }

    /*
     * Add the given subgraph to this Graph's subgraph list and set its owner
     * attribute accordingly, ensuring it gets first removed from any other
     * Graph it might possibly belong to
     */
    private void addSubgraph(Graph sg) {
        if (!checkSanity || !getTopLevelGraph().containsSubgraph(sg)) {
            if (checkSanity && (sg.getOwner() != null)) {
                if (sg.getOwner().getTopLevelGraph() == getTopLevelGraph()) {
                    sg.getOwner().subgraphs.remove(sg);
                } else {
                    sg.getOwner().removeSubgraph(sg);
                }
            }
            subgraphs.add(sg);
            sg.setOwner(this);
        } else {
            throw new GraphException(this, "Cannot add subgraph since id "
                    + sg.getIdString() + " is already in use.");
        }
    }

    /*
     * Create a new subgraph and add it to this Graph's subgraph list
     */
    private Graph doCreateSubgraph(Object id, Properties p) {
        Graph sg = new Graph(id, directed, p);
        addSubgraph(sg);
        return sg;
    }

    // --- Nodes

    /*
     * Add all this Graph's Nodes to the given list; recursively proceed with
     * all this Graph's subgraphs
     */
    private void fillNodeList(List<Node> nl) {
        nl.addAll(nodes.values());
        for (Graph sg : subgraphs) {
            sg.fillNodeList(nl);
        }
    }

    /*
     * Add the given Node to this Graph's node map and set its owner attribute
     * accordingly, ensuring it gets first removed (and disconnected, if
     * necessary) from any other Graph it might possibly belong to
     */
    private void addNode(Node n) {
        if (!checkSanity || !getTopLevelGraph().containsNode(n.getId())) {
            if (checkSanity && (n.getOwner() != null)) {
                if (n.getOwner().getTopLevelGraph() == getTopLevelGraph()) {
                    n.getOwner().nodes.remove(n.getId());
                } else {
                    n.getOwner().removeNode(n);
                }
            }
            nodes.put(n.getId(), n);
            n.setOwner(this);
        } else {
            throw new GraphException(this, "Cannot add node since id "
                    + n.getIdString() + " is already in use.");
        }
    }

    /*
     * Create a new Node and add it to this Graph's node map
     */
    private Node doCreateNode(Object id, Properties p) {
        Node n = new Node(id, p);
        addNode(n);
        return n;
    }

    // --- Edges

    /*
     * Add all this Graph's Edges to the given list; recursively proceed with
     * all this Graph's subgraphs
     */
    private void fillEdgeList(List<Edge> list) {
        list.addAll(edges);
        for (Graph sg : subgraphs) {
            sg.fillEdgeList(list);
        }
    }

    /*
     * Adds the given Edge to this Graph and sets its owner attribute
     * accordinly, ensuring that both the Edges tail and head Nodes belong to
     * the same top level Graph as the Edge
     */
    private void addEdge(Edge e) {
        if (!checkSanity || !getTopLevelGraph().containsEdge(e)) {
            if (checkSanity) {
                Graph t = getTopLevelGraph();
                if ((e.getTail().getOwner().getTopLevelGraph() != t)
                        || (e.getHead().getOwner().getTopLevelGraph() != t)) {
                    throw new GraphException(this,
                            "Cannot add edge since head/tail is not in same graph.");
                }
                if (e.getOwner() != null) {
                    e.getOwner().edges.remove(e);
                }
            }
            edges.add(e);
            e.setOwner(this);
        }
    }

    /*
     * Create a new Edge connecting the given Nodes and add it to this Graph's
     * edge list
     */
    private Edge doCreateEdge(Node tail, Node head, Properties p) {
        Edge e = new Edge(tail, head, p);
        addEdge(e);
        return e;
    }

    // --- Protected methods

    /*
     * (non-Javadoc)
     * 
     * @see org.comlink.home.tcrass.graph.GraphObject#toXML(org.w3c.dom.Element
     * , java.util.List, java.util.List)
     */
    @Override
    protected void toXML(Element e, boolean plainGraphML,
            Set<String> whiteList, Set<String> blackList, List<String> usedIds,
            List<Element> anonElements) {
        super.toXML(e, plainGraphML, whiteList, blackList, usedIds,
                anonElements);
        Document d = e.getOwnerDocument();
        e.setAttribute("edgedefault", directed ? "directed" : "undirected");
        for (Graph sg : subgraphs) {
            Element nodeElement = d.createElement("node");
            if (!plainGraphML) {
                // XMLUtils.setAttributeNS(nodeElement, SCHEMA_NAME,
                // getNSPrefix() + ":subgraph", true);
                XMLUtils.setAttributeNS(nodeElement, SCHEMA_NAME, getNSPrefix()
                        + ":anonymous", true);
            }
            anonElements.add(nodeElement);
            Element subgraphElement = d.createElement("graph");
            sg.toXML(subgraphElement, plainGraphML, whiteList, blackList,
                    usedIds, anonElements);
            nodeElement.appendChild(subgraphElement);
            e.appendChild(nodeElement);
        }
        for (Node n : nodes.values()) {
            Element nodeElement = d.createElement("node");
            n.toXML(nodeElement, plainGraphML, whiteList, blackList, usedIds,
                    anonElements);
            e.appendChild(nodeElement);
        }
        for (Edge k : edges) {
            Element edgeElement = d.createElement("edge");
            k.toXML(edgeElement, plainGraphML, whiteList, blackList, usedIds,
                    anonElements);
            e.appendChild(edgeElement);
        }
    }

    // --- Package-scope methods

    /**
     * Creates a clone of the given node, assign it the given id and adds it to
     * the given target graph.
     * 
     * @param n
     *            the node to be cloned
     * @param id
     *            the cloned node's id; should be either an Integer or a String
     * @return the cloned node
     */
    Node cloneNode(Node n, Object id) {
        Node clone = (Node) n.clone();
        clone.id = id;
        addNode(clone);
        return clone;
    }

    /**
     * Moves the node specified by the given id from its current owner to the
     * given target graph.
     * 
     * @param id
     *            the id of the node to be moved; should be either an Integer or
     *            a String
     * @param target
     *            the graph the node is to be moved to
     */
    void moveNode(Node n) {
        n.getOwner().nodes.remove(n.getId());
        addNode(n);
    }

    /*
     * Create a clone of the given Edge, use it for connecting the given Nodes
     * and add it to the given target Graph
     */
    Edge cloneEdge(Edge e, Node tail, Node head) {
        Edge clone = null;
        if (!checkSanity
                || ((getTopLevelGraph() == tail.owner.getTopLevelGraph()) && (getTopLevelGraph() == head.owner
                        .getTopLevelGraph()))) {
            clone = (Edge) e.clone();
            clone.setTail(tail);
            tail.addOutEdge(clone);
            clone.setHead(head);
            head.addInEdge(clone);
            addEdge(clone);
        } else {
            throw new GraphException(
                    "Cannot clone edge since tail and/or head node is/are not in the target graph!");
        }
        return clone;
    }

    // --- Public methods ---

    // --- (Sub)Graphs
    /**
     * Creates a version of this graph which has the direction of all its edges
     * reversed.
     * 
     * @return the reversed Graph
     */
    public Graph createReverse() {
        Graph R = createClone();
        R.doReverse();
        return R;
    }

    /**
     * Gets the top level graph this graph belongs to (or this graph, if it
     * doesn't have an owner and hence already is the top level graph).
     * 
     * @return this graph's top level graph
     */
    public Graph getTopLevelGraph() {
        Graph g = this;
        if (owner != null) {
            g = owner.getTopLevelGraph();
        }
        return g;
    }

    /**
     * Creates a new subgraph owned by this graph.
     * 
     * @param id
     *            the new subgraph's id
     * @param p
     *            the new subgraph's Graphviz attributes
     * @return the newly created subgraph
     */
    public Graph createSubgraph(int id, Properties p) {
        return doCreateSubgraph(Integer.valueOf(id), p);
    }

    /**
     * Creates a new subgraph owned by this graph.
     * 
     * @param id
     *            the new subgraph's id
     * @return the newly created subgraph
     */
    public Graph createSubgraph(int id) {
        return createSubgraph(id, null);
    }

    /**
     * Creates a new subgraph owned by this graph.
     * 
     * @param id
     *            the new subgraph's id
     * @param p
     *            the new subgraph's Graphviz attributes
     * @return the newly created subgraph
     */
    public Graph createSubgraph(String id, Properties p) {
        return doCreateSubgraph(id, p);
    }

    /**
     * Creates a new subgraph owned by this graph.
     * 
     * @param id
     *            the new subgraph's id
     * @return the newly created subgraph
     */
    public Graph createSubgraph(String id) {
        return createSubgraph(id, null);
    }

    /**
     * Creates a new subgraph owned by this graph.
     * 
     * @return the newly created subgraph
     */
    public Graph createSubgraph() {
        return doCreateSubgraph(null, null);
    }

    /**
     * Returns all subgraphs directly owned by this graph.
     * 
     * @return the subgraphs
     */
    public List<Graph> getDirectlyOwnedSubgraphs() {
        return new ArrayList<Graph>(subgraphs);
    }

    /**
     * Returns all subgraphs owned by this graph or (recursivley) one of its
     * subgraphs.
     * 
     * @return the subgraphs (recursivley)
     */
    public List<Graph> getSubgraphs() {
        List<Graph> sl = new ArrayList<Graph>();
        fillSubgraphsList(sl);
        return sl;
    }

    /**
     * Returns the subgraph having the given id and being owned by this graph or
     * (recursively) any of its subgraphs.
     * 
     * @param id
     *            the subgraph's id; should be either an Integer or a String
     * @return the requested subgraph (or null if no subgraph with the given id
     *         is found)
     */
    public Graph getSubgraph(Object id) {
        Graph g = null;
        for (Graph sg : subgraphs) {
            if (((id == null) && (sg.getId() == null)) || id.equals(sg.getId())) {
                g = sg;
            } else {
                g = sg.getSubgraph(id);
            }
            if (g != null) {
                break;
            }
        }
        return g;
    }

    /**
     * Tells whether or not the given subgraph is owned by this graph or
     * (recursively) any of its subgraphs.
     * 
     * @param sg
     *            the subgraph under question
     * @return true, if the subgraph is owned by this graph or (recursively) any
     *         of its subgraphs
     */
    public boolean containsSubgraph(Graph sg) {
        return getSubgraphs().contains(sg);
    }

    /**
     * Tells whether or not this graph or (recursively) any of its subgraphs
     * contains a subgraph with the given id.
     * 
     * @param id
     *            the subgraph's id; should be either an Integer or a String
     * @return true, if subgraph owned by this graph or (recursively) any of its
     *         subgraphs is found
     */
    public boolean containsSubgraph(Object id) {
        return getSubgraph(id) != null;
    }

    /**
     * Remove the given subgraph from its owner, if owned by this graph or
     * (recursively) any of its subgraphs.
     * 
     * @param sg
     *            the subgraph to be removed
     * @return the removed subgraph
     */
    public Graph removeSubgraph(Graph sg) {
        if (!checkSanity || containsSubgraph(sg)) {
            for (Node n : sg.getNodes()) {
                sg.removeNode(n);
            }
            for (Edge e : sg.getEdges()) {
                sg.removeEdge(e);
            }
            sg.getOwner().subgraphs.remove(sg);
            sg.setOwner(null);
        }
        return sg;
    }

    /**
     * Remove the subgraph with the given id from its owner, if owned by this
     * graph or (recursively) any of its subgraphs
     * 
     * @param id
     *            the id of the subgraph to be removed; should be either an
     *            Integer or a String
     * @return the removed subgraph
     */
    public Graph removeSubgraph(Object id) {
        Graph sg = getSubgraph(id);
        if (sg != null) {
            removeSubgraph(sg);
        }
        return sg;
    }

    // --- Nodes

    /**
     * Returns this graph's (possibly indirect) supergraph owning the given node
     * or this graph, if directly owning the node.
     * 
     * @param n
     *            the node whose containing graph is requested
     * @return the supergraph containing the node
     */
    public Graph getSupergraphOwningNode(Node n) {
        return getSupergraphOwningNode(n.getId());
    }

    /**
     * Returns this graph's (possibly indirect) supergraph owning the node with
     * the given id, or this graph, if directly owning this node.
     * 
     * @param id
     *            the id of the node whose containing graph is requested; should
     *            be either an Integer or a String
     * @return the supergraph containing the node
     */
    public Graph getSupergraphOwningNode(Object id) {
        Graph g = null;
        if (directlyOwnsNode(id)) {
            g = this;
        } else {
            if (owner != null) {
                g = owner.getSupergraphOwningNode(id);
            }
        }
        return g;
    }

    /**
     * Creates a new node with the given id and owned by this graph.
     * 
     * @param id
     *            the new node's id
     * @return the new node
     */
    public Node createNode(int id) {
        return doCreateNode(Integer.valueOf(id), null);
    }

    /**
     * Creates a new node with the given id and owned by this graph.
     * 
     * @param id
     *            the new node's id
     * @param p
     *            the new node's Graphviz attributes
     * @return the new node
     */
    public Node createNode(int id, Properties p) {
        return doCreateNode(Integer.valueOf(id), p);
    }

    /**
     * Creates a new node with the given id and owned by this graph.
     * 
     * @param id
     *            the new node's id
     * @return the new node
     */
    public Node createNode(String id) {
        return doCreateNode(id, null);
    }

    /**
     * Creates a new node with the given id and owned by this graph.
     * 
     * @param id
     *            the new node's id
     * @param p
     *            the new node's Graphviz attributes
     * @return the new node
     */
    public Node createNode(String id, Properties p) {
        return doCreateNode(id, p);
    }

    /**
     * Returns all nodes directly owned by this graph.
     * 
     * @return all nodes directly owned by this graph
     */
    public List<Node> getDirectlyOwnedNodes() {
        return new ArrayList<Node>(nodes.values());
    }

    /**
     * Tells whether or not this graph directly owns the given node.
     * 
     * @param n
     *            the node in question
     * @return true, if the given node is owned by this graph
     */
    public boolean directlyOwnsNode(Node n) {
        return nodes.containsValue(n);
    }

    /**
     * Tells whether or not this graph directly owns the node specified by the
     * given id.
     * 
     * @param id
     *            the id of the node under question; should be either an Integer
     *            or a String
     * @return true, if the specified node is owned by this graph
     */
    public boolean directlyOwnsNode(Object id) {
        return nodes.containsKey(id);
    }

    /**
     * Returns all nodes owned by this graph or (recursively) any of its
     * subgraphs.
     * 
     * @return all nodes owned by this graph or (recursively) any of its
     *         subgraphs
     */
    public List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        fillNodeList(nodes);
        return nodes;
    }

    /**
     * Returns the node with the given id if owned by this graph or
     * (recursively) any of its subgraphs
     * 
     * @param id
     *            the requested nodes id; should be either an Integer or a
     *            String
     * @return the requested node
     */
    public Node getNode(Object id) {
        Node n = nodes.get(id);
        if (n == null) {
            for (Graph sg : subgraphs) {
                n = sg.getNode(id);
                if (n != null) {
                    break;
                }
            }
        }
        return n;
    }

    /**
     * Tells whether or not this graph or (recursively) any of its subgraphs
     * contains this node.
     * 
     * @param n
     *            the node under question
     * @return true, if the given node is owned by this graph or (recursively)
     *         any of its subgraphs
     */
    public boolean containsNode(Node n) {
        return (n.getOwner() == this) || containsSubgraph(n.getOwner());
    }

    /**
     * Tells whether or not this graph or (recursively) any of its subgraphs
     * contain a node with the given id.
     * 
     * @param id
     *            the id of the node under question; should be either an Integer
     *            or a String
     * @return true, if the specified node is owned by this graph or
     *         (recursively) any of its subgraphs
     */
    public boolean containsNode(Object id) {
        return getNode(id) != null;
    }

    /**
     * Removes the given node from this graph, if contained therein or
     * (recursively) in one of its subgraphs.
     * 
     * @param n
     *            the node to be removed
     * @return the removed node (which is identical to the given node)
     */
    public Node removeNode(Node n) {
        if (!checkSanity || containsNode(n)) {
            n.disconnect();
            n.getOwner().nodes.remove(n.getId());
            n.owner = null;
        }
        return n;
    }

    /**
     * Removes the given node from this graph, if contained therein or
     * (recursively) in one of its subgraphs.
     * 
     * @param id
     *            the id of the node to be removed; should be either an Integer
     *            or a String
     * @return the removed node
     */
    public Node removeNode(Object id) {
        Node n = getNode(id);
        if (n != null) {
            removeNode(n);
        }
        return n;
    }

    // --- Edges

    /**
     * Removes the given node from this graph, but creates clones of either all
     * incoming ( {@link #KEEP_IN_EDGES}), outgoing ( {@link #KEEP_OUT_EDGES})
     * or both kinds of edges ( {@link #KEEP_ALL_EDGES} (depending on the given
     * specification) so that each node previously preceeding the removed node
     * is connected to each the node's original successors.
     * 
     * @param n
     *            the node to be removed
     * @param keep
     *            a specification of which kinds of edges are to be used for
     *            mending the "hole" resulting from the node's removal
     * @return the removed node (which is identical to the given node)
     */
    public Node removeNodeMendingEdges(Node n, int keep) {
        if (!checkSanity || containsNode(n)) {
            if ((keep & KEEP_IN_EDGES) != 0) {
                for (Edge in : n.getInEdges()) {
                    for (Node succ : n.getSuccessors()) {
                        in.createClone(in.getTail(), succ);
                    }
                }
            }
            if ((keep & KEEP_OUT_EDGES) != 0) {
                for (Edge out : n.getOutEdges()) {
                    for (Node pred : n.getPredecessors()) {
                        out.createClone(pred, out.getHead());
                    }
                }
            }
            removeNode(n);
        }
        return n;
    }

    /**
     * Removes the node specified by the given id from this graph, but creates
     * clones of either all incoming ( {@link #KEEP_IN_EDGES}), outgoing (
     * {@link #KEEP_OUT_EDGES}) or both kinds of edges ( {@link #KEEP_ALL_EDGES}
     * (depending on the given specification) so that each node previously
     * preceeding the removed node is connected to each the node's original
     * successors.
     * 
     * @param id
     *            the id of the node to be removed; should either be an
     *            {@link java.lang.Integer} or a {@link java.lang.String}
     * @param keep
     *            a specification of which kinds of edges are to be used for
     *            mending the "hole" resulting from the node's removal
     * @return the removed node (which is identical to the given node)
     */
    public Node removeNodeMendingEdges(Object id, int keep) {
        return removeNodeMendingEdges(getNode(id), keep);
    }

    /**
     * Creates within this graph an edge connecting the given head and tail
     * node.
     * 
     * @param tail
     *            the tail node
     * @param head
     *            the head node
     * @return the newly created edge
     */
    public Edge createEdge(Node tail, Node head) {
        return createEdge(tail, head, null);
    }

    /**
     * Creates within this graph an edge connecting the given head and tail node
     * and assigns the given Graphviz attributes to it.
     * 
     * @param tail
     *            the tail node
     * @param head
     *            the head node
     * @param p
     *            the edge's Graphviz attributes
     * @return the newly created edge
     */
    public Edge createEdge(Node tail, Node head, Properties p) {
        Edge e = null;
        e = doCreateEdge(tail, head, p);
        return e;
    }

    /**
     * Creates within this graph an edge connecting the head and tail nodes
     * specified by the given ids.
     * 
     * @param tailId
     *            the tail node's id
     * @param headId
     *            the head node's id
     * @param p
     *            the edge's Graphviz attributes
     * @return the newly created edge
     */
    public Edge createEdge(Object tailId, Object headId, Properties p) {
        Node tail = getTopLevelGraph().getNode(tailId);
        if (tail == null) {
            throw new GraphException(this, "Cannot create edge from " + tailId
                    + ": no such node in graph.");
        }
        Node head = getTopLevelGraph().getNode(headId);
        if (head == null) {
            throw new GraphException(this, "Cannot create edge towards "
                    + headId + ": no such node in graph.");
        }
        return doCreateEdge(tail, head, p);
    }

    /**
     * Creates within this graph an edge connecting the head and tail nodes
     * specified by the given ids.
     * 
     * @param tailId
     *            the tail node's id
     * @param headId
     *            the head node's id
     * @return the newly created edge
     */
    public Edge createEdge(Object tailId, Object headId) {
        return createEdge(tailId, headId, null);
    }

    /**
     * Returns all edges owend by this graph or (recursively) one of its
     * subgraphs.
     * 
     * @return the edges (recursively)
     */
    public List<Edge> getEdges() {
        List<Edge> edgeList = new ArrayList<Edge>();
        fillEdgeList(edgeList);
        return edgeList;
    }

    /**
     * Returns all edges directly owend by this graph.
     * 
     * @return the edges
     */
    public List<Edge> getDirectlyOwnedEdges() {
        return new ArrayList<Edge>(edges);
    }

    /**
     * Returns all edges owned by this graph or (recursively) on of its
     * subgraphs connecting the given tail and head nodes.
     * 
     * @param tail
     *            the tail node
     * @param head
     *            the head node
     * @return all edges connecting tail and head node
     */
    public List<Edge> getEdges(Node tail, Node head) {
        List<Edge> edges = new ArrayList<Edge>();
        if (!checkSanity
                || (getTopLevelGraph().containsNode(tail) && getTopLevelGraph()
                        .containsNode(head))) {
            for (Edge e : tail.getOutEdges()) {
                if (e.getHead() == head) {
                    edges.add(e);
                }
            }
            if (!isDirected()) {
                for (Edge e : head.getOutEdges()) {
                    if (e.getHead() == tail) {
                        edges.add(e);
                    }
                }
            }
        }
        return edges;
    }

    /**
     * Returns all edges owned by this graph or (recursively) on of its
     * subgraphs connecting the tail and head nodes specified by the given ids.
     * 
     * @param tailId
     *            the tail node's id; should be either an Integer or a String
     * @param headId
     *            the head node's id; should be either an Integer or a String
     * @return all edges connecting tail and head node
     */
    public List<Edge> getEdges(Object tailId, Object headId) {
        Node tail = getTopLevelGraph().getNode(tailId);
        if (tail == null) {
            throw new GraphException(this, "Cannot find edges from " + tailId
                    + ": no such node in graph.");
        }
        Node head = getTopLevelGraph().getNode(headId);
        if (head == null) {
            throw new GraphException(this, "Cannot find edges towards "
                    + headId + ": no such node in graph.");
        }
        return getEdges(tail, head);
    }

    /**
     * Returns one edge owned by this graph or (recursively) on of its subgraphs
     * connecting the given tail and head nodes.
     * 
     * @param tail
     *            the tail node
     * @param head
     *            the head node
     * @return one edge or null, if no edge exists between the tail and the head
     *         node
     */
    public Edge getEdge(Node tail, Node head) {
        Edge e = null;
        List<Edge> l = getEdges(tail, head);
        if (l.size() > 0) {
            e = l.get(0);
        }
        return e;
    }

    /**
     * Returns one edge owned by this graph or (recursively) on of its subgraphs
     * connecting the tail and head nodes specified by the given ids.
     * 
     * @param tailId
     *            the tail node's id; should be either an Integer or a String
     * @param headId
     *            the head node's id; should be either an Integer or a String
     * @return one edge or null, if no edge exists between the tail and the head
     *         node
     */
    public Edge getEdge(Object tailId, Object headId) {
        Edge e = null;
        List<Edge> l = getEdges(tailId, headId);
        if (l.size() > 0) {
            e = l.get(0);
        }
        return e;
    }

    /**
     * Tells wheter or not there is at least one edge owned by this graph or
     * (recursively) on of its subgraphs connecting the given tail and head
     * nodes.
     * 
     * @param tail
     *            the tail node
     * @param head
     *            the head node
     * @return true, if this graph (recursively) owns at least one edge
     *         connecting tail and head node
     */
    public boolean containsEdge(Node tail, Node head) {
        return getEdges(tail, head).size() > 0;
    }

    /**
     * Tells wheter or not there is at least one edge owned by this graph or
     * (recursively) on of its subgraphs connecting the tail and head nodes
     * specified by the given ids.
     * 
     * @param tailId
     *            the tail node's id; should be either an Integer or a String
     * @param headId
     *            the head node's id; should be either an Integer or a String
     * @return true, if this graph (recursively) owns at least one edge
     *         connecting tail and head node
     */
    public boolean containsEdge(Object tailId, Object headId) {
        return getEdges(tailId, headId).size() > 0;
    }

    /**
     * Tells wheter or not the given edge is owned by this graph or
     * (recursively) on of its subgraphs.
     * 
     * @param e
     *            the edge under question
     * @return true, if the edge is owned by this graph (recursively)
     */
    public boolean containsEdge(Edge e) {
        return (e.getOwner() == this) || (containsSubgraph(e.getOwner()));
    }

    /**
     * Tells whether or not the given edge is directly owned by this graph.
     * 
     * @param e
     *            the edge under question
     * @return true, if the edge is directly owned by this graph
     */
    public boolean directlyOwnsEdge(Edge e) {
        return e.getOwner() == this;
    }

    /**
     * Removes the given edge from this graph, if contained therein or
     * (recursively) in one of its subgraphs.
     * 
     * @param e
     *            the edge to be removed
     * @return the removed edge (which is edentical to the given edge)
     */
    public Edge removeEdge(Edge e) {
        if (!checkSanity || containsEdge(e)) {
            e.getTail().removeOutEdge(e);
            e.getHead().removeInEdge(e);
            e.getOwner().edges.remove(e);
            e.owner = null;
        }
        return e;
    }

    /**
     * Returns a {@link java.lang.String} representation of this Graph
     * 
     * @return a String description of this Graph
     */
    @Override
    public String toString() {
        return "(id=" + super.toString() + ")";
    }

    // --- Copying data between "normal" and Graphviz data

    /**
     * For this graph and all its nodes and edges, copy data from their
     * DataStores to their Graphviz attributes. Recursively proceed with all
     * this graph's subgraphs.
     * 
     * @param mapping
     *            the mapping between DataStore keys and Graphviz attribute
     *            names
     */
    public void copyAllDataToGVAttributes(Properties mapping) {
        copyDataToGVAttributes(mapping);
        copyNodeDataToGVAttributes(mapping);
        copyEdgeDataToGVAttributes(mapping);
        for (Graph g : subgraphs) {
            g.copyAllDataToGVAttributes(mapping);
        }
    }

    /**
     * For all nodes directly owned by this graph, copy data from their
     * DataStores to their Graphviz attributes.
     * 
     * @param mapping
     *            the mapping between DataStore keys and Graphviz attribute
     *            names
     */
    public void copyNodeDataToGVAttributes(Properties mapping) {
        for (Node n : nodes.values()) {
            n.copyDataToGVAttributes(mapping);
        }
    }

    /**
     * For all edges directly owned by this graph, copy data from their
     * DataStores to their Graphviz attributes.
     * 
     * @param mapping
     *            the mapping between DataStore keys and Graphviz attribute
     *            names
     */
    public void copyEdgeDataToGVAttributes(Properties mapping) {
        for (Edge e : edges) {
            e.copyDataToGVAttributes(mapping);
        }
    }

    // --- Implementation of Cloneable

    /**
     * Creates a copy of this graph (recursively including its subgraphs),
     * <em>but neigher</em> of its nodes <em>nor</em> its edges. The cloned
     * graph will have the given id.
     * 
     * @param the
     *            new graph's id
     * @return a basic clone of this graph
     */
    private Graph createBasicClone(Object id) {
        Graph clone = (Graph) super.clone();
        clone.id = id;
        clone.nodes = new Hashtable<Object, Node>();
        clone.edges = new ArrayList<Edge>();
        clone.subgraphs = new ArrayList<Graph>();
        clone.setDirected(isDirected());
        for (Graph sg : subgraphs) {
            clone.addSubgraph(sg.createBasicClone());
        }
        return clone;
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs),
     * <em>but neigher</em> of its nodes <em>nor</em> its edges.
     * 
     * @return a basic clone of this graph
     */
    public Graph createBasicClone() {
        return createBasicClone(id);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs),
     * <em>but neigher</em> of its nodes <em>nor</em> its edges. The cloned
     * graph will have the given id.
     * 
     * @param id
     *            the new graph's id
     * @return a basic clone of this graph
     */
    public Graph createBasicClone(int id) {
        return createBasicClone((Object) id);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs),
     * <em>but neigher</em> of its nodes <em>nor</em> its edges. The cloned
     * graph will have the given id.
     * 
     * @param id
     *            the new graph's id
     * @return a basic clone of this graph
     */
    public Graph createBasicClone(String id) {
        return createBasicClone((Object) id);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing only those nodes satisfying the given
     * {@link de.tcrass.graph.condition.GraphObjectCondition} and <em>no</em>
     * edges. The cloned graph will have the given id.
     * 
     * @param the
     *            cloned graph's id
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @return an unconnected clone of this graph
     */
    private Graph createUnconnectedClone(Object id,
            GraphObjectCondition nodeCond) {
        Graph clone = createBasicClone(id);
        cloneNodes(this, clone, nodeCond);
        return clone;
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing only those nodes satisfying the given
     * {@link de.tcrass.graph.condition.GraphObjectCondition} and <em>no</em>
     * edges.
     * 
     * @param id
     *            the cloned graph's id
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @return an unconnected clone of this graph
     */
    public Graph createUnconnectedClone(int id, GraphObjectCondition nodeCond) {
        return createUnconnectedClone((Object) id, nodeCond);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing its nodes, <em>but not</em> its edges. The cloned graph will
     * have the given id.
     * 
     * @param id
     *            the cloned graph's id
     * @return an unconnected clone of this graph
     */
    public Graph createUnconnectedClone(int id) {
        return createUnconnectedClone((Object) id, null);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing only those nodes satisfying the given
     * {@link de.tcrass.graph.condition.GraphObjectCondition} and <em>no</em>
     * edges.
     * 
     * @param id
     *            the cloned graph's id
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @return an unconnected clone of this graph
     */
    public Graph createUnconnectedClone(String id, GraphObjectCondition nodeCond) {
        return createUnconnectedClone((Object) id, nodeCond);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing its nodes, <em>but not</em> its edges. The cloned graph will
     * have the given id.
     * 
     * @param id
     *            the cloned graph's id
     * @return an unconnected clone of this graph
     */
    public Graph createUnconnectedClone(String id) {
        return createUnconnectedClone((Object) id, null);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing only those nodes satisfying the given
     * {@link de.tcrass.graph.condition.GraphObjectCondition} and <em>no</em>
     * edges.
     * 
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @return an unconnected clone of this graph
     */
    public Graph createUnconnectedClone(GraphObjectCondition nodeCond) {
        return createUnconnectedClone(id, nodeCond);
    }

    /**
     * Creates a copy of this graph (recursively including its subgraphs)
     * containing its nodes, <em>but not</em> its edges.
     * 
     * @return an unconnected clone of this graph
     */
    public Graph createUnconnectedClone() {
        return createUnconnectedClone(id, null);
    }

    /**
     * Creates a copy of this graph containing only those nodes and edges which
     * satisfy the given node and edge condition, respectively. The cloned graph
     * will have the given id.
     * 
     * @param id 
     *           the cloned graph's id
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @param edgeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by an edge in order to be included into the
     *            cloned graph (or null, if no special conditions are to be met)
     * @return a clone of this graph containing only those nodes and edges which
     *         satisfy the given conditions
     */
    private Graph createClone(Object id, GraphObjectCondition nodeCond,
            GraphObjectCondition edgeCond) {
        Graph clone = createUnconnectedClone(id, nodeCond);
        cloneEdges(this, clone, nodeCond, edgeCond);
        return clone;
    }

    /**
     * Creates a copy of this graph containing only those nodes and edges which
     * satisfy the given node and edge condition, respectively. The cloned graph
     * will have the given id.
     * 
     * @param id
     *            the cloned graph's id
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @param edgeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by an edge in order to be included into the
     *            cloned graph (or null, if no special conditions are to be met)
     * @return a clone of this graph containing only those nodes and edges which
     *         satisfy the given conditions
     */
    public Graph createClone(int id, GraphObjectCondition nodeCond,
            GraphObjectCondition edgeCond) {
        Graph clone = createUnconnectedClone((Object) id, nodeCond);
        cloneEdges(this, clone, nodeCond, edgeCond);
        return clone;
    }

    /**
     * Creates a copy of this graph containing only those nodes and edges which
     * satisfy the given node and edge condition, respectively. The cloned graph
     * will have the given id.
     * 
     * @param id
     *            the cloned graph's id
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @param edgeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by an edge in order to be included into the
     *            cloned graph (or null, if no special conditions are to be met)
     * @return a clone of this graph containing only those nodes and edges which
     *         satisfy the given conditions
     */
    public Graph createClone(String id, GraphObjectCondition nodeCond,
            GraphObjectCondition edgeCond) {
        Graph clone = createUnconnectedClone((Object) id, nodeCond);
        cloneEdges(this, clone, nodeCond, edgeCond);
        return clone;
    }

    /**
     * Creates a copy of this graph containing only those nodes and edges which
     * satisfy the given node and edge condition, respectively.
     * 
     * @param nodeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by a node in order to be included into the cloned
     *            graph (or null, if no special conditions are to be met)
     * @param edgeCond
     *            the {@link de.tcrass.graph.condition.GraphObjectCondition} to
     *            be satisfied by an edge in order to be included into the
     *            cloned graph (or null, if no special conditions are to be met)
     * @return a clone of this graph containing only those nodes and edges which
     *         satisfy the given conditions
     */
    public Graph createClone(GraphObjectCondition nodeCond,
            GraphObjectCondition edgeCond) {
        Graph clone = createUnconnectedClone(id, nodeCond);
        cloneEdges(this, clone, nodeCond, edgeCond);
        return clone;
    }

    /**
     * Creates a full copy of this graph.
     * 
     * @return a clone of this graph
     */
    public Graph createClone() {
        return createClone(id, null, null);
    }

    /**
     * Creates a full copy of this graph. The cloned graph will have the given
     * id.
     * 
     * @param id
     *            the cloned graph's id
     * @return a clone of this graph
     */
    public Graph createClone(int id) {
        return createClone((Object) id, null, null);
    }

    /**
     * Creates a full copy of this graph. The cloned graph will have the given
     * id.
     * 
     * @param the
     *            cloned graph's id
     * @return a clone of this graph
     */
    public Graph createClone(String id) {
        return createClone((Object) id, null, null);
    }

    // --- GraphML-related stuff ---

    /**
     * Returns a GraphML document representing this graph.
     * 
     * @param plainGraphML
     *            whether to use plain or extended GraphML
     * @param whiteList
     *            the list of {@link DataStore} keys to be included in the
     *            Document (all, if null)
     * @param blackList
     *            a set of {@link DataStore} keys not to be included in the
     *            Document (overrides {whiteList} entries)
     * @return the GraphML document
     */
    public Document toGraphMLDocument(boolean plainGraphML,
            Set<String> whiteList, Set<String> blackList) {
        Set<String> bl = blackList;
        if (bl == null) {
            bl = new HashSet<String>();
        }
        Document d = XMLUtils.createDocument();
        Element root = d.createElementNS(
                "http://graphml.graphdrawing.org/xmlns/", "graphml");

        if (!plainGraphML) {
            XMLUtils.setAttribute(root, "extended_graphml", true);
        }

        Map<String, String> m;
        Map<String, String> keyTypeMapping = getKeyTypeMapping();
        if (whiteList != null) {
            m = new HashMap<String, String>();
            for (String key : keyTypeMapping.keySet()) {
                if (whiteList.contains(key)) {
                    m.put(key, keyTypeMapping.get(key));
                }
            }
        } else {
            m = new HashMap<String, String>(keyTypeMapping);
        }
        for (String key : keyTypeMapping.keySet()) {
            if (plainGraphML && (keyTypeMapping.get(key).indexOf(':') >= 0)) {
                bl.add(key);
            }
            if (bl.contains(key)) {
                m.remove(key);
            }
        }

        for (String key : m.keySet()) {
            Element keyElement = d.createElement("key");
            keyElement.setAttribute("id", key);
            keyElement.setAttribute("attr.type", m.get(key));
            root.appendChild(keyElement);
        }

        Element graphElement = d.createElement("graph");
        List<Element> anonElements = new ArrayList<Element>();
        List<String> usedIds = new ArrayList<String>();
        toXML(graphElement, plainGraphML, whiteList, bl, usedIds, anonElements);
        root.appendChild(graphElement);
        d.appendChild(root);

        int anonId = 0;
        for (Element ae : anonElements) {
            while (usedIds.contains("anonymous:" + Integer.toString(anonId))) {
                anonId++;
            }
            ae.setAttribute("id", "anonymous:" + Integer.toString(anonId));
            anonId++;
        }

        return d;
    }

    /**
     * Returns an extended GraphML document representing this graph.
     * 
     * @param blackList
     *            a set of {@link DataStore} keys not to be included in the
     *            Document
     * @return the GraphML document
     */
    public Document toGraphMLDocument(Set<String> blackList) {
        return toGraphMLDocument(false, null, blackList);
    }

    /**
     * Returns a GraphML document representing this graph.
     * 
     * @param plainGraphML
     *            whether to use plain or extended GraphML
     * @return the GraphML document
     */
    public Document toGraphMLDocument(boolean plainGraphML) {
        return toGraphMLDocument(plainGraphML, null, null);
    }

    /**
     * Returns an extended GraphML document representing this graph.
     * 
     * @return the GraphML document
     */
    public Document toGraphMLDocument() {
        return toGraphMLDocument(false, null, null);
    }

    /**
     * Returns a graph compliant with the given GraphML document
     * 
     * @param d
     *            the GraphML document
     * @return the newly created graph
     */
    public static Graph fromGraphMLDocument(Document d) {
        Element root = d.getDocumentElement();
        boolean plainGraphML = XMLUtils.getBooleanAttribute(root,
                "extended_graphml");

        Map<String, String> keyTypeMapping = new HashMap<String, String>();
        for (Element keyElement : XMLUtils.getChildrenByName(root, "key")) {
            String id = keyElement.getAttribute("id");
            String type = keyElement.getAttribute("attr.type");
            keyTypeMapping.put(id, type);
        }

        Element graphElement = XMLUtils.getChildByName(root, "graph");
        Object id = idFromXML(graphElement);
        Graph g = (id instanceof Integer ? new Graph(((Integer) id).intValue())
                : new Graph((String) id));
        g.fromXML(graphElement, keyTypeMapping);
        g.setDirected(XMLUtils.getStringAttribute(graphElement, "edgedefault")
                .equals("directed"));
        Map<String, Graph> subgraphsMap = new Hashtable<String, Graph>();
        g
                .nodesFromXML(graphElement, subgraphsMap, keyTypeMapping,
                        plainGraphML);
        g.edgesFromXML(graphElement, subgraphsMap, keyTypeMapping);
        return g;
    }

}

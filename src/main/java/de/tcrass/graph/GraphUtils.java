package de.tcrass.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import de.tcrass.graph.condition.GraphObjectCondition;

/**
 * This class provides a bunch of basic methods for examining and
 * creating {@link Graph}s
 */
public class GraphUtils {

    // --- Assign same data value to all nodes/edges ---

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Node}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose nodes are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all nodes
     */
    public static void setAllNodeData(Graph g, String key, int val) {
        setAllNodeData(g, key, Integer.valueOf(val));
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Node}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose nodes are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all nodes
     */
    public static void setAllNodeData(Graph g, String key, long val) {
        setAllNodeData(g, key, Long.valueOf(val));
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Node}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose nodes are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all nodes
     */
    public static void setAllNodeData(Graph g, String key, double val) {
        setAllNodeData(g, key, Double.valueOf(val));
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Node}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose nodes are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all nodes
     */
    public static void setAllNodeData(Graph g, String key, String val) {
        setAllNodeData(g, key, (Object) val);
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Node}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose nodes are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all nodes
     */
    public static void setAllNodeData(Graph g, String key, Object val) {
        for (Node n : g.getNodes()) {
            n.data().set(key, val);
        }
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Edge}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose edges are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all edges
     */
    public static void setAllEdgeData(Graph g, String key, int val) {
        setAllEdgeData(g, key, Integer.valueOf(val));
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Edge}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose edges are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all edges
     */
    public static void setAllEdgeData(Graph g, String key, long val) {
        setAllEdgeData(g, key, Long.valueOf(val));
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Edge}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose edges are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all edges
     */
    public static void setAllEdgeData(Graph g, String key, double val) {
        setAllEdgeData(g, key, Double.valueOf(val));
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Edge}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose edges are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all edges
     */
    public static void setAllEdgeData(Graph g, String key, String val) {
        setAllEdgeData(g, key, (Object) val);
    }

    /**
     * Stores the given value into the {@link de.tcrass.util.DataStore}s
     * of all {@link Edge}s owned by the given graph or (recursively)
     * one of its subgraphs, using the given key.
     * 
     * @param g
     *            the graph whose edges are to be processes
     * @param key
     *            the key under which to store the given value
     * @param val
     *            the value to store in all edges
     */
    public static void setAllEdgeData(Graph g, String key, Object val) {
        for (Edge e : g.getEdges()) {
            e.data().set(key, val);
        }
    }

    // --- Retrieve all Nodes/Edges satisfying a condition ---

    /**
     * Returns a {@link List} containing all {@link Node}s owned by the
     * given graph or (recursively) one of its subgraphs which satisfy
     * the given {@link GraphObjectCondition}.
     * 
     * @param g
     *            the graph to be processed
     * @param cond
     *            the {GraphObjectCondition} to verify the nodes against
     * @return the list containing the nodes
     */
    public static List<Node> getNodesSatisfying(Graph g, GraphObjectCondition cond) {
        List<Node> nodes = new ArrayList<Node>();
        for (Node n : g.getNodes()) {
            if (cond.satisfiedBy(n)) {
                nodes.add(n);
            }
        }
        return nodes;
    }

    /**
     * Returns a {@link List} containing all {@link Edge}s owned by the
     * given graph or (recursively) one of its subgraphs which satisfy
     * the given {@link GraphObjectCondition}.
     * 
     * @param g
     *            the graph to be processed
     * @param cond
     *            the GraphObjectCondition to verify the edges against
     * @return the list containing the edgess
     */
    public static List<Edge> getEdgesSatisfying(Graph g, GraphObjectCondition cond) {
        List<Edge> edges = new ArrayList<Edge>();
        for (Edge e : g.getEdges()) {
            if (cond.satisfiedBy(e)) {
                edges.add(e);
            }
        }
        return edges;
    }

    /**
     * Returns a {@link List} containing all this this graph's subgraphs
     * which satisfy the given {@link GraphObjectCondition}.
     * 
     * @param g
     *            the graph to be processed
     * @param cond
     *            the GraphObjectCondition to verify the subgraphs
     *            against
     * @return the list containing the subgraphs
     */
    public static List<Graph> getSubgraphsSatisfying(Graph g, GraphObjectCondition cond) {
        List<Graph> subgraphs = new ArrayList<Graph>();
        for (Graph sg : g.getSubgraphs()) {
            if (cond.satisfiedBy(sg)) {
                subgraphs.add(sg);
            }
        }
        return subgraphs;
    }

    /**
     * Returns a {@link List} containing all {@link Edge}s pointing
     * towards the given {@link Node} which satisfy the given
     * {@link GraphObjectCondition}.
     * 
     * @param n
     *            the node to be processed
     * @param cond
     *            the GraphObjectCondition to verify the edges against
     * @return the list containing the edgess
     */
    public static List<Edge> getInEdgesSatisfying(Node n, GraphObjectCondition cond) {
        List<Edge> edges = new ArrayList<Edge>();
        for (Edge e : n.getInEdges()) {
            if (cond.satisfiedBy(e)) {
                edges.add(e);
            }
        }
        return edges;
    }

    /**
     * Returns a {@link List} containing all {@link Edge}s pointing away
     * from the given {@link Node} which satisfy the given
     * {@link GraphObjectCondition}.
     * 
     * @param n
     *            the node to be processed
     * @param cond
     *            the GraphObjectCondition to verify the edges against
     * @return the list containing the edgess
     */
    public static List<Edge> getOutEdgesSatisfying(Node n, GraphObjectCondition cond) {
        List<Edge> edges = new ArrayList<Edge>();
        for (Edge e : n.getOutEdges()) {
            if (cond.satisfiedBy(e)) {
                edges.add(e);
            }
        }
        return edges;
    }

    /**
     * Returns a {@link List} containing all {@link Edge}s incident to
     * the given {@link Node} which satisfy the given
     * {@link GraphObjectCondition}.
     * 
     * @param n
     *            the node to be processed
     * @param cond
     *            the GraphObjectCondition to verify the edges against
     * @return the list containing the edgess
     */
    public static List<Edge> getIncidentEdgesSatisfying(Node n, GraphObjectCondition cond) {
        List<Edge> edges = getInEdgesSatisfying(n, cond);
        edges.addAll(getOutEdgesSatisfying(n, cond));
        return edges;
    }

    /**
     * Returns a {@link Set} containing all {@link Node}s connected to
     * the given node through incoming {@link Edge}s and which satisfy
     * the given {@link GraphObjectCondition}
     * 
     * @param n
     *            the node to be processed
     * @param edgeCond
     *            the GraphObjectCondition to verify incoming edges 
     *            against
     * @param nodeCond
     *            the GraphObjectCondition to verify the adjacent nodes
     *            against
     * @return the set containing the nodes
     */
    public static Set<Node> getPredecessorsSatisfying(Node n, GraphObjectCondition edgeCond,
                                                      GraphObjectCondition nodeCond) {
        Set<Node> nodes = new HashSet<Node>();
        for (Edge e : getInEdgesSatisfying(n, edgeCond)) {
            Node m = e.getTail();
            if (nodeCond.satisfiedBy(m)) {
                nodes.add(m);
            }
        }
        return nodes;
    }

    /**
     * Returns a {@link Set} containing all {@link Node}s connected to
     * the given node through outgoing {@link Edge}s and which satisfy
     * the given {@link GraphObjectCondition}
     * 
     * @param n
     *            the node to be processed
     * @param edgeCond
     *            the GraphObjectCondition to verify outgoing edges 
     *            against
     * @param nodeCond
     *            the GraphObjectCondition to verify the adjacent nodes
     *            against
     * @return the set containing the nodes
     */
    public static Set<Node> getSuccessorsSatisfying(Node n, GraphObjectCondition edgeCond,
                                                    GraphObjectCondition nodeCond) {
        Set<Node> nodes = new HashSet<Node>();
        for (Edge e : getOutEdgesSatisfying(n, edgeCond)) {
            Node m = e.getHead();
            if (nodeCond.satisfiedBy(m)) {
                nodes.add(m);
            }
        }
        return nodes;
    }

    /**
     * Returns a {@link Set} containing all {@link Node}s adjacent to
     * the given node which satisfy the given
     * {@link GraphObjectCondition}
     * 
     * @param n
     *            the node to be processed
     * @param cond
     *            the GraphObjectCondition to verify the adjacent nodes
     *            against
     * @return the set containing the nodes
     */
    public static Set<Node> getNeighboursSatisfying(Node n, GraphObjectCondition edgeCond,
                                                    GraphObjectCondition cond) {
        Set<Node> nodes = getPredecessorsSatisfying(n, edgeCond, cond);
        nodes.addAll(getSuccessorsSatisfying(n, edgeCond, cond));
        return nodes;
    }

    // --- Create random graphs ---

    /**
     * Adds a number of {@link Node}s to the given graph, assigning them
     * successive ascending integers as ids.
     * 
     * @param g
     *            the graph to add nodes to
     * @param minId
     *            the first in the succession of integers to be used as
     *            the nodes' ids
     * @param nNodes
     *            the number of nodes to add
     */
    public static void addNodes(Graph g, int minId, int nNodes) {
        for (int i = 0; i < nNodes; i++) {
            g.createNode(minId + i);
        }
    }

    /**
     * Adds a number of {@link Edge}s to the given graph which will
     * connect randomly picked {@link Node}s
     * 
     * @param g
     *            the graph to add edges to
     * @param nEdges
     *            the number of edges to add
     * @param r
     *            the {@link Random} stream to be used for picking nodes
     */
    public static void addRandomEdges(Graph g, int nEdges, Random r) {
        List<Node> nodes = g.getNodes();
        int nNodes = nodes.size();
        for (int i = 0; i < nEdges; i++) {
            g.createEdge(nodes.get(r.nextInt(nNodes)), nodes.get(r.nextInt(nNodes)));
        }
    }

    /**
     * Creates a graph containing the specified number of {@link Node}s
     * and {@link Edge}s, the latter connecting randomly picked nodes
     * 
     * @param directed
     *            whether or not to create a directed graph
     * @param nNodes
     *            the number of nodes to add to the graph
     * @param nEdges
     *            the number of edges to add to the graph
     * @param r
     *            the {@link Random} stream to be used for picking nodes
     * @return the created random graph
     */
    public static Graph createRandomGraph(boolean directed, int nNodes, int nEdges, Random r) {
        Graph g = new Graph(directed);
        addNodes(g, 0, nNodes);
        addRandomEdges(g, nEdges, r);
        return g;
    }

}

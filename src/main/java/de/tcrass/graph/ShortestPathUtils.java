package de.tcrass.graph;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * A class providing methods for calculating shortest pahts between
 * {@link Node}s within {@link Graph}s.
 */
public class ShortestPathUtils {

    private static final int   PRIORITY_QUEUE_INITIAL_CAPACITY = 11;

    /**
     * The default key under which distances to some start node will be
     * stored in the
     * 
     * {@link Node}s' {@link de.tcrass.util.DataStore}s.
     */
    public static final String DEFAULT_DATA_KEY_DISTANCE       = "__DISTANCE__";

    // --- Private methods ---

    /*
     * Creates a clone of the original graph an sets each node's
     * distance to +OO, except for the start node which gets a zero
     * distance assigned.
     */
    private static Graph createInitialShortestPathTreeFor(Graph g, Node start,
                                                          String distanceKey) {
        Graph t = new Graph();
        for (Node n : g.getNodes()) {
            Node m = n.createClone(t);
            m.data().set(distanceKey, Double.POSITIVE_INFINITY);
        }
        t.getNode(start.getId()).data().set(distanceKey, 0);
        return t;
    }

    /*
     * The actual shortesPath routine of the Dijkstra algorithm
     */
    private static void shortenPathByDijkstra(Graph t, Node mi, Node mj, Edge e,
                                              WeightFunction w, String distanceKey) {
        double dm = mi.data().getDouble(distanceKey) + w.getWeight(e);
        double dj = mj.data().getDouble(distanceKey);

        if (dm < dj) {
            mj.data().set(distanceKey, dm);
            for (int i = mj.getInEdges().size() - 1; i >= 0; i--) {
                t.removeEdge(mj.getInEdges().get(i));
            }
            t.createEdge(mi, mj);
        }
    }

    // --- Public methods ---

    /**
     * Creates an anonymous {@link WeightFunction} object merely
     * returning a GraphElement's "weight" attribute.
     * 
     * @return the weight function returning a GraphElement's default
     *         weight
     */
    public static WeightFunction createDefaultWeightFunction() {
        return new WeightFunction() {

            public double getWeight(GraphElement o) {
                return o.getWeight();
            }
        };
    }

    /**
     * Creates an anonymous {@link WeightFunction} object always
     * returning 1.
     * 
     * @return a weight function always returning 1
     */
    public static WeightFunction createUnityWeightFunction() {
        return new WeightFunction() {

            public double getWeight(GraphElement o) {
                return 1;
            }
        };
    }

    /**
     * Creates an anonymous {@link WeightFunction} returning for each
     * GraphElement the double value stored in its
     * {@link de.tcrass.util.DataStore} under the given key.
     * 
     * @param key
     *            the key of a double value in the GraphElement's
     *            DataStore
     * @return a weight function accessing the DataStore element
     *         addressed by the key
     */
    public static WeightFunction createDoubleWeightFunction(final String key) {
        return new WeightFunction() {

            public double getWeight(GraphElement o) {
                return o.data().getDouble(key);
            }
        };
    }

    /**
     * Creates an anonymous {@link WeightFunction} returning for each
     * GraphElement the long value (casted to double) stored in its
     * {@link de.tcrass.util.DataStore} under the given key.
     * 
     * @param key
     *            the key of a long value in the GraphElement's
     *            DataStore
     * @return a weight function accessing the DataStore element
     *         addressed by the key
     */
    public static WeightFunction createLongWeightFunction(final String key) {
        return new WeightFunction() {

            public double getWeight(GraphElement o) {
                return o.data().getLong(key);
            }
        };
    }

    /**
     * Creates a {@link java.util.Comparator} comparing the double
     * values stored in a node's {@link de.tcrass.util.DataStore} under
     * the given key, which will eventually contain the node's distance
     * to the start node of a shortest path search.
     * 
     * @param distanceKey
     *            the key of the double value to be used as distance
     *            measure
     * @return a Comparator comparing the double values stored under the
     *         given key in some Nodes' DataStores
     */
    public static Comparator<Node> createDistanceComparator(String distanceKey) {
        final String key = distanceKey;
        Comparator<Node> comp = new Comparator<Node>() {

            public int compare(Node n1, Node n2) {
                double d1 = n1.data().getDouble(key);
                double d2 = n2.data().getDouble(key);
                return -(d1 < d2 ? -1 : (d1 > d2 ? 1 : 0));
            }
        };
        return comp;
    }

    /**
     * Returns a graph representing the tree of shortest paths from the
     * given start node to all other (reachable) nodes. In case of a
     * directed graph, nodes unreachable from the start node will appear
     * as isolated nodes (i.e. the graph will no longer be connected).
     * <p>
     * The usual limitations for the Dijkstra algorithm apply; in
     * particular, negative edge weights are not allowed.
     * 
     * @param g
     *            the graph in which to search for shortest paths
     * @param start
     *            the start node for which to find shortest paths to all
     *            other (reachable) nodes
     * @param w
     *            the edge weight function
     * @param distanceKey
     *            the key under which to store each node's distance to
     *            the start node in its {link de.tcrass.DataStore}
     * @return a graph representing the shortest paths tree
     */
    public static Graph findShortestPathsByDijkstra(Graph g, Node start, WeightFunction w,
                                                    String distanceKey) {
        Graph t = createInitialShortestPathTreeFor(g, start, distanceKey);
        Node s = t.getNode(start.getId());
        PriorityQueue<Node> q = new PriorityQueue<Node>(PRIORITY_QUEUE_INITIAL_CAPACITY,
            createDistanceComparator(distanceKey));
        q.add(s);
        while (!q.isEmpty()) {
            Node mi = q.poll();
            Node ni = g.getNode(mi.getId());
            for (Edge e : ni.getOutEdges()) {
                Node nj = e.getHead();
                Node mj = t.getNode(nj.getId());
                if (Double.isInfinite(mj.data().getDouble(distanceKey))) {
                    q.offer(mj);
                }
                shortenPathByDijkstra(t, mi, mj, e, w, distanceKey);
            }
            if (!g.isDirected()) {
                for (Edge e : ni.getInEdges()) {
                    Node nj = e.getTail();
                    Node mj = t.getNode(nj.getId());
                    if (Double.isInfinite(mj.data().getDouble(DEFAULT_DATA_KEY_DISTANCE))) {
                        q.offer(mj);
                    }
                    shortenPathByDijkstra(t, mi, mj, e, w, distanceKey);
                }
            }
        }
        return t;
    }

    /**
     * Returns a graph representing the tree of shortest paths from the
     * given start node to all other (reachable) nodes. In case of a
     * directed graph, nodes unreachable from the start node will appear
     * as isolated nodes (i.e. the graph will no longer be connected).
     * <p>
     * The usual limitations for the Dijkstra algorithm apply; in
     * particular, negative edge weights are not allowed.
     * 
     * @param g
     *            the graph in which to search for shortest paths
     * @param id
     *            the id of the node for which to find shortest paths to
     *            all other (reachable) nodes; should be either an
     *            {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @param w
     *            the edge weight function
     * @param distanceKey
     *            the key under which to store each node's distance to
     *            the start node in its {link de.tcrass.DataStore}
     * @return a graph representing the shortest paths tree
     */
    public static Graph findShortestPathsByDijkstra(Graph g, Object id, WeightFunction w,
                                                    String distanceKey) {
        return findShortestPathsByDijkstra(g, g.getNode(id), w, distanceKey);
    }

    /**
     * Returns a graph representing the tree of shortest paths from the
     * given start node to all other (reachable) nodes. In case of a
     * directed graph, nodes unreachable from the start node will appear
     * as isolated nodes (i.e. the graph will no longer be connected).
     * <p>
     * The usual limitations for the Dijkstra algorithm apply; in
     * particular, negative edge weights are not allowed.
     * <p>
     * Each node's distance to the start node will be available through
     * its <code>DEFAULT_DATA_KEY_DISTANCE</code> DataStore element.
     * 
     * @param g
     *            the graph in which to search for shortest paths
     * @param start
     *            the start node for which to find shortest paths to all
     *            other (reachable) nodes
     * @param w
     *            the edge weight function
     * @return a graph representing the shortest paths tree
     */
    public static Graph findShortestPathsByDijkstra(Graph g, Node start, WeightFunction w) {
        return findShortestPathsByDijkstra(g, start, w, DEFAULT_DATA_KEY_DISTANCE);
    }

    /**
     * Returns a graph representing the tree of shortest paths from the
     * given start node to all other (reachable) nodes. In case of a
     * directed graph, nodes unreachable from the start node will appear
     * as isolated nodes (i.e. the graph will no longer be connected).
     * <p>
     * The usual limitations for the Dijkstra algorithm apply; in
     * particular, negative edge weights are not allowed.
     * <p>
     * Each node's distance to the start node will be available through
     * its <code>DEFAULT_DATA_KEY_DISTANCE</code> DataStore element.
     * 
     * @param g
     *            the graph in which to search for shortest paths
     * @param id
     *            the id of the node for which to find shortest paths to
     *            all other (reachable) nodes; should be either an
     *            {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @param w
     *            the edge weight function
     * @return a graph representing the shortest paths tree
     */
    public static Graph findShortestPathsByDijkstra(Graph g, Object id, WeightFunction w) {
        return findShortestPathsByDijkstra(g, id, w, DEFAULT_DATA_KEY_DISTANCE);
    }

    /**
     * Returns a graph representing the tree of shortest paths from the
     * given start node to all other (reachable) nodes using the default
     * weight function. In case of a directed graph, nodes unreachable
     * from the start node will appear as isolated nodes (i.e. the graph
     * will no longer be connected).
     * <p>
     * The usual limitations for the Dijkstra algorithm apply; in
     * particular, negative edge weights are not allowed.
     * <p>
     * Each node's distance to the start node will be available through
     * its <code>DEFAULT_DATA_KEY_DISTANCE</code> DataStore element.
     * 
     * @param g
     *            the graph in which to search for shortest paths
     * @param start
     *            the start node for which to find shortest paths to all
     *            other (reachable) nodes
     * @return a graph representing the shortest paths tree
     */
    public static Graph findShortestPathsByDijkstra(Graph g, Node start) {
        return findShortestPathsByDijkstra(g, start, createDefaultWeightFunction(),
            DEFAULT_DATA_KEY_DISTANCE);
    }

    /**
     * Returns a graph representing the tree of shortest paths from the
     * start node identified by the given id to all other (reachable)
     * nodes using the default weight function. In case of a directed
     * graph, nodes unreachable from the start node will appear as
     * isolated nodes (i.e. the graph will no longer be connected).
     * <p>
     * The usual limitations for the Dijkstra algorithm apply; in
     * particular, negative edge weights are not allowed.
     * <p>
     * Each node's distance to the start node will be available through
     * its <code>DEFAULT_DATA_KEY_DISTANCE</code> DataStore element.
     * 
     * @param g
     *            the graph in which to search for shortest paths
     * @param id
     *            the id of the node for which to find shortest paths to
     *            all other (reachable) nodes; should be either an
     *            {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @return a graph representing the shortest paths tree
     */
    public static Graph findShortestPathsByDijkstra(Graph g, Object id) {
        return findShortestPathsByDijkstra(g, id, createDefaultWeightFunction(),
            DEFAULT_DATA_KEY_DISTANCE);
    }

}

package de.tcrass.graph;

import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Element;

import de.tcrass.util.DataStore;

/**
 * A class representing a {@link Graph}'s Edges, i.&nbsp;e.&nbsp;
 * {@link GraphElement}s each connecting two {@link Node}s. Since Edge
 * objects always belong to a graph and cannot exist independently from
 * their tail and head nodes, they cannot be instantiated directly, but
 * only through the corresponding factory methods provided by the graph
 * class. According to the Graphviz data model, Edges have no unique ids
 * assigned to them. However, they may carry information about which
 * so-called "ports" of their tail and/or head nodes are docked to when
 * visualized by Graphviz. Please note, that, however, so-called
 * "compass points" are not supported by the Edge class. As subclass of
 * {@link GraphElement} and {@link GraphObject}, each Edge features an a
 * {@link DataStore} which holds, among any other data added by the
 * user, the Edge's coloring, weight and Graphviz attributes.
 */
public class Edge extends GraphElement {

    private Node tail, head;
    private String tailPort, headPort;

    // --- Constructors

    /**
     * Creates a new Edge instance with the given Properties as Graphviz
     * attributes (if not null) which connects the given tail node with
     * the given head node.
     * 
     * @param tail
     *            the node this Edge is coming from
     * @param head
     *            the node this Edge is pointing towards
     * @param attribs
     *            the Graphviz attributes to be assigned to the new Edge
     *            (may be null)
     */
    Edge(Node tail, Node head, Properties attribs) {
        super(null, attribs);
        this.tail = tail;
        tail.addOutEdge(this);
        this.head = head;
        head.addInEdge(this);
    }

    // --- Attribute access

    /**
     * Returns this Edge's tail node's port to which this Edge will be
     * docked when visualized by Graphviz.
     * 
     * @return the tail node's port
     */
    public String getTailPort() {
        return tailPort;
    }

    /**
     * Defines to which of the Edge's tail node's ports this Edge will
     * be docked to when visualized by Graphviz.
     * 
     * @param s
     *            the tail node's port
     */
    public void setTailPort(String s) {
        tailPort = s;
    }

    /**
     * Returns this Edge's head node's port to which this Edge will be
     * docked when visualized by Graphviz.
     * 
     * @return the head node's port
     */
    public String getHeadPort() {
        return headPort;
    }

    /**
     * Defines to which of the Edge's head node's ports this Edge will
     * be docked to when visualized by Graphviz.
     * 
     * @param s
     *            the head node's port
     */
    public void setHeadPort(String s) {
        headPort = s;
    }

    // --- Package-scope methods

    /**
     * Sets the node this Edge is coming from.
     * 
     * @param n
     *            the Edge's future tail node
     */
    void setTail(Node n) {
        tail = n;
    }

    /**
     * Sets the node this Edge is pointing towards.
     * 
     * @param n
     *            the Edge's future head node
     */
    void setHead(Node n) {
        head = n;
    }

    // --- Public methods

    @Override
    public String getDefaultGVAttributeKey() {
        return Graph.DATA_KEY_GRAPHVIZ_EDGE_ATTRIBUTES;
    }

    /**
     * Returns the node this Edge is coming from.
     * 
     * @return the Edge's tail node
     */
    public Node getTail() {
        return tail;
    }

    /**
     * Returns the node this Edge is pointing towards.
     * 
     * @return the Edge's head node
     */
    public Node getHead() {
        return head;
    }

    /**
     * Flips this Edge's direction, i. e. its head node for its tail
     * node and vice versa.
     */
    public void flip() {
        tail.removeOutEdge(this);
        head.removeInEdge(this);
        Node tmpNode = tail;
        tail = head;
        head = tmpNode;
        String tmpStr = tailPort;
        tailPort = headPort;
        headPort = tmpStr;
        tail.addOutEdge(this);
        head.addOutEdge(this);
    }

    /**
     * Returns a {@link java.lang.String} representation of this Edge
     * 
     * @return a String description of this Edge
     */
    @Override
    public String toString() {
        return "Edge(" + tail.toString() + "->" + head.toString() + ")";
    }

    // --- XML Stuff

    /**
     * Uses an XML element for creating an XML representation of this
     * Edge.
     * <p>
     * The element that may be used for representing this Edges in a
     * GraphML document. Since Edges don't carry individual ids
     * according to the Graphviz data model implemented by {@link Graph}
     * , a new anonymous id will be generated, and the XML element
     * representing this Edge will be added to the given List of
     * anonymous elements.
     * 
     * @param e
     *            XML element to be fille with this Edge's data
     * @param usedIds
     *            a list of already used anonymous ids
     * @param anonElements
     *            a list of XML elements representing the Edge
     */
    @Override
    protected void toXML(Element e, boolean plainGraphML, Set<String> whiteList,
                         Set<String> blackList, List<String> usedIds, List<Element> anonElements) {
        super.toXML(e, plainGraphML, whiteList, blackList, usedIds, anonElements);
        Node tail = getTail();
        e.setAttribute("source", tail.getIdString());
        if (!plainGraphML && (tail.getId() instanceof Integer)) {
            e.setAttributeNS(Graph.SCHEMA_NAME, Graph.getNSPrefix() + ":source_id_type",
                "numeric");
        }
        Node head = getHead();
        e.setAttribute("target", head.getIdString());
        if (!plainGraphML && (head.getId() instanceof Integer)) {
            e.setAttributeNS(Graph.SCHEMA_NAME, Graph.getNSPrefix() + ":target_id_type",
                "numeric");
        }
    }

    // --- Implementation of Cloneable

    /**
     * Creates a clone of this Edge which is NOT connected to any nodes;
     * do not call directly, but use any of the provided createClone
     * methods!
     */
    @Override
    public Object clone() {
        Edge c = (Edge) super.clone();
        c.tail = null;
        c.head = null;
        if (tailPort != null) {
            c.tailPort = new String(tailPort);
        }
        if (headPort != null) {
            c.headPort = new String(headPort);
        }
        return c;
    }

    /**
     * Creates a clone of this Edge connecting the given tail and head
     * node and adds it to the given target graph, the top level graph
     * of which must be (directly or indirectly) the owner of both
     * nodes.
     * 
     * @param target
     *            the graph the cloned Edge to add to
     * @param tail
     *            the cloned Edge's tail node
     * @param head
     *            the cloned Edge's head node
     * @return the new clone of this Edge
     */
    public Edge createClone(Graph target, Node tail, Node head) {
        return target.cloneEdge(this, tail, head);
    }

    /**
     * Creates within this Edge's owner graph a clone which connects the
     * given tail and head nodes, both of which must be (directly or
     * indirectly) owned by the same top level graph as this Edge.
     * 
     * @param tail
     *            the cloned Edge's tail node
     * @param head
     *            the cloned Edge's head node
     * @return the new clone of this Edge
     */
    public Edge createClone(Node tail, Node head) {
        return createClone(owner, tail, head);
    }

    /**
     * Creates a clone of this Edge connecting the tail and head nodes
     * identified by the given ids and adds it to the given target
     * graph, the top level graph of which must be (directly or
     * indirectly) the owner of both nodes.
     * 
     * @param target
     *            the graph the cloned Edge to add to
     * @param tailId
     *            the id of the cloned Edge's tail node; should be
     *            either an {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @param headId
     *            the id of the cloned Edge's head node; should be
     *            either an {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @return the new clone of this Edge
     */
    public Edge createClone(Graph target, Object tailId, Object headId) {
        return createClone(target, target.getTopLevelGraph().getNode(tailId),
            target.getTopLevelGraph().getNode(headId));
    }

    /**
     * Creates within this Edge's owner graph a clone which connects the
     * tail and head nodes identified by the given ids, both of which
     * must be (directly or indirectly) owned by the same top level
     * graph as this Edge.
     * 
     * @param tailId
     *            the id of the cloned Edge's tail node; should be
     *            either an {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @param headId
     *            the id of the cloned Edge's head node; should be
     *            either an {@link java.lang.Integer} or a
     *            {@link java.lang.String}
     * @return the new clone of this Edge
     */
    public Edge createClone(Object tailId, Object headId) {
        return createClone(owner, tailId, headId);
    }

    /**
     * Creates a clone of this Edge connecting those {@link Node}s
     * within the given target graph which feature the same ids as this
     * Edge's tail and head node, respectively, and adds it to the
     * target graph.
     * 
     * @param target
     *            the graph the cloned Edge to add to
     * @return the new clone of this Edge
     */
    public Edge createClone(Graph target) {
        return createClone(target, tail.getId(), head.getId());
    }

    /**
     * Creates a clone of this Edge connecting the same {@link Node}s as
     * this Edge and and adds it to the its owner graph.
     * 
     * @return the new clone of this Edge
     */
    public Edge createClone() {
        return createClone(owner, tail, head);
    }

}

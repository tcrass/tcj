package de.tcrass.graph;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Element;

import de.tcrass.util.Clonator;
import de.tcrass.util.CollectionUtils;
import de.tcrass.util.DataStore;
import de.tcrass.util.SysUtils;
import de.tcrass.xml.XMLUtils;

/**
 * The abstract superclass of all things needed to represent a graph,
 * including the {@link Graph} object itself.
 * <p>
 * All GraphObjects may have an id (should be either an Integer or a
 * String) and can carry arbitrary data stored in their internal
 * {@link DataStore}.
 * 
 * @author tcrass
 */
public abstract class GraphObject implements Cloneable {

    /**
     * Reserved key for storing AT&T Graphviz attributes in
     * GraphObject's DataStore.
     */
    public static final String DATA_KEY_GRAPHVIZ_ATTRIBUTES = "__GRAPHVIZ__";

    /**
     * The GraphObject's id. Must bei either an Integer or a String.
     */
    protected Object           id;

    /**
     * The graph to which this GraphObject belongs.
     */
    protected Graph            owner;

    /**
     * The GraphObjet's internal DataStore.
     */
    private DataStore          dataStore;

    // --- Constructors ---

    /**
     * Creates a new GraphObject instance with the given id and the
     * given Properties as Graphviz attributes (if not null).
     * 
     * @param id
     *            the object to be used as the new GraphObject's id
     *            (should be either an Integer or a String)
     * @param p
     *            the Graphviz attributes to be assigned to the new
     *            GraphObject (may be null)
     */
    protected GraphObject(Object id, Properties p) {
        setId(id);
        dataStore = new DataStore();
        dataStore.setCopyAction(DATA_KEY_GRAPHVIZ_ATTRIBUTES, DataStore.COPY_BY_VALUE);
        if (p != null) {
            setGVAttributes(p);
        }
        else {
            setGVAttributes(new Properties());
        }
    }

    // --- Protected methods ---

    /**
     * Sets this GraphObject's id.
     * 
     * @param id
     *            the object to be used as the GraphObject's new id
     *            (should be either an Integer or a String)
     */
    protected void setId(Object id) {
        this.id = id;
    }

    /**
     * Sets the given graph as this GraphObject's owner.
     * 
     * @param g
     *            the graph this GraphObject is to belongs to
     */
    protected void setOwner(Graph g) {
        owner = g;
    }

    // --- Public methods ---

    /**
     * Returns this GraphObject's id (should be either an Integer or a
     * String)
     * 
     * @return this GraphObjec'ts id
     */
    public Object getId() {
        return id;
    }

    /**
     * Returns a String representation of this GraphObject's id.
     * <p>
     * This will usually be either the id String itself, or, in case of
     * a numeric id, a String representation thereof. If no id has been
     * assigned, an empty String is returned.
     * 
     * @return this GraphObject's id (converted to a String, if it isn't
     *         one already)
     */
    public String getIdString() {
        return (id == null ? "" : id.toString());
    }

    /**
     * Returns a numeric representation of this GraphObject's id.
     * <p>
     * If a numeric id has been assigned, it will be returned directly;
     * in case of a String id, an attempt will be made to parse the
     * String as Integer value (may lead to a NumberFormatExcpetion).
     * <p>
     * If no id has been assigned, or the id cannot be converted to an
     * int value, 0 will be returned.
     * 
     * @return a numeric representation of this GraphObject's id
     */
    public int getIdCode() {
        int result = 0;
        if (id != null) {
            if (id instanceof Integer) {
                result = ((Integer) id).intValue();
            }
            else {
                result = Integer.parseInt((String) id);
            }
        }
        return result;
    }

    /**
     * Returns this GraphObject's owner.
     * 
     * @return the graph owningt this GraphObject
     */
    public Graph getOwner() {
        return owner;
    }

    /**
     * Returns this GraphObject's DataStore object.
     * 
     * @return this GraphObject's DataStore
     */
    public DataStore data() {
        return dataStore;
    }

    // --- Graphviz stuff

    /**
     * Returns the key under which this GraphObject's owner would store
     * its Graphviz default attributes in its DataStore.
     * 
     * @return this GraphObject's owner's DataStore key under which to
     *         find its Graphviz default values
     */
    public String getDefaultGVAttributeKey() {
        return DATA_KEY_GRAPHVIZ_ATTRIBUTES;
    }

    /**
     * Returns this GraphObject's Graphviz attributes.
     * 
     * @return this GraphObject's Graphviz attributes
     */
    public Properties getGVAttributes() {
        return dataStore.getProperties(DATA_KEY_GRAPHVIZ_ATTRIBUTES);
    }

    /**
     * Returns this GrapObject's Graphviz attribute denoted by the given
     * attribute name.
     * 
     * @param attrName
     *            the name of the Graphviz attribute to be returned
     * @return the requested Graphviz attribute value
     */
    public String getGVAttribute(String attrName) {
        return dataStore.getProperties(DATA_KEY_GRAPHVIZ_ATTRIBUTES).getProperty(attrName);
    }

    /**
     * Returns the value of the named Graphviz attribute that applies to
     * this GraphObject. If this GraphObject doesn't feature a such an
     * entry on its own, its owner graphs are successively checked for
     * whether they provide a matching entry in their corresponding
     * default settings until successful or the top level graph is
     * reached.
     * 
     * @param attrName
     *            the name of the Graphviz attribute under question
     * @return the attribute value applying to this GraphObject (or
     *         null, if no matching entry was found)
     */
    public String findGVAttribute(String attrName) {
        String val = dataStore.getProperty(DATA_KEY_GRAPHVIZ_ATTRIBUTES, attrName);
        Graph o = owner;
        while ((val == null) && (o != null)) {
            val = o.data().getProperty(getDefaultGVAttributeKey(), attrName);
            o = o.owner;
        }
        return val;
    }

    /**
     * Sets this GraphObject's Graphviz attributes.
     * 
     * @param attribs
     *            the Properties to be used as Graphviz attributes
     */
    public void setGVAttributes(Properties attribs) {
        dataStore.set(DATA_KEY_GRAPHVIZ_ATTRIBUTES, attribs);
    }

    /**
     * Sets this GrapObject's Graphviz attribute denoted by the given
     * attribute name to the given value.
     * 
     * @param key
     *            the name of the Graphviz attribute to be set
     * @param val
     *            the Graphviz attribute's new value
     */
    public void setGVAttribute(String key, String val) {
        dataStore.setProperty(DATA_KEY_GRAPHVIZ_ATTRIBUTES, key, val);
    }

    /**
     * Adds to this GraphObject's Graphviz attributes all values from p
     * which are not already present.
     * 
     * @param addattribs
     *            the Graphviz attributes to be added to this
     *            GraphObject
     */
    protected void addGVAttributes(Properties addattribs) {
        Properties q = getGVAttributes();
        CollectionUtils.mergeAdd(q, addattribs);
        setGVAttributes(addattribs);
    }

    /**
     * Sets this GrapObject's Graphviz coordinates attribute denoted by
     * the given attribute name to the given coordinate values according
     * to the Graphviz pointfList specification. The Graphviz
     * <code>pos</code> and <code>size</code> attributes may serve as
     * examples.
     * 
     * @param attrName
     *            the name of the Graphviz coordinate attribute to be
     *            set
     * @param coords
     *            the array of alternating x- and y-coordinates
     */
    public void setGVPointAttribute(String attrName, int... coords) {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < coords.length; i++) {
            s.append(coords[i]);
            if (i < (coords.length - 1)) {
                if ((i % 2) == 0) {
                    s.append(',');
                }
                else {
                    s.append(' ');
                }
            }
        }
        setGVAttribute(attrName, s.toString());
    }

    /**
     * Sets this GrapObject's Graphviz coordinates attribute denoted by
     * the given attribute name to the given coordinate values according
     * to the Graphviz pointfList specification. The Graphviz
     * <code>pos</code> and <code>size</code> attributes may serve as
     * examples.
     * 
     * @param attrName
     *            the name of the Graphviz coordinate attribute to be
     *            set
     * @param coords
     *            the array of alternating x- and y-coordinates
     */
    public void setGVPointFAttribute(String attrName, double... coords) {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < coords.length; i++) {
            s.append(coords[i]);
            if (i < (coords.length - 1)) {
                if ((i % 2) == 0) {
                    s.append(',');
                }
                else {
                    s.append(' ');
                }
            }
        }
        setGVAttribute(attrName, s.toString());
    }

    /**
     * Returns this GrapObject's Graphviz attribute denoted by the given
     * attribute name in the form of an n-element int value array parsed
     * according to the Graphviz pointfList attribute specification. In
     * other words, the attribute's String value should consist of
     * space-separated pairs of comma-separated double values, each of
     * which will (after conversion to int) be stored in the array just
     * in the order they appear within the String. Please note, however,
     * that this method will not check for strict alteration of commas
     * and spaces, but just splits the String at each occuring comma
     * (possibly surrounded by spaces) or pure space sequences in order
     * to separate the expected double values.
     * <p>
     * In most cases, the array will, of course, only contain two
     * values, i. e. the x-coordinate as element 0 and the y-coordinate
     * as element 1.
     * <p>
     * The Graphviz <code>pos</code> and <code>size</code> attributes
     * may serve as examples.
     * 
     * @param attrName
     *            the name of the Graphviz pointfList attribute to be
     *            returned
     * @return an array containing x- and y-coordinate(s) of the
     *         requested pointfList attribute
     */
    public int[] getGVPointAttribute(String attrName) {
        int[] coords = null;
        String[] parts = getGVAttribute(attrName).split("(\\s*,\\s*|\\s+)");
        coords = new int[parts.length];
        for (int i = 0; i < parts.length; i++) {
            coords[i] = Integer.parseInt(parts[i]);
        }
        return coords;
    }

    /**
     * Returns this GrapObject's Graphviz attribute denoted by the given
     * attribute name in the form of an n-element double value array
     * parsed according to the Graphviz pointfList attribute
     * specification. In other words, the attribute's String value
     * should consist of space-separated pairs of comma-separated double
     * values, each of which will be stored in the array just in the
     * order they appear within the String. Please note, however, that
     * this method will not check for strict alteration of commas and
     * spaces, but just splits the String at each occuring comma
     * (possibly surrounded by spaces) or pure space sequences in order
     * to separate the expected double values.
     * <p>
     * In most cases, the array will, of course, only contain two
     * values, i. e. the x-coordinate as element 0 and the y-coordinate
     * as element 1.
     * <p>
     * The Graphviz <code>pos</code> and <code>size</code> attributes
     * may serve as examples.
     * 
     * @param attrName
     *            the name of the Graphviz pointfList attribute to be
     *            returned
     * @return an array containing x- and y-coordinate(s) of the
     *         requested pointfList attribute
     */
    public double[] getGVPointFAttribute(String attrName) {
        double[] coords = null;
        String[] parts = getGVAttribute(attrName).split("(\\s*,\\s*|\\s+)");
        coords = new double[parts.length];
        for (int i = 0; i < parts.length; i++) {
            coords[i] = Double.parseDouble(parts[i]);
        }
        return coords;
    }

    /**
     * Copies data from the GraphObject's DataStore to its Graphviz
     * attributes according to the mapping provided by the given
     * {@link Properties}. In detail, a data values is tried to be
     * retrieved from the DataStore for each Property name, stringified
     * and set as Graphviz attribute using the corresponding Property
     * value as attribute name.
     * 
     * @param mapping
     *            the mapping telling which DataStore entry is to be
     *            used as which Graphviz attribute
     */
    public void copyDataToGVAttributes(Properties mapping) {
        for (Object object : mapping.keySet()) {
            String key = (String) object;
            Object data = data().get(key);
            if (data != null) {
                setGVAttribute(mapping.getProperty(key), data().get(key).toString());
            }
        }
    }

    /**
     * Returns a {@link java.lang.String} representation of this
     * GraphObject's id
     * 
     * @return a String description of this GraphObject
     */
    @Override
    public String toString() {
        return (id instanceof Integer ? ((Integer) id).toString() : "\"" + id + "\"");
    }

    // --- Implementation of Cloneable

    /**
     * Creates a clone of this GraphObject, including a clone of its
     * DataStore.
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Object clone() {
        GraphObject c = null;
        try {
            c = (GraphObject) super.clone();
            if (id != null) {
                if (id instanceof Integer) {
                    c.id = Integer.valueOf(((Integer) id).intValue());
                }
                else if (id instanceof String) {
                    c.id = new String((String) id);
                }
                // The following should actually never be necessary
                else {
                    c.id = Clonator.clone(id);
                }
            }
            c.owner = null;
            c.dataStore = (DataStore) dataStore.clone();
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
        return c;
    }

    // --- GraphML stuff ---

    /**
     * Uses an XML element for creating an XML representation of this
     * GraphObject.
     * <p>
     * The element that may be used for representing this GraphObject in
     * a GraphML document. If this GraphObject is anonymous (i.e. it has
     * a null id), a new anonymous id will be generated, and the XML
     * element representing this GraphObject will be added to the given
     * List of anonymous elements.
     * <p>
     * If you choose to create a plain GraphML representation of this
     * GraphObject, some data loss is likely to occur (see the
     * description of the {@link Graph} class for details).
     * 
     * @param e
     *            XML element to be fille with this GraphObject's data
     * @param plainGraphML
     *            whether to use plain or extended GraphML
     * @param usedIds
     *            a list of already used anonymous ids
     * @param anonElements
     *            a list of XML elements representing anonymous
     *            GraphObjects
     */
    protected void toXML(Element e, boolean plainGraphML, Set<String> whiteList,
                         Set<String> blackList, List<String> usedIds, List<Element> anonElements) {
        if (id != null) {
            e.setAttribute("id", getIdString());
            if (!plainGraphML) {
                if (id instanceof Integer) {
                    XMLUtils.setAttributeNS(e, Graph.SCHEMA_NAME, Graph.getNSPrefix() +
                                                                  ":id_type", "numeric");
                }
            }
            if (usedIds != null) {
                usedIds.add(getIdString());
            }
        }
        else if (anonElements != null) {
            if (!plainGraphML) {
                XMLUtils.setAttributeNS(e, Graph.SCHEMA_NAME, Graph.getNSPrefix() +
                                                              ":anonymous", true);
            }
            anonElements.add(e);
        }
        dataStore.toXML(e, whiteList, blackList, true);
    }

    /**
     * Creates a GraphObject-suitable id from an XML element.
     * 
     * @param e
     *            the XML element from which to create the id
     * @return the id generated from the given XML element
     */
    protected static Object idFromXML(Element e) {
        Object id = null;
        if (!XMLUtils.getBooleanAttribute(e, Graph.getNSPrefix() + ":anonymous")) {
            if (XMLUtils.getStringAttribute(e, Graph.getNSPrefix() + ":id_type").equals(
                "numeric")) {
                id = Integer.parseInt(e.getAttribute("id"));
            }
            else {
                id = e.getAttribute("id");
            }
        }
        return id;
    }

    /**
     * Restores this GraphObject's DataStore from the given XML element.
     * 
     * @param e
     *            the XML element containing the information to be
     *            restored as DataStore
     * @param keyTypeMapping
     *            a map telling which data keys are used and which data
     *            type they are of
     */
    protected void fromXML(Element e, Map<String, String> keyTypeMapping) {
        dataStore = DataStore.fromXML(e, keyTypeMapping);
    }

}

package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A base class for {@link de.tcrass.graph.GraphObjectCondition}s which
 * logically combine the outcomes of two other teste. Using multiple
 * such conditions, almost arbitrarily complex queries on
 * {@link GraphObject}s can be formulated.
 */
public abstract class LogicalCombinationCondition extends GraphObjectConditionImpl {

    /**
     * The one condition to be combined in a single test.
     */
    protected final GraphObjectCondition cond1;
    /**
     * The other condition to be combined in a single test.
     */
    protected final GraphObjectCondition cond2;

    /**
     * Creates a LogicalCombinationCondition object holding two other
     * GraphObjectConditions which may be used by subclasses to
     * calculate a combined (and possibly negated) test result.
     * 
     * @param cond1
     *            the one sub-condition
     * @param cond2
     *            the other sub-condition
     * @param negating
     *            whether or not to negate the test result
     */
    public LogicalCombinationCondition(GraphObjectCondition cond1, GraphObjectCondition cond2,
                                       boolean negating) {
        super(negating);
        this.cond1 = cond1;
        this.cond2 = cond2;
    }

}

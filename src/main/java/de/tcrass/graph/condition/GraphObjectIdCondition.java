package de.tcrass.graph.condition;

import de.tcrass.graph.Graph;
import de.tcrass.graph.GraphObject;
import de.tcrass.graph.Node;

/**
 * A {@link GraphObjectCondition} implementation testing whether a given
 * {@link GraphObject}'s id is (negation: not) identical to some value
 * specified during instantiation.
 * <p>
 * Rarely used since {@link Graph}s already provides methods for
 * retrieving {@link Node}s or subgraphs with a certain id.
 */
public class GraphObjectIdCondition extends GraphObjectConditionImpl {

    private final Object id;

    /**
     * Creates a GraphObjectIdCondition instance checking for
     * {@link GraphObject}s having (or, in case of negation, not having)
     * the given {@link String} id
     * 
     * @param id
     *            the id to check for
     * @param negating
     *            whether or not to test for the opposite
     */
    public GraphObjectIdCondition(String id, boolean negating) {
        super(negating);
        this.id = id;
    }

    /**
     * Creates a GraphObjectIdCondition instance checking for
     * {@link GraphObject}s having the given {@link String} id
     * 
     * @param id
     *            the id to check for
     */
    public GraphObjectIdCondition(String id) {
        this(id, false);
    }

    /**
     * Creates a GraphObjectIdCondition instance checking for
     * {@link GraphObject}s having (or, in case of negation, not having)
     * the given {@link String} id
     * 
     * @param id
     *            the id to check for
     * @param negating
     *            whether or not to test for the opposite
     */
    public GraphObjectIdCondition(int id, boolean negating) {
        super(negating);
        this.id = Integer.valueOf(id);
    }

    /**
     * Creates a GraphObjectIdCondition instance checking for
     * {@link GraphObject}s having the given {@link String} id
     * 
     * @param id
     *            the id to check for
     */
    public GraphObjectIdCondition(int id) {
        this(id, false);
    }

    /**
     * Tells whether or not the given {@link GraphObject} features an id
     * identical to the one given during instantiation of this
     * GraphObjectIdCondition object.
     * 
     * @param o
     *            the GraphObject under question
     * @return true, if the given GraphObject satisfies the above
     *         condition
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(id.equals(o.getId()));
    }

}

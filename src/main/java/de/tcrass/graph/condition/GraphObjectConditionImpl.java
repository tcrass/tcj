package de.tcrass.graph.condition;

/**
 * An abstract implementation of the {@link GraphObjectCondition}
 * interface providing means for negating the test result, if requested.
 */
public abstract class GraphObjectConditionImpl implements GraphObjectCondition {

    /**
     * Whether or not to negate the test result
     */
    protected final boolean negating;

    /**
     * Creates a GraphObjectConditionImpl instance which does or does
     * note negate the test result, depending on the given parameter.
     * 
     * @param negating
     *            whether or not to negate the test result
     */
    public GraphObjectConditionImpl(boolean negating) {
        this.negating = negating;
    }

    /**
     * Inverts the given value, if negation has been specified. Pass all
     * your test results through this function to benefit from the
     * negation capabilities of this class.
     * 
     * @param val
     *            the test result to be adjusted according to the
     *            negation setting
     * @return the test result, possibly negated
     */
    protected boolean resultFor(boolean val) {
        return (negating ? !val : val);
    }

}

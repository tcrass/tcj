package de.tcrass.graph.condition;

import de.tcrass.graph.Edge;
import de.tcrass.graph.Graph;
import de.tcrass.graph.GraphObject;
import de.tcrass.graph.GraphUtils;
import de.tcrass.graph.Node;

/**
 * This interface represents the concept of a condition a given
 * {@link GraphObject} may or may not satisfy. Whether or not the
 * condition represented by an Object implementating this interface is
 * satisfied is indicated by the {@link boolean} return value of its
 * satsifiedBy-method.
 * <p>
 * Objects implementing this interface may be used for selecting
 * GraphObjects (i.e. {@link Node}s, {@link Edge}s and subgraphs owned
 * by a {@link Graph} by using it as a Visitor for all those objects in
 * question. Methods making use of this concept are provided e. g. by
 * the {@link GraphUtils} class.
 */
public interface GraphObjectCondition {

    /**
     * Tells whether or not the given GraphObject satisfies the
     * condition represented by this GraphObjectCondition object
     * 
     * @param o
     *            the GraphObject under question
     * @return true, if the given GraphObject satisfies the condition
     */
    public boolean satisfiedBy(GraphObject o);

}

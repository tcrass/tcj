package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A {@link GraphObjectCondition} telling whether or not a {@link float}
 * item stored in its {@link de.tcrass.util.DataStore} and identified by
 * its key has (negation: doesn't have) a certain value
 */
public class FloatDataCondition extends GraphObjectConditionImpl {

    private String key;
    private double value;

    /**
     * Creates a GraphObjectCondition for checking whether a
     * GraphObject's data item identified by the given key has
     * (negation: doesn't have) the given {@link float} value
     * 
     * @param k
     *            the key identifying the data item to be examind within
     *            the GraphObject's DataStore
     * @param v
     *            the value the specified data item is to be checked for
     * @param negating
     *            whether or not to test for the opposite
     */
    public FloatDataCondition(String k, float v, boolean negating) {
        super(negating);
        key = k;
        value = v;
    }

    /**
     * Creates a GraphObjectCondition for checking whether a
     * GraphObject's data item identified by the given key has the given
     * {@link float} value
     * 
     * @param k
     *            the key identifying the data item to be examind within
     *            the GraphObject's DataStore
     * @param v
     *            the value the specified data item is to be checked for
     */
    public FloatDataCondition(String k, float v) {
        this(k, v, false);
    }

    /**
     * Tells whether or not the given GraphObject's data item identified
     * by the key given during instantiation has the value previously
     * specified
     * 
     * @param o
     *            the GraphObject under question
     * @return true, if the given GraphObject satisfies the above
     *         condition
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(o.data().getFloat(key) == value);
    }

}

package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A {@link GraphObjectCondition} telling whether or not a given
 * {@link de.tcrass.graph.GraphObject} features a Graphviz attribute
 * having (negation: not having) a certain value. If the GraphObject
 * doesn't have the property under question set itself, its owners are
 * successivley checked for a corresponding default setting until found
 * or the top level graph is reached.
 */
public class GVAttributeCondition extends GraphObjectConditionImpl {

    private String key;
    private String value;

    /**
     * Creates a GraphObjectCondition for checking whether a
     * GraphObject's Graphviz attribute identified by the given key has
     * (negation: doesn't have) the given {@link String} value
     * 
     * @param k
     *            the key identifying the Graphviz attribute to be
     *            examind
     * @param v
     *            the value the specified Graphviz attribute is to be
     *            checked for
     * @param negating
     *            whether or not to test for the opposite
     */
    public GVAttributeCondition(String k, String v, boolean negating) {
        super(negating);
        key = k;
        value = v;
    }

    /**
     * Creates a GraphObjectCondition for checking whether a
     * GraphObject's data item identified by the given key has the given
     * {@link String} value
     * 
     * @param k
     *            the key identifying the data item to be examind within
     *            the GraphObject's DataStore
     * @param v
     *            the value the specified data item is to be checked for
     */
    public GVAttributeCondition(String k, String v) {
        this(k, v, false);
    }

    /**
     * Tells whether or not the given GraphObject's data item identified
     * by the key given during instantiation has the value previously
     * specified
     * 
     * @param o
     *            the GraphObject under question
     * @return true, if the given GraphObject satisfies the above
     *         condition
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(value == null ? o.findGVAttribute(key) == null
                                      : value.equals(o.findGVAttribute(key)));
    }

}

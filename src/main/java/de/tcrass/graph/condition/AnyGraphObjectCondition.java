package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A sort of "dummy" condition compliant with either all or none of the
 * {@link GraphObject}s.
 */
public class AnyGraphObjectCondition extends GraphObjectConditionImpl {

    /**
     * Creates a GraphObjectCondition either accepting (@param negating}
     * = <code>false</code>) or rejecting (@param negating} =
     * <code>true</code>) all GraphObjects
     * 
     * @param acceptAll
     *            whether to accept all (false) or no (true)
     *            GraphObjects
     */
    public AnyGraphObjectCondition(boolean acceptAll) {
        super(!acceptAll);
    }

    public AnyGraphObjectCondition() {
        this(true);
    }

    /**
     * True for all {@link GraphObject}s
     * 
     * @return true
     */
    public boolean satisfiedBy(GraphObject o) {
        return resultFor(true);
    }

}

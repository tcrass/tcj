package de.tcrass.graph.condition;

import de.tcrass.graph.GraphObject;

/**
 * A {@link GraphObjectCondition} implementation testing whether a given
 * {@link GraphObject}'s id is numerical (i.&nbsp;e.&nbsp;an Integer) or
 * not (i.&nbsp;e.&nbsp;a String).
 */
public class GraphObjectIdTypCondition implements GraphObjectCondition {

    private boolean checkForNumericId;

    /**
     * Creates a GraphObjectIdTypeCondition instance checking for
     * {@link GraphObject}s having numerical or textual ids,
     * respectively.
     * 
     * @param checkForNumericId
     *            whether to check for numerical (true) or textual
     *            (false) ids
     */
    public GraphObjectIdTypCondition(boolean checkForNumericId) {
        this.checkForNumericId = checkForNumericId;
    }

    /**
     * Tells whether or not the given {@link GraphObject}'s id is of the
     * type specified during construction of this
     * GraphObjectIdTypeCondition instance.
     * 
     * @param o
     *            the GraphObject under question
     * @return true, if the given GraphObject satisfies the above
     *         condition
     */
    public boolean satisfiedBy(GraphObject o) {
        return checkForNumericId ? (o.getId() instanceof Integer)
                                : !(o.getId() instanceof Integer);
    }

}

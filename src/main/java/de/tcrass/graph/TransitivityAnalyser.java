package de.tcrass.graph;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Objects of this class are capable of generating a given directed
 * {@link Graph}'s transitive closure and reduction. Since both results
 * can be calculated following the basically same procedure, instances
 * of this class provide methods to access both result graphs after an
 * analysis has been performed.
 * 
 * @author tcrass
 */
public class TransitivityAnalyser {

    private Graph r, c;

    // --- Private methods ---

    /*
     * Does the actual closure and reduction calculation
     */
    private void createClosureAndReduction(Graph g, Graph c, Graph r, String topoNumKey) {

        TopologyUtils.assignTopologicalNumbering(g);

        Comparator<Node> comp = TopologyUtils.createTopologicalNumberingComparator(topoNumKey);

        Node[] nodes = g.getNodes().toArray(new Node[] {});
        Arrays.sort(nodes, comp);
        for (int i = nodes.length - 1; i >= 0; i--) {
            Node ni = nodes[i];
            Object idi = ni.getId();
            if (!c.containsEdge(idi, idi)) {
                c.createEdge(c.getNode(idi), c.getNode(idi));
            }
            Node[] successors = ni.getSuccessors().toArray(new Node[] {});
            Arrays.sort(successors, comp);
            for (int j = 0; j < successors.length; j++) {
                Node nj = successors[j];
                Object idj = nj.getId();
                if (!c.containsEdge(idi, idj)) {
                    for (int k = j; k < nodes.length; k++) {
                        Node nk = nodes[k];
                        Object idk = nk.getId();
                        if (!c.containsEdge(idi, idk)) {
                            if (c.containsEdge(idj, idk)) {
                                c.createEdge(c.getNode(idi), c.getNode(idk));
                            }
                        }
                    }
                    if (!r.containsEdge(idi, idj)) {
                        r.createEdge(r.getNode(idi), r.getNode(idj));
                    }
                }
            }
        }
    }

    // --- Public methods ---

    // --- Transitive Closure and Reduction

    /**
     * Performs the transitive closure and reduction calculation on the
     * given {@link Graph}. The topological numbering required by the
     * calculation gets stored in the original graph's {@link Node}s'
     * {@link de.tcrass.util.DataStore}'s under the given key.
     * 
     * @param g
     *            the graph on which to perform the transitive closure
     *            and reduction calculation
     * @param topoNumKey
     *            the key under which to store the topological numbering
     *            in the original graph's nodes
     */
    public void analyse(Graph g, String topoNumKey) {
        r = g.createUnconnectedClone();
        c = g.createUnconnectedClone();
        createClosureAndReduction(g, c, r, topoNumKey);
    }

    /**
     * Performs the transitive closure and reduction calculation on the
     * given {@link Graph}. The topological numbering required by the
     * calculation gets stored in the original graph's {@link Node}s'
     * {@link de.tcrass.util.DataStore}'s under the
     * <code>{@link TopologyUtils#DEFAULT_DATA_KEY_TOPOLOGICAL_NUMBER}</code>
     * key.
     * 
     * @param g
     *            the graph on which to perform the transitive closure
     *            and reduction calculation
     */
    public void analyse(Graph g) {
        analyse(g, TopologyUtils.DEFAULT_DATA_KEY_TOPOLOGICAL_NUMBER);
    }

    /**
     * Returns the transitive closure of the previously analysed
     * {@link Graph}.
     * 
     * @return the graph's transitive closure
     */
    public Graph getClosure() {
        return c;
    }

    /**
     * Returns the transitive reduction of the previously analysed
     * {@link Graph}.
     * 
     * @return the graph's transitive reduction
     */
    public Graph getReduction() {
        return r;
    }

}

package de.tcrass.graph;

public class GraphException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Graph             g;

    public GraphException(Graph g, String msg) {
        super(msg);
        this.g = g;
    }

    public GraphException(Graph g) {
        super();
        this.g = g;
    }

    public GraphException(String msg) {
        super(msg);
    }

    public Graph getGraph() {
        return g;
    }

}

package de.tcrass.awt.image.tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import de.tcrass.util.SysUtils;

public class makePalette {

    private static final int ENTRY_WIDTH  = 4;
    private static final int ENTRY_HEIGHT = 4;

    private static void printUsage() {
        System.out.println("Usage: makePalette [<steps_per_channel>] [-o <palette_file_name>]");
    }

    public static void main(String[] args) {

        if (args.length == 0) {
            printUsage();
            System.exit(0);
        }
        else {
            String paletteName = "palette.png";
            int steps = 4;
            // int logType = 0;

            int i = 0;
            while (i < args.length) {
                if (args[i].equals("-o")) {
                    i++;
                    paletteName = args[i];
                }
                // if (args[i].equals("-l")) {
                // i++;
                // logType = Integer.parseInt(args[i]);
                // }
                else {
                    steps = Integer.parseInt(args[i]);
                }
                i++;
            }

            int nCols = (steps + 1) * (steps + 1) * (steps + 1);
            int size = (int) Math.ceil(Math.sqrt(nCols));

            int u = 0;
            int v = 0;

            BufferedImage im = new BufferedImage(size * ENTRY_WIDTH, size * ENTRY_HEIGHT,
                BufferedImage.TYPE_INT_RGB);
            Graphics gr = im.getGraphics();
            for (int r = 0; r <= steps; r++) {
                int red = (int) (256 * (double) r / steps);
                if (red > 255) {
                    red = 255;
                }
                for (int g = 0; g <= steps; g++) {
                    int green = (int) (256 * (double) g / steps);
                    if (green > 255) {
                        green = 255;
                    }
                    for (int b = 0; b <= steps; b++) {
                        int blue = (int) (256 * (double) b / steps);
                        if (blue > 255) {
                            blue = 255;
                        }
                        Color c = new Color(red, green, blue);
                        gr.setColor(c);
                        int x = u * ENTRY_WIDTH;
                        int y = v * ENTRY_HEIGHT;
                        gr.fillRect(x, y, ENTRY_WIDTH, ENTRY_HEIGHT);
                        u++;
                        if (u >= size) {
                            u = 0;
                            v++;
                        }
                    }
                }
            }
            try {
                ImageIO.write(im, "png", new File(paletteName));
            }
            catch (IOException e) {
                SysUtils.reportException(e);
                System.exit(1);
            }
        }

    }
}

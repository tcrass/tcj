package de.tcrass.awt.image.tools;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

import de.tcrass.util.SysUtils;

public class tileImage {

    public static void main(String[] argv) {

        try {

            String source = argv[0];
            String target = argv[1];
            int sourceSliceWidth = Integer.parseInt(argv[2]);
            int sourceSliceHeight = Integer.parseInt(argv[3]);
            int targetSliceWidth = Integer.parseInt(argv[4]);
            int targetSliceHeight = (int) ((float) targetSliceWidth * (float) sourceSliceHeight / sourceSliceWidth);

            JComponent io = new JPanel();
            // byte[] imageBuffer = new byte[0];
            // byte[] tempBuffer = new byte[1024];
            // FileInputStream is = new FileInputStream(source);
            // int c = is.read(tempBuffer);
            // while (c > 0) {
            // byte[] newBuffer = new byte[imageBuffer.length + c];
            // System.arraycopy(imageBuffer, 0, newBuffer, 0,
            // imageBuffer.length);
            // System.arraycopy(tempBuffer, 0, newBuffer,
            // imageBuffer.length, c);
            // imageBuffer = newBuffer;
            // c = is.read(tempBuffer);
            // }
            // is.close();
            Image sourceImage = io.getToolkit().createImage(source);
            MediaTracker mt = new MediaTracker(io);
            mt.addImage(sourceImage, 0);
            mt.waitForAll();
            mt.removeImage(sourceImage);

            if ((sourceImage != null) && (sourceImage.getWidth(io) != -1) &&
                (sourceImage.getHeight(io) != -1)) {

                int i = 5;
                int x = 0;
                int y = 0;

                while (i < argv.length) {
                    String name = argv[i];
                    if (name.equals("\\")) {
                        x = 0;
                        y++;
                    }
                    else {
                        BufferedImage targetImage = new BufferedImage(targetSliceWidth,
                            targetSliceHeight, BufferedImage.TYPE_INT_ARGB);
                        Graphics2D g2 = targetImage.createGraphics();
                        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
                        g2.drawImage(sourceImage, 0, 0, targetSliceWidth, targetSliceHeight,
                            x * sourceSliceWidth, y * sourceSliceHeight, (x + 1) *
                                                                         sourceSliceWidth,
                            (y + 1) * sourceSliceHeight, io);
                        File imageFile = new File(target, name + ".png");
                        ImageIO.write(targetImage, "png", imageFile);
                        x++;
                    }
                    i++;
                }
            }
        }
        catch (Exception e) {
            SysUtils.doNothing();
        }
    }
}

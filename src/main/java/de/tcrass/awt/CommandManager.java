package de.tcrass.awt;

import java.awt.Component;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;

public class CommandManager extends Hashtable<String, List<Component>> {

    private static final long serialVersionUID = 1L;
    private boolean           locked           = false;

    public void add(Component c) {
        String key = null;
        if (c instanceof AbstractButton) {
            key = ((AbstractButton) c).getActionCommand();
        }
        if (key != null) {
            if (get(key) == null) {
                put(key, new Vector<Component>());
            }
            get(key).add(c);
        }
    }

    public void remove(Component c) {
        String key = null;
        if (c instanceof AbstractButton) {
            key = ((AbstractButton) c).getActionCommand();
        }
        if (key != null) {
            if (get(key) == null) {
                put(key, new Vector<Component>());
            }
            get(key).remove(c);
        }
    }

    public void disable(String key) {
        if (!locked) {
            if (get(key) != null) {
                for (Component c : get(key)) {
                    c.setEnabled(false);
                }
            }
        }
    }

    public void disableAll() {
        if (!locked) {
            for (String key : keySet()) {
                disable(key);
            }
        }
    }

    public void enable(String key) {
        if (!locked) {
            if (get(key) != null) {
                for (Component c : get(key)) {
                    c.setEnabled(true);
                }
            }
        }
    }

    public void enableAll() {
        if (!locked) {
            for (String key : keySet()) {
                enable(key);
            }
        }
    }

    public void setEnabled(String key, boolean state) {
        if (!locked) {
            if (get(key) != null) {
                if (state) {
                    enable(key);
                }
                else {
                    disable(key);
                }
            }
        }
    }

    public void setLocked(boolean lock) {
        locked = lock;
    }

    public boolean isLocked() {
        return locked;
    }

}

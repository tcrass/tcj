package de.tcrass.awt.dnd;

import java.awt.Point;

public interface DnDTarget {

    void draggedItemEntered(DnDManager dm, Object item, Point pos);

    void draggedItemExited(DnDManager dm, Object item, Point pos);

    boolean acceptDraggedItem(DnDManager dm, Object item, Point pos);

}

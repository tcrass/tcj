package de.tcrass.awt.dnd;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

import de.tcrass.awt.AWTUtils;
import de.tcrass.util.SysUtils;

public class DnDManager extends Component
implements AWTEventListener, MouseListener, MouseMotionListener {

    // --- constants
    // -----------------------------------------------------

    /**
     * 
     */
    private static final long serialVersionUID           = 1L;

    public static final int   DEFAULT_MIN_DRAG_DELTA     = 2;

    // consts regarding the DnDManager's state
    public static final int   DRAGGING_IDLE              = 0;

    public static final int   DRAGGING_STARTED           = 1;

    public static final int   DRAGGING_IN_PROGRESS       = 2;

    public static final int   DRAGGING_OVER              = 4;

    // consts defining basic dragging mode
    private static final int  DRAG_MODE_NONE             = 0;
    private static final int  DRAG_MODE_MOVE             = 1;
    private static final int  DRAG_MODE_RESIZE           = 2;

    // constants regarding restrictions on drag directions
    private static final int  DRAG_HORIZONTALLY          = 1;
    private static final int  DRAG_VERTICALLY            = 2;
    private static final int  DRAG_ORTHOGONAL            = DRAG_HORIZONTALLY + DRAG_VERTICALLY;

    private static final int  DRAG_RESTRICTIONS_MASK     = DRAG_ORTHOGONAL;

    // constants somehow modifying drag behaviour

    public static final int   DRAG_RESTRICT_ON_SHIFT_KEY = 4;
    public static final int   DRAG_BORDER_HANDLES        = 8;

    public static final int   DRAG_MOVE_TRANSFER         = 16;

    public static final int   DRAG_MOVE_COMPONENT        = 256;
    public static final int   DRAG_RESIZE_COMPONENT      = 512;
    public static final int   DRAG_TRANSFER_COMPONENT    = 1024;
    public static final int   DRAG_DELETE_COMPONENT      = 2048;

    public static final int   DRAG_AUTOSCROLL            = 4096;

    // private static final int DRAG_MODIFYERS_MASK =
    // DRAG_RESTRICT_ON_SHIFT_KEY
    // + DRAG_BORDER_HANDLES
    // + DRAG_MOVE_TRANSFER
    // + DRAG_MOVE_COMPONENT
    // + DRAG_TRANSFER_COMPONENT
    // + DRAG_DELETE_COMPONENT
    // + DRAG_AUTOSCROLL;

    // const defining which resize handle is in use
    public static final int   DRAG_HANDLE_NORTH          = Rectangle2D.OUT_TOP;

    public static final int   DRAG_HANDLE_EAST           = Rectangle2D.OUT_RIGHT;

    public static final int   DRAG_HANDLE_SOUTH          = Rectangle2D.OUT_BOTTOM;

    public static final int   DRAG_HANDLE_WEST           = Rectangle2D.OUT_LEFT;

    public static final int   DRAG_HANDLE_NORTHEAST      = DRAG_HANDLE_NORTH + DRAG_HANDLE_EAST;

    public static final int   DRAG_HANDLE_SOUTHEAST      = DRAG_HANDLE_SOUTH + DRAG_HANDLE_EAST;

    public static final int   DRAG_HANDLE_SOUTHWEST      = DRAG_HANDLE_SOUTH + DRAG_HANDLE_WEST;

    public static final int   DRAG_HANDLE_NORTHWEST      = DRAG_HANDLE_NORTH + DRAG_HANDLE_WEST;

    // --- private properties
    // --------------------------------------------

    // container containing all relevant components, usually root frame
    private Container         owner;

    // last event (used for recognizing drag gestures)
    private MouseEvent        lastEvent                  = null;

    // position of last mousePressed event
    private Point             lastDown;

    // the DnDManager's state
    private int               dragState;

    // where do the mouse events come from?
    private Object            eventSource;

    // minimum drag distance before dragging starts
    private int               minDragDelta               = DEFAULT_MIN_DRAG_DELTA;

    // coords of mouse click initiating drag
    private Point             dragStart;

    // the component that delivered the item to be dragged
    private Component         dragSource;

    // drag mode and restrictions
    private int               dragPolicy;

    // current mouseDragged event
    private MouseEvent        dragEvent;

    // the item to be dragged
    private Object            draggedItem;

    // a component representing the dragged item
    private Component         dragDummy;

    // rel. position of mouse within dragged item
    private Point             dragHandlePoint;

    // resize: which corner or border is the mouse in?
    private int               dragHandleType;

    // half the size of dragged item
    private Point             itemCenter;

    // size of dragged item
    private Point             itemSize;

    // width of resize-sensitive borders
    private int               dragInset;

    // move or resize?
    private int               dragMode;

    // the component in charge of painting the dragged item (DnDManager,
    // if null)
    private Component         dragMedium;
    private DnDTarget         lastDropTarget;

    // the component on which the item is dropped
    private Component         dropTarget;

    // did the drop target accept the item?
    private boolean           itemAccepted;

    // --- constructors
    // --------------------------------------------------

    public DnDManager(Container owner) {
        this.owner = owner;
        owner.getToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK);
        owner.getToolkit().addAWTEventListener(this, AWTEvent.MOUSE_MOTION_EVENT_MASK);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    // --- public methods
    // ------------------------------------------------

    // --- property access methods ---

    public Container getOwner() {
        return owner;
    }

    public Component getDnDSource() {
        return dragSource;
    }

    public Component getDnDTarget() {
        return dropTarget;
    }

    // public void setPaintDnDItem(boolean paintItem) {
    // this.paintItem = paintItem;
    // }
    //
    public int getDnDState() {
        return dragState;
    }

    public void setDnDMedium(Component dragMedium) {
        if (dragMedium instanceof DnDMedium) {
            this.dragMedium = dragMedium;
        }
    }

    public Component getDnDMedium() {
        return dragMedium;
    }

    public Object getDnDItem() {
        return draggedItem;
    }

    public void setDragPolicy(int mode) {
        dragPolicy = mode;
    }

    public int getDragPolicy() {
        return dragPolicy;
    }

    public void setDragHandleInset(int inset) {
        dragInset = inset;
    }

    public int getDragHandleInset() {
        return dragInset;
    }

    public void setMinDragDelta(int delta) {
        minDragDelta = delta;
    }

    public int getMinDragDelta() {
        return minDragDelta;
    }

    // --- geometry stuff ---

    // --- transforms coords from source component's to target
    // component's
    // coordinate system
    public Point transformDnDCoords(Point where, Component source, Component target) {
        Point whereNew = new Point(where);
        whereNew.translate(-target.getLocationOnScreen().x + source.getLocationOnScreen().x,
            -target.getLocationOnScreen().y + source.getLocationOnScreen().y);
        /*
         * Graphics g = target.getGraphics(); g.setColor(Color.red);
         * g.drawLine((int)whereNew.getX()-1, (int)whereNew.getY()-1,
         * (int)whereNew.getX()+1, (int)whereNew.getY()+1);
         * g.drawLine((int)whereNew.getX()-1, (int)whereNew.getY()+1,
         * (int)whereNew.getX()+1, (int)whereNew.getY()-1);
         */
        return whereNew;
    }

    // --- returns dragged items bound relative to target, taking into
    // consideration drag restrictions
    public Rectangle getDropBounds(Point where, int modifiers, Component item, Component target) {
        Rectangle r = null;
        Point dropWhere = getDropPoint(where, modifiers);
        int dx = dropWhere.x - dragStart.x;
        int dy = dropWhere.y - dragStart.y;
        if (dragMode == DRAG_MODE_MOVE) {
            Point pos = transformDnDCoords(dropWhere, (Component) eventSource, target);
            pos.translate(-dragHandlePoint.x, -dragHandlePoint.y);
            r = new Rectangle(pos.x, pos.y, itemSize.x, itemSize.y);
        }
        else {
            int cx = dragHandlePoint.x - itemCenter.x;
            int cy = dragHandlePoint.y - itemCenter.y;
            int hx = itemSize.x - dragHandlePoint.x;
            int hy = itemSize.y - dragHandlePoint.y;
            Point p1 = new Point(0, 0);
            Point p2 = new Point(itemSize);
            if ((dragHandleType & DRAG_HANDLE_NORTH) != 0) {
                p1.setLocation(p1.x, itemCenter.y + cy - dragHandlePoint.y + dy);
                if ((p2.y - p1.y) < item.getMinimumSize().height) {
                    p1.setLocation(p1.x, p2.y - item.getMinimumSize().height);
                }
            }
            if ((dragHandleType & DRAG_HANDLE_EAST) != 0) {
                p2.setLocation(itemCenter.x + cx + hx + dx, p2.y);
                if ((p2.x - p1.x) < item.getMinimumSize().width) {
                    p2.setLocation(p1.x + item.getMinimumSize().width, p2.y);
                }
            }
            if ((dragHandleType & DRAG_HANDLE_SOUTH) != 0) {
                p2.setLocation(p2.x, itemCenter.y + cy + hy + dy);
                if ((p2.y - p1.y) < item.getMinimumSize().height) {
                    p2.setLocation(p2.x, p1.y + item.getMinimumSize().height);
                }
            }
            if ((dragHandleType & DRAG_HANDLE_WEST) != 0) {
                p1.setLocation(itemCenter.x + cx - dragHandlePoint.x + dx, p1.y);
                if ((p2.x - p1.x) < item.getMinimumSize().width) {
                    p1.setLocation(p2.x - item.getMinimumSize().width, p1.y);
                }
            }
            p1 = transformDnDCoords(p1, item, target);
            p2 = transformDnDCoords(p2, item, target);
            r = new Rectangle(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
        }
        return r;
    }

    // --- drag gesture methods, implementing AWTEventListener,
    // MouseListener &
    // MouseMotionListener ---

    public void eventDispatched(AWTEvent e) {
        processEvent(e);
    }

    public void mousePressed(MouseEvent event) {
        if (eventHasOwnSource(event)) {
            lastEvent = event; // this might be the beginning of a
            // drag...
            lastDown = event.getPoint();
        }
    }

    // --- drag starts and proceeds here
    public void mouseDragged(MouseEvent event) {
        if (lastEvent != null) {
            dragEvent = event;
            if (lastDown != null) {
                int downDelta = Math.abs(event.getPoint().x - lastDown.x) +
                                Math.abs(event.getPoint().y - lastDown.y);
                reset(lastEvent);
                if (eventHasOwnSource(event) && ((downDelta > minDragDelta))) {
                    // a drag has begun indeed...
                    lastDown = null;
                    // see if an item can be retrieved from some drag
                    // source at mouse
                    // coords
                    try {
                        dragSource = (((Container) eventSource).findComponentAt(lastEvent.getPoint()));
                    }
                    catch (ClassCastException cce) {
                        SysUtils.doNothing();
                    }
                    while ((draggedItem == null) && (dragSource != null)) {
                        if (dragSource instanceof DnDSource) {
                            // try {
                            draggedItem = ((DnDSource) dragSource).getDraggableItem(this); // might
                            // lead
                            // to
                            // ClassCastException
                            dragState = DRAGGING_STARTED;

                            if (draggedItem != null) {
                                if (draggedItem instanceof Component) {
                                    Component c = (Component) draggedItem;
                                    itemSize = new Point(c.getWidth(), c.getHeight());
                                    if (c.getParent() != null) {
                                        dragHandlePoint = transformDnDCoords(
                                            lastEvent.getPoint(),
                                            (Component) lastEvent.getSource(), c);
                                        itemCenter = new Point((c.getWidth() / 2),
                                            (c.getHeight() / 2));
                                    }
                                    else {
                                        dragHandlePoint = new Point((c.getWidth() / 2),
                                            (c.getHeight() / 2));
                                        itemCenter = new Point(dragHandlePoint);
                                    }
                                    dragDummy.setLocation(c.getLocation());
                                    dragDummy.setSize(itemSize.x, itemSize.y);
                                }
                                determineDragMode();
                                if (dragMode != DRAG_MODE_NONE) {
                                    dragState = DRAGGING_IN_PROGRESS;
                                    if (dragMedium != null) {
                                        ((DnDMedium) dragMedium).dragStarted(this, draggedItem,
                                            transformDnDCoords(getDropPoint(event.getPoint(),
                                                lastEvent.getModifiers()),
                                                (Component) eventSource, dragMedium));
                                        ((DnDMedium) dragMedium).itemDragged(this, draggedItem,
                                            transformDnDCoords(getDropPoint(event.getPoint(),
                                                event.getModifiers()), (Component) eventSource,
                                                dragMedium));
                                    }
                                    lastDropTarget = findDropTargetAt(lastEvent);
                                    if (lastDropTarget != null) {
                                        lastDropTarget.draggedItemEntered(this, draggedItem,
                                            event.getPoint());
                                    }
                                    paintDraggedItem();
                                }
                                else {
                                    dragState = DRAGGING_IDLE;
                                }
                            }
                        }
                        // catch (ClassCastException cce)
                        // {System.out.println(cce.toString());}
                        // catch (NullPointerException npe)
                        // {System.out.println(npe.toString());}
                        if (draggedItem == null) {
                            dragSource = dragSource.getParent();
                        }
                    }
                }
            }
            else if (eventHasOwnSource(event) &&
                     (lastEvent.getID() == MouseEvent.MOUSE_DRAGGED)) {
                if (dragState == DRAGGING_IN_PROGRESS) {
                    // dragging is in progress already
                    // paintDraggedItem(lastEvent.getPoint(),
                    // lastEvent.getModifiers(),
                    // false);
                    if (dragMedium != null) {
                        ((DnDMedium) dragMedium).itemDragged(this, draggedItem,
                            transformDnDCoords(getDropPoint(event.getPoint(),
                                event.getModifiers()), (Component) eventSource, dragMedium));
                    }
                    if (((dragPolicy & DRAG_AUTOSCROLL) != 0) &&
                        (dragMedium instanceof JComponent)) {
                        Point rel = transformDnDCoords(event.getPoint(),
                            (Component) event.getSource(), dragMedium);
                        Rectangle r = new Rectangle(rel.x, rel.y, 1, 1);
                        ((JComponent) dragMedium).scrollRectToVisible(r);
                    }
                    paintDraggedItem();
                }
            }
            if (eventHasOwnSource(event) && (dragState != DRAGGING_IDLE)) {
                DnDTarget t = findDropTargetAt(event);
                if (t != lastDropTarget) {
                    if (lastDropTarget != null) {
                        lastDropTarget.draggedItemExited(this, draggedItem, event.getPoint());
                    }
                    lastDropTarget = t;
                    if (lastDropTarget != null) {
                        lastDropTarget.draggedItemEntered(this, draggedItem, event.getPoint());
                    }
                }
            }
            lastEvent = event;
        }
    }

    // --- drag ends here
    public void mouseReleased(MouseEvent event) {
        if (dragState == DRAGGING_IN_PROGRESS) {
            // seems dragging is over
            dragState = DRAGGING_OVER;
            // deliver item to drop target
            itemAccepted = false;

            if (lastDropTarget != null) {
                lastDropTarget.draggedItemExited(this, draggedItem, event.getPoint());
            }

            if (eventHasOwnSource(event)) {
                if ((dragPolicy & DRAG_MOVE_TRANSFER) != 0) {
                    // care for drop target accepting the item only if
                    // drag is declared a
                    // transfer
                    // dropTarget =
                    // (owner.findComponentAt(transformDnDCoords(
                    // getDropPoint(event.getPoint(),
                    // event.getModifiers()), (Component)
                    // event.getSource(), owner)));
                    Window w = AWTUtils.getContainingWindow((Component) event.getSource());
                    dropTarget = (w.findComponentAt(transformDnDCoords(getDropPoint(
                        event.getPoint(), event.getModifiers()), (Component) event.getSource(),
                        w)));
                    while (!itemAccepted && (dropTarget != null)) {
                        try {
                            DnDTarget t = (DnDTarget) dropTarget;
                            itemAccepted = t.acceptDraggedItem(this, draggedItem,
                                event.getPoint());
                        }
                        catch (ClassCastException e) {
                            SysUtils.doNothing();
                        }
                        if (!itemAccepted) {
                            dropTarget = dropTarget.getParent();
                        }
                    }
                    if ((draggedItem instanceof Component) && (dragSource instanceof Container)) {
                        Component c = (Component) draggedItem;
                        if (!itemAccepted && ((dragPolicy & DRAG_DELETE_COMPONENT) != 0)) {
                            ((Container) dragSource).remove(c);
                        }
                        if (itemAccepted && ((dragPolicy & DRAG_TRANSFER_COMPONENT) != 0) &&
                            (dropTarget instanceof Container)) {
                            ((Container) dragSource).remove(c);
                            ((Container) dropTarget).add(c);
                            Rectangle bounds = getDropBounds(event.getPoint(),
                                event.getModifiers(), c, dropTarget);
                            c.setBounds(bounds);
                        }
                    }
                }
                else if ((draggedItem instanceof Component) &&
                         ((dragPolicy & DRAG_MOVE_COMPONENT) != 0)) {

                    Component c = (Component) draggedItem;
                    dropTarget = c.getParent();
                    if (dropTarget != null) {
                        // only alter items bounds if contained
                        // somewhere
                        Rectangle bounds = getDropBounds(event.getPoint(),
                            event.getModifiers(), c, dropTarget);
                        Rectangle relBounds = new Rectangle(bounds);
                        relBounds.translate(dropTarget.getX(), dropTarget.getY());
                        boolean intersects = dropTarget.getBounds().intersects(relBounds);
                        if (intersects) {
                            c.setBounds(bounds);
                            ((DnDSource) c).itemDropped(this, draggedItem, dropTarget);
                        }
                        else {
                            if ((dragPolicy & DRAG_DELETE_COMPONENT) != 0) {
                                // remove item if outside target bounds
                                // and delete allowed
                                ((DnDSource) dragSource).itemDropped(this, draggedItem, null);
                                ((Container) dropTarget).remove(c);
                            }

                        }
                    }
                }
            }

            if (itemAccepted) {
                if (dragMedium != null) {
                    ((DnDMedium) dragMedium).itemDropped(this, draggedItem, dropTarget);
                }
                ((DnDSource) dragSource).itemDropped(this, draggedItem, dropTarget);
            }
            else {
                if (dragMedium != null) {
                    ((DnDMedium) dragMedium).itemDropped(this, draggedItem, null);
                }
                ((DnDSource) dragSource).itemDropped(this, draggedItem, null);
            }
            if (dragMedium == null) {
                owner.repaint();
            }
            else {
                dragMedium.repaint();
            }
            dragMedium = null;
            dragState = DRAGGING_IDLE;
        }
        lastEvent = null;

        // if ((dragPolicy & DRAG_MOVE_TRANSFER) != 0) {
        // // care for drop target only if drag is declared a transfer
        // dropTarget =
        // (owner.findComponentAt(transformDnDCoords(getDropPoint(
        // event.getPoint(), event.getModifiers()), (Component) event
        // .getSource(), owner)));
        // while (!itemAccepted && dropTarget != null) {
        // try {
        // DnDTarget t = (DnDTarget) dropTarget;
        // itemAccepted = t.acceptDnDItem(this, draggedItem,
        // event.getPoint());
        // if (itemAccepted && ((dragPolicy & DRAG_TRANSFER_COMPONENT)
        // != 0)
        // && (draggedItem instanceof Component)) {
        // Component c = (Component) draggedItem;
        // if (dropTarget instanceof Container) {
        // if (c.getParent() != null) {
        // c.getParent().remove(c);
        // }
        // ((Container) dropTarget).add(c);
        // }
        // Rectangle bounds = getDropBounds(event.getPoint(), event
        // .getModifiers(), c, dropTarget);
        // c.setBounds(bounds);
        // }
        // }
        // catch (ClassCastException e) {
        // SysUtils.doNothing();
        // }
        // if (!itemAccepted) {
        // dropTarget = dropTarget.getParent();
        // }
        // }
        // if (itemAccepted) {
        // if (dragMedium != null) {
        // ((DnDMedium) dragMedium).itemDropped(this, draggedItem,
        // dropTarget);
        // }
        // ((DnDSource) dragSource).itemDropped(this, draggedItem,
        // dropTarget);
        // }
        // else {
        // if (dragMedium != null) {
        // ((DnDMedium) dragMedium).itemDropped(this, draggedItem,
        // null);
        // }
        // ((DnDSource) dragSource).itemDropped(this, draggedItem,
        // null);
        // }
        // }
        // else if (draggedItem instanceof Component) {
        // // drags which aren't transfers only make sense with
        // components as items
        // Component c = (Component) draggedItem;
        // dropTarget = c.getParent();
        // if (dropTarget != null) {
        // // only alter items bounds if contained somewhere
        // Rectangle bounds = getDropBounds(event.getPoint(), event
        // .getModifiers(), c, dropTarget);
        // Rectangle relBounds = new Rectangle(bounds);
        // relBounds.translate(dropTarget.getX(), dropTarget.getY());
        // boolean intersects =
        // dropTarget.getBounds().intersects(relBounds);
        // if (intersects) {
        // c.setBounds(bounds);
        // ((DnDSource) c).itemDropped(this, draggedItem, dropTarget);
        // }
        // else {
        // if ((dragPolicy & DRAG_MOVE_DELETE) != 0) {
        // // remove item if outside target bounds and delete allowed
        // ((DnDSource) dragSource).itemDropped(this, draggedItem,
        // null);
        // ((Container) dropTarget).remove(c);
        // }
        // }
        // }
        // }
    }

    public void mouseMoved(MouseEvent event) {
    }

    public void mouseEntered(MouseEvent event) {
    }

    public void mouseExited(MouseEvent event) {
    }

    public void mouseClicked(MouseEvent event) {
    }

    // --- private methods
    // ----------------------------------------------

    private DnDTarget findDropTargetAt(MouseEvent event) {
        DnDTarget t = null;
        Container w = owner;
        if (event.getSource() instanceof Component) {
            w = AWTUtils.getContainingWindow((Component) event.getSource());
        }
        Component c = (w.findComponentAt(transformDnDCoords(getDropPoint(event.getPoint(),
            event.getModifiers()), (Component) event.getSource(), w)));
        while ((c != null)) {
            if (c instanceof DnDTarget) {
                t = (DnDTarget) c;
                break;
            }
            c = c.getParent();
        }
        return t;
    }

    // --- geometry stuff ---

    private boolean eventHasOwnSource(MouseEvent e) {
        boolean has = false;
        if (e.getSource() instanceof Component) {
            if (AWTUtils.getTopLevelAncestor((Component) e.getSource()) == owner) {
                has = true;
            }
        }
        return has;
    }

    // --- initially determines whether the item is to be dragged or to
    // be resized
    private void determineDragMode() {
        if (draggedItem instanceof Component) {
            Component c = (Component) draggedItem;
            Rectangle inset = new Rectangle(dragInset, dragInset, c.getWidth() - 2 * dragInset,
                c.getHeight() - 2 * dragInset);
            if ((dragPolicy & DRAG_RESIZE_COMPONENT) == 0) {
                if ((dragPolicy & DRAG_BORDER_HANDLES) != 0) {
                    if (inset.contains(dragHandlePoint)) {
                        dragMode = DRAG_MODE_NONE;
                    }
                    else {
                        dragMode = DRAG_MODE_MOVE;
                    }
                }
                else {
                    dragMode = DRAG_MODE_MOVE;
                }
            }
            else {
                int dragX = (c.getWidth() - dragInset) / 2;
                int dragY = (c.getHeight() - dragInset) / 2;
                if ((dragPolicy & DRAG_BORDER_HANDLES) != 0) {
                    if (inset.contains(dragHandlePoint)) {
                        dragMode = DRAG_MODE_NONE;
                    }
                    else {
                        dragMode = DRAG_MODE_MOVE;
                        dragHandleType = inset.outcode(dragHandlePoint);
                        if ((dragHandleType == DRAG_HANDLE_NORTHEAST) ||
                            (dragHandleType == DRAG_HANDLE_SOUTHEAST) ||
                            (dragHandleType == DRAG_HANDLE_SOUTHWEST) ||
                            (dragHandleType == DRAG_HANDLE_NORTHWEST)) {
                            dragMode = DRAG_MODE_RESIZE;
                        }
                        else if (((dragHandleType == DRAG_HANDLE_WEST) || (dragHandleType == DRAG_HANDLE_EAST)) &&
                                 ((dragHandlePoint.y > dragY) && (dragHandlePoint.y < (dragY + dragInset)))) {
                            dragMode = DRAG_MODE_RESIZE;
                        }
                        else if (((dragHandleType == DRAG_HANDLE_NORTH) || (dragHandleType == DRAG_HANDLE_SOUTH)) &&
                                 ((dragHandlePoint.x > dragX) && (dragHandlePoint.x < (dragX + dragInset)))) {
                            dragMode = DRAG_MODE_RESIZE;
                        }
                    }
                }
                else {
                    if (inset.contains(dragHandlePoint)) {
                        dragMode = DRAG_MODE_MOVE;
                    }
                    else {
                        dragHandleType = inset.outcode(dragHandlePoint);
                        dragMode = DRAG_MODE_RESIZE;
                    }
                }
            }
        }
        else {
            dragMode = DRAG_MODE_MOVE;
        }
    }

    // --- calculates the position of intended mouse cursor depending on
    // drag
    // restrictions in _event source's_ coordinate system
    private Point getDropPoint(Point where, int modifiers) {
        int dragRestrictions = dragPolicy & DRAG_RESTRICTIONS_MASK;
        if ((dragPolicy & DRAG_RESTRICT_ON_SHIFT_KEY) != 0) {
            if ((modifiers & InputEvent.SHIFT_MASK) != 0) {
                dragRestrictions = dragRestrictions | DRAG_ORTHOGONAL;
            }
        }
        int dx = where.x - dragStart.x;
        int dy = where.y - dragStart.y;
        switch (dragRestrictions) {
            case DRAG_HORIZONTALLY:
                dy = 0;
                break;
            case DRAG_VERTICALLY:
                dx = 0;
                break;
            case DRAG_ORTHOGONAL:
                if (dragMode == DRAG_MODE_MOVE) {
                    if (Math.abs(dx) > Math.abs(dy)) {
                        dy = 0;
                    }
                    else {
                        dx = 0;
                    }
                }
                else {
                    if (Math.abs(dx) > Math.abs(dy)) {
                        if ((dragHandleType == DRAG_HANDLE_SOUTHWEST) ||
                            (dragHandleType == DRAG_HANDLE_NORTHEAST)) {
                            dy = -dx;
                        }
                        else if ((dragHandleType == DRAG_HANDLE_NORTHWEST) ||
                                 (dragHandleType == DRAG_HANDLE_SOUTHEAST)) {
                            dy = dx;
                        }
                    }
                    else {
                        if ((dragHandleType == DRAG_HANDLE_SOUTHWEST) ||
                            (dragHandleType == DRAG_HANDLE_NORTHEAST)) {
                            dx = -dy;
                        }
                        else if ((dragHandleType == DRAG_HANDLE_NORTHWEST) ||
                                 (dragHandleType == DRAG_HANDLE_SOUTHEAST)) {
                            dx = dy;
                        }
                    }
                }
                break;
            default:
        }
        Point dropWhere = new Point(dragStart.x + dx, dragStart.y + dy);
        return dropWhere;
    }

    public void paintDraggedItem(Component c, Graphics g) {
        if (dragState == DRAGGING_IN_PROGRESS) {

            if (((dragMedium == null) && (c == owner)) || (dragMedium == c)) {
                Rectangle bounds;
                if (draggedItem instanceof Component) {
                    bounds = getDropBounds(dragEvent.getPoint(), dragEvent.getModifiers(),
                        (Component) draggedItem, (dragMedium == null ? owner : dragMedium));
                }
                else {
                    bounds = getDropBounds(dragEvent.getPoint(), dragEvent.getModifiers(),
                        dragDummy, (dragMedium == null ? owner : dragMedium));
                }
                g.setColor(Color.BLACK);
                g.setXORMode(Color.WHITE);
                g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
            }
        }
    }

    // --- paint, either by DnDManager or by drag medium
    private void paintDraggedItem() {
        if (dragMedium == null) {
            // owner.invalidate();
            owner.repaint();
        }
        else {
            // dragMedium.invalidate();
            dragMedium.repaint();
            // ((DnDMedium)dragMedium).paintDnDItem(this, draggedItem,
            // transformDnDCoords(where, (Component)eventSource,
            // dragMedium), mode);
        }
    }

    // --- other stuff ---

    // --- sets the DnDManager during new drag start
    private void reset(MouseEvent e) {
        eventSource = e.getSource();
        dragStart = e.getPoint();

        dragSource = null;

        draggedItem = null;
        itemSize = new Point(16, 16);
        itemCenter = new Point(8, 8);
        dragDummy = new Panel();
        dragDummy.setLocation(0, 0);
        dragDummy.setSize(itemSize.x, itemSize.y);

        dragInset = 8;
        dragHandlePoint = new Point(8, 8);
        // paintItem = true;

        dragMedium = null;

        lastDropTarget = null;
        dropTarget = null;
        itemAccepted = false;

        dragState = DRAGGING_IDLE;
        dragPolicy = DRAG_RESTRICT_ON_SHIFT_KEY;
        dragMode = DRAG_MODE_NONE;
    }

}

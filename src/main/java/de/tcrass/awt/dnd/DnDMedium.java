package de.tcrass.awt.dnd;

import java.awt.Component;
import java.awt.Point;

public interface DnDMedium {

    void dragStarted(DnDManager dm, Object item, Point where);

    void itemDragged(DnDManager dm, Object item, Point where);

    void itemDropped(DnDManager dm, Object item, Component target);

}

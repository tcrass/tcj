package de.tcrass.awt.dnd;

import java.awt.Component;

public interface DnDSource {

    Object getDraggableItem(DnDManager dm);

    void itemDropped(DnDManager dm, Object item, Component target);

}

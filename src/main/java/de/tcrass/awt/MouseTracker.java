package de.tcrass.awt;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;

import de.tcrass.util.SysUtils;

public class MouseTracker extends Component implements AWTEventListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Container         owner;
    private Point             lastPos;

    public MouseTracker(Container owner) {
        this.owner = owner;
        owner.getToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK);
        owner.getToolkit().addAWTEventListener(this, AWTEvent.MOUSE_MOTION_EVENT_MASK);
        lastPos = new Point(-1, -1);
    }

    // --- Attribute access ---

    public Container getOwner() {
        return owner;
    }

    // --- Public methods ---

    public Point getMousePos() {
        return lastPos;
    }

    public boolean isMouseIn(Component c) {
        boolean in = false;
        if (c.isShowing()) {
            in = new Rectangle(c.getLocationOnScreen(), c.getSize()).contains(lastPos);
        }
        return in;
    }

    // --- Implementation of AWTEventListener ---

    public void eventDispatched(AWTEvent e) {
        if (e instanceof MouseEvent) {
            MouseEvent me = (MouseEvent) e;
            if ((me.getSource() instanceof Component)) {
                Component s = (Component) me.getSource();
                if (s.isShowing()) {
                    try {
                        int x = s.getLocationOnScreen().x + me.getPoint().x;
                        int y = s.getLocationOnScreen().y + me.getPoint().y;
                        lastPos = new Point(x, y);
                    }
                    catch (RuntimeException e1) {
                        SysUtils.doNothing();
                    }
                }
            }
        }
        processEvent(e);
    }

}

package de.tcrass.awt;

public interface CommandManagerOwner {

    public CommandManager getCommandManager();

    public void updateCommands();

}

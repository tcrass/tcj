package de.tcrass.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import de.tcrass.io.FileUtils;
import de.tcrass.util.StringUtils;

// This is a test...

public class AWTUtils extends WindowAdapter {

    public static WindowAdapter newWindowClosingAdapter() {
        return new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                Frame source = (Frame) e.getSource();
                source.setVisible(false);
                source.dispose();
                System.exit(0);
            }
        };
    }

    public static GridBagConstraints newGBC(int x, int y, int w, int h, double wx, double wy,
                                            int fill, int anchor) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = w;
        gbc.gridheight = h;
        gbc.weightx = wx;
        gbc.weighty = wy;
        gbc.fill = fill;
        gbc.anchor = anchor;
        gbc.insets = new Insets(1, 1, 1, 1);
        return gbc;
    }

    public static Point convertCoords(Point where, Component source, Component target) {
        Point p = null;
        if (source.isShowing() && target.isShowing()) {
            int x = where.x + source.getLocationOnScreen().x - target.getLocationOnScreen().x;
            int y = where.y + source.getLocationOnScreen().y - target.getLocationOnScreen().y;
            p = new Point(x, y);
        }
        else {
            p = new Point(0, 0);
        }
        return p;
    }

    public static Component getTopLevelAncestor(Component c) {
        Component anc = null;
        if (c.getParent() == null) {
            anc = c;
        }
        else {
            anc = getTopLevelAncestor(c.getParent());
        }
        return anc;
    }

    public static Window getContainingWindow(Component c) {
        Window w = null;
        if (c instanceof Window) {
            w = (Window) c;
        }
        else {
            if (c.getParent() != null) {
                w = getContainingWindow(c.getParent());
            }
        }
        return w;
    }

    public static BufferedImage readImageResource(String path, String name, Component io)
    throws IOException {
        BufferedImage bi = null;
        InputStream is = FileUtils.createResourceInputStream(path, name);
        if (is != null) {
            byte[] imageBuffer = new byte[0];
            byte[] tempBuffer = new byte[1024];
            int c = is.read(tempBuffer);
            while (c > 0) {
                byte[] newBuffer = new byte[imageBuffer.length + c];
                System.arraycopy(imageBuffer, 0, newBuffer, 0, imageBuffer.length);
                System.arraycopy(tempBuffer, 0, newBuffer, imageBuffer.length, c);
                imageBuffer = newBuffer;
                c = is.read(tempBuffer);
            }
            is.close();
            Image i = Toolkit.getDefaultToolkit().createImage(imageBuffer);
            MediaTracker mt = new MediaTracker(io);
            mt.addImage(i, 0);
            try {
                mt.waitForAll();
            }
            catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            mt.removeImage(i);
            bi = new BufferedImage(i.getWidth(io), i.getHeight(io), BufferedImage.TYPE_INT_ARGB);
            Graphics g = bi.getGraphics();
            g.drawImage(i, 0, 0, null, io);
        }
        return bi;
    }

    public static BufferedImage toBufferedImage(Image i, Component io) {
        BufferedImage b = new BufferedImage(i.getWidth(io), i.getHeight(io),
            BufferedImage.TYPE_INT_ARGB);
        Graphics g = b.getGraphics();
        g.drawImage(i, 0, 0, io);
        return b;
    }

    public static String color2argb(Color c) {
        String hex = Long.toHexString((long) (Math.pow(2, 24) * c.getAlpha() + Math.pow(2, 16) *
                                              c.getRed() + Math.pow(2, 8) * c.getGreen() + c.getBlue()));
        return StringUtils.polymerize("0", 8 - hex.length()) + hex;
    }

    public static String color2rgba(Color c) {
        String hex = Long.toHexString((long) (Math.pow(2, 24) * c.getRed() + Math.pow(2, 16) *
                                              c.getGreen() + Math.pow(2, 8) * c.getBlue() + c.getAlpha()));
        return StringUtils.polymerize("0", 8 - hex.length()) + hex;
    }

    public static String color2rgb(Color c) {
        String hex = Long.toHexString((long) (Math.pow(2, 16) * c.getRed() + Math.pow(2, 8) *
                                              c.getGreen() + c.getBlue()));
        return StringUtils.polymerize("0", 6 - hex.length()) + hex;
    }

    public static Color argb2color(String s) {
        Color c = new Color(0);
        String hex = new String(s);
        if (hex.startsWith("#")) {
            hex = hex.substring(1);
        }
        try {
            int i = Integer.parseInt(hex, 16);
            c = new Color(i, true);
        }
        catch (NumberFormatException ex) {
        }
        return c;
    }

    public static Color rgba2color(String s) {
        String hex = new String(s);
        if (hex.startsWith("#")) {
            hex = hex.substring(1);
        }
        if (hex.length() > 6) {
            hex = hex.substring(6) + hex.substring(0, 5);
        }
        return argb2color(hex);
    }

    private static Window findActiveSubWindow(Window root) {
        Window act = null;
        Window[] subWins = root.getOwnedWindows();
        int i = 0;
        while ((act == null) && (i < subWins.length)) {
            Window sub = subWins[i];
            if (sub.isActive()) {
                act = sub;
            }
            else {
                act = findActiveSubWindow(sub);
            }
            i++;
        }
        return act;
    }

    public static Window getActiveWindow(Window root) {
        Window act = findActiveSubWindow(root);
        if ((act == null) && (root.getOwner() == null)) {
            act = root;
        }
        return act;
    }

    public static void centerOnScreen(Window w) {
        w.setLocation((w.getToolkit().getScreenSize().width - w.getWidth()) / 2,
            (w.getToolkit().getScreenSize().height - w.getHeight()) / 2);
    }

}

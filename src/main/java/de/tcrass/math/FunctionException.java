package de.tcrass.math;

public class FunctionException extends MathException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    Function                  f;

    public FunctionException(String msg, Function f) {
        super(msg);
        this.f = f;
    }

}

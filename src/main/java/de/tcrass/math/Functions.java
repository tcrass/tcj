package de.tcrass.math;

public class Functions {

    public static double PI_2 = Math.PI / 2;

    public static double rad(double a) {
        return a * Math.PI / 180;
    }

    public static double grad(double a) {
        return a * 180 / Math.PI;
    }

    public static double ang(double x, double y) {
        // double len = Math.sqrt(x*x + y*y);
        if (x == 0) {
            return (y > 0 ? PI_2 : -PI_2);
        }
        else {
            return Math.atan(y / x);
        }
    }

}

package de.tcrass.math;

public interface Function {

    public abstract Number[] vals(Number[] x);

}

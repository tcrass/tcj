package de.tcrass.math;

public class Complex extends Number implements Cloneable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    double                    real, imag;

    public static Complex polar(double r, double phi) {
        return new Complex(r * Math.cos(phi), r * Math.sin(phi));
    }

    public Complex(double r, double i) {
        real = r;
        imag = i;
    }

    public Complex(double r) {
        this(r, 0);
    }

    public Complex() {
        this(0, 0);
    }

    // Implementation of Number

    @Override
    public double doubleValue() {
        return abs();
    }

    @Override
    public float floatValue() {
        return Double.valueOf(abs()).floatValue();
    }

    @Override
    public int intValue() {
        return Double.valueOf(abs()).intValue();
    }

    @Override
    public long longValue() {
        return Double.valueOf(abs()).longValue();
    }

    // --- Arithmetics changing *this* number

    public Complex conjugate() {
        imag = -imag;
        return this;
    }

    public Complex add(Complex a) {
        real += a.real;
        imag += a.imag;
        return this;
    }

    public Complex subtract(Complex a) {
        real -= a.real;
        imag -= a.imag;
        return this;
    }

    public Complex multiplyBy(Double f) {
        real *= f;
        imag *= f;
        return this;
    }

    public Complex multiplyBy(Complex c) {
        double r = real * c.real - imag * c.imag;
        double i = real * c.imag + imag * c.real;
        real = r;
        imag = i;
        return this;
    }

    public Complex divideBy(Double f) {
        real /= f;
        imag /= f;
        return this;
    }

    public Complex divideBy(Complex c) {
        double r = (real * c.real + imag * c.imag) / (c.real * c.real + c.imag * c.imag);
        double i = (imag * c.real - real * c.imag) / (c.real * c.real + c.imag * c.imag);
        real = r;
        imag = i;
        return this;
    }

    public Complex exponentiate(int n) {
        Complex c = pow(n);
        real = c.real;
        imag = c.imag;
        return this;
    }

    // --- Arithmetics creating a new number

    public Complex bar() {
        return new Complex(real, -imag);
    }

    public Complex plus(Complex a) {
        return new Complex(real + a.real, imag + a.imag);
    }

    public Complex minus(Complex a) {
        return new Complex(real - a.real, imag - a.imag);
    }

    public Complex times(Double f) {
        return new Complex(real * f, imag * f);
    }

    public Complex times(Complex c) {
        return new Complex(real * c.real - imag * c.imag, real * c.imag + imag * c.real);
    }

    public Complex by(double f) {
        return new Complex(real / f, imag / f);
    }

    public Complex by(Complex c) {
        return new Complex((real * c.real + imag * c.imag) /
                           (c.real * c.real + c.imag * c.imag),
            (imag * c.real - real * c.imag) / (c.real * c.real + c.imag * c.imag));
    }

    public Complex pow(int n) {
        return Complex.polar(Math.pow(abs(), n), n * arg());
    }

    // --- Other stuff

    public double abs() {
        return real * real + imag * imag;
    }

    public double arg() {
        return Math.atan2(real, imag);
    }

}

package de.tcrass.math.linalg;

public interface FloatVector {

    public double[] elems();

    public double length();

}

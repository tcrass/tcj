package de.tcrass.math.linalg;

public class SLE {

    public static Matrix solve(Matrix A, Matrix B) {
        if (A.mRows() != B.mRows()) {
            throw new SLEException(A, B,
                "Right-hand side matrix has a row number different from coefficient matrix.");
        }
        else {
            double[] elemsA = A.getArray();
            int mRowsA = A.mRows();
            int nColsA = A.nCols();
            int addrA = 0;
            int addrA0 = 0;

            double piv = 0;
            int pivInd = 0;
            int pivAddr = 0;
            int diagAddr = 0;

            double[] elemsB = B.getArray();
            int nColsB = B.nCols();
            int addrB = 0;
            int pivB = 0;

            int n = (mRowsA < nColsA ? mRowsA : nColsA);

            // iterator over columns
            for (int j = 0; j < n; j++) {
                addrA = j;

                // find pivot element in current column
                for (int i = j; i < mRowsA; i++) {
                    if (Math.abs(elemsA[addrA]) > piv) {
                        piv = Math.abs(elemsA[addrA]);
                        pivInd = i;
                    }
                    addrA += nColsA;
                }
                if (piv == 0) {
                    throw new SLEException(A, B, "Coefficient matrix is singular.");
                }
                else {
                    pivAddr = j + pivInd * nColsA;
                    piv = elemsA[pivAddr];

                    // swap rows so to bring pivot element to current
                    // diagonal position
                    if (pivInd != j) {
                        A.swapRows(j + 1, pivInd + 1);
                        B.swapRows(j + 1, pivInd + 1);
                    }

                    // process pivot row
                    addrA = j + j * nColsA;
                    elemsA[addrA] = 1;
                    addrA++;
                    for (int k = j + 1; k < nColsA; k++) {
                        elemsA[addrA] = elemsA[addrA] / piv;
                        addrA++;
                    }
                    addrB = j * nColsB;
                    for (int k = 0; k < nColsB; k++) {
                        elemsB[addrB] = elemsB[addrB] / piv;
                        addrB++;
                    }

                    // process non-pivot rows
                    addrA0 = j;
                    addrB = 0;
                    for (int i = 0; i < mRowsA; i++) {
                        if (i != j) {
                            double a = elemsA[addrA0];
                            elemsA[addrA0] = 0;
                            addrA = addrA0 + 1;
                            pivAddr = diagAddr + 1;
                            for (int k = j + 1; k < nColsA; k++) {
                                elemsA[addrA] = elemsA[addrA] - a * elemsA[pivAddr];
                                addrA++;
                                pivAddr++;
                            }
                            pivAddr = pivB;
                            for (int k = 0; k < nColsB; k++) {
                                elemsB[addrB] = elemsB[addrB] - a * elemsB[pivAddr];
                                addrB++;
                                pivAddr++;
                            }
                        }
                        else {
                            addrB += nColsB;
                        }
                        addrA0 += nColsA;
                    }
                }
                diagAddr += nColsA;
                diagAddr++;
                pivB += nColsB;
            }
        }
        return B;
    }

    public static Matrix ndSolve(Matrix A, Matrix B) {
        return solve(A.createClone(), B.createClone());
    }

}

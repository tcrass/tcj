package de.tcrass.math.linalg;

public class RowVector extends Matrix implements FloatVector {

    public RowVector(int cols) {
        super(1, cols);
    }

    public RowVector(double[] elems) {
        this(elems.length);
        setArray(elems.clone(), 1, elems.length);
    }

    @Override
    public Matrix transpose() {
        throw newException("transpose() not supported by RowVector; use transposed() instead");
    }

    @Override
    public Matrix transposed() {
        return new ColVector(super.transpose().getArray());
    }

    // --- Implementation of FloatVector

    public double length() {
        return nCols;
    }

    public double[] elems() {
        return getRow(1);
    }

}

package de.tcrass.math.data;

import java.util.List;

public interface Tabular {

    public int MIN_CAPACITY = 100;

    public int nCols();

    public Class<?>[] getColTypes();

    public String[] getColNames();

    public int mRows();

    public List<?> getRow(int row);

    public List<?> getCol(int col);

    public List<?> getCol(String colName);

    public Object getData(int col, int row);

    public Object getData(String colName, int row);

    public void setData(int col, int row, Object data);

    public void setData(String colName, int row, Object data);
}

package de.tcrass.math.data;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

import de.tcrass.util.CollectionUtils;
import de.tcrass.util.StringUtils;

public abstract class TabularImplStub implements Tabular {

    protected final Class<?>[]           colTypes;
    protected final String[]             colNames;
    protected final Map<String, Integer> names2cols;

    protected TabularImplStub(Class<?>[] types, String[] names) {
        if ((types.length == 0) || (names.length == 0) || (types.length != names.length)) {
            throw (new DataException("Mismatching number of column types and/or names!", this));
        }
        colTypes = types;
        colNames = names;
        names2cols = new Hashtable<String, Integer>();
        for (int i = 0; i < colNames.length; i++) {
            names2cols.put(colNames[i], i);
        }
    }

    // --- Implementation of Data

    public int nCols() {
        return colTypes.length;
    }

    public Class<?>[] getColTypes() {
        return colTypes;
    }

    public String[] getColNames() {
        return colNames;
    }

    @Override
    public String toString() {
        StringBuffer s = new StringBuffer(super.toString());
        s.append(" with columns " + CollectionUtils.arrayToString(colNames));
        s.append(" of types " + CollectionUtils.listToString(Arrays.asList(colTypes)));
        return s.toString();
    }

    private String toStringTable(String rowIndex, boolean title, String sep) {
        StringBuffer s = new StringBuffer();
        if (rowIndex != null) {
            s.append(rowIndex + sep);
        }
        if (title) {
            s.append(StringUtils.join(sep, colNames) + "\n");
        }
        int c = (rowIndex != null ? 1 : 0);
        for (int row = 0; row < mRows(); row++) {
            String[] d = new String[nCols() + c];
            if (rowIndex != null) {
                d[0] = Integer.valueOf(row).toString();
            }
            for (int col = 0; col < nCols(); col++) {
                if (getData(col, row) != null) {
                    d[col + c] = getData(col, row).toString();
                }
                else {
                    d[col + c] = "";
                }
            }
            s.append(StringUtils.join(sep, d) + "\n");
        }
        return s.toString();
    }

    public String toCSV() {
        return toCSV(null, false);
    }

    public String toCSV(String rowIndex, boolean title) {
        return toStringTable(rowIndex, title, ", ");
    }

    public String toTSV() {
        return toTSV(null, false);
    }

    public String toTSV(String rowIndex, boolean title) {
        return toStringTable(rowIndex, title, "\t");
    }

}

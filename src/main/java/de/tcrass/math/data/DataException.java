package de.tcrass.math.data;

import de.tcrass.math.MathException;

public class DataException extends MathException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Tabular           data;

    public DataException(String msg, Tabular data) {
        super(msg);
        this.data = data;
    }

    public Tabular getData() {
        return data;
    }

}

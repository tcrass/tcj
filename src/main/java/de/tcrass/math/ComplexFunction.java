package de.tcrass.math;

public abstract class ComplexFunction extends BasicFunctionImpl {

    // --- Protected methods

    protected Complex[] num2complex(Number[] n) {
        Complex[] c = new Complex[n.length];
        for (int i = 0; i < n.length; i++) {
            c[i] = ((Complex) n[i]);
        }
        return c;
    }

    protected Number[] complex2num(Complex[] c) {
        Number[] n = new Number[c.length];
        for (int i = 0; i < c.length; i++) {
            n[i] = c[i];
        }
        return n;
    }

    protected Complex[] result(Complex... y) {
        checkNumberOfOuts((Object[]) y);
        return y;
    }

    // --- Public methods

    public abstract Complex[] vals(Complex... x);

    public Complex val(Complex x) {
        checkNumberOfArgs(x);
        return vals(x)[0];
    }

    public Number[] vals(Number[] x) {
        return complex2num(vals(num2complex(x)));
    }

}

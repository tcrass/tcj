package de.tcrass.math;

public abstract class BasicFunctionImpl implements Function {

    int nArgs;
    int nOut;

    protected void checkNumberOfArgs(Object... x) {
        if (x.length != nArgs) {
            throw new FunctionException("Incompatible number of arguments: is (" + x.length +
                                        ", should be " + nArgs + "!", this);
        }
    }

    protected void checkNumberOfOuts(Object... y) {
        if (y.length != nOut) {
            throw new FunctionException("Incompatilbe number of result values: is " + y.length +
                                        ", should be " + nOut + "!", this);
        }
    }

}

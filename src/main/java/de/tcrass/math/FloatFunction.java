package de.tcrass.math;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class FloatFunction extends BasicFunctionImpl {

    public double                     DEFAULT_EPSILON = 1.0 / 1024.0;

    double                            epsilon         = DEFAULT_EPSILON;
    double                            epsilon2        = 2 * epsilon;
    double                            epsilonsq       = epsilon * epsilon;

    int                               nArgs;
    int                               nOut;
    List<Map<Integer, FloatFunction>> derivs;

    public FloatFunction(int nArgs, int nOut) {
        this.nArgs = nArgs;
        this.nOut = nOut;
        derivs = new ArrayList<Map<Integer, FloatFunction>>();
        for (int i = 0; i < nArgs; i++) {
            derivs.add(new HashMap<Integer, FloatFunction>());
        }
    }

    // --- Protected methods

    protected double[] num2float(Number[] n) {
        double[] d = new double[n.length];
        for (int i = 0; i < n.length; i++) {
            d[i] = n[i].doubleValue();
        }
        return d;
    }

    protected Number[] float2num(double[] d) {
        Number[] n = new Number[d.length];
        for (int i = 0; i < d.length; i++) {
            n[i] = Double.valueOf(d[i]);
        }
        return n;
    }

    protected double[] result(double... y) {
        checkNumberOfOuts(y);
        return y;
    }

    // --- Public methods

    public abstract double[] vals(double... x);

    public double val(double x) {
        checkNumberOfArgs(x);
        return vals(x)[0];
    }

    public double[] derivs(int d, int i, double... x) {
        double[] result = null;
        if (d == 0) {
            result = vals(x);
        }
        else if (derivs.get(i).keySet().contains(Integer.valueOf(i))) {
            result = derivs.get(i).get(Integer.valueOf(i)).vals(x);
        }
        else if ((d % 2) == 0) {
            result = new double[nOut];

            double[] x1 = x.clone();
            x1[i] -= epsilon;
            double[] x2 = x.clone();
            x2[i] += epsilon;

            double[] y1 = derivs(d - 2, i, x1);
            double[] y = derivs(d - 2, i, x);
            double[] y2 = derivs(d - 2, i, x2);

            for (int j = 0; j < nOut; j++) {
                result[j] = (y2[j] - 2 * y[j] + y1[j]) / epsilonsq;
            }
        }
        else {
            result = new double[nOut];

            double[] x1 = x.clone();
            x1[i] -= epsilon;
            double[] x2 = x.clone();
            x2[i] += epsilon;

            double[] y1 = derivs(d - 1, i, x1);
            double[] y2 = derivs(d - 1, i, x2);

            for (int j = 0; j < nOut; j++) {
                result[j] = (y2[j] - y1[j]) / epsilon2;
            }
        }
        checkNumberOfOuts(result);
        // System.out.println(d + ". deriv at " + x[0] + " is " +
        // result[0]);
        return result;
    }

    public double deriv(int d, int i, double... x) {
        return derivs(d, i, x)[0];
    }

    public double deriv(int d, double x) {
        return derivs(d, 0, x)[0];
    }

    public void setDeriv(int d, int i, FloatFunction f) {
        derivs.get(i).put(Integer.valueOf(d), f);
    }

    public void setDeriv(int d, FloatFunction f) {
        derivs.get(0).put(Integer.valueOf(d), f);
    }

    // --- Implementation of Function

    public Number[] vals(Number[] x) {
        return float2num(vals(num2float(x)));
    }

}

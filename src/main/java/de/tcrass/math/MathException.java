package de.tcrass.math;

public class MathException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public MathException(String msg) {
        super(msg);
    }

}

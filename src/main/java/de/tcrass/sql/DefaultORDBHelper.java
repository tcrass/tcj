package de.tcrass.sql;

import java.util.ArrayList;
import java.util.List;

public class DefaultORDBHelper implements ORDBHelper {

    protected EasySQL esql;

    public void setEasySQL(EasySQL esql) {
        this.esql = esql;
    }

    public List<String> getParentTables(String tableName) {
        return new ArrayList<String>();
    }

    public List<String> getChildTables(String tableName) {
        return new ArrayList<String>();
    }

    public boolean areCompatibleColumns(ORColumn col1, ORColumn col2) {
        return (((col1.getType() == null) && (col2.getType() == null)) || ((col1.getType() != null) && (col1.getType().equals(col2.getType()))));
    }
    
    public boolean isPossiblePrimaryKey(ORColumn col) {
        return col.isPrimaryKey() || (col.isUnique() && col.isNotNull());
    }

}

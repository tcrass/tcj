package de.tcrass.sql.tools;

import de.tcrass.graph.Graph;
import de.tcrass.graph.Graphviz;
import de.tcrass.io.ArgumentsParser;
import de.tcrass.io.FileUtils;
import de.tcrass.sql.EasyPSQL;
import de.tcrass.sql.EasySQL;
import de.tcrass.sql.ORSchema;
import de.tcrass.sql.ORSchemaLayouter;
import de.tcrass.util.CollectionUtils;
import de.tcrass.util.SysUtils;

public class drawORSchema {

    public static final String                            VERSION                   = "1.1.1";

    public static String                                  DEFAULT_DRIVER            = EasyPSQL.PSQL_DRIVER_CLASSNAME;
    public static String                                  DEFAULT_OUTPUT_FORMAT     = "png";
    public static String                                  DEFAULT_OUTPUT_FILENAME   = "schema";
    public static EasySQL.findRefsForPKs                  DEFAULT_PK_POLICY         = EasySQL.findRefsForPKs.NONE;
    public static double                                  DEFAULT_MAPPING_THRESHOLD = 0.5;
    public static ORSchemaLayouter.showInferredReferences DEFAULT_SHOW_INFERRED     = ORSchemaLayouter.showInferredReferences.NONE;

    private static void printUsage() {
        System.out.println("\ndrawORSchema -- Create an (object-)relational database schema diagram");
        System.out.println("Version: " + VERSION);
        System.out.println("------------------------------------------------------------------------");
        // System.out.println("Creates a graphical representation of an (object-)relational");
        // System.out.println("database's object-relational schema.");
        System.out.println("Usage:");
        System.out.println("java org.comlink.home.tcrass.sql.tools.getORSchema [<options>] <db-url>");
        System.out.println("with possible options:");
        System.out.println("-?|--help:                  : Show this message and abort.");
        System.out.println("-a|--shade-inferred         : Saturate color of inferred references");
        System.out.println("                              according to mapping score.");
        System.out.println("-d|--driver <class>:        : Class name of JDBC driver to use");
        System.out.println("                            : [" + DEFAULT_DRIVER + "].");
        System.out.println("                              (Note: The MySQL driver's class name is");
        System.out.println("                              \"com.mysql.jdbc.Driver\")");
        System.out.println("-f|--output-format <format> : Output file format. All formats understood");
        System.out.println("                              by Graphviz' -T option are acceptable.");
        System.out.println("                              [" + DEFAULT_OUTPUT_FORMAT + "]");
        System.out.println("-i|--inherited-attribs      : Show inherited attributes in table");
        System.out.println("                              descriptions.");
        System.out.println("-k|--pk-policy <which>      : Which primary keys are also considered");
        System.out.println("                              putative foreign key references; must be");
        System.out.println("                              in " +
                           CollectionUtils.arrayToString((EasySQL.findRefsForPKs.values())) +
                           ".");
        System.out.println("                              [" + DEFAULT_PK_POLICY.toString() +
                           "]");
        System.out.println("-o|--output-file <path>     : Path to output file. [" +
                           DEFAULT_OUTPUT_FILENAME + "." + DEFAULT_OUTPUT_FORMAT + "]");
        System.out.println("-p|--password <passwd>      : The database user's password.");
        System.out.println("-r|--show-inferred <what>   : What portion of inferred references it to");
        System.out.println("                              be shown; must be in " +
                           CollectionUtils.arrayToString((ORSchemaLayouter.showInferredReferences.values())) +
                           ".");
        System.out.println("                              [" +
                           DEFAULT_SHOW_INFERRED.toString() + "]");
        System.out.println("-s|--subset-checks          : Increase accuracy of reference prediction");
        System.out.println("                              by considering only columns containing a");
        System.out.println("                              subset of the values contained in a puta-");
        System.out.println("                              tive primary key. N.B.: May significantly");
        System.out.println("                              increase run-time!");
        System.out.println("-t|--mapping-threshold <t>  : Threshold for displaying inferred referen-");
        System.out.println("                              ces; must be in [0..1]. [" +
                           DEFAULT_MAPPING_THRESHOLD + "]");
        System.out.println("-u|--user <name>            : Username with which to connect to");
        System.out.println("                              the database.");
        System.out.println("<db-url>:                     The database's URL according to JDBC spe-cs,");
        System.out.println("                              cifications, e.g.");
        System.out.println("                              jdbc:postgresql://host:port/db_name\"");
        System.out.println("\nPlease note: The jar file containing the JDBC drive to be used");
        System.out.println("MUST be in the classpath! Furthermore, AT&T's \"dot\" program (from the");
        System.out.println("\"graphviz\" package) MUST be in the executable search path.");
    }

    public static void main(String[] args) {

        // SuffixTree foo = new SuffixTree();
        // foo.addString("Hello");
        // foo.addString("Alloha");
        // foo.addString("Halloween");
        // SysUtils.dumpList(foo.getKeys());
        // System.out.println(foo.getLongestCommonSubstrings(0));
        // System.exit(0);

        SysUtils.setApplicationName("drawORSchema");
        SysUtils.setExceptionNote("An error has occured. Please inform torsten.crass@eBiology.de about this error and include the following information:");

        try {

            String driver;
            String dbUrl;
            String user;
            String password;
            String outputFormat;
            String outputFile;
            boolean showInheritedAttribs;
            double mappingThreshold;
            boolean doSubsetChecks;
            EasySQL.findRefsForPKs pkPolicy;
            ORSchemaLayouter.showInferredReferences showInferred;
            boolean shadeInferred;

            if (args.length == 0) {
                printUsage();
                System.exit(0);
            }
            else {
                ArgumentsParser ap = new ArgumentsParser();
                ap.setMaxFilenames(1);
                ap.setBoolOptionName("help", '?');
                ap.setBoolOptionName("shade-inferred", 'a');
                ap.setStringArgName("driver", 'd', DEFAULT_DRIVER);
                ap.setStringArgName("output-format", 'f', DEFAULT_OUTPUT_FORMAT);
                ap.setBoolOptionName("inherited-attribs", 'i');
                ap.setStringArgName("pk-policy", 'k', DEFAULT_PK_POLICY.toString());
                ap.setStringArgName("output-file", 'o');
                ap.setStringArgName("password", 'p', "");
                ap.setStringArgName("show-inferred", 'r', DEFAULT_SHOW_INFERRED.toString());
                ap.setBoolOptionName("subset-checks", 's');
                ap.setFloatArgName("mapping-threshold", 't', DEFAULT_MAPPING_THRESHOLD);
                ap.setStringArgName("user", 'u', "");

                ap.parse(args);

                dbUrl = ap.getFilename(0);

                if (ap.getBoolOption("help")) {
                    printUsage();
                    System.exit(0);
                }

                driver = ap.getStringArg("driver");
                user = ap.getStringArg("user");
                password = ap.getStringArg("password");

                outputFormat = ap.getStringArg("output-format");
                outputFile = DEFAULT_OUTPUT_FILENAME + outputFormat;
                if (ap.getStringArg("output-file") != null) {
                    outputFile = ap.getStringArg("output-file");
                    if (outputFormat == null) {
                        outputFormat = FileUtils.getExtension(outputFile);
                    }
                }

                showInheritedAttribs = ap.getBoolOption("inherited-attribs");

                String showInferredName = ap.getStringArg("show-inferred").toUpperCase();
                showInferred = ORSchemaLayouter.showInferredReferences.valueOf(showInferredName);

                String pkPolicyName = ap.getStringArg("pk-policy").toUpperCase();
                pkPolicy = EasySQL.findRefsForPKs.valueOf(pkPolicyName);

                mappingThreshold = ap.getFloatArg("mapping-threshold");
                if ((mappingThreshold < 0) || (mappingThreshold > 1)) {
                    throw new RuntimeException("Illegal value for mapping threshold: " +
                                               mappingThreshold + "; must be in [0,1]");
                }
                doSubsetChecks = ap.getBoolOption("subset-checks");

                shadeInferred = ap.getBoolOption("shade-inferred");

                EasySQL db = new EasySQL(driver, dbUrl, user, password);

                ORSchema schema = db.getORSchema(
                    showInferred != ORSchemaLayouter.showInferredReferences.NONE,
                    mappingThreshold, doSubsetChecks, pkPolicy);
                ORSchemaLayouter layouter = new ORSchemaLayouter();
                layouter.setShowInheritedAttribs(showInheritedAttribs);
                layouter.setInferredReferencesMappingThreshold(mappingThreshold);
                layouter.setShowInferredReferences(showInferred);
                layouter.shadeInferredReferences(shadeInferred);
                Graph g = layouter.getSchemaGraph(schema);

                Graphviz.doLayoutToFile(g, outputFile, Graphviz.LayoutMethod.DOT, outputFormat);
            }
        }
        catch (Exception ex) {
            SysUtils.reportException(ex);
            printUsage();
            System.exit(1);
        }

    }

}

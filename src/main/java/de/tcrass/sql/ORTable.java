package de.tcrass.sql;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tcrass.util.CollectionUtils;

public class ORTable {

    String                name;
    Map<String, ORColumn> columns;
    List<ORTable>         superClasses;
    List<ORTable>         subClasses;
    ORSchema              schema;

    protected ORTable(ORSchema schema, String name) {
        this.schema = schema;
        this.name = name;
        columns = new LinkedHashMap<String, ORColumn>();
        superClasses = new ArrayList<ORTable>();
        subClasses = new ArrayList<ORTable>();
    }

    public String getName() {
        return name;
    }

    private void fillColumnMap(Map<String, ORColumn> allColumns, Set<ORTable> seenClasses) {
        seenClasses.add(this);
        for (ORColumn a : columns.values()) {
            if (!allColumns.containsKey(a.getQName())) {
                allColumns.put(a.getQName(), a);
            }
        }
        for (ORTable e : superClasses) {
            if (!seenClasses.contains(e)) {
                e.fillColumnMap(allColumns, seenClasses);
            }
        }
    }

    private void fillSuperClassesList(List<ORTable> allSuperClasses, List<ORTable> seenClasses) {
        seenClasses.add(this);
        for (ORTable e : superClasses) {
            if (!allSuperClasses.contains(e)) {
                allSuperClasses.add(e);
                e.fillSuperClassesList(allSuperClasses, seenClasses);
            }
        }
    }

    private void fillSubClassesList(List<ORTable> allSubClasses, List<ORTable> seenClasses) {
        seenClasses.add(this);
        for (ORTable e : subClasses) {
            if (!allSubClasses.contains(e)) {
                allSubClasses.add(e);
                e.fillSuperClassesList(allSubClasses, seenClasses);
            }
        }
    }

    public ORColumn createColumn(String name, String type) {
        ORColumn column = null;
        if (columns.containsKey(name)) {
            throw new ORException("Column " + name + " already declared in table " + this.name +
                                  "!");
        }
        column = new ORColumn(this, name, type);
        columns.put(name, column);
        return column;
    }

    public ORColumn getDeclaredColumn(String name) {
        return columns.get(name);
    }

    public List<String> getDeclaredColumnNames() {
        return new ArrayList<String>(columns.keySet());
    }

    public List<ORColumn> getDeclaredColumns() {
        return new ArrayList<ORColumn>(columns.values());
    }

    public List<String> getAllColumnNames() {
        List<String> cols = new ArrayList<String>();
        for (ORColumn col : getAllColumns()) {
            cols.add(col.getName());
        }
        return cols;
    }

    public List<String> getAllColumnQNames() {
        Map<String, ORColumn> allAttribs = new LinkedHashMap<String, ORColumn>();
        Set<ORTable> seenClasses = new HashSet<ORTable>();
        fillColumnMap(allAttribs, seenClasses);
        return new ArrayList<String>(allAttribs.keySet());
    }

    public List<ORColumn> getAllColumns() {
        Map<String, ORColumn> allAttribs = new LinkedHashMap<String, ORColumn>();
        Set<ORTable> seenClasses = new HashSet<ORTable>();
        fillColumnMap(allAttribs, seenClasses);
        return new ArrayList<ORColumn>(allAttribs.values());
    }

    public List<ORTable> getSubClasses() {
        return new ArrayList<ORTable>(subClasses);
    }

    public List<ORTable> getSuperClasses() {
        return new ArrayList<ORTable>(superClasses);
    }

    public List<ORTable> getAllSuperClasses() {
        List<ORTable> allSuperClasses = new ArrayList<ORTable>();
        List<ORTable> seenClasses = new ArrayList<ORTable>();
        fillSuperClassesList(allSuperClasses, seenClasses);
        return allSuperClasses;
    }

    public List<ORTable> getAllSubClasses() {
        List<ORTable> allSubClasses = new ArrayList<ORTable>();
        List<ORTable> seenClasses = new ArrayList<ORTable>();
        fillSubClassesList(allSubClasses, seenClasses);
        return allSubClasses;
    }

    public void makeSuperClassOf(ORTable sub) {
        if (getDeclaredColumns().size() > 0) {
            throw new ORException("Can't make " + name + " superclass of " + sub.getName() +
                                  " 'cause it already contains attributes!");
        }
        if (!subClasses.contains(sub)) {
            subClasses.add(sub);
            sub.superClasses.add(this);
        }
    }

    public void makeSubClassOf(ORTable sup) {
        if (getDeclaredColumns().size() > 0) {
            throw new ORException("Can't make " + name + " subclass of " + sup.getName() +
                                  " 'cause it already contains attributes!");
        }
        if (!superClasses.contains(sup)) {
            superClasses.add(sup);
            sup.subClasses.add(this);
        }
    }

    public List<ORColumn> getPrimaryKeys() {
        List<ORColumn> pkCols = new ArrayList<ORColumn>();
        for (ORColumn col : getAllColumns()) {
            if (col.isPrimaryKey()) {
                pkCols.add(col);
            }
        }
        return pkCols;
    }

    
    public String toString() {
        return "ORTable(name='" + name + "', columns=" + CollectionUtils.listToString(getDeclaredColumnNames()) + ")";
    }
    
}

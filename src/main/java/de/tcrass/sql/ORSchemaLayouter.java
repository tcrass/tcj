package de.tcrass.sql;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import de.tcrass.awt.AWTUtils;
import de.tcrass.graph.Edge;
import de.tcrass.graph.Graph;
import de.tcrass.graph.Node;
import de.tcrass.util.CollectionUtils;
import de.tcrass.util.StringUtils;

public class ORSchemaLayouter {

    public enum showInferredReferences {
        NONE, ALL, THRESHOLD, BEST
    }
    
    private static final Properties DEFAULT_DIAGRAM_PROPERTIES;
    private static final Properties DEFAULT_TABLE_PROPERTIES;
    private static final Properties DEFAULT_REFERNCE_PROPERTIES;
    private static final Properties DEFAULT_INFERRED_REFERNCE_PROPERTIES;
    private static final Properties DEFAULT_INHERITANCE_PROPERTIES;
    private static final String     DEFAULT_FONT_NAME    = "Times";
    private static final String     DEFAULT_HEADER_COLOR = "#80D0FF";
    private static final String     DEFAULT_REF_COLOR = "#FF6000";
    private static final String     DEFAULT_INFERRED_REF_COLOR = "#FF0060";
    private static final int        DEFAULT_FONT_SIZE    = 10;

    private boolean                 showInheritedAttribs = true;
    private Properties                      diagramProperties;
    private Properties                      tableProperties;
    private Properties                      referenceProperties;
    private Properties                      inferredReferenceProperties;
    private Properties                      inheritanceProperties;
    private int                             fontSize;
    private String                          fontName;
    private String                          headerColor;
    private showInferredReferences showInferredRefs = showInferredReferences.BEST;
    private double mappingThreshold = 0;
    private boolean shadeInferredRefs = false;
    
    static {
        DEFAULT_DIAGRAM_PROPERTIES = new Properties();
        DEFAULT_DIAGRAM_PROPERTIES.setProperty("rankdir", "TB");
        DEFAULT_DIAGRAM_PROPERTIES.setProperty("overlap", "false");
        DEFAULT_DIAGRAM_PROPERTIES.setProperty("pack", "true");
        DEFAULT_DIAGRAM_PROPERTIES.setProperty("splines", "true");
        DEFAULT_DIAGRAM_PROPERTIES.setProperty("sep", "0.2");
        DEFAULT_DIAGRAM_PROPERTIES.setProperty("fontsize", Integer.toString(DEFAULT_FONT_SIZE));
        // DEFAULT_DIAGRAM_PROPERTIES.setProperty("size", "10.6,7.2");
        // DEFAULT_DIAGRAM_PROPERTIES.setProperty("center", "true");
        // DEFAULT_DIAGRAM_PROPERTIES.setProperty("orientation",
        // "landscape");
        // DEFAULT_DIAGRAM_PROPERTIES.setProperty("page", "8.27,11.69");

        DEFAULT_TABLE_PROPERTIES = new Properties();
        DEFAULT_TABLE_PROPERTIES.setProperty("shape", "plaintext");
        DEFAULT_TABLE_PROPERTIES.setProperty("fontsize", Integer.toString(DEFAULT_FONT_SIZE));

        DEFAULT_REFERNCE_PROPERTIES = new Properties();
        DEFAULT_REFERNCE_PROPERTIES.setProperty("arrowhead", "normal");
        DEFAULT_REFERNCE_PROPERTIES.setProperty("arrowtail", "dot");
        DEFAULT_REFERNCE_PROPERTIES.setProperty("arrowsize", "0.5");
        DEFAULT_REFERNCE_PROPERTIES.setProperty("color", DEFAULT_REF_COLOR);
        DEFAULT_REFERNCE_PROPERTIES.setProperty("fontsize", Integer.toString(DEFAULT_FONT_SIZE));

        DEFAULT_INFERRED_REFERNCE_PROPERTIES = (Properties) DEFAULT_REFERNCE_PROPERTIES.clone();
        DEFAULT_INFERRED_REFERNCE_PROPERTIES.setProperty("color", DEFAULT_INFERRED_REF_COLOR);

        DEFAULT_INHERITANCE_PROPERTIES = new Properties();
        DEFAULT_INHERITANCE_PROPERTIES.setProperty("arrowtail", "onormal");
        DEFAULT_INHERITANCE_PROPERTIES.setProperty("arrowhead", "none");
        DEFAULT_INHERITANCE_PROPERTIES.setProperty("color", "#808080");
        DEFAULT_INHERITANCE_PROPERTIES.setProperty("fontsize",
            Integer.toString(DEFAULT_FONT_SIZE));
    }

    // --- constructor ---

    public ORSchemaLayouter() {
        diagramProperties = (Properties) DEFAULT_DIAGRAM_PROPERTIES.clone();
        tableProperties = (Properties) DEFAULT_TABLE_PROPERTIES.clone();
        referenceProperties = (Properties) DEFAULT_REFERNCE_PROPERTIES.clone();
        inferredReferenceProperties = (Properties) DEFAULT_INFERRED_REFERNCE_PROPERTIES.clone();
        inheritanceProperties = (Properties) DEFAULT_INHERITANCE_PROPERTIES.clone();
        fontName = DEFAULT_FONT_NAME;
        fontSize = DEFAULT_FONT_SIZE;
        headerColor = DEFAULT_HEADER_COLOR;
    }

    // --- getter/setter methods ---

    public void setDiagramProperties(Properties diagramProperties) {
        this.diagramProperties = diagramProperties;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public void setHeaderColor(String headerColor) {
        this.headerColor = headerColor;
    }

    public void setInheritanceProperties(Properties inheritanceProperties) {
        this.inheritanceProperties = inheritanceProperties;
    }

    public void setReferenceProperties(Properties referenceProperties) {
        this.referenceProperties = referenceProperties;
    }

    public void setInferredReferenceProperties(Properties inferredReferenceProperties) {
        this.inferredReferenceProperties = inferredReferenceProperties;
    }

    public void setTableProperties(Properties tableProperties) {
        this.tableProperties = tableProperties;
    }

    public void setShowInheritedAttribs(boolean show) {
        showInheritedAttribs = show;
    }

    public void setShowInferredReferences(showInferredReferences showWhat) {
        showInferredRefs = showWhat;
    }

    public void setInferredReferencesMappingThreshold(double threshold) {
        mappingThreshold = threshold;
    }
    
    public void shadeInferredReferences(boolean shade) {
        shadeInferredRefs = shade;
    }
    
    // --- private methods ---

    private String portStr(String port) {
        return StringUtils.replaceAll(port, ":", "_");
    }

    private String createColumnLabel(ORColumn c, boolean italic) {

        String ITON = (italic ? "<FONT FACE=\"" + fontName + "-Italic\">" : "");
        String ITOFF = (italic ? "</FONT>" : "");

        String label = "";

        label = label + "<TR>";

        label = label + "<TD WIDTH=\"4\" PORT=\"" + portStr(c.getName()) + ".in\">" + ITON;
        label = label + (c.isPrimaryKey() ? "#" : " ");
        label = label + ITOFF + "</TD>";

        label = label + "<TD ALIGN=\"LEFT\">" + ITON;
        label = label + c.getName();
        label = label + ITOFF + "</TD>";

        label = label + "<TD ALIGN=\"LEFT\">" + ITON + c.getType() + ITOFF + "</TD>";
        label = label + "<TD ALIGN=\"LEFT\">" + ITON;
        String s = "";
        if (c.isUnique()) {
            s = s + "U";
        }
        if (c.isNotNull()) {
            s = s + "N";
        }
        if (s.equals("")) {
            s = " ";
        }
        label = label + s;
        label = label + ITOFF + "</TD>";
        label = label + "<TD ALIGN=\"RIGHT\" PORT=\"" + portStr(c.getName()) + ".out\">";
        label = label + (c.isForeignKey() ? "&gt;" : "");
        label = label + "</TD>";

        label = label + "</TR>\n";

        return label;
    }

    // --- public methods

    public Graph getSchemaGraph(ORSchema schema) {
        Graph g = new Graph((Properties) diagramProperties.clone());

        for (Object element : schema.getTables()) {
            ORTable table = (ORTable) element;
            Node n = g.createNode(table.getName(), (Properties) tableProperties.clone());
            String label = "<<FONT POINT-SIZE=\"" +
                           fontSize +
                           "\">" +
                           "<TABLE PORT=\"t\" CELLBORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"4\">" +
                           "<TR><TD COLSPAN=\"5\" BGCOLOR=\"" + headerColor +
                           "\" PORT=\"_CLASS_\">" + "<FONT FACE=\"" + fontName + "-Bold\">" +
                           table.getName() + "</FONT></TD></TR>\n";
            List<ORColumn> attribs = table.getDeclaredColumns();
            for (ORColumn a : attribs) {
                label = label + createColumnLabel(a, false);
            }
            if (showInheritedAttribs) {
                for (ORColumn a : table.getAllColumns()) {
                    if (!attribs.contains(a)) {
                        label = label + createColumnLabel(a, true);
                    }
                }
            }
            label = label + "</TABLE></FONT>>";
            n.setGVAttribute("label", label);
        }

        for (ORTable table : schema.getTables()) {
            for (ORTable parent : table.getSuperClasses()) {
                Node tail = g.getNode(parent.getName());
                Node head = g.getNode(table.getName());
                Edge e = g.createEdge(parent.getName(), table.getName(),
                    (Properties) inheritanceProperties.clone());
                e.setTailPort("t");
                e.setHeadPort("t");
            }
        }

        Color infRefColor = AWTUtils.rgba2color((String)inferredReferenceProperties.getProperty("color"));
        
        for (ORTable table : schema.getTables()) {
            for (ORColumn column : table.getDeclaredColumns()) {
                ORColumn refColumn = column.getReferencedColumn();
                if (refColumn != null) {
                    ORTable refTable = column.getReferencedTable();
                    Node outNode = g.getNode(table.getName());
                    Node inNode = g.getNode(refTable.getName());
                    Edge e = g.createEdge(outNode, inNode,
                            (Properties) referenceProperties.clone());
                    e.setTailPort(portStr(column.getName()) + ".out");
                    e.setHeadPort(portStr(refColumn.getName()) + ".in");
                }
                if (showInferredRefs != showInferredReferences.NONE) {
                    List<ORColumn.ForeignKeyCandidate> candidates = new ArrayList<ORColumn.ForeignKeyCandidate>(column.getInferredForeignKeys());
                    if (candidates.size() > 0) {
                        CollectionUtils.sortList(candidates, new Comparator<ORColumn.ForeignKeyCandidate>() {
                            public int compare(ORColumn.ForeignKeyCandidate one, ORColumn.ForeignKeyCandidate other) {
                                return -Double.valueOf(one.getMapingScore()).compareTo(other.getMapingScore());
                            }
                        });
                        double thr = 0;
                       if (showInferredRefs == showInferredReferences.THRESHOLD) {
                            thr = mappingThreshold;
                        }
                        else if (showInferredRefs == showInferredReferences.BEST) {
                            thr = candidates.get(0).getMapingScore();
                            if (thr < mappingThreshold) {
                                thr = mappingThreshold;
                            }
                        }
                        for (int i = 0; (i < candidates.size()) && (candidates.get(i).getMapingScore() >= thr); i++) {
                            ORColumn.ForeignKeyCandidate cand = candidates.get(i);
                            Node outNode = g.getNode(table.getName());
                            Node inNode = g.getNode(cand.getReferencedTable().getName());
                            Edge e = g.createEdge(outNode, inNode,
                                    (Properties) inferredReferenceProperties.clone());
                            e.setTailPort(portStr(column.getName()) + ".out");
                            e.setHeadPort(portStr(cand.getReferencedColumn().getName()) + ".in");
                            if (shadeInferredRefs) {
                                Color shade = new Color(
                                    (int)(infRefColor.getRed() + (1.0 - cand.getMapingScore())*(255.0 - infRefColor.getRed())),
                                    (int)(infRefColor.getGreen() + (1.0 - cand.getMapingScore())*(255.0 - infRefColor.getGreen())),
                                    (int)(infRefColor.getBlue() + (1.0 - cand.getMapingScore())*(255.0 - infRefColor.getBlue()))
                                );
                                e.setGVAttribute("color", "#" + AWTUtils.color2rgba(shade));
                                
                            }
                        }
                        
                    }
                }
                
            }
        }
        return g;
    }

}

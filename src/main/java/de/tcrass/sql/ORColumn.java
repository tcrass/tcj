package de.tcrass.sql;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ORColumn {

    public class ForeignKeyCandidate {

        private ORColumn column;
        private ORTable  table;
        private double   score = 0;

        public ForeignKeyCandidate(ORColumn column, ORTable table, double score) {
            this.column = column;
            this.table = table;
            this.score = score;
        }

        public ForeignKeyCandidate(ORColumn column, double score) {
            this(column, column.getTable(), score);
        }
        
        public ORColumn getReferencedColumn() {
            return column;
        }
        
        public ORTable getReferencedTable() {
            return table;
        }
        
        public double getMapingScore() {
            return score;
        }
        
    }
    
    private ORTable  owner;
    private String   name;
    private boolean  notNull;
    private boolean  unique;
    private boolean  primaryKey;
    private ORColumn referencedColumn;
    private ORTable  referencedTable;
//    private boolean  isInferredFK = false;
    private Set<ForeignKeyCandidate> refCandidates = new HashSet<ForeignKeyCandidate>();
    private String   type;

    protected ORColumn(ORTable owner, String name, String type) {
        this.owner = owner;
        this.name = name;
        this.type = type;
    }

    public ORTable getTable() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public String getQName() {
        return owner.getName() + "." + getName();
    }

    public boolean isForeignKey() {
        return referencedColumn != null;
    }

    public Set<ForeignKeyCandidate> getInferredForeignKeys() {
        return refCandidates;
    }

    public void setInferredForeignKeys(Set<ForeignKeyCandidate> candidates) {
        refCandidates = candidates;
    }

    public void addInferredForeignKey(ForeignKeyCandidate candidate) {
        refCandidates.add(candidate);
    }

    public void addInferredForeignKey(ORColumn column, ORTable table, double score) {
        refCandidates.add(new ForeignKeyCandidate(column, table, score));
    }

    public void addInferredForeignKey(ORColumn column, double score) {
        refCandidates.add(new ForeignKeyCandidate(column, score));
    }

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    /* packagescope */void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public ORColumn getReferencedColumn() {
        return referencedColumn;
    }

    public void setReferencedColumn(ORColumn referencedColumn) {
        this.referencedColumn = referencedColumn;
        setReferencedTable(referencedColumn.getTable());
    }

    public ORTable getReferencedTable() {
        return referencedTable;
    }

    public void setReferencedTable(ORTable referencedTable) {
        this.referencedTable = referencedTable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public String toString() {
        return "ORColumn(name='" + name + "', type=" + type + ", flags=" +
               (isPrimaryKey() ? "#" : "") + (isUnique() ? "U" : "") +
               (isNotNull() ? "N" : "") + ")";
    }

}

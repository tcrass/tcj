/*
 * 7 Created on 02.11.2004 TODO To change the template for this
 * generated file go to Window - Preferences - Java - Code Style - Code
 * Templates
 */
package de.tcrass.sql;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import de.tcrass.util.CollectionUtils;
import de.tcrass.util.ListOfMaps;
import de.tcrass.util.MapOfMaps;
import de.tcrass.util.SuffixTree;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class EasySQL {

    public enum findRefsForPKs {
        NONE,
        MULTIPLE,
        ALL
    }
    
    private String                               driver;
    private ORDBHelper                analyzer;
    private Hashtable<String, PreparedStatement> SIMPLE_CONDITIONAL_QUERIES = new Hashtable<String, PreparedStatement>();

    // private boolean canExcept = true;

    private Connection                           connection;

    private static String simpleQueryKey(String tableName, String fieldName) {
        return tableName + "::" + fieldName;
    }

    public static ListOfMaps<String, Object> resultSet2listOfMaps(ResultSet rset) {
        ListOfMaps<String, Object> result = null;
        if (rset != null) {
            try {
                result = new ListOfMaps<String, Object>();
                ResultSetMetaData md = rset.getMetaData();
                int cols = md.getColumnCount();
                String[] colNames = new String[cols];
                for (int i = 0; i < cols; i++) {
                    colNames[i] = md.getColumnName(i + 1);
                }
                while (rset.next()) {
                    Map<String, Object> row = new LinkedHashMap<String, Object>();
                    for (int i = 0; i < cols; i++) {
                        Object o = rset.getObject(i + 1);
                        if (o != null) {
                            row.put(colNames[i], o);
                        }
                    }
                    result.add(row);
                }
            }
            catch (SQLException e) {
                SysUtils.reportException(e);
            }
        }
        return result;
    }

    public static MapOfMaps<Object, String, Object> resultSet2mapOfMaps(ResultSet rset,
                                                                        String key) {
        MapOfMaps<Object, String, Object> result = null;
        if (rset != null) {
            try {
                result = new MapOfMaps<Object, String, Object>();
                ResultSetMetaData md = rset.getMetaData();
                int cols = md.getColumnCount();
                String[] colNames = new String[cols];
                int keyIndex = -1;
                for (int i = 0; i < cols; i++) {
                    colNames[i] = md.getColumnName(i + 1);
                    if (colNames[i].equals(key)) {
                        keyIndex = i;
                    }
                }
                if (keyIndex >= 0) {
                    while (rset.next()) {
                        Map<String, Object> row = new LinkedHashMap<String, Object>();
                        for (int i = 0; i < cols; i++) {
                            Object o = rset.getObject(i + 1);
                            if (o != null) {
                                row.put(colNames[i], o);
                            }
                        }
                        result.put(row.get(key), row);
                    }
                }
            }
            catch (SQLException e) {
                SysUtils.reportException(e);
            }
        }
        return result;
    }

    public static List<Object> column2list(String colName, ResultSet rset) {
        ArrayList<Object> result = null;
        if (rset != null) {
            result = new ArrayList<Object>();
            try {
                while (rset.next()) {
                    result.add(rset.getObject(colName));
                }
            }
            catch (SQLException e) {
                SysUtils.reportException(e);
            }
        }
        return result;
    }

    // --- Constructors --------------------------------------

    public EasySQL(String confname) throws IOException {
        this(CollectionUtils.loadHostProperties(confname));
    }

    public EasySQL(File conffile) throws IOException {
        this(CollectionUtils.loadProperties(conffile));
    }

    public EasySQL(Properties p) {
        this(p.getProperty("driver"), p.getProperty("url"), p);
    }

    public EasySQL(String driver, String url, String user, String password) {
        this(driver, url, user, password, new Properties());
    }

    public EasySQL(String driver, String url, String user, String password, Properties prop) {
        Properties p = new Properties(prop);
        if ((user != null)) {
            p.setProperty("user", user);
        }
        if ((password != null) && (!password.equals(""))) {
            p.setProperty("password", password);
        }
        init(driver, url, p);
    }

    public EasySQL(String driver, String url, Properties p) {
        init(driver, url, p);
    }

    protected void init(String driver, String url, Properties p) {
        try {
            Class.forName(driver);

            connection = DriverManager.getConnection(url, p);
            this.driver = driver;
            analyzer = ORDBHelperFactory.createFor(this);
        }
        catch (ClassNotFoundException e) {
            SysUtils.reportException(e, "JDBC driver not installed or not in classpath?");
        }
        catch (SQLException e) {
            SysUtils.reportException(e,
                "Database not reachable, wrong driver or typo in username/password?");
        }
    }

    // --- Connection info ---

    public boolean isConnected() {
        return (connection != null);
    }

    public Connection getConnection() {
        return connection;
    }

    public String getDriverName() {
        return driver;
    }

    // --- Query ---

    public Statement createStatement() {
        Statement stm = null;
        try {
            stm = connection.createStatement();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return stm;
    }

    public ResultSet executeQuery(String sql) {
        ResultSet rs = null;
        try {
            Statement stm = createStatement();
            rs = stm.executeQuery(sql);
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return rs;
    }

    public ResultSet executePreparedStatement(PreparedStatement stm) {
        ResultSet rs = null;
        try {
            rs = stm.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return rs;
    }

    public PreparedStatement prepareStatement(String template) {
        PreparedStatement stm = null;
        try {
            stm = connection.prepareStatement(template);
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return stm;
    }

    public ListOfMaps<String, Object> getAllRecordsFromTable(String tableName) {
        ResultSet rset = executeQuery("SELECT * FROM " + tableName);
        return resultSet2listOfMaps(rset);
    }

    public MapOfMaps<Object, String, Object> getRecordMapFromTable(String tableName, String key) {
        ResultSet rset = executeQuery("SELECT * FROM " + tableName);
        return resultSet2mapOfMaps(rset, key);
    }

    public List<Object> getAllRecordsFromTable(String colName, String tableName) {
        ResultSet rset = executeQuery("SELECT " + colName + " FROM " + tableName);
        return column2list(colName, rset);
    }

    private ResultSet getResultSetFromTableWhere(String tableName, String fieldName, int value) {
        ResultSet rset = null;
        PreparedStatement s = null;
        if (SIMPLE_CONDITIONAL_QUERIES.contains(simpleQueryKey(tableName, fieldName))) {
            s = SIMPLE_CONDITIONAL_QUERIES.get(simpleQueryKey(tableName, fieldName));
        }
        else {
            s = prepareStatement("SELECT * FROM " + tableName + " WHERE " + fieldName + "=?");
            SIMPLE_CONDITIONAL_QUERIES.put(simpleQueryKey(tableName, fieldName), s);
        }
        try {
            s.setInt(1, value);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return rset;
    }

    public ListOfMaps<String, Object> getRecordsFromTableWhere(String tableName,
                                                               String fieldName, int value) {
        ResultSet rset = getResultSetFromTableWhere(tableName, fieldName, value);
        return resultSet2listOfMaps(rset);
    }

    public MapOfMaps<Object, String, Object> getRecordMapFromTableWhere(String tableName,
                                                                        String fieldName,
                                                                        int value, String key) {
        ResultSet rset = getResultSetFromTableWhere(tableName, fieldName, value);
        return resultSet2mapOfMaps(rset, key);
    }

    private ResultSet getResultSetFromTableWhere(String tableName, String fieldName,
                                                 String value) {
        ResultSet rset = null;
        PreparedStatement s = null;
        if (SIMPLE_CONDITIONAL_QUERIES.contains(simpleQueryKey(tableName, fieldName))) {
            s = SIMPLE_CONDITIONAL_QUERIES.get(simpleQueryKey(tableName, fieldName));
        }
        else {
            s = prepareStatement("SELECT * FROM " + tableName + " WHERE " + fieldName + "=?");
            SIMPLE_CONDITIONAL_QUERIES.put(simpleQueryKey(tableName, fieldName), s);
        }
        try {
            s.setString(1, value);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return rset;
    }

    public ListOfMaps<String, Object> getRecordsFromTableWhere(String tableName,
                                                               String fieldName, String value) {
        ResultSet rset = getResultSetFromTableWhere(tableName, fieldName, value);
        return resultSet2listOfMaps(rset);
    }

    public MapOfMaps<Object, String, Object> getRecordMapFromTableWhere(String tableName,
                                                                        String fieldName,
                                                                        String value, String key) {
        ResultSet rset = getResultSetFromTableWhere(tableName, fieldName, value);
        return resultSet2mapOfMaps(rset, key);
    }

    public Map<String, Object> getRecordFromTableWhere(String tableName, String fieldName,
                                                       int value) {
        ListOfMaps<String, Object> result = getRecordsFromTableWhere(tableName, fieldName,
            value);
        return result.getRow(0);
    }

    public Map<String, Object> getRecordFromTableWhere(String tableName, String fieldName,
                                                       String value) {
        ListOfMaps<String, Object> result = getRecordsFromTableWhere(tableName, fieldName,
            value);
        return result.getRow(0);
    }

    public List<Object> getFieldsFromTableWhere(String colName, String tableName,
                                                String fieldName, String value) {
        ResultSet rset = null;
        PreparedStatement stm = prepareStatement("SELECT " + colName + " FROM " + tableName +
                                                 " WHERE " + fieldName + "=?");
        try {
            stm.setString(1, value);
            rset = stm.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return column2list(colName, rset);
    }

    public List<Object> getFieldsFromTableWhere(String colName, String tableName,
                                                String fieldName, int value) {
        ResultSet rset = null;
        PreparedStatement stm = prepareStatement("SELECT " + colName + " FROM " + tableName +
                                                 " WHERE " + fieldName + "=?");
        try {
            stm.setInt(1, value);
            rset = stm.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return column2list(colName, rset);
    }

    public Object getFieldFromTableWhere(String colName, String tableName, String fieldName,
                                         String value) {
        Object result = null;
        List<Object> v = getFieldsFromTableWhere(colName, tableName, fieldName, value);
        if (v.size() > 0) {
            result = v.get(0);
        }
        return result;
    }

    public Object getFieldFromTableWhere(String colName, String tableName, String fieldName,
                                         int value) {
        Object result = null;
        List<Object> v = getFieldsFromTableWhere(colName, tableName, fieldName, value);
        if (v.size() > 0) {
            result = v.get(0);
        }
        return result;
    }

    public ListOfMaps<String, Object> getRecordsFromTableWhereFunction(String tableName,
                                                                       String functionName,
                                                                       String fieldName,
                                                                       String value) {
        ResultSet rset = null;
        PreparedStatement s = prepareStatement("SELECT * FROM " + tableName + " WHERE " +
                                               functionName + "(" + fieldName + ",?)");
        try {
            s.setString(1, value);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return resultSet2listOfMaps(rset);
    }

    public Map<String, Object> getRecordFromTableWhereFunction(String tableName,
                                                               String functionName,
                                                               String fieldName, String value) {
        ListOfMaps<String, Object> result = getRecordsFromTableWhereFunction(tableName,
            functionName, fieldName, value);
        return result.getRow(0);
    }

    public ListOfMaps<String, Object> getRecordsFromTableWhereFunction(String tableName,
                                                                       String functionName,
                                                                       String fieldName,
                                                                       int value) {
        ResultSet rset = null;
        PreparedStatement s = prepareStatement("SELECT * FROM " + tableName + " WHERE " +
                                               functionName + "(" + fieldName + ",?)");
        try {
            s.setInt(1, value);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return resultSet2listOfMaps(rset);
    }

    public Map<String, Object> getRecordFromTableWhereFunction(String tableName,
                                                               String functionName,
                                                               String fieldName, int value) {
        ListOfMaps<String, Object> result = getRecordsFromTableWhereFunction(tableName,
            functionName, fieldName, value);
        return result.getRow(0);
    }

    public List<Object> getFieldsFromTableWhereFunction(String colName, String tableName,
                                                        String functionName, String fieldName,
                                                        String value) {
        ResultSet rset = null;
        PreparedStatement s = prepareStatement("SELECT " + colName + " FROM " + tableName +
                                               " WHERE " + functionName + "(" + fieldName +
                                               ",?)");
        try {
            s.setString(1, value);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return column2list(colName, rset);
    }

    public Object getFieldFromTableWhereFunction(String colName, String tableName,
                                                 String functionName, String fieldName,
                                                 String value) {
        Object result = null;
        List<Object> v = getFieldsFromTableWhereFunction(colName, tableName, functionName,
            fieldName, value);
        if (v.size() > 0) {
            result = v.get(0);
        }
        return result;
    }

    public List<Object> getFieldsFromTableWhereFunction(String colName, String tableName,
                                                        String functionName, String fieldName,
                                                        int value) {
        ResultSet rset = null;
        PreparedStatement s = prepareStatement("SELECT " + colName + " FROM " + tableName +
                                               " WHERE " + functionName + "(" + fieldName +
                                               ",?)");
        try {
            s.setInt(1, value);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return column2list(colName, rset);
    }

    public Object getFieldFromTableWhereFunction(String colName, String tableName,
                                                 String functionName, String fieldName,
                                                 int value) {
        Object result = null;
        List<Object> v = getFieldsFromTableWhereFunction(colName, tableName, functionName,
            fieldName, value);
        if (v.size() > 0) {
            result = v.get(0);
        }
        return result;
    }

    public boolean existsRecordInTableWhere(String tableName, String fieldName, String value) {
        return getRecordsFromTableWhere(tableName, fieldName, value).size() > 0;
    }

    public boolean existsRecordInTableWhere(String tableName, String fieldName, int value) {
        return getRecordsFromTableWhere(tableName, fieldName, value).size() > 0;
    }

    // --- update, insert, delete ---

    public void fillStatementPlaceholders(PreparedStatement s, List<Object> values)
    throws SQLException {
        for (int i = 0; i < values.size(); i++) {
            Object v = values.get(i);
            if (v instanceof Boolean) {
                s.setBoolean(i + 1, ((Boolean) v).booleanValue());
            }
            else if (v instanceof Byte) {
                s.setInt(i + 1, ((Byte) v).intValue());
            }
            else if (v instanceof Short) {
                s.setInt(i + 1, ((Short) v).intValue());
            }
            else if (v instanceof Integer) {
                s.setInt(i + 1, ((Integer) v).intValue());
            }
            else if (v instanceof Long) {
                s.setLong(i + 1, ((Long) v).longValue());
            }
            else if (v instanceof Float) {
                s.setFloat(i + 1, ((Float) v).floatValue());
            }
            else if (v instanceof Double) {
                s.setDouble(i + 1, ((Double) v).doubleValue());
            }
            else if (v instanceof String) {
                s.setString(i + 1, ((String) v));
            }
        }
    }

    public void fillStatementPlaceholders(PreparedStatement s, Map<String, Object> fieldsValues)
    throws SQLException {
        List<Object> values = new ArrayList<Object>();
        for (String key : fieldsValues.keySet()) {
            values.add(fieldsValues.get(key));
        }
        fillStatementPlaceholders(s, values);
    }

    public int insertIntoTable(String tableName, Map<String, Object> fieldsValues) {
        int n = -1;
        StringBuffer stm = new StringBuffer("INSERT INTO " + tableName + " (");
        for (Iterator<String> i = fieldsValues.keySet().iterator(); i.hasNext();) {
            String field = i.next();
            stm.append(field);
            if (i.hasNext()) {
                stm.append(", ");
            }
        }
        stm.append(") VALUES (");
        for (Iterator<String> i = fieldsValues.keySet().iterator(); i.hasNext();) {
            i.next();
            stm.append("?");
            if (i.hasNext()) {
                stm.append(", ");
            }
        }
        stm.append(")");
        PreparedStatement s = prepareStatement(stm.toString());
        try {
            fillStatementPlaceholders(s, fieldsValues);
            n = s.executeUpdate();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return n;
    }

    private String createBasicUpdateString(String tableName, Map<String, Object> fieldsValues) {
        StringBuffer stm = new StringBuffer("UPDATE " + tableName + " SET ");
        for (Iterator<String> i = fieldsValues.keySet().iterator(); i.hasNext();) {
            String field = i.next();
            stm.append(field + "=?");
            if (i.hasNext()) {
                stm.append(", ");
            }
        }
        return stm.toString();
    }

    public int updateTableWhere(String tableName, Map<String, Object> fieldsValues,
                                String fieldName, String value) {
        int n = -1;
        String stm = createBasicUpdateString(tableName, fieldsValues) + " WHERE " + fieldName +
                     "=?";
        PreparedStatement s = prepareStatement(stm);
        try {
            fillStatementPlaceholders(s, fieldsValues);
            s.setString(fieldsValues.keySet().size() + 1, value);
            n = s.executeUpdate();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return n;
    }

    public int updateTableWhere(String tableName, Map<String, Object> fieldsValues,
                                String fieldName, int value) {
        int n = -1;
        String stm = createBasicUpdateString(tableName, fieldsValues) + " WHERE " + fieldName +
                     "=?";
        PreparedStatement s = prepareStatement(stm);
        try {
            fillStatementPlaceholders(s, fieldsValues);
            s.setInt(fieldsValues.keySet().size() + 1, value);
            n = s.executeUpdate();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return n;
    }

    public int insertOrUpdateTableWhere(String tableName, Map<String, Object> fieldsValues,
                                        String fieldName, String value) {
        int n = -1;
        if (existsRecordInTableWhere(tableName, fieldName, value)) {
            n = updateTableWhere(tableName, fieldsValues, fieldName, value);
        }
        else {
            n = insertIntoTable(tableName, fieldsValues);
        }
        return n;
    }

    public int insertOrUpdateTableWhere(String tableName, Map<String, Object> fieldsValues,
                                        String fieldName, int value) {
        int n = -1;
        if (existsRecordInTableWhere(tableName, fieldName, value)) {
            n = updateTableWhere(tableName, fieldsValues, fieldName, value);
        }
        else {
            n = insertIntoTable(tableName, fieldsValues);
        }
        return n;
    }

    public int clearTable(String tableName) {
        int n = -1;
        PreparedStatement s = prepareStatement("DELETE FROM " + tableName);
        try {
            n = s.executeUpdate();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return n;
    }

    public int deleteFromTableWhere(String tableName, String fieldName, String value) {
        int n = -1;
        PreparedStatement s = prepareStatement("DELETE FROM " + tableName + " WHERE " +
                                               fieldName + "=?");
        try {
            s.setString(1, value);
            n = s.executeUpdate();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return n;
    }

    public int deleteFromTableWhere(String tableName, String fieldName, int value) {
        int n = -1;
        PreparedStatement s = prepareStatement("DELETE FROM " + tableName + " WHERE " +
                                               fieldName + "=?");
        try {
            s.setInt(1, value);
            n = s.executeUpdate();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        return n;
    }

    // --- Schema building ---

    private ORTable addTableToSchema(String tableName, ORSchema schema) throws SQLException {
        ORTable table = schema.getTable(tableName);
        if (table == null) {
            table = schema.createTable(tableName);
            for (String parentName : analyzer.getParentTables(tableName)) {
                ORTable parent = addTableToSchema(parentName, schema);
                table.makeSubClassOf(parent);
            }
            ResultSet crset = connection.getMetaData().getColumns(null, null, tableName, null);
            ListOfMaps<String, Object> columns = EasySQL.resultSet2listOfMaps(crset);
            List<String> columnNames = table.getAllColumnNames();
            for (Map<String, Object> columnDescr : columns) {
                keys2uc(columnDescr);
                String columnName = (String) columnDescr.get("COLUMN_NAME");
                if (!columnNames.contains(columnName)) {
                    int dataType = Integer.parseInt(columnDescr.get("DATA_TYPE").toString());
                    String typeName = ((String) columnDescr.get("TYPE_NAME")).toUpperCase();
                    String type = typeName;
                    if ((dataType == Types.VARCHAR) && !typeName.toUpperCase().equals("TEXT")) {
                        int len = Integer.valueOf(columnDescr.get("CHAR_OCTET_LENGTH").toString());
                        if (len >= 0) {
                            type += "(" + len + ")";
                        }
                    }

                    ORColumn column = table.createColumn(columnName, type);

                    int nullable = Integer.parseInt(columnDescr.get("NULLABLE").toString());
                    if (nullable == DatabaseMetaData.columnNoNulls) {
                        column.setNotNull(true);
                    }
                }
            }
            ResultSet pkrset = connection.getMetaData().getPrimaryKeys(null, null, tableName);
            ListOfMaps<String, Object> pKeys = EasySQL.resultSet2listOfMaps(pkrset);
            columnNames = table.getDeclaredColumnNames();
            for (Map<String, Object> pkDescr : pKeys) {
                keys2uc(pkDescr);
                String pkName = (String) pkDescr.get("COLUMN_NAME");
                if (columnNames.contains(pkName)) {
                    table.getDeclaredColumn(pkName).setPrimaryKey(true);
                }
            }
        }
        return table;
    }

    private void keys2uc(Map<String, Object> map) {
        Set<String> keys = new LinkedHashSet<String>(map.keySet());
        for (String key : keys) {
            Object val = map.remove(key);
            map.put(key.toUpperCase(), val);
        }
    }

    public ORSchema getORSchema() {
        return getORSchema(false, 0, false, findRefsForPKs.MULTIPLE);
    }

    public ORSchema getORSchema(boolean inferRelationships, double minScore, boolean doSubsetChecks, findRefsForPKs pkPolicy) {
        ORSchema schema = new ORSchema();
        try {
            DatabaseMetaData dmd = connection.getMetaData();
            // Get descriptions of all tables
            ResultSet trset = dmd.getTables(null, null, null, new String[] {
                "TABLE"
            });
            ListOfMaps<String, Object> tlist = EasySQL.resultSet2listOfMaps(trset);
            // Iterate through all tables' data...
            for (Map<String, Object> tmap : tlist) {
                keys2uc(tmap);
                String tableName = (String) tmap.get("TABLE_NAME");
                // ...and add to schema
                addTableToSchema(tableName, schema);
            }
            // Find all foreign key constraints
            for (ORTable table : schema.getTables()) {
                List<String> columnNames = table.getDeclaredColumnNames();
                ResultSet fkrset = dmd.getImportedKeys(null, null, table.getName());
                ListOfMaps<String, Object> fklist = EasySQL.resultSet2listOfMaps(fkrset);
                for (Map<String, Object> fkDescr : fklist) {
                    keys2uc(fkDescr);
                    String fkName = (String) fkDescr.get("FKCOLUMN_NAME");
                    String pkTableName = (String) fkDescr.get("PKTABLE_NAME");
                    String pkColumnName = (String) fkDescr.get("PKCOLUMN_NAME");
                    if (columnNames.contains(fkName)) {
                        ORTable t = schema.getTable(pkTableName);
                        ORColumn c = t.getDeclaredColumn(pkColumnName);
                        ORColumn fk = table.getDeclaredColumn(fkName);
                        fk.setReferencedColumn(c);
                    }
                }
            }

            // If requested, guess not specifically declared foreign
            // keys
        }
        catch (SQLException ex) {
            SysUtils.reportException(ex);
        }
        if (inferRelationships) {
            guessForeignKeys(schema, minScore, doSubsetChecks, pkPolicy);
        }
        return schema;
    }

    private boolean isIdSubset(ORColumn col, ORColumn refCol) {
        boolean isSubset = true;
        String stm;
        // if (canExcept) {
        // String tmpName = col.getTable().getName() + "__" +
        // refCol.getTable().getName();
        // stm = "SELECT COUNT(" + tmpName + "." + col.getName() + ")
        // FROM (" +
        // "SELECT " + col.getName() + " FROM " +
        // col.getTable().getName() + " EXCEPT " +
        // "SELECT " + refCol.getName() + " FROM " +
        // refCol.getTable().getName() +
        // ") AS " + tmpName;
        //    	
        // }
        // else {
        stm = "SELECT COUNT(" + col.getName() + ") " + "FROM " + col.getTable().getName() +
              " " + "WHERE " + col.getName() + " IS NULL OR (" + col.getName() + " NOT IN (" +
              "SELECT " + refCol.getName() + " " + "FROM " + refCol.getTable().getName() + "))";
        // }
//        System.out.println("SQL: " + stm);
        ResultSet rs = executeQuery(stm);
        try {
            rs.next();
//            System.out.print("     => " + rs.getLong(1));
            if (rs.getLong(1) > 0) {
                isSubset = false;
//                System.out.println(" => FALSE");
            }
            else {
//                System.out.println(" => TRUE");
            }
        }
        catch (Exception ex) {
            isSubset = false;
            SysUtils.reportException(ex);
        }
        return isSubset;
    }

    private void guessForeignKeys(ORSchema schema, double minScore, boolean doSubsetChecks, findRefsForPKs pkPolicy) {

        Map<ORTable, List<ORColumn>> pks = new HashMap<ORTable, List<ORColumn>>();
        for (ORTable table : schema.getTables()) {
            List<ORColumn> pkList = new ArrayList<ORColumn>();
            pks.put(table, pkList);
            for (ORColumn col : table.getAllColumns()) {
                if (analyzer.isPossiblePrimaryKey(col)) {
                    pkList.add(col);
                }
            }
        }
        
//        Map<ORColumn, PositionList<ORColumn.ForeignKeyCandidate>> candidates = new HashMap<ORColumn, PositionList<ForeignKeyCandidate>>();
        for (ORTable table1 : schema.getTables()) {
//            System.out.println("Analysing table " + table1.getName());

            for (ORTable table2 : schema.getTables()) {

                if (table1 != table2) {

//                    System.out.println("  Comparing to table " + table2.getName());

//                    System.out.println("    Found primary keys: " + CollectionUtils.listToString(pks2));
                    if (pks.get(table2).size() == 1) {
                        ORColumn pk2 = pks.get(table2).get(0);
//                        System.out.println("    => singular primary key!");

                        for (ORColumn col1 : table1.getDeclaredColumns()) {
                            if ((pkPolicy == findRefsForPKs.ALL) ||
                                ((pkPolicy == findRefsForPKs.MULTIPLE) && ((pks.get(table1).size() > 1)) ||
                                !analyzer.isPossiblePrimaryKey(col1))) {
//                                if (candidates.get(col1) == null) {
//                                    candidates.put(col1, new PositionList<ForeignKeyCandidate>());
//                                }

//                                System.out.println("    Checking " + col1.getQName());

                                if ((col1.getReferencedColumn() == null) &&
                                    analyzer.areCompatibleColumns(pk2, col1)) {

//                                    System.out.println("    => " + col1.getQName() +
//                                                       " is type candidate");
                                    System.out.println("      Checking col name " +
                                                       col1.getName() + " against table name " +
                                                       table2.getName());

                                    SuffixTree st = new SuffixTree();
                                    st.addString(col1.getName().toLowerCase());
                                    st.addString(table2.getName().toLowerCase());
                                    List<String> lcs = st.getLongestCommonSubstrings(0);
                                    int len = lcs.get(lcs.size() - 1).length();

//                                    System.out.println("      Found CSs " +
//                                                       CollectionUtils.listToString(lcs) +
//                                                       ", length of LCSs is " + len);

                                    for (int i = lcs.size() - 1; (i >= 0) &&
                                                                 (lcs.get(i).length() == len); i--) {
                                        String cPattern = ".*" + lcs.get(i) + "\\w{0,3}";
                                        String tPattern = ".*" + lcs.get(i) + "\\w{0,2}";

//                                        System.out.println("        Testing pattern " +
//                                                           cPattern + " on " + col1.getName() +
//                                                           " and " + tPattern + " on " +
//                                                           table2.getName());

                                        if (col1.getName().toLowerCase().matches(cPattern) &&
                                            table2.getName().toLowerCase().matches(tPattern)) {
                                            double score = 0.5 * len / col1.getName().length() +
                                                           0.5 * len /
                                                           table2.getName().length();

//                                            System.out.println("        => Yields score " +
//                                                               score);

                                            if ((score > minScore) &&
                                                (!doSubsetChecks || isIdSubset(col1, pk2))) {
//                                                System.out.println("          * Adding " +
//                                                                   col1.getQName() +
//                                                                   " as possible fk referencing " +
//                                                                   pk2.getQName());
                                                col1.addInferredForeignKey(pk2, table2, score);
                                            }
                                        }
                                    }

//                                    System.out.println("      Checking col name " +
//                                                       col1.getName() +
//                                                       " against pk column name " +
//                                                       pk2.getName());
                                    st = new SuffixTree();
                                    st.addString(col1.getName().toLowerCase());
                                    st.addString(pk2.getName().toLowerCase());
                                    lcs = st.getLongestCommonSubstrings(0);
                                    len = lcs.get(lcs.size() - 1).length();

//                                    System.out.println("      Found CSs " +
//                                                       CollectionUtils.listToString(lcs) +
//                                                       ", length of LCSs is " + len);

                                    for (int i = lcs.size() - 1; (i >= 0) &&
                                                                 (lcs.get(i).length() == len); i--) {
                                        String cPattern = ".*" + lcs.get(i) + "\\w{0,3}";
                                        String pkPattern = ".*" + lcs.get(i) + "\\w{0,2}";

//                                        System.out.println("        Testing pattern " +
//                                                           cPattern + " on " + col1.getName() +
//                                                           " and " + pkPattern + " on " +
//                                                           pk2.getName());

                                        if (col1.getName().toLowerCase().matches(cPattern) &&
                                            table2.getName().toLowerCase().matches(pkPattern)) {
                                            double score = 0.5 * lcs.get(i).length() /
                                                           col1.getName().length() + 0.5 *
                                                           lcs.get(i).length() /
                                                           pk2.getName().length();

//                                            System.out.println("        => Yields score " +
//                                                               score);

                                            if ((score > minScore) &&
                                                (!doSubsetChecks || isIdSubset(col1, pk2))) {
//                                                System.out.println("          * Adding " +
//                                                                   col1.getQName() +
//                                                                   " as possible fk referencing " +
//                                                                   pk2.getQName());
                                                col1.addInferredForeignKey(pk2, table2, score);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
//        for (ORColumn col1 : candidates.keySet()) {
//            if (col1.getReferencedColumn() == null) {
//                PositionList<ForeignKeyCandidate> references = candidates.get(col1);
//                if (references.size() > 0) {
//                    ForeignKeyCandidate cm = references.elementAtIndex(references.size() - 1);
////                    System.out.println("Setting " +
////                                       col1.getQName() +
////                                       " -> " +
////                                       cm.column.getName() + " in table " + cm.table.getName());
//                    col1.setReferencedColumn(cm.column);
//                    col1.setReferencedTable(cm.table );
//                    col1.setIsInferredForeignKey(true);
//                }
//            }
//        }
    }


}

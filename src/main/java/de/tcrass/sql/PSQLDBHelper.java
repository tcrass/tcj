package de.tcrass.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.tcrass.util.SysUtils;

public class PSQLDBHelper extends DefaultORDBHelper {

    @Override
    public List<String> getParentTables(String tableName) {
        ResultSet rset = null;
        PreparedStatement s = esql.prepareStatement("SELECT p.relname FROM  pg_class AS p INNER JOIN ("
                                                    + "  SELECT t.inhparent FROM ("
                                                    + "    SELECT * FROM pg_inherits AS i WHERE i.inhrelid IN ("
                                                    + "      SELECT q.oid FROM pg_class AS q WHERE q.relname=?"
                                                    + "    ) ORDER BY i.inhseqno"
                                                    + "  ) AS t "
                                                    + ") AS x ON p.oid=x.inhparent");
        try {
            s.setString(1, tableName);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        List<String> result = new ArrayList<String>();
        for (Object table : EasySQL.column2list("relname", rset)) {
            result.add((String) table);
        }
        return result;
    }

    @Override
    public List<String> getChildTables(String tableName) {
        ResultSet rset = null;
        PreparedStatement s = esql.prepareStatement("SELECT p.relname FROM  pg_class AS p INNER JOIN ("
                                                    + "  SELECT t.inhrelid FROM ("
                                                    + "    SELECT * FROM pg_inherits AS i WHERE i.inhparent IN ("
                                                    + "      SELECT q.oid FROM pg_class AS q WHERE q.relname=?"
                                                    + "    ) ORDER BY i.inhseqno"
                                                    + "  ) AS t "
                                                    + ") AS x ON p.oid=x.inhrelid");
        try {
            s.setString(1, tableName);
            rset = s.executeQuery();
        }
        catch (SQLException e) {
            SysUtils.reportException(e);
        }
        List<String> result = new ArrayList<String>();
        for (Object table : EasySQL.column2list("relname", rset)) {
            result.add((String) table);
        }
        return result;
    }

    private boolean isCompatibleSerialForType(String serial, String type) {
        boolean compat = false;
        if ((serial != null)) {
            compat = ((serial.equals("SERIAL") || serial.equals("SERIAL4")) && (type.equals("INTEGER") || type.equals("INT4"))) ||
                     ((serial.equals("BIGSERIAL") || serial.equals("SERIAL8")) && (type.equals("BIGINT") || type.equals("INT8")));
        }
        return compat;
    }
    
    public boolean areCompatibleColumns(ORColumn col1, ORColumn col2) {
        boolean compat = super.areCompatibleColumns(col1, col2);
        if (!compat) {
            compat = isCompatibleSerialForType(col1.getType(), col2.getType()) || isCompatibleSerialForType(col2.getType(), col1.getType()); 
        }
        return compat;
    }
    
    public boolean isPossiblePrimaryKey(ORColumn col) {
        boolean isPPK = super.isPossiblePrimaryKey(col);
        if (!isPPK) {
            isPPK = (col.getType().startsWith("SERIAL") || col.getType().equals("BIGSERIAL"));
        }
        return isPPK;
    }
    
}

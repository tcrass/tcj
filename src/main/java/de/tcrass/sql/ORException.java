package de.tcrass.sql;

public class ORException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ORException(String msg) {
        super(msg);
    }

}

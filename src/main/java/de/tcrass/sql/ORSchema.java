package de.tcrass.sql;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tcrass.util.CollectionUtils;

public class ORSchema {

    private Map<String, ORTable> tables = new Hashtable<String, ORTable>();

    // --- constructors ---

    public ORSchema() {
        tables = new LinkedHashMap<String, ORTable>();
    }

    // --- public methods ---

    public ORTable createTable(String name) {
        if (tables.containsKey(name)) {
            throw new ORException("Table " + name + " already exists!");
        }
        ORTable t = new ORTable(this, name);
        tables.put(name, t);
        return t;
    }

    public ORTable getTable(String name) {
        return tables.get(name);
    }

    public List<ORTable> getTables() {
        return new ArrayList<ORTable>(tables.values());
    }

    public String toString() {
        return "ORSchema(tables=" + CollectionUtils.listToString(tables.keySet()) + ")";
    }
    
}

/*
 * Created on 21.05.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.conv;

import javax.media.Buffer;
import javax.media.Codec;
import javax.media.Format;
import javax.media.ResourceUnavailableException;
import javax.media.format.RGBFormat;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class FFMPEG2RGBCodec implements Codec {

    private Format   inputFormat            = new RGBFormat();
    private Format   outputFormat           = new RGBFormat();

    private Format[] supportedInputFormats  = new Format[] {
                                                new RGBFormat()
                                            };
    private Format[] supportedOutputFormats = new Format[] {
                                                new RGBFormat()
                                            };

    public Format[] getSupportedInputFormats() {
        return supportedInputFormats;
    }

    public Format setInputFormat(Format format) {
        if (format instanceof RGBFormat) {
            inputFormat = format;
        }
        return inputFormat;
    }

    public Format[] getSupportedOutputFormats(Format inFormat) {
        if (inFormat instanceof RGBFormat) {
            return supportedOutputFormats;
        }
        else {
            return new Format[0];
        }
    }

    public Format setOutputFormat(Format format) {
        if (format instanceof RGBFormat) {
            outputFormat = format.intersects(inputFormat);
        }
        return outputFormat;
    }

    public String getName() {
        return "FFMPEG Video to RGB conversion codec";
    }

    public void close() {
    }

    public void reset() {
    }

    public void open() throws ResourceUnavailableException {
    }

    public Object[] getControls() {
        return new Object[0];
    }

    public Object getControl(String ctrlName) {
        return null;
    }

    public int process(Buffer in, Buffer out) {
        System.out.println(inputFormat + " - >" + outputFormat);
        out.copy(in);
        // int[] inData = (int[])in.getData();
        // int[] outData = (int[])out.getData();
        // for (int i = 0; i < inData.length; i++) {
        // outData[i] = inData[i];
        // }
        return BUFFER_PROCESSED_OK;
    }

}

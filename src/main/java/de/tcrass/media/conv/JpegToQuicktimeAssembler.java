/*
 * Based on SUN's JpegToQuicktime JFM demonstration program
 */

package de.tcrass.media.conv;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.MediaTracker;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.util.List;

import javax.media.Buffer;
import javax.media.ConfigureCompleteEvent;
import javax.media.Controller;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.DataSink;
import javax.media.EndOfMediaEvent;
import javax.media.Format;
import javax.media.Manager;
import javax.media.MediaException;
import javax.media.MediaLocator;
import javax.media.NoDataSinkException;
import javax.media.PrefetchCompleteEvent;
import javax.media.Processor;
import javax.media.RealizeCompleteEvent;
import javax.media.ResourceUnavailableEvent;
import javax.media.Time;
import javax.media.control.TrackControl;
import javax.media.datasink.DataSinkErrorEvent;
import javax.media.datasink.DataSinkEvent;
import javax.media.datasink.DataSinkListener;
import javax.media.datasink.EndOfStreamEvent;
import javax.media.format.VideoFormat;
import javax.media.protocol.ContentDescriptor;
import javax.media.protocol.DataSource;
import javax.media.protocol.FileTypeDescriptor;
import javax.media.protocol.PullBufferDataSource;
import javax.media.protocol.PullBufferStream;

/**
 * This program takes a list of image files and convert them into a
 * movie file.
 */
public class JpegToQuicktimeAssembler implements ControllerListener, DataSinkListener {

    private Object  waitSync          = new Object();
    private boolean stateTransitionOK = true;

    private Object  waitFileSync      = new Object();
    private boolean fileDone          = false;
    private boolean fileSuccess       = true;

    // --- Inner classes ---------------------------------------

    /**
     * A DataSource to read from a list of JPEG image files and turn
     * that into a stream of JMF buffers. The DataSource is not seekable
     * or positionable.
     */
    private class ImageDataSource extends PullBufferDataSource {

        ImageSourceStream streams[];

        ImageDataSource(int width, int height, int frameRate, List images) {
            streams = new ImageSourceStream[1];
            streams[0] = new ImageSourceStream(width, height, frameRate, images);
        }

        @Override
        public void setLocator(MediaLocator source) {
        }

        @Override
        public MediaLocator getLocator() {
            return null;
        }

        @Override
        public String getContentType() {
            return ContentDescriptor.RAW;
        }

        @Override
        public void connect() {
        }

        @Override
        public void disconnect() {
        }

        @Override
        public void start() {
        }

        @Override
        public void stop() {
        }

        @Override
        public PullBufferStream[] getStreams() {
            return streams;
        }

        @Override
        public Time getDuration() {
            return DURATION_UNKNOWN;
        }

        @Override
        public Object[] getControls() {
            return new Object[0];
        }

        @Override
        public Object getControl(String type) {
            return null;
        }
    }

    class ImageSourceStream implements PullBufferStream {

        List        images;
        int         width, height;
        VideoFormat format;

        int         nextImage = 0;    // index of the next image
        // to be read.
        boolean     ended     = false;

        public ImageSourceStream(int width, int height, int frameRate, List images) {
            this.width = width;
            this.height = height;
            this.images = images;

            format = new VideoFormat(VideoFormat.JPEG, new Dimension(width, height),
                Format.NOT_SPECIFIED, Format.byteArray, frameRate);
        }

        public boolean willReadBlock() {
            return false;
        }

        public void read(Buffer buf) throws IOException {
            // Check if we've finished all the frames.
            if (nextImage >= images.size()) {
                // We are done. Set EndOfMedia.
                buf.setEOM(true);
                buf.setOffset(0);
                buf.setLength(0);
                ended = true;
                return;
            }
            String imageFile = (String) images.get(nextImage);
            nextImage++;

            // Open a random access file for the next image.
            RandomAccessFile raFile;
            raFile = new RandomAccessFile(imageFile, "r");
            byte data[] = null;
            // Check the input buffer type & size.
            if (buf.getData() instanceof byte[]) {
                data = (byte[]) buf.getData();
            }
            // Check to see the given buffer is big enough for the
            // frame.
            if ((data == null) || (data.length < raFile.length())) {
                data = new byte[(int) raFile.length()];
                buf.setData(data);
            }
            // Read the entire JPEG image from the file.
            raFile.readFully(data, 0, (int) raFile.length());

            buf.setOffset(0);
            buf.setLength((int) raFile.length());
            buf.setFormat(format);
            buf.setFlags(buf.getFlags() | Buffer.FLAG_KEY_FRAME);
            // Close the random access file.
            raFile.close();
        }

        public Format getFormat() {
            return format;
        }

        public ContentDescriptor getContentDescriptor() {
            return new ContentDescriptor(ContentDescriptor.RAW);
        }

        public long getContentLength() {
            return 0;
        }

        public boolean endOfStream() {
            return ended;
        }

        public Object[] getControls() {
            return new Object[0];
        }

        public Object getControl(String type) {
            return null;
        }
    }

    // --- Constructor

    public JpegToQuicktimeAssembler() {
    }

    // --- Private methods

    private MediaLocator createMediaLocator(String url) {
        MediaLocator ml;
        if ((url.indexOf(":") > 0) && ((ml = new MediaLocator(url)) != null)) {
            return ml;
        }
        if (url.startsWith(File.separator)) {
            if ((ml = new MediaLocator("file:" + url)) != null) {
                return ml;
            }
        }
        else {
            String file = "file:" + System.getProperty("user.dir") + File.separator + url;
            if ((ml = new MediaLocator(file)) != null) {
                return ml;
            }
        }
        return null;
    }

    private void doIt(int width, int height, int frameRate, List inFiles, MediaLocator outML)
    throws MediaException, IOException {
        ImageDataSource ids = new ImageDataSource(width, height, frameRate, inFiles);
        Processor p;
        p = Manager.createProcessor(ids);
        p.addControllerListener(this);
        // Put the Processor into configured state so we can set
        // some processing options on the processor.
        p.configure();
        if (!waitForState(p, Processor.Configured)) {
            throw new MediaException();
        }
        // Set the output content descriptor to QuickTime.
        p.setContentDescriptor(new ContentDescriptor(FileTypeDescriptor.QUICKTIME));
        // Query for the processor for supported formats.
        // Then set it on the processor.
        TrackControl tcs[] = p.getTrackControls();
        Format f[] = tcs[0].getSupportedFormats();
        if ((f == null) || (f.length <= 0)) {
            throw new MediaException();
        }
        tcs[0].setFormat(f[0]);
        // We are done with programming the processor. Let's just
        // realize it.
        p.realize();
        if (!waitForState(p, Controller.Realized)) {
            throw new MediaException();
        }
        // Now, we'll need to create a DataSink.
        DataSink dsink;
        if ((dsink = createDataSink(p, outML)) == null) {
            throw new MediaException();
        }
        dsink.addDataSinkListener(this);
        fileDone = false;
        // OK, we can now start the actual transcoding.
        p.start();
        dsink.start();
        // Wait for EndOfStream event.
        waitForFileDone();
        // Cleanup.
        dsink.close();
        p.removeControllerListener(this);
    }

    private DataSink createDataSink(Processor p, MediaLocator outML) throws MediaException,
    IOException {
        DataSource ds;
        if ((ds = p.getDataOutput()) == null) {
            System.err.println("The processor does not have an output DataSource!");
            return null;
        }
        DataSink dsink = null;
        try {
            dsink = Manager.createDataSink(ds, outML);
        }
        catch (NoDataSinkException e) {
            throw new MediaException();
        }
        dsink.open();
        return dsink;
    }

    private boolean waitForState(Processor p, int state) {

        synchronized (waitSync) {
            try {
                while ((p.getState() < state) && stateTransitionOK) {
                    waitSync.wait();
                }
            }
            catch (Exception e) {
            }
        }
        return stateTransitionOK;
    }

    private boolean waitForFileDone() {
        synchronized (waitFileSync) {
            try {
                while (!fileDone) {
                    waitFileSync.wait();
                }
            }
            catch (Exception e) {
            }
        }
        return fileSuccess;
    }

    // --- Public methods

    public void assemble(List inputFiles, File outputFile, double frameRate)
    throws MediaException, IOException {

        int width = -1, height = -1;
        Frame io = new Frame();
        Image tempImage = io.getToolkit().createImage((String) inputFiles.get(0));
        MediaTracker mt = new MediaTracker(io);
        mt.addImage(tempImage, 0);
        try {
            mt.waitForAll();
        }
        catch (InterruptedException e) {
        }
        mt.removeImage(tempImage);
        width = tempImage.getWidth(io);
        height = tempImage.getHeight(io);
        if ((width < 0) || (height < 0)) {
            System.err.println("Images have negative size!");
        }

        if (frameRate < 1) {
            frameRate = 1;
        }

        // Generate the output media locators.
        URL outURL = null;
        try {
            outURL = outputFile.toURL();
        }
        catch (Exception e) {
        }
        MediaLocator oml;
        if ((oml = createMediaLocator(outURL.toExternalForm())) == null) {
            System.err.println("Cannot build media locator from " + outputFile.getPath() + "!");
            System.exit(0);
        }

        doIt(width, height, (int) frameRate, inputFiles, oml);

        System.exit(0);
    }

    // --- Implementation of ControllerListener

    public void controllerUpdate(ControllerEvent evt) {

        if ((evt instanceof ConfigureCompleteEvent) || (evt instanceof RealizeCompleteEvent) ||
            (evt instanceof PrefetchCompleteEvent)) {
            synchronized (waitSync) {
                stateTransitionOK = true;
                waitSync.notifyAll();
            }
        }
        else if (evt instanceof ResourceUnavailableEvent) {
            synchronized (waitSync) {
                stateTransitionOK = false;
                waitSync.notifyAll();
            }
        }
        else if (evt instanceof EndOfMediaEvent) {
            evt.getSourceController().stop();
            evt.getSourceController().close();
        }
    }

    // --- Implementation of DataSinkListener

    public void dataSinkUpdate(DataSinkEvent evt) {
        if (evt instanceof EndOfStreamEvent) {
            synchronized (waitFileSync) {
                fileDone = true;
                waitFileSync.notifyAll();
            }
        }
        else if (evt instanceof DataSinkErrorEvent) {
            synchronized (waitFileSync) {
                fileDone = true;
                fileSuccess = false;
                waitFileSync.notifyAll();
            }
        }
    }

}

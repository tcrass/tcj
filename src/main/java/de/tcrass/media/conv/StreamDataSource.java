/*
 * Created on 11.03.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.conv;

import javax.media.Control;
import javax.media.Format;
import javax.media.MediaLocator;
import javax.media.Time;
import javax.media.protocol.ContentDescriptor;
import javax.media.protocol.PushBufferDataSource;
import javax.media.protocol.PushBufferStream;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class StreamDataSource extends PushBufferDataSource {

    // Processor proc;
    PushBufferDataSource source;
    PushBufferStream     sourceStream;
    SourceStream[]       streams;

    // int index;
    // boolean done = false;

    // --- Constructor ---

    public StreamDataSource(PushBufferDataSource s, int index) {
        source = s;
        sourceStream = s.getStreams()[index];
        streams = new SourceStream[1];
        streams[0] = new SourceStream(this);
    }

    // --- Package-scope methods

    public PushBufferStream getSourceStream() {
        return sourceStream;
    }

    // --- Public methods ---

    @Override
    public void connect() {
    }

    @Override
    public PushBufferStream[] getStreams() {
        return streams;
    }

    public Format getStreamFormat() {
        return sourceStream.getFormat();
    }

    @Override
    public void start() throws java.io.IOException {
        // proc.start();
        source.start();
    }

    @Override
    public void stop() {
    }

    @Override
    public Object getControl(String name) {
        return null;
    }

    @Override
    public Object[] getControls() {
        return new Control[0];
    }

    @Override
    public Time getDuration() {
        return source.getDuration();
    }

    @Override
    public void disconnect() {
    }

    @Override
    public String getContentType() {
        return ContentDescriptor.RAW;
    }

    @Override
    public MediaLocator getLocator() {
        return source.getLocator();
    }

    @Override
    public void setLocator(MediaLocator ml) {
    }

}

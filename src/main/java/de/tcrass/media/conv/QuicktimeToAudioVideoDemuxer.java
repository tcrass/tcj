/*
 * Created on 11.03.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.conv;

import java.io.File;
import java.io.IOException;

import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.EndOfMediaEvent;
import javax.media.Manager;
import javax.media.MediaException;
import javax.media.MediaLocator;
import javax.media.Processor;
import javax.media.control.TrackControl;
import javax.media.format.AudioFormat;
import javax.media.format.VideoFormat;
import javax.media.protocol.ContentDescriptor;
import javax.media.protocol.FileTypeDescriptor;
import javax.media.protocol.PushBufferDataSource;
import javax.media.protocol.PushBufferStream;

import de.tcrass.media.jmf.MediaUtils;
import de.tcrass.util.SysUtils;

// import org.comlink.home.tcrass.util.*;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class QuicktimeToAudioVideoDemuxer implements ControllerListener {

    private boolean         videoRunning = false;
    private boolean         audioRunning = false;
    private Processor       videoProc    = null;
    private Processor       audioProc    = null;
    private MediaFileWriter videoWriter  = null;
    private MediaFileWriter audioWriter  = null;
    private Object          runLock      = new Object();

    public QuicktimeToAudioVideoDemuxer() {
    }

    public void demux(File inputFile, File audioFile, File videoFile) throws MediaException,
    IOException {

        synchronized (runLock) {
            while (videoRunning || audioRunning) {
                try {
                    runLock.wait();
                }
                catch (InterruptedException ex) {
                    SysUtils.doNothing();
                }
            }

            String inURL = "file:" + inputFile.getPath();
            MediaLocator inLocator = new MediaLocator(inURL);

            Processor inProc = Manager.createProcessor(inLocator);
            MediaUtils.realize(inProc);
            inProc.start();

            StreamDataSource audioSource = null;
            StreamDataSource videoSource = null;
            PushBufferDataSource inSource = (PushBufferDataSource) inProc.getDataOutput();
            PushBufferStream[] streams = inSource.getStreams();
            for (int i = 0; i < streams.length; i++) {
                if ((videoSource == null) && (streams[i].getFormat() instanceof VideoFormat)) {
                    videoSource = new StreamDataSource(inSource, i);
                    videoSource.start();
                    videoRunning = true;
                }
                if ((audioSource == null) && (streams[i].getFormat() instanceof AudioFormat)) {
                    audioSource = new StreamDataSource(inSource, i);
                    audioSource.start();
                    audioRunning = true;
                }
            }

            if (videoRunning) {
                videoProc = Manager.createProcessor(videoSource);
                MediaUtils.configure(videoProc);
                videoProc.setContentDescriptor(new FileTypeDescriptor(
                    FileTypeDescriptor.QUICKTIME));
                // TrackControl[] tracks = videoProc.getTrackControls();
                // for (int i = 0; i < tracks.length; i++) {
                // if (tracks[i].getFormat() instanceof VideoFormat) {
                // Format[] sfs = tracks[i].getSupportedFormats();
                // int colordepth = 0;
                // int index = -1;
                // for (int j = 0; j < sfs.length; j++) {
                // if (sfs[j] instanceof RGBFormat) {
                // RGBFormat rgb = (RGBFormat)sfs[j];
                // if (rgb.getBitsPerPixel() > colordepth) {
                // index = j;
                // }
                // }
                // }
                // tracks[i].setFormat(sfs[index]);

                // Codec[] cc = new Codec[] {
                // new DePacketizer()
                // };
                // tracks[i].setCodecChain(cc);

                // }
                // }
                MediaUtils.realize(videoProc);
                videoProc.addControllerListener(this);
                videoProc.start();

                // Processor jpegProc =
                // Manager.createProcessor(videoProc.getDataOutput());
                // MediaUtils.configure(jpegProc);
                // jpegProc.setContentDescriptor(new
                // FileTypeDescriptor(FileTypeDescriptor.QUICKTIME));
                // tracks = jpegProc.getTrackControls();
                // for (int i = 0; i < tracks.length; i++) {
                // if (tracks[i].getFormat() instanceof VideoFormat) {
                // tracks[i].setFormat(new
                // VideoFormat(VideoFormat.JPEG));
                // }
                // }
                // MediaUtils.realize(jpegProc);
                // jpegProc.addControllerListener(this);
                // jpegProc.start();
                // videoWriter = new
                // MediaFileWriter(jpegProc.getDataOutput(), videoFile);

                videoWriter = new MediaFileWriter(videoProc.getDataOutput(), videoFile);
            }

            if (audioRunning) {
                audioProc = Manager.createProcessor(audioSource);
                MediaUtils.configure(audioProc);
                TrackControl[] tracks = audioProc.getTrackControls();
                for (TrackControl element : tracks) {
                    if (element.getFormat() instanceof AudioFormat) {
                        // tracks[i].setFormat(new
                        // Format(AudioFormat.*));
                    }
                }
                audioProc.setContentDescriptor(new ContentDescriptor("audio.x_wav"));
                MediaUtils.realize(audioProc);
                audioProc.addControllerListener(this);
                audioProc.start();
                audioWriter = new MediaFileWriter(audioProc.getDataOutput(), audioFile);
            }

            while (videoRunning || audioRunning) {
                try {
                    runLock.wait();
                }
                catch (InterruptedException ex) {
                    SysUtils.doNothing();
                }
            }

            if (videoWriter != null) {
                videoWriter.close();
                videoWriter = null;
            }
            if (audioWriter != null) {
                audioWriter.close();
            }
        }
    }

    // --- Implementation of ControllerListener

    public synchronized void controllerUpdate(ControllerEvent e) {
        Processor p = (Processor) e.getSource();
        if (e instanceof EndOfMediaEvent) {
            p.close();
            synchronized (runLock) {
                if (p == videoProc) {
                    videoRunning = false;
                }
                else if (p == audioProc) {
                    audioRunning = false;
                }
                runLock.notifyAll();
            }
        }
    }

}

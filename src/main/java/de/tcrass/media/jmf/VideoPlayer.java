package de.tcrass.media.jmf;

import java.awt.Component;
import java.io.File;
import java.net.URL;

import de.tcrass.media.MediaPlayerTimeoutException;
import de.tcrass.util.SysUtils;

public class VideoPlayer {

    public static final URL NULL_URL = null;

    private VideoChannel    channel;
    private VideoSettings   settings;

    public VideoPlayer() {
        channel = new VideoChannel();
        // settings = new VideoSettings();
    }

    public VideoPlayer(boolean lightweight) {
        channel = new VideoChannel(lightweight);
    }

    public void dispose() {
        synchronized (channel) {
            if (channel != null) {
                channel.setLoop(false);
                channel.dispose();
            }
        }
    }

    // --- Attribute access methods

    public VideoSettings getCurrentSettings() {
        return settings;
    }

    public URL getCurrentURL() {
        return settings.getURL();
    }

    public float getCurrentVolume() {
        return settings.getGain();
    }

    public long getCurrentMediaDuration() {
        long duration = -1;
        try {
            synchronized (channel) {
                duration = channel.getMediaDuration();
            }
        }
        catch (MediaPlayerTimeoutException e) {
            SysUtils.reportException(e);
        }
        return duration;
    }

    public void playURL(URL url, float gain, boolean loop) {
        try {
            VideoSettings s = new VideoSettings();
            s.setURL(url);
            s.setGain(gain);
            s.setLoop(loop);
            synchronized (channel) {
                settings = s;
                channel.applySettings(settings);
                channel.play();
            }
        }
        catch (MediaPlayerTimeoutException e) {
            SysUtils.reportException(e);
        }
    }

    public void playURL(String urlString, float gain, boolean loop) {
        VideoSettings s = new VideoSettings();
        s.setURL(urlString);
        playURL(s.getURL(), gain, loop);
    }

    public void playFile(File file, float gain, boolean loop) {
        VideoSettings s = new VideoSettings();
        s.setFile(file);
        playURL(s.getURL(), gain, loop);
    }

    public void playFile(String filename, float gain, boolean loop) {
        VideoSettings s = new VideoSettings();
        s.setFile(filename);
        playURL(s.getURL(), gain, loop);
    }

    public void stop() {
        synchronized (channel) {
            channel.stop();
        }
    }

    public Component getVisualComponent() {
        Component c = null;
        if (channel != null) {
            c = channel.getVisualComponent();
        }
        return c;
    }

}

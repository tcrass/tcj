package de.tcrass.media.jmf;

import java.io.File;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import de.tcrass.media.MediaPlayerTimeoutException;
import de.tcrass.util.SysUtils;
import de.tcrass.util.TimerEvent;
import de.tcrass.util.TimerListener;

public class AudioPlayer implements TimerListener {

    public static final URL                        NULL_URL = null;
    // public static final AudioSettings nullTarget = new
    // AudioSettings();

    private AudioChannel                           currentChannel;
    private AudioSettings                          currentTarget;
    private Map<AudioChannel, AudioSettings> settings = new Hashtable<AudioChannel, AudioSettings>();

    private boolean                                fading   = false;

    // --- Constructor

    public AudioPlayer() {
        createNewCurrentChannel();
    }

    public void dispose() {
        synchronized (settings) {
            fading = false;
            for (AudioChannel channel : settings.keySet()) {
                disposeChannel(channel);
            }
        }
    }

    // --- Attribute access methods

    public AudioSettings getCurrentSettings() {
        return settings.get(currentChannel);
    }

    public URL getCurrentURL() {
        return getCurrentSettings().getURL();
    }

    public float getCurrentVolume() {
        return getCurrentSettings().getGain();
    }

    public long getCurrentMediaDuration() {
        long duration = -1;
        try {
            duration = currentChannel.getMediaDuration();
        }
        catch (MediaPlayerTimeoutException e) {
            SysUtils.reportException(e);
        }
        return duration;
    }

    // --- Private methods

    private void createNewCurrentChannel() {
        currentChannel = new AudioChannel();
        currentTarget = new AudioSettings();
        AudioSettings s = (AudioSettings) currentTarget.clone();
        synchronized (settings) {
            settings.put(currentChannel, s);
        }
    }

    private void disposeChannel(AudioChannel channel) {
        if (channel != null) {
            channel.setLoop(false);
            channel.dispose();
            synchronized (settings) {
                settings.remove(channel);
            }
        }
    }

    private int calculateSteps(int duration) {
        return duration / MediaServer.getTimeResolution();
    }

    private void scheduleFading(int delay) {
        fading = true;
        MediaServer.schedule(new TimerEvent(this, this, "fade"), delay);
    }

    private AudioSettings createMuteTarget(AudioSettings s) {
        AudioSettings t = (AudioSettings) s.clone();
        t.setURL(MediaSettings.NULL_URL);
        t.setGain(0);
        return t;
    }

    private boolean moreFadeSteps(AudioChannel c, AudioSettings s, AudioSettings t) {
        boolean more = false;
        int steps = s.getRemainingFadeSteps();
        if (steps != 0) {
            float dGain = (t.getGain() - s.getGain()) / steps;
            s.setGain(s.getGain() + dGain);
            s.setRemainingFadeSteps(steps - 1);
            applySettings(c, s);
            more = true;
        }
        else {
            AudioSettings newS = (AudioSettings) t.clone();
            applySettings(c, newS);
            if ((c != currentChannel) && (newS.getGain() == 0)) {
                disposeChannel(c);
            }
            fading = false;
        }
        return more;
    }

    private void applySettings(AudioChannel c, AudioSettings s) {
        try {
            c.setGain(s.getGain());
            // System.out.println("Setting "+c+" to "+s.getLoop());
            c.setLoop(s.getLoop());
        }
        catch (MediaPlayerTimeoutException e) {
            SysUtils.reportException(e);
        }
    }

    // --- Public methods

    public void fade(AudioSettings targetSettings, int duration) {
        AudioSettings currentSettings = getCurrentSettings();
        URL currentSource = currentSettings.getURL();
        URL targetSource = targetSettings.getURL();
        // if ((currentSource == null) && (targetSource == null)) {
        // }
        if ((currentSource == null) && (targetSource != null)) {
            // System.out.println("Fading in...");
            try {
                currentChannel.setURL(targetSource);
                currentSettings.setURL(targetSource);
                currentSettings.setGain(0);
                currentSettings.setLoop(targetSettings.getLoop());
                currentSettings.setRemainingFadeSteps(calculateSteps(duration));
                currentTarget = (AudioSettings) targetSettings.clone();
                currentChannel.setGain(0);
                currentChannel.play();
                scheduleFading(0);
            }
            catch (MediaPlayerTimeoutException e) {
                SysUtils.reportException(e);
            }
        }
        else if ((currentSource != null) && (targetSource == null)) {
            // System.out.println("Changing volume...");
            currentSettings.setRemainingFadeSteps(calculateSteps(duration));
            currentTarget.setURL(currentSource);
            currentTarget.setGain(targetSettings.getGain());
            currentTarget.setLoop(targetSettings.getLoop());
            scheduleFading(0);
        }
        else {
            // System.out.println("Crossfading...");
            try {
                currentSettings.setRemainingFadeSteps(calculateSteps(duration));
                createNewCurrentChannel();
                currentChannel.setURL(targetSource);
                currentSettings = getCurrentSettings();
                currentSettings.setRemainingFadeSteps(calculateSteps(duration));
                currentSettings.setURL(targetSource);
                currentSettings.setLoop(targetSettings.getLoop());
                currentTarget = (AudioSettings) targetSettings.clone();
                currentChannel.setGain(0);
                currentChannel.play();
                scheduleFading(0);
            }
            catch (MediaPlayerTimeoutException e) {
                SysUtils.reportException(e);
            }
        }
    }

    public void stop() {
        fade(createMuteTarget(currentTarget), 0);
    }

    public void playURL(URL url, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setURL(url);
        s.setGain(gain);
        s.setLoop(loop);
        fade(s, duration);
    }

    public void playURL(URL url, float gain, boolean loop) {
        playURL(url, gain, loop, 0);
    }

    public void playURL(URL url, float gain) {
        playURL(url, gain, false, 0);
    }

    public void playURL(URL url) {
        AudioSettings s = (AudioSettings) currentTarget.clone();
        if (url.equals(s.getURL())) {
            s.setURL(NULL_URL);
        }
        fade(s, 0);
    }

    public void replay() {
        AudioSettings s = (AudioSettings) currentTarget.clone();
        s.setURL(NULL_URL);
        fade(s, 0);
    }

    public void playURL(String urlString, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setURL(urlString);
        playURL(s.getURL(), gain, loop, duration);
    }

    public void playURL(String urlString, float gain, boolean loop) {
        AudioSettings s = new AudioSettings();
        s.setURL(urlString);
        playURL(s.getURL(), gain, loop);
    }

    public void playURL(String urlString, float gain) {
        AudioSettings s = new AudioSettings();
        s.setURL(urlString);
        playURL(s.getURL(), gain);
    }

    public void playURL(String urlString) {
        AudioSettings s = new AudioSettings();
        s.setURL(urlString);
        playURL(s.getURL());
    }

    public void playFile(File file, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setFile(file);
        playURL(s.getURL(), gain, loop, duration);
    }

    public void playFile(File file, float gain, boolean loop) {
        AudioSettings s = new AudioSettings();
        s.setFile(file);
        playURL(s.getURL(), gain, loop);
    }

    public void playFile(File file, float gain) {
        AudioSettings s = new AudioSettings();
        s.setFile(file);
        playURL(s.getURL(), gain);
    }

    public void playFile(File file) {
        AudioSettings s = new AudioSettings();
        s.setFile(file);
        playURL(s.getURL());
    }

    public void playFile(String filename, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setFile(filename);
        playURL(s.getURL(), gain, loop, duration);
    }

    public void playFile(String filename, float gain, boolean loop) {
        AudioSettings s = new AudioSettings();
        s.setFile(filename);
        playURL(s.getURL(), gain, loop);
    }

    public void playFile(String filename, float gain) {
        AudioSettings s = new AudioSettings();
        s.setFile(filename);
        playURL(s.getURL(), gain);
    }

    public void playFile(String filename) {
        AudioSettings s = new AudioSettings();
        s.setFile(filename);
        playURL(s.getURL());
    }

    public void changeVolume(float gain, int duration) {
        AudioSettings s = (AudioSettings) currentTarget.clone();
        s.setURL(NULL_URL);
        s.setGain(gain);
        fade(s, duration);
    }

    public void setVolume(float gain) {
        changeVolume(gain, 0);
    }

    public void fadeOut(int duration) {
        changeVolume(0, duration);
    }

    public void fadeInURL(URL url, float gain, boolean loop, int duration) {
        playURL(url, 0, loop);
        changeVolume(gain, duration);
    }

    public void fadeInURL(URL url, float gain, int duration) {
        fadeInURL(url, gain, currentTarget.getLoop(), duration);
    }

    public void fadeInURL(String urlString, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setURL(urlString);
        fadeInURL(s.getURL(), gain, loop, duration);
    }

    public void fadeInURL(String urlString, float gain, int duration) {
        AudioSettings s = new AudioSettings();
        s.setURL(urlString);
        fadeInURL(s.getURL(), gain, currentTarget.getLoop(), duration);
    }

    public void fadeInFile(File file, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setFile(file);
        fadeInURL(s.getURL(), gain, loop, duration);
    }

    public void fadeInFile(File file, float gain, int duration) {
        AudioSettings s = new AudioSettings();
        s.setFile(file);
        fadeInURL(s.getURL(), gain, currentTarget.getLoop(), duration);
    }

    public void fadeInFile(String filename, float gain, boolean loop, int duration) {
        AudioSettings s = new AudioSettings();
        s.setFile(filename);
        fadeInURL(s.getURL(), gain, loop, duration);
    }

    public void fadeInFile(String filename, float gain, int duration) {
        AudioSettings s = new AudioSettings();
        s.setFile(filename);
        fadeInURL(s.getURL(), gain, currentTarget.getLoop(), duration);
    }

    // --- Implementation of TimerListener()

    public void timerAlarm(TimerEvent e) {
        // System.out.println("Timer! "+fading);
        if (fading) {
            boolean more = false;
            synchronized (settings) {
                for (AudioChannel c : settings.keySet()) {
                    AudioSettings s = settings.get(c);
                    if (c == currentChannel) {
                        if (moreFadeSteps(c, s, currentTarget)) {
                            more = true;
                        }
                    }
                    else {
                        if (moreFadeSteps(c, s, createMuteTarget(s))) {
                            more = true;
                        }
                    }
                }
            }
            if (more) {
                scheduleFading(MediaServer.TIME_RESOLUTION);
            }
            else {
                // System.out.println("...finished!");
            }
        }
    }

}

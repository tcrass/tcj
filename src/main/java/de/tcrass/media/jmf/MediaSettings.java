package de.tcrass.media.jmf;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import de.tcrass.util.SysUtils;

public class MediaSettings implements Cloneable {

    public static final URL NULL_URL = null;

    private URL             source   = null;

    private boolean         loop     = false;

    private float           gain     = 0.0F;

    public void setURL(URL url) {
        source = url;
    }

    public void setURL(String urlStr) {
        try {
            URL url = new URL(urlStr);
            setURL(url);
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
    }

    public void setFile(String filename) {
        try {
            URL url = new File(filename).toURI().toURL();
            if (SysUtils.getOSType() == SysUtils.OS_TYPE_WINDOWS) {
                String urlStr = url.toExternalForm();
                // urlStr = StringUtils.replaceAllMatches(urlStr,
                // "/\\p{Alpha}:/", "///");
                url = new URL(urlStr);
            }
            setURL(url);
        }
        catch (MalformedURLException ex) {
            SysUtils.reportException(ex);
        }
        // setURL("file:" + filename);
    }

    public void setFile(File file) {
        try {
            // setFile(file.getCanonicalPath());
            setFile(file.getPath());
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
    }

    public void clearSource() {
        source = null;
    }

    public URL getURL() {
        return source;
    }

    public boolean getLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public float getGain() {
        return gain;
    }

    public void setGain(float gain) {
        this.gain = gain;
    }

    // --- Implementation of Cloneable

    @Override
    public Object clone() {
        Object o = null;
        try {
            o = super.clone();
        }
        catch (Exception e) {
            SysUtils.doNothing();
        }
        return o;
    }

}

package de.tcrass.media.jmf;

import javax.media.Duration;
import javax.media.Time;

public class ChannelSettings {

    private int          state    = MediaServer.UNREALIZED;
    private MediaChannel channel;
    private Time         duration = Duration.DURATION_UNKNOWN;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public MediaChannel getChannel() {
        return channel;
    }

    public void setChannel(MediaChannel channel) {
        this.channel = channel;
    }

    public Time getDuration() {
        return duration;
    }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

}

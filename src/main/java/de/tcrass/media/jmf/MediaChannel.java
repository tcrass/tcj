package de.tcrass.media.jmf;

import java.net.URL;

import javax.media.GainControl;
import javax.media.Player;
import javax.media.Time;

import de.tcrass.media.MediaPlayerTimeoutException;
import de.tcrass.util.SysUtils;

class MediaChannel {

    public static final float GAIN_RESOLUTION = 10;

    private Player            player          = null;
    private URL               source          = null;
    private boolean           loop            = false;
    private boolean           lightweight     = false;
    private float             gainFactor      = 0.8f;

    // --- Constructor

    public MediaChannel() {
    }

    public MediaChannel(boolean lightweight) {
        this.lightweight = lightweight;
    }

    public void dispose() {
        if (getPlayer() != null) {
            // loop = false;
            MediaServer.dispose(player);
            source = null;
            player = null;
        }
    }

    // --- Attribute access methods

    public Player getPlayer() {
        return player;
    }

    public int getStatus() {
        int s = -1;
        if (getPlayer() != null) {
            s = MediaServer.getPlayerStatus(player);
        }
        return s;
    }

    public URL getURL() {
        return source;
    }

    public boolean getLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public long getMediaDuration() throws MediaPlayerTimeoutException {
        long d = -1;
        if (getPlayer() != null) {
            Time duration = MediaServer.getMediaDuration(player);
            d = duration.getNanoseconds() / 1000000;
        }
        return d;
    }

    /**
     * @return
     */
    public boolean isLightweight() {
        return lightweight;
    }

    /**
     * @param b
     */
    public void setLightweight(boolean b) {
        lightweight = b;
    }

    // --- Protected methods

    protected void waitForRealization() throws MediaPlayerTimeoutException {
        MediaServer.waitForPlayerStatus(player, MediaServer.REALIZED);
    }

    protected void determineGainFactor(GainControl g) {
        // if (gainFactor < 0) {
        // gainFactor = 1;
        // float lastGain = 0;
        // for (int f = 1; f <= GAIN_RESOLUTION; f ++) {
        // System.out.println("Testing gain factor " + f);
        // try {
        // g.setLevel(1/f);
        // }
        // catch (Exception ex) {
        // System.out.println("Exception at " + f + ", factor is " +
        // lastGain);
        // ex.printStackTrace();
        // gainFactor = lastGain;
        // break;
        // }
        // lastGain = 1/f;
        // }
        // }
    }

    // --- Public methods

    public void setURL(URL source) {
        dispose();
        try {
            this.source = source;
            if (source != null) {
                player = MediaServer.createPlayer(this);
            }
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
    }

    public void play() throws MediaPlayerTimeoutException {
        if ((getPlayer() != null) && (source != null)) {
            try {
                player.start();
            }
            catch (Exception e) {
                SysUtils.reportException(e);
            }
        }
    }

    public void stop() {
        if (getPlayer() != null) {
            player.stop();
        }
    }

    public void setGain(float gain) throws MediaPlayerTimeoutException {
        if (getPlayer() != null) {
            waitForRealization();
            GainControl g = getPlayer().getGainControl();
            if (g != null) {
                determineGainFactor(g);
                // System.out.println("GAIN: " + gain + " => " +
                // gain*gainFactor);
                g.setLevel(gain * gainFactor);
            }
            // System.out.println(" GAIN: " + g.getLevel() + " (" +
            // g.getDB() + " db)");
        }
    }

    public void applySettings(MediaSettings s) throws MediaPlayerTimeoutException {
        setURL(s.getURL());
        setGain(s.getGain());
        setLoop(s.getLoop());
    }

}

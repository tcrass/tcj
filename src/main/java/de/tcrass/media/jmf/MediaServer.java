package de.tcrass.media.jmf;

import java.util.Hashtable;

import javax.media.Controller;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.Duration;
import javax.media.EndOfMediaEvent;
import javax.media.Manager;
import javax.media.Player;
import javax.media.Time;

import de.tcrass.media.MediaPlayerTimeoutException;
import de.tcrass.util.ApplicationTimer;
import de.tcrass.util.SysUtils;
import de.tcrass.util.ThreadPool;
import de.tcrass.util.TimerEvent;

public class MediaServer implements ControllerListener {

    public static final String                        MEDIA_TIMER_NAME = "__MEDIA__";

    public static final int                           UNREALIZED       = Controller.Unrealized;
    public static final int                           REALIZING        = Controller.Realizing;
    public static final int                           REALIZED         = Controller.Realized;
    public static final int                           PREFETCHING      = Controller.Prefetching;
    public static final int                           PREFETCHED       = Controller.Prefetched;
    public static final int                           STARTED          = Controller.Started;

    public static int                                 TIME_RESOLUTION  = 20;

    private static MediaServer                        server;
    private static Hashtable<Player, ChannelSettings> settings         = new Hashtable<Player, ChannelSettings>();
    private static int                                playerTimeout    = 15000;

    // --- Constructor

    private MediaServer() {
        // try {
        // Processor pr = Manager.createRealizedProcessor(new
        // ProcessorModel());
        // contentTypes = pr.getSupportedContentDescriptors();
        // for (int i = 0; i < contentTypes.length; i++) {
        // System.out.println(contentTypes[i].getContentType());
        // }
        // }
        // catch (Exception e) {
        // System.err.println("MediaServer: "+e.toString());
        // e.printStackTrace();
        // }
    }

    // --- Singleton access method

    public static MediaServer getMediaServer() {
        if (server == null) {
            server = new MediaServer();
        }
        return server;
    }

    public static void schedule(TimerEvent e, long delay) {
        ApplicationTimer.schedule(MEDIA_TIMER_NAME, e, delay);
    }

    // --- Private methods

    private static ChannelSettings getSettings(Player player) {
        // System.out.println("MediaServer.getSettings()");
        ChannelSettings cs = settings.get(player);
        if (cs == null) {
            cs = new ChannelSettings();
            settings.put(player, cs);
        }
        return cs;
    }

    private static void updatePlayerStatus(Player player) {
        // System.out.println("MediaServer.updatePlayerStatus()");
        try {
            synchronized (settings) {
                settings.get(player).setState(player.getState());
                settings.get(player).setDuration(player.getDuration());
                settings.notifyAll();
            }
        }
        catch (Exception ex) {
            SysUtils.reportException(ex);
            // System.out.println("MediaServer.updatePlayerStatus:
            // "+ex.toString());
        }
    }

    // --- Public methods

    public static int getTimeResolution() {
        return TIME_RESOLUTION;
    }

    public static void setTimeResolution(int r) {
        TIME_RESOLUTION = r;
        if (TIME_RESOLUTION < 1) {
            TIME_RESOLUTION = 1;
        }
    }

    public static int getPlayerTimeout() {
        return playerTimeout;
    }

    public static void setPlayerTimout(int t) {
        playerTimeout = t;
        if (playerTimeout < 1) {
            playerTimeout = 1;
        }
    }

    public static Player createPlayer(MediaChannel channel) {
        // System.out.println("MediaServer.createPlayer()");
        Player player = null;
        try {

            //==========================================================
            // ==========

            Manager.setHint(Manager.LIGHTWEIGHT_RENDERER,
                Boolean.valueOf(channel.isLightweight()));

            //==========================================================
            // ==========

            final Player p = Manager.createPlayer(channel.getURL());
            synchronized (settings) {
                settings.get(p).setState(p.getState());
                settings.get(p).setChannel(channel);
                settings.notifyAll();
            }
            p.addControllerListener(getMediaServer());
            Thread t = new Thread("MediaServer: Realising player") {

                @Override
                public void run() {
                    p.realize();
                }
            };
            // t.start();
            ThreadPool.getDefaultThreadPool().addTask(t);
            player = p;
            // player.start();
        }
        catch (Exception e) {
            SysUtils.reportException(e);
            // System.out.println("MediaServer.createPlayer:
            // "+e.toString());
        }
        return player;
    }

    public static void dispose(Player player) {
        try {
            player.stop();
            waitForPlayerStatusNot(player, STARTED);
            player.removeControllerListener(getMediaServer());
            synchronized (settings) {
                settings.remove(player);
                settings.notifyAll();
            }
            player.deallocate();
            player.close();
        }
        catch (Exception ex) {
            SysUtils.doNothing();
        }
    }

    public static void waitForPlayerStatus(Player player, int target)
    throws MediaPlayerTimeoutException {
        // System.out.println("MediaServer.waitForPlayerStatus()");
        boolean reached = false;
        synchronized (settings) {
            long t0 = System.currentTimeMillis();
            long dt = 0;
            reached = (getPlayerStatus(player) >= target);
            while (!reached && (dt < playerTimeout)) {
                // System.out.println("Waiting for "+target+"...");
                try {
                    settings.wait(playerTimeout);
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                dt = System.currentTimeMillis() - t0;
                reached = (getPlayerStatus(player) >= target);
            }
            settings.notifyAll();
        }
        if (!reached) {
            throw new MediaPlayerTimeoutException(target, playerTimeout);
        }
    }

    public static void waitForPlayerStatusNot(Player player, int target)
    throws MediaPlayerTimeoutException {
        // System.out.println("MediaServer.waitForNotPlayerStatus()");
        boolean reached = false;
        synchronized (settings) {
            long t0 = System.currentTimeMillis();
            long dt = 0;
            reached = (getPlayerStatus(player) < target);
            while (!reached && (dt < playerTimeout)) {
                try {
                    settings.wait(playerTimeout);
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                dt = System.currentTimeMillis() - t0;
                reached = (getPlayerStatus(player) < target);
            }
            settings.notifyAll();
        }
        if (!reached) {
            throw new MediaPlayerTimeoutException(target, playerTimeout);
        }
    }

    public static int getPlayerStatus(Player player) {
        int s = -1;
        synchronized (settings) {
            s = getSettings(player).getState();
            settings.notifyAll();
        }
        return s;
    }

    public static Time getMediaDuration(Player player) throws MediaPlayerTimeoutException {
        // System.out.println("MediaServer.getMediaDuration()");
        Time duration = Duration.DURATION_UNKNOWN;
        synchronized (settings) {
            long t0 = System.currentTimeMillis();
            long dt = 0;
            while ((duration == Duration.DURATION_UNKNOWN) && (dt < playerTimeout)) {
                try {
                    settings.wait(playerTimeout);
                }
                catch (Exception e) {
                    SysUtils.reportException(e);
                }
                duration = getSettings(player).getDuration();
                dt = System.currentTimeMillis() - t0;
            }
            settings.notifyAll();
        }
        if (duration == Duration.DURATION_UNKNOWN) {
            throw new MediaPlayerTimeoutException(REALIZED, playerTimeout);
        }
        return duration;
    }

    // --- Implementation of ControllerListener();

    public void controllerUpdate(ControllerEvent e) {
        final Player player = (Player) e.getSource();
        // System.out.println("ControllerEvent: "+e.toString());
        updatePlayerStatus(player);
        // System.out.println(" ->
        // "+getPlayerStatus((Player)e.getSource()));
        if (e instanceof EndOfMediaEvent) {
            // System.out.println(" loop:
            // "+getSettings(player).getChannel().getLoop());
            if (getSettings(player).getChannel().getLoop()) {
                player.setMediaTime(new Time(0));
                player.start();
            }
        }
        // else if (e instanceof DurationUpdateEvent) {
        // synchronized(settings) {
        // settings.notify();
        // }
        // }
    }

}

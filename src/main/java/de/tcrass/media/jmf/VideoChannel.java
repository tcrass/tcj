package de.tcrass.media.jmf;

import java.awt.Component;

import de.tcrass.media.MediaPlayerTimeoutException;
import de.tcrass.util.SysUtils;

public class VideoChannel extends MediaChannel {

    public VideoChannel() {
        super();
    }

    public VideoChannel(boolean lightweight) {
        super(lightweight);
    }

    public Component getVisualComponent() {
        Component c = null;
        if (getPlayer() != null) {
            try {
                waitForRealization();
                c = getPlayer().getVisualComponent();
            }
            catch (MediaPlayerTimeoutException e) {
                SysUtils.reportException(e);
            }
        }
        return c;
    }

}

package de.tcrass.media;

import java.io.File;
import java.net.URL;


public interface AudioPlayer extends MediaPlayer {

    public void setGain(double gain, int time);
    public void crossfade(URL src, double gain, int time);
    public void crossfade(File src, double gain, int time);
    public void crossfade(String src, double gain, int time);
    
}

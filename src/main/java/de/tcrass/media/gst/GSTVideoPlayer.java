package de.tcrass.media.gst;

import java.awt.Component;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.gstreamer.State;
import org.gstreamer.elements.PlayBin;
import org.gstreamer.swing.VideoComponent;

import de.tcrass.media.VideoPlayer;


public class GSTVideoPlayer implements VideoPlayer {

    File src;
    double gain = DEFAULT_GAIN;
    boolean autoRepeat = false;

    PlayBin playbin;
    VideoComponent component;
    
    public GSTVideoPlayer() {
        GSTMediaManager.init();
        playbin = new PlayBin("VideoPlayer");
        component = new VideoComponent();
        playbin.setVideoSink(component.getElement());
    }
    
    public void setSource(URI src) {
        setSource(new File(src));
    }

    
    public void setSource(File src) {
        playbin.setInputFile(src);
    }
    
    public void setSource(String src) {
        try {
            URI uri = new URI(src);
            setSource(uri);
        }
        catch (Exception ex) {
            setSource(new File(src));
        }
    }
    
    public void play() {
        playbin.setState(State.PLAYING);
    }
    
    public void play(URI src) {
        setSource(src);
        play();
    }
    
    public void play(File src) {
        setSource(src);
        play();
    }
    
    public void play(String src) {
        setSource(src);
        play();
    }

    public void pause() {
        playbin.setState(State.PAUSED);
    }
    
    public void stop() {
        pause();
        rewind();
    }

    public State getState() {
        return playbin.getState();
    }

    public void rewind() {
        seekPos(0);
    }

    public void seekPos(long pos) {
        State currentState = getState();
        playbin.seek(pos, TimeUnit.MILLISECONDS);
        playbin.setState(currentState);
    }

    public long getPos() {
        return playbin.getClock().getTime().toMillis();
    }

    public void setGain(double gain) {
        playbin.setVolume(gain);
    }
    
    public double getGain() {
        return gain;
    }

    public boolean isAutoRepeat() {
        return autoRepeat;
    }
    
    public void setAutoRepeat(boolean autoRepeat) {
        this.autoRepeat = autoRepeat;
    }
    
    public Component getComponent() {
        return component;
    }

    public void dispose() {
        stop();
    }

}

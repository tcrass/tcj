/*
 * Created on 10.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */

package de.tcrass.media.tools;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Vector;

import de.tcrass.io.RegexFilenameFilter;
import de.tcrass.media.conv.JpegToQuicktimeAssembler;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class jpeg2mov {

    public static void main(String[] args) {

        String inputFilePattern = null;
        String inputDirPath = "";
        double frameRate = 1;
        Vector inputFiles = new Vector();
        String outputFilename = null;

        if (args.length == 0) {
            printUsage();
            System.exit(0);
        }

        int i = 0;
        while (i < args.length) {
            if (args[i].equals("-f")) {
                i++;
                if (i >= args.length) {
                    printUsage();
                    System.exit(0);

                }
                frameRate = new Double(args[i]).intValue();
            }
            else if (args[i].equals("-d")) {
                i++;
                if (i >= args.length) {
                    printUsage();
                    System.exit(0);

                }
                inputDirPath = args[i];
            }
            else if (args[i].equals("-o")) {
                i++;
                if (i >= args.length) {
                    printUsage();
                    System.exit(0);

                }
                outputFilename = args[i];
                if (!outputFilename.toLowerCase().endsWith(".mov")) {
                    outputFilename = outputFilename + ".mov";
                }
            }
            else {
                inputFilePattern = args[i];
                // inputFiles.addElement(args[i]);
            }
            i++;
        }

        if (inputFilePattern == null) {
            printUsage();
            System.exit(0);

        }
        inputFiles = readFileList(inputDirPath, inputFilePattern);

        if ((outputFilename == null) || (inputFiles.size() == 0)) {
            printUsage();
            System.exit(0);

        }
        File outputFile = new File(outputFilename);

        JpegToQuicktimeAssembler a = new JpegToQuicktimeAssembler();
        try {
            a.assemble(inputFiles, outputFile, frameRate);
        }
        catch (Exception e) {
            SysUtils.reportException(e);
            printUsage();
            System.exit(1);
        }
        System.exit(0);
    }

    private static void printUsage() {
        System.out.println("Usage:\njava jpg2mov [-d <working directory>] -f <frame rate> -o <output filename> <input filename regex pattern>");
    }

    private static Vector readFileList(String inputDirPath, String inputFilePattern) {
        Vector files = new Vector();
        File dir = new File(inputDirPath);
        FilenameFilter filter = new RegexFilenameFilter(inputFilePattern);
        String[] fileNames = dir.list(filter);
        Arrays.sort(fileNames);
        for (String element : fileNames) {
            File f = new File(inputDirPath, element);
            files.add(f.getPath());
        }
        return files;
    }

}

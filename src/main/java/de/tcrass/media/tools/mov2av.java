/*
 * Created on 12.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.media.tools;

import java.io.File;

import de.tcrass.media.conv.QuicktimeToAudioVideoDemuxer;
import de.tcrass.util.SysUtils;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class mov2av {

    private static void printUsage() {
        System.err.println("Usage: java Split <input_filename> [-a <audio_file>] [-v <videofile>]");
    }

    /**
     * Main program
     */
    public static void main(String[] args) {

        String audioFilename = null;
        String videoFilename = null;
        String inputFilename = null;

        if (args.length == 0) {
            printUsage();
            System.exit(0);
        }

        int i = 0;
        while (i < args.length) {

            if (args[i].equals("-a")) {
                i++;
                if (i >= args.length) {
                    printUsage();
                    System.exit(0);
                }
                audioFilename = args[i];
            }
            else if (args[i].equals("-v")) {
                i++;
                if (i >= args.length) {
                    printUsage();
                    System.exit(0);
                }
                videoFilename = args[i];
            }
            else {
                inputFilename = args[i];
            }
            i++;
        }

        if (inputFilename == null) {
            printUsage();
            System.exit(0);
        }

        if (audioFilename == null) {
            audioFilename = "audiotrack.wav";
        }

        if (videoFilename == null) {
            videoFilename = "videotrack.mov";
        }

        File inputFile = new File(inputFilename);
        File audioFile = new File(audioFilename);
        File videoFile = new File(videoFilename);

        QuicktimeToAudioVideoDemuxer ad = new QuicktimeToAudioVideoDemuxer();
        try {
            ad.demux(inputFile, audioFile, videoFile);
        }
        catch (Exception e) {
            SysUtils.reportException(e);
            System.exit(1);
        }
        System.exit(0);

    }

}

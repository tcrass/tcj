/*
 * Created on 27.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.t3d;

import java.awt.image.BufferedImage;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class JOGLCubeMapperListener implements GLEventListener {

    private boolean               inited        = false;

    public static final float[][] FACE_ROTATION = {
        {
            90, 0, 1, 0
        }, {
            0, 0, 1, 0
        }, {
            -90, 0, 1, 0
        }, {
            -180, 0, 1, 0
        }, {
            -90, 1, 0, 0
        }, {
            90, 1, 0, 0
        }
    };

    private JOGLCubeMapper        mapper;
    private GLAutoDrawable        canvas;
    private GLU                   glu;
    private Texture[]             textures;

    private BufferedImage[]       map;

    // --- Constructor ---

    JOGLCubeMapperListener(JOGLCubeMapper m, GLAutoDrawable d) {
        mapper = m;
        canvas = d;
    }

    // --- Private methods ---

    private void drawFace(GL2 gl, GLU glu, int face) {
        textures[face].bind(gl);
        double d = 1.0 - 1.5 / mapper.getMap()[0].getWidth();
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2d(1.0f, 1.0f);
        gl.glVertex3d(-1, -1, d);
        gl.glTexCoord2d(0, 1);
        gl.glVertex3d(1, -1, d);
        gl.glTexCoord2d(0, 0);
        gl.glVertex3d(1, 1, d);
        gl.glTexCoord2d(1, 0);
        gl.glVertex3d(-1, 1, d);
        gl.glEnd();
    }

    // --- Package-scope methods

    void updateTextures(BufferedImage[] map) {
        dispose();
        textures = new Texture[6];
        GL2 gl = canvas.getGL().getGL2();
        for (int i = 0; i < 6; i++) {
            textures[i] = AWTTextureIO.newTexture(JOGLCubeMapper.GL_PROFILE, map[i], false);
            textures[i].bind(gl);
            gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
        }
    }

    void dispose() {
        textures = null;
        // GL gl = canvas.getGL();
        // if (textures != null) {
        // for (int i = 0; i < 6; i++) {
        // textures[i].dispose();
        // }
        // }
    }

    // --- Implementation of GLEventListener

    @Override
    public void init(GLAutoDrawable d) {
        GL2 gl = d.getGL().getGL2();
        glu = new GLU();

        gl.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        gl.glEnable(GL2.GL_TEXTURE_2D);

        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[] {
            0, 0, 0
        }, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float[] {
            1, 1, 1
        }, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, new float[] {
            1, 1, 1, 1
        }, 0);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LIGHT0);
        gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glEnable(GL2.GL_TEXTURE_2D);

    }

    @Override
    public void display(GLAutoDrawable d) {
        if (!inited) {
            init(d);
        }
        GL2 gl = d.getGL().getGL2();
        if ((mapper.getMap() != null) && (mapper.getMap()[0] != null)) {
            if (textures == null) {
                updateTextures(mapper.getMap());
            }

            gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

            gl.glMatrixMode(GL2.GL_MODELVIEW);
            gl.glLoadIdentity();

            gl.glRotated(mapper.getPhi(), 1, 0, 0);
            gl.glRotated(mapper.getTheta(), 0, 1, 0);

            gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT, new float[] {
                1, 1, 1, 1
            }, 0);

            for (int i = 0; i < 6; i++) {
                gl.glPushMatrix();
                gl.glRotatef(FACE_ROTATION[i][0], FACE_ROTATION[i][1], FACE_ROTATION[i][2],
                    FACE_ROTATION[i][3]);
                drawFace(gl, glu, i);
                gl.glPopMatrix();
            }
        }
    }

    @Override
    public void reshape(GLAutoDrawable d, int x, int y, int width, int height) {

        if (!inited) {
            init(d);
        }
        GL2 gl = d.getGL().getGL2();

        gl.glViewport(0, 0, (int) mapper.getCanvasSize().getWidth(),
            (int) mapper.getCanvasSize().getHeight());

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(60, (double) mapper.getCanvasSize().getWidth() /
            (double) mapper.getCanvasSize().getHeight(), 0.01, 10);
        glu.gluLookAt(0, 0, 0, 0, 0, 1, 0, 1, 0);
    }

    public void displayChanged(GLAutoDrawable d, boolean modeChanged, boolean deviceChanged) {

        if (!inited) {
            init(d);
        }
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub

    }

}

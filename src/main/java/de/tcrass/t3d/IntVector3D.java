/*
 * Created on 05.07.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.t3d;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class IntVector3D {

    public static final int X = 0;
    public static final int Y = 1;
    public static final int Z = 2;

    public int[]            c = new int[3];

    public IntVector3D() {
    }

    public IntVector3D(int x, int y, int z) {
        c[X] = x;
        c[Y] = y;
        c[Z] = z;
    }

    public IntVector3D(IntVector3D r) {
        c[X] = r.c[X];
        c[Y] = r.c[Y];
        c[Z] = r.c[Z];
    }

    public int getX() {
        return c[X];
    }

    public int getY() {
        return c[Y];
    }

    public int getZ() {
        return c[Z];
    }

    public int comp(int i) {
        return c[i];
    }

    public IntVector3D add(IntVector3D p) {
        return new IntVector3D(c[X] + p.c[X], c[Y] + p.c[Y], c[Z] + p.c[Z]);
    }

    public IntVector3D mult(double s) {
        return new IntVector3D((int) (c[X] * s), (int) (c[Y] * s), (int) (c[Z] * s));
    }

    public IntVector3D mult(int s) {
        return new IntVector3D(c[X] * s, c[Y] * s, c[Z] * s);
    }

    public IntVector3D div(int s) {
        return new IntVector3D(c[X] / s, c[Y] / s, c[Z] / s);
    }

    public IntVector3D neg() {
        IntVector3D p = new IntVector3D(this);
        return p.mult(-1);
    }

    public IntVector3D shr(int s) {
        return new IntVector3D(c[X] >> s, c[Y] >> s, c[Z] >> s);
    }

    public IntVector3D shl(int s) {
        return new IntVector3D(c[X] << s, c[Y] << s, c[Z] << s);
    }

    public IntVector3D rotateX(double alpha) {
        double a = alpha * Math.PI / 180;
        IntVector3D p = new IntVector3D(this);
        p.c[Y] = (int) ((c[Y]) * Math.cos(a) - (c[Z]) * Math.sin(a));
        p.c[Z] = (int) ((c[Z]) * Math.cos(a) + (c[Y]) * Math.sin(a));
        return p;
    }

    public IntVector3D rotateY(double alpha) {
        double a = alpha * Math.PI / 180;
        IntVector3D p = new IntVector3D(this);
        p.c[X] = (int) ((c[X]) * Math.cos(a) + (c[Z]) * Math.sin(a));
        p.c[Z] = (int) ((c[Z]) * Math.cos(a) - (c[X]) * Math.sin(a));
        return p;
    }

    public IntVector3D abs() {
        return new IntVector3D(Math.abs(c[X]), Math.abs(c[Y]), Math.abs(c[Z]));
    }

    @Override
    public String toString() {
        return super.toString() + "[" + c[X] + "," + c[Y] + "," + c[Z] + "]";
    }

}

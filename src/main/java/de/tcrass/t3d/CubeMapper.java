/*
 * Created on 05.01.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.t3d;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public interface CubeMapper {

    public Component getCanvas();

    public Dimension getCanvasSize();
    public void setCanvasSize(Dimension size);

    public void setRotation(double theta, double phi);

    public void setMap(BufferedImage[] map);

    public void dispose();

    public BufferedImage getScreenshot();

}

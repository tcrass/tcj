package de.tcrass.t3d;

/*
 * abcdefhijklmnopqrstuwxyz grv %a image width in pixels %b image height
 * in pixels %x from pixel x %p to pixel p in X direction %w w pixels in
 * width %y from pixel y %q to pixel q in Y direction %h h pixels in
 * height %i input file %j input file's directory %c include file %o
 * output file %k project directory %u input format (extension) %f
 * output format (extension) ( %m render mode (design|game) ) %l movie
 * length (s) %s movie speed (fps) %e movie speed (delay, ms) %t movie
 * relative time %r current frame %d first frame %z last frame %n
 * soundtrack file
 */
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;

import javax.imageio.ImageIO;

import de.tcrass.io.ExternalProcess;
import de.tcrass.io.FileUtils;
import de.tcrass.util.StringUtils;

public class ExternalRenderer {

    private String                    command;
    private File                      workingDirectory;
    private String                    argumentsFormat;
    private String                    intDeclarationFormat;
    private String                    floatDeclarationFormat;
    private String                    stringDeclarationFormat;
    private String                    includeFormat;
    private boolean                   doPostClipping = true;
    // private int pixelOffset;
    // private int postDelay;
    private Hashtable<String, Object> params         = new Hashtable<String, Object>();

    private String                    lastStdOut;
    private String                    lastStdErr;
    private String                    lastCommand;

    // --- Parameter access methods

    public int getImageWidth() {
        return getIntParam("%a");
    }

    public void setImageWidth(int val) {
        setIntParam("%a", val);
    }

    public int getImageHeight() {
        return getIntParam("%b");
    }

    public void setImageHeight(int val) {
        setIntParam("%b", val);
    }

    public int getStartCol() {
        return getIntParam("%x");
    }

    public void setStartCol(int val) {
        setIntParam("%x", val);
    }

    public int getCols() {
        return getIntParam("%w");
    }

    public void setCols(int val) {
        setIntParam("%w", val);
    }

    public int getEndCol() {
        return getIntParam("%p");
    }

    public void setEndCol(int val) {
        setIntParam("%p", val);
    }

    public int getStartRow() {
        return getIntParam("%y");
    }

    public void setStartRow(int val) {
        setIntParam("%y", val);
    }

    public int getEndRow() {
        return getIntParam("%q");
    }

    public void setEndRow(int val) {
        setIntParam("%q", val);
    }

    public int getRows() {
        return getIntParam("%h");
    }

    public void setRows(int val) {
        setIntParam("%h", val);
    }

    public String getInputFile() {
        return getParamAsString("%i");
    }

    public String getInputDirectory() {
        return getParamAsString("%j");
    }

    public void setInputFile(String val) {
        setStringParam("%i", val);
        int p = val.lastIndexOf(".");
        if ((p >= 0) && (p < val.length())) {
            String format = val.substring(p + 1);
            if (format.indexOf(File.separatorChar) < 0) {
                setInputFormat(format);
            }
        }
        File f = new File(val);
        setStringParam("%j", f.getParent());
    }

    public String getIncludeFile() {
        return getParamAsString("%c");
    }

    public void setIncludeFile(String val) {
        setStringParam("%c", val);
    }

    public String getOutputFile() {
        return getParamAsString("%o");
    }

    public void setOutputFile(String val) {
        setStringParam("%o", val);
        int p = val.lastIndexOf(".");
        if ((p >= 0) && (p < val.length())) {
            String format = val.substring(p + 1);
            if (format.indexOf(File.separatorChar) < 0) {
                setOutputFormat(format);
            }
        }
    }

    public String getSoundtrackFile() {
        return getParamAsString("%n");
    }

    public void setSoundtrackFile(String val) {
        setStringParam("%n", val);
    }

    public String getProjectDir() {
        return getParamAsString("%k");
    }

    public void setProjectDir(String val) {
        setStringParam("%k", val);
    }

    public String getInputFormat() {
        return getParamAsString("%u");
    }

    public void setInputFormat(String val) {
        setStringParam("%u", val);
    }

    public String getOutputFormat() {
        return getParamAsString("%f");
    }

    public void setOutputFormat(String val) {
        setStringParam("%f", val);
    }

    public double getMovieLength() {
        return getFloatParam("%l");
    }

    public void setMovieLength(double val) {
        setFloatParam("%l", val);
    }

    public double getMovieSpeed() {
        return getFloatParam("%s");
    }

    public void setMovieSpeed(double val) {
        setFloatParam("%s", val);
    }

    public double getFrameDelay() {
        return getFloatParam("%e");
    }

    public void setFrameDelay(double val) {
        setFloatParam("%e", val);
    }

    public double getMovieClock() {
        return getFloatParam("%t");
    }

    public void setMovieClock(double val) {
        setFloatParam("%t", val);
    }

    public int getCurrentFrame() {
        return getIntParam("%r");
    }

    public void setCurrentFrame(int val) {
        setIntParam("%r", val);
    }

    public int getStartFrame() {
        return getIntParam("%d");
    }

    public void setStartFrame(int val) {
        setIntParam("%d", val);
    }

    public int getEndFrame() {
        return getIntParam("%z");
    }

    public void setEndFrame(int val) {
        setIntParam("%z", val);
    }

    // --- Attribute access methods

    public boolean getDoPostClipping() {
        return doPostClipping;
    }

    public void setDoPostClipping(boolean doPostClipping) {
        this.doPostClipping = doPostClipping;
    }

    // public int getPixelOffset() {
    // return pixelOffset;
    // }
    //
    // public void setPixelOffset(int offset) {
    // pixelOffset = offset;
    // }
    //
    // public int getPostDelay() {
    // return postDelay;
    // }
    //
    // public void setPostDelay(int postDelay) {
    // this.postDelay = postDelay;
    // }

    public String getRenderCommand() {
        return command;
    }

    public void setRenderCommand(String command) {
        this.command = command;
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setWorkingDirectory(File dir) {
        workingDirectory = dir;
    }

    public String getArgumentsFormat() {
        return argumentsFormat;
    }

    public void setArgumentsFormat(String argumentsFormat) {
        this.argumentsFormat = argumentsFormat;
    }

    public String getIntDeclarationFormat() {
        return intDeclarationFormat;
    }

    public void setIntDeclarationFormat(String intDeclarationFormat) {
        this.intDeclarationFormat = intDeclarationFormat;
    }

    public String getFloatDeclarationFormat() {
        return floatDeclarationFormat;
    }

    public void setFloatDeclarationFormat(String floatDeclarationFormat) {
        this.floatDeclarationFormat = floatDeclarationFormat;
    }

    public String getStringDeclarationFormat() {
        return stringDeclarationFormat;
    }

    public void setStringDeclarationFormat(String stringDeclarationFormat) {
        this.stringDeclarationFormat = stringDeclarationFormat;
    }

    public String getIncludeFormat() {
        return includeFormat;
    }

    public void setIncludeFormat(String includeFormat) {
        this.includeFormat = includeFormat;
    }

    public String getLastStdOut() {
        return lastStdOut;
    }

    public String getLastStdErr() {
        return lastStdErr;
    }

    public String getLastCommand() {
        return lastCommand;
    }

    // --- Private methods

    private String createDeclarationLine(String key, Object v) {
        Object val = v;
        String line;
        if (val instanceof String) {
            for (String param : params.keySet()) {
                if (param.equals(val)) {
                    val = getParam(param);
                    break;
                }
            }
        }
        if ((val instanceof Integer) || (val instanceof Long)) {
            line = StringUtils.replaceAll(intDeclarationFormat, "%v", val.toString());
        }
        else if ((val instanceof Float) || (val instanceof Double)) {
            line = StringUtils.replaceAll(floatDeclarationFormat, "%v", val.toString());
        }
        else {
            line = StringUtils.replaceAll(stringDeclarationFormat, "%v", val.toString());
        }
        line = StringUtils.replaceAll(line, "%k", key);
        return line + "\n";
    }

    // --- Protected methods

    protected void setIntParam(String key, int val) {
        Integer i = new Integer(val);
        params.put(key, i);
    }

    protected void setFloatParam(String key, double val) {
        Float d = new Float(val);
        params.put(key, d);
    }

    protected void setStringParam(String key, String val) {
        String s = val;
        params.put(key, s);
    }

    protected Object getParam(String key) {
        return params.get(key);
    }

    protected int getIntParam(String key) {
        int i = 0;
        Object o = getParam(key);
        if (o instanceof Integer) {
            i = ((Integer) o).intValue();
        }
        return i;
    }

    protected float getFloatParam(String key) {
        float f = 0f;
        Object o = getParam(key);
        if (o instanceof Float) {
            f = ((Float) o).floatValue();
        }
        return f;
    }

    protected String getStringParam(String key) {
        String s = "";
        Object o = getParam(key);
        if (o instanceof String) {
            s = (String) o;
        }
        return s;
    }

    protected String getParamAsString(String key) {
        String param = null;
        if (params.get(key) != null) {
            param = params.get(key).toString();
        }
        return param;
    }

    protected String bindValues(String s) {
        String a = s;
        for (String param : params.keySet()) {
            a = StringUtils.replaceAll(a, param, getParamAsString(param));
        }
        return a;
    }

    protected String[] buildCommandLine() {
        String[] args = argumentsFormat.split(" ");
        String[] cmd = new String[args.length + 1];
        cmd[0] = command;
        for (int i = 0; i < args.length; i++) {
            cmd[i + 1] = bindValues(args[i]);
        }
        return cmd;
    }

    protected String createIncludeData(Hashtable<String, Object> declarations) {
        StringBuffer d = new StringBuffer();
        for (String key : declarations.keySet()) {
            Object val = declarations.get(key);
            String line = createDeclarationLine(key, val);
            d.append(line);
        }
        String id = StringUtils.replaceAll(includeFormat, "%d", d.toString());
        return id;
    }

    // --- Public methods

    public int render(Hashtable<String, Object> declarations, Component io)
    throws InterruptedException, IOException {

        String[] cmd = buildCommandLine();

        if (getIncludeFile() != null) {
            String incData = createIncludeData(declarations);
            FileUtils.createFullDirectoryPath(new File(getIncludeFile()).getParentFile());
            FileWriter w = new FileWriter(getIncludeFile());
            w.write(incData);
            w.close();
        }

        if (getOutputFile() != null) {
            FileUtils.createFullDirectoryPath(new File(getOutputFile()).getParentFile());
        }

        ExternalProcess p = new ExternalProcess(cmd, null, workingDirectory);
        p.launchAndWait();

        lastStdOut = p.getStdOut().toString();
        lastStdErr = p.getStdErr().toString();
        lastCommand = "";
        for (int i = 0; i < cmd.length; i++) {
            if (i > 0) {
                lastCommand = lastCommand + " ";
            }
            lastCommand = lastCommand + cmd[i];
        }

        if (doPostClipping) {
            clipImage(getOutputFile(), getStartCol(), getStartRow(), getCols(), getRows(), io);
        }
        return p.getExitCode();
    }

    public void clipImage(String imageFilename, int x, int y, int width, int height,
                          Component io) throws InterruptedException, IOException {

        // System.out.println("Clipping to "+x+","+y+" with
        // "+width+","+height);
        Image tempImage = io.getToolkit().createImage(imageFilename);
        MediaTracker mt = new MediaTracker(io);
        mt.addImage(tempImage, 0);
        mt.waitForAll();
        mt.removeImage(tempImage);
        if ((tempImage != null) && (tempImage.getWidth(io) != -1) &&
            (tempImage.getHeight(io) != -1)) {
            // BufferedImage rawImage = new
            // BufferedImage(tempImage.getWidth(io),
            // tempImage.getHeight(io),
            // BufferedImage.TYPE_INT_RGB);
            // Graphics g = rawImage.getGraphics();
            // g.drawImage(tempImage, 0, 0, io);
            BufferedImage clippedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
            Graphics g2 = clippedImage.getGraphics();
            g2.drawImage(tempImage, 0, 0, width, height, x, y, x + width, y + height,
                Color.black, io);
            File imageFile = new File(imageFilename);
            // imageFile.delete();
            ImageIO.write(clippedImage, getOutputFormat(), imageFile);
        }
    }

}

package de.tcrass.t3d;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

public class SoftwareCubeMapperCanvas extends JComponent {

    /**
     * 
     */
    private static final long  serialVersionUID = 1L;
    private SoftwareCubeMapper mapper;

    public SoftwareCubeMapperCanvas(SoftwareCubeMapper m) {
        mapper = m;
    }

    public void dispose() {

    }

    public void render() {
        repaint();
    }

    public Component getCanvas() {
        return this;
    }

    @Override
    public void paint(Graphics g) {
        render(g);
    }

    protected void render(Graphics g) {
        g.setPaintMode();
        BufferedImage buff = mapper.render(g);
        g.drawImage(buff, 0, 0, getWidth(), getHeight(), this);
    }

}

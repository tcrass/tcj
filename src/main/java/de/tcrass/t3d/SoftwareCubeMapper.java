/*
 * Created on 05.07.2004 To change the template for this generated file
 * go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */
package de.tcrass.t3d;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import de.tcrass.util.SysUtils;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class SoftwareCubeMapper implements CubeMapper {

    public static final int          NO_OF_FACES           = 6;

    public static final int          PRECISION_BITS        = 10;
    public static final int          DOUBLE_PRECISION_BITS = 2 * PRECISION_BITS;

    public static final int          LEFT                  = 0;
    public static final int          FRONT                 = 1;
    public static final int          RIGHT                 = 2;
    public static final int          BACK                  = 3;
    public static final int          UP                    = 4;
    public static final int          DOWN                  = 5;

    private BufferedImage            buff;
    private BufferedImage[]          map                   = null;
    private int                      imageSize             = 0;
    private double                   theta                 = 0;
    private double                   phi                   = 0;
    private int[]                    projection;
    private SoftwareCubeMapperCanvas canvas;
    private Dimension canvasSize;

    private void createProjectionLookupTable() {
        projection = new int[imageSize + 1];
        for (int i = 1; i <= imageSize; i++) {
            projection[i] = (int) (((double) ((imageSize / 2) << PRECISION_BITS) / (double) i));
        }
    }

    public void setMap(BufferedImage[] m) {
        map = m;
        BufferedImage[] mm = new BufferedImage[6];
        if (map != null) {
            imageSize = map[0].getWidth(null);
            for (int i = 0; i < 6; i++) {
                BufferedImage b = new BufferedImage(map[i].getWidth(), map[i].getHeight(),
                    map[i].getType());
                Graphics g = b.getGraphics();
                g.drawImage(map[i], 0, 0, Color.BLACK, null);
                mm[i] = b;
            }
            if (buff != null) {
                createProjectionLookupTable();
            }
        }
        map = mm;
    }


    @Override
    public Dimension getCanvasSize() {
        return canvasSize;
    }
    
    public void setCanvasSize(Dimension d) {
        canvasSize = d;
        buff = new BufferedImage(canvasSize.width, canvasSize.height, BufferedImage.TYPE_INT_RGB);
        if (map != null) {
            createProjectionLookupTable();
        }
    }

    public void setRotation(double t, double p) {
        theta = t;
        phi = p;
    }

    public Component getCanvas() {
        if (canvas == null) {
            canvas = new SoftwareCubeMapperCanvas(this);
            canvas.setSize(canvasSize.width, canvasSize.height);
        }
        return canvas;
    }

    public BufferedImage render(Graphics g) {

        // System.out.println(" Starting mapping process...");
        // long t0 = System.currentTimeMillis();

        if ((map != null) && (buff != null)) {

            int dist = imageSize / 2;
            int imageSize1 = imageSize - 1;

            // Initial corners of projection plane
            IntVector3D A = new IntVector3D(-canvasSize.width / 2, canvasSize.height / 2, canvasSize.width / 2);
            IntVector3D B = new IntVector3D(canvasSize.width / 2, canvasSize.height / 2, canvasSize.width / 2);
            IntVector3D C = new IntVector3D(-canvasSize.width / 2, -canvasSize.height / 2, canvasSize.width / 2);
            IntVector3D D = new IntVector3D(canvasSize.width / 2, -canvasSize.height / 2, canvasSize.width / 2);

            // Scale projection plane to map size and rotate
            // according to theta and phi
            A = A.shl(PRECISION_BITS).mult((float) imageSize / (float) canvasSize.width).rotateX(
                -phi).rotateY(theta);
            B = B.shl(PRECISION_BITS).mult((float) imageSize / (float) canvasSize.width).rotateX(
                -phi).rotateY(theta);
            C = C.shl(PRECISION_BITS).mult((float) imageSize / (float) canvasSize.width).rotateX(
                -phi).rotateY(theta);
            D = D.shl(PRECISION_BITS).mult((float) imageSize / (float) canvasSize.width).rotateX(
                -phi).rotateY(theta);

            // How to modify the viewing vector R when traversing
            // the viewing plane in x- and y-direction, respectively
            IntVector3D dRx = B.add(A.neg()).mult(1 / (float) canvasSize.width);
            IntVector3D dRy = C.add(A.neg()).mult(1 / (float) canvasSize.height);

            // Initial viewing vector (bottom left corner)
            IntVector3D R = new IntVector3D(A);

            // Components of texture vector
            int u = 0;
            int v = 0;
            int r = 0;

            // index of selected cube map face
            int i = 0;
            // scaling factor for conversion
            // viewing vector -> texture coordinates
            int f = 0;

            // x-, y- and z-component of viewing vector
            int rx, ry, rz;

            // How to change viewing vector components
            // when traversing a viewing plane row
            int drx = dRx.getX();
            int dry = dRx.getY();
            int drz = dRx.getZ();

            // Iterate over the viewing plane's pixel rows
            for (int row = 0; row < canvasSize.height; row++) {

                // Initialise viewing vector components with
                // coordinates of row start
                rx = R.getX();
                ry = R.getY();
                rz = R.getZ();

                // Iterate over viewing plane's pixel columns
                for (int col = 0; col < canvasSize.width; col++) {

                    // Determine maximum component of viewing vector
                    int ary = Math.abs(ry);
                    int maxr = Math.abs(rx);
                    int max = 0 + (rx < 0 ? 3 : 0);
                    if (ary > maxr) {
                        maxr = ary;
                        max = 1 + (ry < 0 ? 3 : 0);
                    }
                    if (Math.abs(rz) > maxr) {
                        max = 2 + (rz < 0 ? 3 : 0);
                    }

                    // Depending on selected maximum component,
                    // select a cube map face and calculate
                    // initial texture vector coordinates
                    switch (max) {
                        case (0):
                            i = 2;
                            r = rx;
                            u = -rz;
                            v = ry;
                        break;
                        case (1):
                            i = 4;
                            r = ry;
                            u = rx;
                            v = -rz;
                        break;
                        case (2):
                            i = 1;
                            r = rz;
                            u = rx;
                            v = ry;
                        break;
                        case (3):
                            i = 0;
                            r = -rx;
                            u = rz;
                            v = ry;
                        break;
                        case (4):
                            i = 5;
                            r = -ry;
                            u = rx;
                            v = rz;
                        break;
                        case (5):
                            i = 3;
                            r = -rz;
                            u = -rx;
                            v = ry;
                        break;
                    }
                    try {

                        // Get factor for scaling texture coordinates in
                        // order
                        // to correct for distance between the tip of
                        // the
                        // viewing vector and the selected face of the
                        // cube map
                        f = projection[r >> PRECISION_BITS];

                        // Scale texture coordinates and translate
                        // origin to
                        // top left corner of cube map face
                        u = ((u * f) >> DOUBLE_PRECISION_BITS) + dist;
                        v = dist - ((v * f) >> DOUBLE_PRECISION_BITS);

                        u = (u < 0 ? 0 : u);
                        u = (u > imageSize1 ? imageSize1 : u);
                        v = (v < 0 ? 0 : v);
                        v = (v > imageSize1 ? imageSize1 : v);

                        // Finally, transfer the selected texture
                        // pixel's
                        // color to the viewing plane buffer
                        buff.setRGB(col, row, map[i].getRGB(u, v));
                    }
                    catch (ArrayIndexOutOfBoundsException e) {
                        // Just in case we tried to select pixel
                        // outside the texture...
                        SysUtils.doNothing();
                    }

                    // Go to next column, i.e. to next pixel in row
                    rx = rx + drx;
                    ry = ry + dry;
                    rz = rz + drz;
                }

                // Go to next row
                R = R.add(dRy);
            }

            // Draw the projected cubic environment map
            g.drawImage(buff, 0, 0, canvasSize.width, canvasSize.height, null);

            // System.out.println(" ...mapping took " +
            // (System.currentTimeMillis() - t0) + " ms");

        }
        // Just in case someone wants to access the created image...
        return buff;
    }

    public void dispose() {
        buff = null;
        if (map != null) {
            for (int i = 0; i < 6; i++) {
                if (map[i] != null) {
                    map[i].flush();
                    map[i] = null;
                }
            }
        }
        map = null;
    }

    public BufferedImage getScreenshot() {
        return render(buff.getGraphics());
    }

}

package de.tcrass.io;

public class ParseException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ParseException(TTokenizer t) {
        super("Syntax error at token \"" + t.getCharStr() + "\" in line " + t.lineno());
    }

}

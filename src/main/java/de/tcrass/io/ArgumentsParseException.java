package de.tcrass.io;

public class ArgumentsParseException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ArgumentsParseException(String msg) {
        super(msg);
    }

}

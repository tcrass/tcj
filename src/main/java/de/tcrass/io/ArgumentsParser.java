package de.tcrass.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArgumentsParser {

    private Map<String, Character>    boolOptionNames;
    private Map<String, Character>    boolSwitchNames;
    private Map<String, Character>    intArgNames;
    private Map<String, Character>    floatArgNames;
    private Map<String, Character>    stringArgNames;
    private Map<String, Character>    stringListNames;

    private Map<String, Boolean>      boolOptions;
    private Map<String, Boolean>      boolSwitches;
    private Map<String, Long>         intArgs;
    private Map<String, Double>       floatArgs;
    private Map<String, String>       stringArgs;
    private Map<String, List<String>> stringLists;

    private List<String>              filenameList;
    private int                       maxFilenames = -1;

    public ArgumentsParser() {
        clear();
    }

    private String name4symbol(Map<String, Character> names, Character symbol) {
        String name = null;
        for (String key : names.keySet()) {
            if ((names.get(key)).equals(symbol)) {
                name = key;
                break;
            }
        }
        return name;
    }

    public void clear() {
        boolOptionNames = new HashMap<String, Character>();
        boolSwitchNames = new HashMap<String, Character>();
        intArgNames = new HashMap<String, Character>();
        floatArgNames = new HashMap<String, Character>();
        stringArgNames = new HashMap<String, Character>();
        stringListNames = new HashMap<String, Character>();

        boolOptions = new HashMap<String, Boolean>();
        boolSwitches = new HashMap<String, Boolean>();
        intArgs = new HashMap<String, Long>();
        floatArgs = new HashMap<String, Double>();
        stringArgs = new HashMap<String, String>();
        stringLists = new HashMap<String, List<String>>();

        filenameList = new ArrayList<String>();
        maxFilenames = -1;
    }

    public void setBoolOptionName(String name) {
        setBoolOptionName(name, (char) 0);
    }

    public void setBoolOptionName(String name, char symbol) {
        boolOptionNames.put(name, new Character(symbol));
    }

    public void setBoolSwitchName(String name) {
        setBoolSwitchName(name, (char) 0, false);
    }

    public void setBoolSwitchName(String name, boolean def) {
        setBoolSwitchName(name, (char) 0, def);
    }

    public void setBoolSwitchName(String name, char symbol) {
        setBoolSwitchName(name, symbol, false);
    }

    public void setBoolSwitchName(String name, char symbol, boolean def) {
        boolSwitchNames.put(name, new Character(symbol));
        boolSwitches.put(name, new Boolean(def));
    }

    public void setIntArgName(String name) {
        setIntArgName(name, (char) 0, 0);
    }

    public void setIntArgName(String name, long def) {
        setIntArgName(name, (char) 0, def);
    }

    public void setIntArgName(String name, char symbol) {
        setIntArgName(name, symbol, 0);
    }

    public void setIntArgName(String name, char symbol, long def) {
        intArgNames.put(name, new Character(symbol));
        intArgs.put(name, new Long(def));
    }

    public void setFloatArgName(String name) {
        setFloatArgName(name, (char) 0, 0);
    }

    public void setFloatArgName(String name, double def) {
        setFloatArgName(name, (char) 0, def);
    }

    public void setFloatArgName(String name, char symbol) {
        setFloatArgName(name, symbol, 0);
    }

    public void setFloatArgName(String name, char symbol, double def) {
        floatArgNames.put(name, new Character(symbol));
        floatArgs.put(name, new Double(def));
    }

    public void setStringArgName(String name) {
        setStringArgName(name, (char) 0, null);
    }

    public void setStringArgName(String name, String def) {
        setStringArgName(name, (char) 0, def);
    }

    public void setStringArgName(String name, char symbol) {
        setStringArgName(name, symbol, null);
    }

    public void setStringArgName(String name, char symbol, String def) {
        stringArgNames.put(name, new Character(symbol));
        stringArgs.put(name, def);
    }

    public void setStringListName(String name) {
        setStringListName(name, (char) 0);
    }

    public void setStringListName(String name, char symbol) {
        stringListNames.put(name, new Character(symbol));
        stringLists.put(name, new ArrayList<String>());
    }

    public void setMaxFilenames(int max) {
        maxFilenames = max;
    }

    public boolean getBoolOption(String name) {
        boolean opt = false;
        if (boolOptions.containsKey(name)) {
            opt = (boolOptions.get(name)).booleanValue();
        }
        return opt;
    }

    public boolean getBoolSwitch(String name) {
        boolean opt = false;
        if (boolSwitches.containsKey(name)) {
            opt = (boolSwitches.get(name)).booleanValue();
        }
        return opt;
    }

    public long getIntArg(String name) {
        long opt = 0;
        if (intArgs.containsKey(name)) {
            opt = (intArgs.get(name)).longValue();
        }
        return opt;
    }

    public double getFloatArg(String name) {
        double opt = 0;
        if (floatArgs.containsKey(name)) {
            opt = (floatArgs.get(name)).doubleValue();
        }
        return opt;
    }

    public String getStringArg(String name) {
        String opt = null;
        if (stringArgs.containsKey(name)) {
            opt = (stringArgs.get(name));
        }
        return opt;
    }

    public List<String> getStringList(String name) {
        List<String> opt = null;
        if (stringLists.containsKey(name)) {
            opt = stringLists.get(name);
        }
        return opt;
    }

    public List<String> getFilenames() {
        return filenameList;
    }

    public String getFilename(int i) {
        String filename = null;
        if ((i >= 0) && (i < filenameList.size())) {
            filename = filenameList.get(i);
        }
        return filename;
    }

    public void parse(String[] argv) {
        try {
            int i = 0;
            while (i < argv.length) {
                boolean found = false;
                String arg = argv[i];
                if (arg.startsWith("--")) {
                    String name = arg.substring(2);
                    if (boolOptionNames.containsKey(name)) {
                        boolOptions.put(name, new Boolean(true));
                        found = true;
                    }
                    else {
                        int eq = name.indexOf('=');
                        if (eq >= 0) {
                            String val = name.substring(eq + 1);
                            name = name.substring(0, eq);
                            if (boolSwitches.containsKey(name)) {
                                if (val.toLowerCase().equals("true")) {
                                    boolSwitches.put(name, new Boolean(true));
                                    found = true;
                                }
                                else if (val.toLowerCase().equals("false")) {
                                    boolSwitches.put(name, new Boolean(false));
                                    found = true;
                                }
                                else {
                                    throw new ArgumentsParseException(
                                        "Illegal value in argument: " + arg);
                                }
                            }
                            else if (intArgs.containsKey(name)) {
                                intArgs.put(name, new Long(val));
                                found = true;
                            }
                            else if (floatArgs.containsKey(name)) {
                                floatArgs.put(name, new Double(val));
                                found = true;
                            }
                            else if (stringArgs.containsKey(name)) {
                                stringArgs.put(name, val);
                                found = true;
                            }
                            else if (stringLists.containsKey(name)) {
                                stringLists.get(name).add(val);
                                found = true;
                            }
                        }
                    }
                }
                else if (argv[i].startsWith("-") || argv[i].startsWith("+")) {
                    String sign = argv[i].substring(0, 1);
                    String name = argv[i].substring(1);
                    if (name.length() > 1) {
                        for (int j = 0; j < name.length(); j++) {
                            Character symbol = new Character(name.charAt(j));
                            if (boolOptionNames.containsValue(symbol)) {
                                boolOptions.put(name4symbol(boolOptionNames, symbol),
                                    new Boolean(true));
                                found = true;
                            }
                            else if (boolSwitchNames.containsValue(symbol)) {
                                boolSwitches.put(name4symbol(boolSwitchNames, symbol),
                                    new Boolean(sign.equals("+")));
                                found = true;
                            }
                        }
                    }
                    else {
                        Character symbol = new Character(name.charAt(0));
                        if (boolOptionNames.containsValue(symbol)) {
                            boolOptions.put(name4symbol(boolOptionNames, symbol), new Boolean(
                                true));
                            found = true;
                        }
                        else if (boolSwitchNames.containsValue(symbol)) {
                            boolSwitches.put(name4symbol(boolSwitchNames, symbol), new Boolean(
                                sign.equals("+")));
                            found = true;
                        }
                        else {
                            i++;
                            if (i < argv.length) {
                                String val = argv[i];
                                if (intArgNames.containsValue(symbol)) {
                                    intArgs.put(name4symbol(intArgNames, symbol), new Long(val));
                                    found = true;
                                }
                                else if (floatArgNames.containsValue(symbol)) {
                                    floatArgs.put(name4symbol(floatArgNames, symbol),
                                        new Double(val));
                                    found = true;
                                }
                                else if (stringArgNames.containsValue(symbol)) {
                                    stringArgs.put(name4symbol(stringArgNames, symbol), val);
                                    found = true;
                                }
                                else if (stringListNames.containsValue(symbol)) {
                                    stringLists.get(name4symbol(stringListNames, symbol)).add(
                                        val);
                                    found = true;
                                }
                            }
                            else {
                                throw new MissingValueForArgumentException(arg);
                            }
                        }
                    }
                }
                if (!found) {
                    for (int j = i; j < argv.length; j++) {
                        filenameList.add(argv[j]);
                    }
                    i = argv.length;
                }
                i++;
            }
            if (filenameList.size() > maxFilenames) {
                if (maxFilenames > 0) {
                    throw new TooManyArgumentsException(filenameList.get(0).toString());
                }
                else if (maxFilenames == 0) {
                    throw new IllegalCommandLineArgumentException(
                        filenameList.get(0).toString());
                }
            }
        }
        catch (ArgumentsParseException ex) {
            throw ex;
        }
        catch (RuntimeException ex) {
            throw new ArgumentsParseException("Error while parsing command-line arguments!");
        }
    }

}

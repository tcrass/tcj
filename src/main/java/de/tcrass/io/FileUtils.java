package de.tcrass.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import de.tcrass.util.StringUtils;
import de.tcrass.util.SysUtils;

public class FileUtils {

    public static final int BUFFER_SIZE = 65536;

    // --- Private methods

    // --- Public methods

    public static String getUserHomePath() {
        return System.getProperty("user.home");
    }

    public static String getCurrentUserPath() {
        return System.getProperty("user.dir");
    }

    public static File getUserHomeDirectory() {
        return new File(System.getProperty("user.home"));
    }

    public static File getCurrentUserDirectory() {
        return new File(System.getProperty("user.dir"));
    }

    public static URI getUserHomeURI() {
        return getUserHomeDirectory().toURI();
    }

    public static URI getCurrentUserURI() {
        return getCurrentUserDirectory().toURI();
    }

    public static void createFullDirectoryPath(File dir) {
        if (dir != null) {
            if (dir.getParentFile() != null) {
                createFullDirectoryPath(dir.getParentFile());
            }
            if (!dir.exists()) {
                dir.mkdir();
            }
        }
    }

    public static String removeTrailingSeparator(String filename) {
        String s = filename;
        if (s.endsWith(File.separator)) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    public static String escapeFilename(String filename) {
        String esc;
        if (SysUtils.getOSType() == SysUtils.OS_TYPE_WINDOWS) {
            esc = "\"" + filename + "\"";
        }
        else {
            esc = StringUtils.replaceAllMatches(filename, "([ '\"*?$])", "\\$1");
        }
        return esc;
    }

    public static void copy(File source, File target) throws IOException {
        FileInputStream fi = new FileInputStream(source);
        FileOutputStream fo = new FileOutputStream(target);
        streamCopy(fi, fo);
        fi.close();
        fo.close();
    }

    public static void dirCopy(File source, File target, boolean recursive) throws IOException {
        File[] files = source.listFiles();
        for (File element : files) {
            if (element.isDirectory()) {
                if (recursive) {
                    File newTarget = new File(target, element.getName());
                    createFullDirectoryPath(newTarget);
                    dirCopy(element, newTarget, recursive);
                }
            }
            else {
                copy(element, new File(target, element.getName()));
            }
        }
    }

    public static String getRelativePath(String root, String path) {
        String result = path;
        if (path.startsWith(root)) {
            result = new String(path.substring(root.length()));
            if (result.startsWith(File.separator)) {
                result = new String(result.substring(1));
            }
        }
        return result;
    }

    public static File createRelativeFile(File root, File path) throws IOException {
        File result = null;
        if ((root != null) && (path != null)) {
            result = new File(getRelativePath(root.getCanonicalPath(), path.getCanonicalPath()));
        }
        return result;
    }

    public static String getAbsolutePath(String root, String path) throws IOException {
        String result = path;
        if (!new File(path).isAbsolute()) {
            result = new File(root, path).getCanonicalPath();
        }
        return result;
    }

    public static File createAbsoluteFile(File root, File path) throws IOException {
        File result = new File(path.getCanonicalPath());
        if ((root != null) && (path != null)) {
            result = new File(getAbsolutePath(root.getCanonicalPath(), path.getPath()));
        }
        return result;
    }

    public static String getExtension(String filename) {
        String ext = "";
        if (filename.lastIndexOf(".") >= 0) {
            ext = filename.substring(filename.lastIndexOf(".") + 1);
        }
        return ext;
    }

    public static String getExtension(File file) {
        return getExtension(file.getName());
    }

    public static String getNameWithoutExtension(String filename) {
        String name = "";
        if (filename.lastIndexOf(".") >= 0) {
            name = filename.substring(0, filename.lastIndexOf("."));
        }
        return name;
    }

    public static String getNameWithoutExtension(File file) {
        return getNameWithoutExtension(file.getName());
    }

    public static List<String> getResourceNames(String jarfile, String packageName)
                                                                                   throws IOException {
        ArrayList<String> entries = new ArrayList<String>();
        String path = packageName.replace('.', File.separatorChar);
        File dir = new File(path);
        if (dir.exists() && dir.isDirectory()) {
            String[] files = dir.list();
            for (String element : files) {
                File file = new File(dir, element);
                if (!file.isDirectory()) {
                    entries.add(element);
                }
            }
        }
        else if (jarfile != null) {
            JarFile jar = new JarFile(jarfile);
            packageName = packageName.replace('.', '/') + "/";
            for (Enumeration<JarEntry> e = jar.entries(); e.hasMoreElements();) {
                String entry = e.nextElement().getName();
                if (entry.startsWith(packageName)) {
                    String tail = entry.substring(packageName.length());
                    if (tail.indexOf('/') < 0) {
                        entries.add(tail);
                    }
                }
            }
        }
        return entries;
    }

    public static List<String> getResourceNames(String packageName) throws IOException {
        return getResourceNames(null, packageName);
    }

    public static InputStream createResourceInputStream(String path, String name)
                                                                                 throws IOException {
        File file = new File(path, name);
        InputStream is = null;
        if (file.exists()) {
            is = new FileInputStream(file.getPath());
        }
        else {
            String newPath = "/" + path.replace('.', '/') + '/' + name;
            Class<?> c = file.getClass();
            is = c.getResourceAsStream(newPath);
        }
        return is;
    }

    public static String readStringResource(String path, String name) throws IOException {
        String result = null;
        InputStream is = createResourceInputStream(path, name);
        if (is != null) {
            StringBuffer s = new StringBuffer();
            int c = is.read();
            while (c != -1) {
                s.append((char) c);
                c = is.read();
            }
            is.close();
            result = s.toString();
        }
        return result;
    }

    public static void copyResource(String path, String name, String target) throws IOException {
        copyResource(path, name, new File(target));
    }

    public static void copyResource(String path, String name, File target) throws IOException {
        InputStream is = createResourceInputStream(path, name);
        FileOutputStream fo = new FileOutputStream(target);
        streamCopy(is, fo);
        is.close();
        fo.close();
    }

    public static void streamCopy(InputStream is, OutputStream os) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int count = is.read(buffer);
        while (count != -1) {
            os.write(buffer, 0, count);
            count = is.read(buffer);
        }
    }

    public static File fromURL(URL url) {
        URI uri;
        try {
            uri = url.toURI();
        }
        catch (URISyntaxException e) {
            try {
                uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(),
                    url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            }
            catch (URISyntaxException e1) {
                throw new IllegalArgumentException("Broken URL: " + url);
            }

        }
        return new File(uri);
    }
}

package de.tcrass.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import de.tcrass.util.StringUtils;
import de.tcrass.util.SysUtils;

public class ExternalProcess {

    // -------------------------------------------------------

    public class InputStreamRecorder {

        private InputStream  stream;
        private StringBuffer buffer;
        private Object       lock = new Object();
        private boolean      eos  = false;
        private Thread       readerThread;

        private InputStreamRecorder(InputStream is) throws IOException {
            stream = is;
            buffer = new StringBuffer();
            readerThread = new Thread() {

                @Override
                public void run() {
                    try {
                        int chr = stream.read();
                        while (chr != -1) {
                            buffer.append((char) chr);
                            chr = stream.read();
                        }
                        stream.close();
                        synchronized (lock) {
                            eos = true;
                            lock.notifyAll();
                        }
                    }
                    catch (IOException ex) {
                        // SysUtils.reportException(ex);
                    }
                }
            };
            readerThread.start();
        }

        private void waitFor() {
            synchronized (lock) {
                while (!eos) {
                    try {
                        lock.wait();
                    }
                    catch (InterruptedException e) {
                        try {
                            stream.close();
                        }
                        catch (IOException ex) {
                            SysUtils.reportException(ex);
                        }
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        private void destroy() {
            synchronized (lock) {
                readerThread.interrupt();
                try {
                    stream.close();
                }
                catch (IOException ex) {
                    // SysUtils.reportException(ex);
                }
            }
        }

        public StringBuffer getStringBuffer() {
            return buffer;
        }

        public int[] getIntBuffer() {
            int size = buffer.length();
            int[] array = new int[size];
            for (int i = 0; i < size; i++) {
                array[i] = buffer.charAt(i);
            }
            return array;
        }

        public byte[] getByteBuffer() {
            int size = buffer.length();
            byte[] array = new byte[size];
            for (int i = 0; i < size; i++) {
                array[i] = (byte) buffer.charAt(i);
            }
            return array;
        }

    }

    // -------------------------------------------------------

    private String             cmdStr;
    private String[]           cmd;
    private String[]           env;
    private File               dir;

    private boolean            checkForErrors;

    private Process            proc      = null;
    private int                exitCode  = -1;
    private Object             lock      = new Object();
    private boolean            isRunning = false;

    private OutputStreamWriter inWriter;
    private InputStreamRecorder outRecorder, errRecorder;

    public ExternalProcess(String cmdStr) {
        this(cmdStr, null, null);
    }

    public ExternalProcess(String cmdStr, String[] env) {
        this(cmdStr, env, null);
    }

    public ExternalProcess(String cmdStr, File dir) {
        this(cmdStr, null, dir);
    }

    public ExternalProcess(String cmdStr, String[] env, File dir) {
        cmd = null;
        this.cmdStr = cmdStr;
        this.env = env;
        this.dir = dir;
    }

    public ExternalProcess(String[] cmd) {
        this(cmd, null, null);
    }

    public ExternalProcess(String[] cmd, String[] env) {
        this(cmd, env, null);
    }

    public ExternalProcess(String[] cmd, File dir) {
        this(cmd, null, dir);
    }

    public ExternalProcess(String[] cmd, String[] env, File dir) {
        this.cmd = cmd;
        cmdStr = null;
        this.env = env;
        this.dir = dir;
    }

    public void setCheckForErrors(boolean checkForErrors) {
        this.checkForErrors = checkForErrors;
    }

    public void launch() throws IOException {
        synchronized (lock) {
            if (cmdStr != null) {
                proc = Runtime.getRuntime().exec(cmdStr, env, dir);
            }
            else {
                proc = Runtime.getRuntime().exec(cmd, env, dir);
            }
            isRunning = true;
        }
        outRecorder = new InputStreamRecorder(proc.getInputStream());
        errRecorder = new InputStreamRecorder(proc.getErrorStream());
        inWriter = new OutputStreamWriter(proc.getOutputStream());
    }

    public int waitFor() throws IOException {
        if (proc != null) {
            try {
                proc.getOutputStream().flush();
                proc.getOutputStream().close();
                synchronized (lock) {
                    exitCode = proc.waitFor();
                    outRecorder.waitFor();
                    errRecorder.waitFor();
                    isRunning = false;
                }
            }
            catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            // catch (IOException ex) {
            // SysUtils.reportException(ex);
            // }
        }
        if (checkForErrors) {
            checkForErrorsNow();
        }
        return exitCode;
    }

    public int launchAndWait() throws IOException {
        launch();
        return waitFor();
    }

    public OutputStreamWriter getStdInWriter() {
        return inWriter;
    }

    public void writeToStdIn(String s) throws IOException {
        // try {
        inWriter.write(s);
        inWriter.flush();
        // }
        // catch (IOException ex) {
        // SysUtils.doNothing();
        // }
    }

    public InputStreamRecorder getStdOutRecorder() {
        return outRecorder;
    }

    public InputStreamRecorder getStdErrRecorder() {
        return errRecorder;
    }

    public String getStdOut() {
        return outRecorder.getStringBuffer().toString();
    }

    public String getStdErr() {
        return errRecorder.getStringBuffer().toString();
    }

    public int getExitCode() {
        return exitCode;
    }

    public void kill() {
        if (proc != null) {
            synchronized (lock) {
                if (isRunning) {
                    proc.destroy();
                    outRecorder.destroy();
                    errRecorder.destroy();
                    isRunning = false;
                }
            }
        }
    }

    public boolean isRunning() {
        synchronized (lock) {
            return isRunning;
        }
    }

    public void checkForErrorsNow() throws IOException {
        if ((exitCode > 0) || (StringUtils.matchesAnywhere(getStdErr(), "\\S"))) {
            String msg = "External process \"";
            if (cmd == null) {
                msg += cmdStr;
            }
            else {
                msg += StringUtils.join(" ", cmd);
            }
            msg += "\" exited with code " + exitCode;
            if (StringUtils.matchesAnywhere(getStdErr(), "\\S")) {
                msg += "; STDERR says:\n" + getStdErr();
            }
            throw new IOException(msg);
        }
    }

}

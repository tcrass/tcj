package de.tcrass.io;

public class TooManyArgumentsException extends ArgumentsParseException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TooManyArgumentsException(String arg) {
        super("Too many arguments or unknown argument: " + arg);
    }

}

package de.tcrass.io;

import java.io.File;
import java.io.FilenameFilter;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window&gt;Preferences&gt;Java&gt;Code
 *         Generation&gt;Code and Comments
 */
public class FileExtensionFilter implements FilenameFilter {

    private String[] extensions;
    private boolean  caseInsensitive = true;

    public FileExtensionFilter(String[] extensions) {
        this(extensions, true);
    }

    public FileExtensionFilter(String[] extensions, boolean caseInsensitive) {
        this.caseInsensitive = caseInsensitive;
        if (extensions != null) {
            this.extensions = new String[extensions.length];
            for (int i = 0; i < extensions.length; i++) {
                if (caseInsensitive) {
                    this.extensions[i] = extensions[i].toLowerCase();
                }
                else {
                    this.extensions[i] = new String(extensions[i]);
                }
            }
        }
    }

    public String[] getExtensions() {
        return extensions;
    }

    public boolean accept(File dir, String name) {
        boolean matched = false;
        String fileName;
        if (caseInsensitive) {
            fileName = name.toLowerCase();
        }
        else {
            fileName = name;
        }
        if (extensions != null) {
            for (String element : extensions) {
                if (fileName.endsWith(element)) {
                    matched = true;
                    break;
                }
            }
        }
        else {
            matched = true;
        }
        return matched;
    }

    public String getFilterString() {
        String filter;
        if (extensions != null) {
            filter = "";
            for (int i = 0; i < extensions.length; i++) {
                if (i > 0) {
                    filter = filter + ";";
                }
                filter = filter + "*." + extensions[i];
            }
        }
        else {
            filter = "*";
        }
        return filter;
    }

}

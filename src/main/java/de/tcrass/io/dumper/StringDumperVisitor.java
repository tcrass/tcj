package de.tcrass.io.dumper;


public class StringDumperVisitor implements FieldVisitor {

    public boolean canRestore() {
        return false;
    }

    public Object createDumpContainer(Object o) {
        return new StringBuffer();
    }

    public void beginObject(Object container, Object o) {
        
    }
    
    public void dumpField(Object container, Object o) {
        
    }
    
    public void endObject(Object container, Object o) {
        
    }
    
}

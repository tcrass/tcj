package de.tcrass.io.dumper;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

public class Dumper {

    FieldVisitor fv;
    Map<Object,Integer> registry;
    int nextId;
    
    public Dumper(FieldVisitor fv) {
        this.fv = fv;
    }
    
    public static String toDumpString() {
        return null;
    }

    public static Document toXML() {
        return null;
    }

    private void doDump(Object container, Object o) {
        fv.beginObject(container, o);
        for (Field f : o.getClass().getDeclaredFields()) {
            try {
                Object val = f.get(o);
                if (!registry.containsKey(val)) {
                    registry.put(val, nextId);
                    nextId++;
                    doDump(container, val);
                }
            }
            catch (IllegalAccessException ex) {
                System.err.println(ex.getMessage());
            }
        }
        fv.endObject(container, o);
    }
    
    public Object dump(Object o) {
        registry = new HashMap<Object,Integer>();
        nextId = 0;
        Object container = fv.createDumpContainer(o);
        doDump(container, o);
        return container;
    }
    
}

package de.tcrass.io;

public class IllegalCommandLineArgumentException extends ArgumentsParseException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public IllegalCommandLineArgumentException(String arg) {
        super("Illegal argument: " + arg);
    }

}

/*
 * Created on 27.06.2004 To change the template for this generated file
 * go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.util;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.io.PrintStream;
import java.net.InetAddress;
import java.util.Map;

import de.tcrass.swing.dialog.TDialog;
import de.tcrass.swing.panel.TTextDisplayPanel;

/**
 * @author tcrass To change the template for this generated type comment
 *         go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class SysUtils {

    public static int     OS_TYPE_WINDOWS  = 0;
    public static int     OS_TYPE_LINUX    = 1;
    public static int     OS_TYPE_MACOS    = 2;
    public static int     OS_TYPE_OTHER    = -1;

    private static String exceptionNote    = "An error has occured. Please inform the manufacturer of this software about the problem and include the following information in your error report:";
    private static String guiExceptionNote = "<div style=\"font-weight:bold\">" +
                                             exceptionNote + "</div>";
    private static String applicationName;
    private static String applicationVersion;
    private static Window applicationWindow;

    // --- Static attribute access methods

    public static void setApplicationWindow(Window w) {
        applicationWindow = w;
    }

    public static Window getApplicationWindow() {
        return applicationWindow;
    }

    public static void setExceptionNote(String note) {
        exceptionNote = note;
    }

    public static String getExceptionNote() {
        return exceptionNote;
    }

    public static void setGUIExceptionNote(String note) {
        guiExceptionNote = note;
    }

    public static String getGUIExceptionNote() {
        return guiExceptionNote;
    }

    public static String getApplicationName() {
        return applicationName;
    }

    public static String getApplicationVersion() {
        return applicationVersion;
    }

    public static void setApplicationName(String string) {
        applicationName = string;
    }

    public static void setApplicationVersion(String string) {
        applicationVersion = string;
    }

    // --- Private static methods ---

    private static TDialog createExceptionDialog(String note, Throwable t, String cause,
                                                 Frame owner) {
        TDialog d = new TDialog("Exception", TDialog.OK_BUTTON, TDialog.OK_BUTTON,
            TDialog.WARNING_ICON, owner);
        insertExceptionDialogComponents(d, note, t, cause);
        return d;
    }

    private static TDialog createExceptionDialog(String note, Throwable t, String cause,
                                                 Dialog owner) {
        TDialog d = new TDialog("Exception", TDialog.OK_BUTTON, TDialog.OK_BUTTON,
            TDialog.WARNING_ICON, owner);
        insertExceptionDialogComponents(d, note, t, cause);
        return d;
    }

    private static void insertExceptionDialogComponents(TDialog d, String note, Throwable t,
                                                        String cause) {
        TTextDisplayPanel t1 = new TTextDisplayPanel(TTextDisplayPanel.TEXT_HTML,
            "Error notification", 6, 40);
        t1.setContent(StringUtils.toHTMLCaption(note));
        d.addBlockComponent(t1);
        TTextDisplayPanel t2 = new TTextDisplayPanel(TTextDisplayPanel.TEXT_HTML,
            "Error description", 12, 40);
        String s = "";
        if (applicationName != null) {
            s = s + "<div style=\"font-weight:bold\">Application:</div>";
            s = s + "<div style=\"color:#FF0000\">" + applicationName;
            if (applicationVersion != null) {
                s = s + " Vers. " + applicationVersion;
            }
            s = s + "</div>\n";
        }
        if (cause != null) {
            s = s + "<div style=\"font-weight:bold\">Possible cause:</div>";
            s = s + "<div style=\"color:#FF0000\">" + cause + "</div>\n";
        }
        s = s + "<div style=\"font-weight:bold\">Details:</div>";
        s = s + "<pre style=\"color:#FF0000;\">" + t.toString() + "\nStack trace:\n" +
            sprintStackTrace(t) + "</pre>";
        t2.setContent(StringUtils.toHTMLCaption(s));
        d.addBlockComponent(t2);
    }

    // --- Public static mehtods

    public static String getQualifiedProgramName() {
        StackTraceElement[] stack = new Throwable().getStackTrace();
        StackTraceElement startup = stack[stack.length - 1];
        String startupClassName = startup.getClassName();
        Class<?> applicationClass = null;
        try {
            applicationClass = Class.forName(startupClassName);
        }
        catch (ClassNotFoundException ex) {
            doNothing();
        }
        String applicationClassName = applicationClass.getName();
        return applicationClassName;
    }

    public static String getProgramName() {
        String applicationClassName = getQualifiedProgramName();
        if (applicationClassName.lastIndexOf(".") >= 0) {
            applicationClassName = applicationClassName.substring(applicationClassName.lastIndexOf(".") + 1);
        }
        return applicationClassName;
    }

    public static String getLocalHostName() {
        String host = null;
        try {
            InetAddress addr = InetAddress.getLocalHost();
            host = addr.getHostName().toLowerCase();
        }
        catch (Exception ex) {
            SysUtils.doNothing();
        }
        return host;
    }

    public static void doNothing() {
    }

    public static void printStackTrace() {
        System.out.println(sprintStackTrace());
    }

    public static String sprintStackTrace() {
        return sprintStackTrace(new Throwable());
    }

    public static String sprintStackTrace(Throwable t) {
        String s = "";
        StackTraceElement[] sel = t.getStackTrace();
        for (StackTraceElement element : sel) {
            s = s + "  " + element.toString() + "\n";
        }
        return s;
    }

    public static void reportException(Throwable t, Frame owner) {
        TDialog d = createExceptionDialog(guiExceptionNote, t, null, owner);
        d.showDialog();
    }

    public static void reportException(Throwable t, Dialog owner) {
        TDialog d = createExceptionDialog(guiExceptionNote, t, null, owner);
        d.showDialog();
    }

    public static void reportException(Throwable t) {
        String cause = null;
        if (applicationWindow != null) {
            if (applicationWindow instanceof Frame) {
                reportException(t, cause, (Frame) applicationWindow);
            }
            else if (applicationWindow instanceof Frame) {
                reportException(t, cause, (Dialog) applicationWindow);
            }
        }
        else {
            reportException(t, cause);
        }
    }

    public static void reportException(Throwable t, String cause, Frame owner) {
        TDialog d = createExceptionDialog(guiExceptionNote, t, cause, owner);
        d.showDialog();
    }

    public static void reportException(Throwable t, String cause, Dialog owner) {
        TDialog d = createExceptionDialog(guiExceptionNote, t, cause, owner);
        d.showDialog();
    }

    public static void reportException(Throwable t, String cause) {
        if (applicationWindow != null) {
            if (applicationWindow instanceof Frame) {
                reportException(t, cause, (Frame) applicationWindow);
            }
            else if (applicationWindow instanceof Dialog) {
                reportException(t, cause, (Dialog) applicationWindow);
            }
        }
        else {
            System.err.println(StringUtils.polymerize("*", 60));
            System.err.println(exceptionNote);
            if (applicationName != null) {
                System.err.print("\nApplication: " + applicationName);
                if (applicationVersion != null) {
                    System.err.print(" (Version " + applicationVersion + ")");
                }
            }
            if (cause != null) {
                System.err.print("\nPossible cause: " + cause);
            }
            System.err.println("\n" + StringUtils.polymerize("-", 60));
            System.err.println("Original exception message: " + t.toString());
            System.err.println("Stack trace:");
            System.err.println(sprintStackTrace(t));
            System.err.println("\n" + StringUtils.polymerize("-", 60));
        }
    }

    public static void sleep(long delay) {
        try {
            Thread.sleep(delay);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static int getOSType() {
        int type = OS_TYPE_OTHER;
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            type = OS_TYPE_WINDOWS;
        }
        else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
            type = OS_TYPE_LINUX;
        }
        else if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
            type = OS_TYPE_MACOS;
        }
        return type;
    }

    public static void dumpList(java.util.Collection<?> l, PrintStream ps) {
        ps.print("List: ");
        ps.println(CollectionUtils.listToString(l));
    }

    public static void dumpList(java.util.Collection<?> l) {
        dumpList(l, System.out);
    }

    public static void dumpMap(Map<?, ?> m, PrintStream ps) {
        ps.print("Map: ");
        ps.println(CollectionUtils.mapToString(m));
    }

    public static void dumpMap(Map<?, ?> m) {
        dumpMap(m, System.out);
    }

}

package de.tcrass.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListOfMaps<K, V> extends ArrayList<Map<K, V>> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Map<K, V> getRow(int rowIndex) {
        Map<K, V> row = null;
        if ((rowIndex >= 0) && (rowIndex < size())) {
            row = get(rowIndex);
        }
        return row;
    }

    public V get(int rowIndex, K key) {
        V result = null;
        Map<K, V> row = getRow(rowIndex);
        if (row != null) {
            result = row.get(key);
        }
        return result;
    }

    public List<V> getColumn(K key) {
        List<V> col = new ArrayList<V>();
        for (Map<K, V> m : this) {
            col.add(m.get(key));
        }
        return col;
    }

}

package de.tcrass.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SuffixTree extends Trie<Map<Integer, List<Integer>>> {

    private List<String> strings;

    public SuffixTree() {
        super();
        strings = new ArrayList<String>();
    }

    public void addString(String s) {
        int index = strings.size();
        strings.add(s);
        for (int i = 0; i <= s.length(); i++) {
            String suffix = s.substring(i);
            add(suffix);
//            TrieNode<Map<Integer, List<Integer>>> n = assertNodeForKey(root, suffix, 0);

            TrieNode<Map<Integer, List<Integer>>> m = assertNodeForKey(root, suffix, 0);
            do { // while (m != null) {
                Map<Integer, List<Integer>> strs = m.data;
                if (strs == null) {
                    strs = new HashMap<Integer, List<Integer>>();
                    m.data = strs;
                }
                List<Integer> poss = strs.get(index);
                if (poss == null) {
                    poss = new LinkedList<Integer>();
                    strs.put(index, poss);
                }
//                StringIndex si = new StringIndex(index, i + m.level);
                poss.add(i + m.level);
                m = m.parent;
            } while (m != null);

        }
    }

    private void doFindCommonSubstrings(TrieNode<Map<Integer, List<Integer>>> n, int minLen, PositionList<String> css) {

        if (n.level >= minLen) {
            if (n.data.size() == strings.size()) {
                css.add(n.getString(), n.level);
            }
        }
        for (TrieNode<Map<Integer, List<Integer>>> child : n.children.getElements()) {
            doFindCommonSubstrings(child, minLen, css);
        }
    }

    public List<String> getLongestCommonSubstrings(int minLen) {
        PositionList<String> css = new PositionList<String>();

        doFindCommonSubstrings(root, minLen, css);

        return css.getElements();
    }

}

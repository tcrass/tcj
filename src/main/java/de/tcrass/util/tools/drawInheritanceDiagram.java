package de.tcrass.util.tools;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.tcrass.graph.Edge;
import de.tcrass.graph.Graph;
import de.tcrass.graph.Graphviz;
import de.tcrass.graph.Node;
import de.tcrass.io.ArgumentsParseException;
import de.tcrass.io.ArgumentsParser;
import de.tcrass.io.FileUtils;
import de.tcrass.util.CollectionUtils;
import de.tcrass.util.StringUtils;

public class drawInheritanceDiagram {

    private static final Properties   DEFAULT_ATTRIBS;
    private static final Properties   PACKAGE_ATTRIBS;
    private static final Properties   ENTITY_ATTRIBS;
    private static final Properties   CLASS_ATTRIBS;
    private static final Properties   INTERFACE_ATTRIBS;
    private static final Properties   INHERITANCE_ATTRIBS;
    private static final Properties   IMPLEMENTATION_ATTRIBS;

    private static final List<String> CLASSPATH;

    private static List<String>       START_CLASSES;
    private static boolean            RECURSE                    = false;
    private static String             OUTPUT_FILE                = "inheritance.svg";
    private static String             OUTPUT_FORMAT              = null;

    private static List<String>       STOP_CLASSES;
    private static List<String>       STOP_INTERFACES;
    private static List<String>       STOP_PACKAGES;
    private static List<String>       BRIEF_CLASSES;
    private static boolean            CLEAR_REGEX_LISTS          = false;

    private static boolean            SORT_FIELDS                = true;
    private static boolean            SORT_METHODS               = true;
    private static boolean            SHOW_PUBLIC_FIELDS         = false;
    private static boolean            SHOW_PROTECTED_FIELDS      = false;
    private static boolean            SHOW_PUBLIC_METHODS        = true;
    private static boolean            SHOW_PROTECTED_METHODS     = false;
    private static boolean            SHOW_STOP_CLASSES          = true;
    private static boolean            SHOW_STOP_INTERFACES       = true;
    private static boolean            SHOW_STOP_PACKAGES         = false;
    private static boolean            SHOW_INNER_CLASSES         = false;
    private static boolean            SHOW_PACKAGES              = true;
    private static boolean            SHOW_EMPTY_PACKAGES        = false;
    private static boolean            SHOW_STOP_FIELDS           = false;
    private static boolean            SHOW_STOP_METHODS          = false;
    private static boolean            SHOW_OBJECT                = false;
    private static boolean            QUALIFIED_INSIDE_PACKAGES  = false;
    private static boolean            QUALIFIED_OUTSIDE_PACKAGES = true;

    static {
        START_CLASSES = new ArrayList<String>();

        BRIEF_CLASSES = new ArrayList<String>();
        BRIEF_CLASSES.add("^java.*\\.");
        BRIEF_CLASSES.add("^javax.*\\.");

        STOP_CLASSES = new ArrayList<String>();
        STOP_CLASSES.add("^java.*\\.");
        STOP_CLASSES.add("^javax.*\\.");

        STOP_INTERFACES = new ArrayList<String>();
        STOP_INTERFACES.add("^java.*\\.");
        STOP_INTERFACES.add("^javax.*\\.");

        STOP_PACKAGES = new ArrayList<String>();
        STOP_PACKAGES.add("^java.*\\.");
        STOP_PACKAGES.add("^javax.*\\.");

        DEFAULT_ATTRIBS = new Properties();
        DEFAULT_ATTRIBS.setProperty("fontname", "monospace");

        PACKAGE_ATTRIBS = (Properties) DEFAULT_ATTRIBS.clone();
        PACKAGE_ATTRIBS.setProperty("labelloc", "t");
        PACKAGE_ATTRIBS.setProperty("labeljust", "l");
        PACKAGE_ATTRIBS.setProperty("rankdir", "BT");

        ENTITY_ATTRIBS = (Properties) DEFAULT_ATTRIBS.clone();
        ENTITY_ATTRIBS.setProperty("shape", "record");

        CLASS_ATTRIBS = (Properties) ENTITY_ATTRIBS.clone();
        CLASS_ATTRIBS.setProperty("color", "#000080");

        INTERFACE_ATTRIBS = (Properties) ENTITY_ATTRIBS.clone();
        INTERFACE_ATTRIBS.setProperty("color", "#ffc000");

        INHERITANCE_ATTRIBS = (Properties) DEFAULT_ATTRIBS.clone();
        INHERITANCE_ATTRIBS.setProperty("arrowhead", "otriangle");
        INHERITANCE_ATTRIBS.setProperty("arrowsize", "1.5");

        IMPLEMENTATION_ATTRIBS = (Properties) DEFAULT_ATTRIBS.clone();
        IMPLEMENTATION_ATTRIBS.setProperty("arrowhead", "otriangle");
        IMPLEMENTATION_ATTRIBS.setProperty("style", "dashed");
        IMPLEMENTATION_ATTRIBS.setProperty("arrowsize", "1.5");

        CLASSPATH = new ArrayList<String>();
        String classPath = System.getProperty("java.class.path");
        String[] classPaths = classPath.split(File.pathSeparator);
        for (String path : classPaths) {
            try {
                CLASSPATH.add(new File(path).getCanonicalPath());
            }
            catch (IOException ex) {
            }
        }
    }

    public static void main(String[] argv) {

        ArgumentsParser ap = new ArgumentsParser();

        ap.setBoolOptionName("help", '?');
        ap.setBoolSwitchName("recurse", 'r', RECURSE);
        ap.setStringArgName("output-format", 'f', OUTPUT_FORMAT);
        ap.setStringArgName("output-file", 'o');

        ap.setStringListName("stop-classes", 'C');
        ap.setStringListName("stop-interfaces", 'I');
        ap.setStringListName("stop-packages", 'P');
        ap.setStringListName("brief-classes", 'B');

        ap.setBoolSwitchName("clear-regex-lists", 'x', CLEAR_REGEX_LISTS);
        ap.setBoolSwitchName("sort-fields", 't', SORT_FIELDS);
        ap.setBoolSwitchName("sort-methods", 'T', SORT_METHODS);
        ap.setBoolSwitchName("show-public-fields", 'a', SHOW_PUBLIC_FIELDS);
        ap.setBoolSwitchName("show-protected-fields", 'A', SHOW_PROTECTED_FIELDS);
        ap.setBoolSwitchName("show-public-methods", 'm', SHOW_PUBLIC_METHODS);
        ap.setBoolSwitchName("show-protected-methods", 'M', SHOW_PROTECTED_METHODS);
        ap.setBoolSwitchName("show-stop-classes", 'c', SHOW_STOP_CLASSES);
        ap.setBoolSwitchName("show-stop-interfaces", 'i', SHOW_STOP_INTERFACES);
        ap.setBoolSwitchName("show-stop-packages", 'p', SHOW_STOP_PACKAGES);
        ap.setBoolSwitchName("show-inner-classes", 'n', SHOW_INNER_CLASSES);
        ap.setBoolSwitchName("show-packages", 'e', SHOW_PACKAGES);
        ap.setBoolSwitchName("show-empty-packages", 'E', SHOW_EMPTY_PACKAGES);
        ap.setBoolSwitchName("show-stop-fields", 's', SHOW_STOP_FIELDS);
        ap.setBoolSwitchName("show-stop-methods", 'S', SHOW_STOP_METHODS);
        ap.setBoolSwitchName("show-object", 'O', SHOW_OBJECT);
        ap.setBoolSwitchName("qualified-inside-packages", 'q', QUALIFIED_INSIDE_PACKAGES);
        ap.setBoolSwitchName("qualified-outside-packages", 'Q', QUALIFIED_OUTSIDE_PACKAGES);
        ap.setMaxFilenames(-1);

        try {
            ap.parse(argv);

            if (ap.getBoolOption("help") || (ap.getFilenames().size() == 0)) {
                printUsage();
                System.exit(0);
            }

            START_CLASSES = ap.getFilenames();
            RECURSE = ap.getBoolSwitch("recurse");
            OUTPUT_FORMAT = ap.getStringArg("output-format");
            if (ap.getStringArg("output-file") != null) {
                OUTPUT_FILE = ap.getStringArg("output-file");
            }
            if (OUTPUT_FORMAT == null) {
                OUTPUT_FORMAT = FileUtils.getExtension(OUTPUT_FILE);
            }

            CLEAR_REGEX_LISTS = ap.getBoolSwitch("clear-regex-lists");
            SORT_FIELDS = ap.getBoolSwitch("sort-fields");
            SORT_METHODS = ap.getBoolSwitch("sort-methods");
            SHOW_PUBLIC_FIELDS = ap.getBoolSwitch("show-public-fields");
            SHOW_PROTECTED_FIELDS = ap.getBoolSwitch("show-protected-fields");
            SHOW_PUBLIC_METHODS = ap.getBoolSwitch("show-public-methods");
            SHOW_PROTECTED_METHODS = ap.getBoolSwitch("show-protected-methods");
            SHOW_STOP_CLASSES = ap.getBoolSwitch("show-stop-classes");
            SHOW_STOP_PACKAGES = ap.getBoolSwitch("show-stop-packages");
            SHOW_STOP_INTERFACES = ap.getBoolSwitch("show-stop-interfaces");
            SHOW_INNER_CLASSES = ap.getBoolSwitch("show-inner-classes");
            SHOW_PACKAGES = ap.getBoolSwitch("show-packages");
            SHOW_EMPTY_PACKAGES = ap.getBoolSwitch("show-empty-packages");
            SHOW_STOP_FIELDS = ap.getBoolSwitch("show-stop-fields");
            SHOW_STOP_METHODS = ap.getBoolSwitch("show-stop-methods");
            SHOW_OBJECT = ap.getBoolSwitch("show-object");
            QUALIFIED_INSIDE_PACKAGES = ap.getBoolSwitch("qualified-inside-packages");
            QUALIFIED_OUTSIDE_PACKAGES = ap.getBoolSwitch("qualified-outside-packages");

            if (CLEAR_REGEX_LISTS) {
                STOP_CLASSES.clear();
                STOP_INTERFACES.clear();
                STOP_PACKAGES.clear();
                BRIEF_CLASSES.clear();
            }

            STOP_CLASSES.addAll(ap.getStringList("stop-classes"));
            STOP_INTERFACES.addAll(ap.getStringList("stop-interfaces"));
            STOP_PACKAGES.addAll(ap.getStringList("stop-packages"));
            BRIEF_CLASSES.addAll(ap.getStringList("brief-classes"));

        }
        catch (ArgumentsParseException ex) {
            ex.printStackTrace();
            printUsage();
            System.exit(1);
        }

        Graph inheritance = new Graph();
        List<Class<?>> classes = findClasses(START_CLASSES);
        for (Class<?> clazz : classes) {
            assertClassNode(inheritance, clazz);
        }

        Graph diagram = new Graph();
        clonePackageStructure(inheritance, diagram);
        diagram.setGVAttributes((Properties) PACKAGE_ATTRIBS.clone());

        try {
            Graphviz.doLayoutToFile(diagram, OUTPUT_FILE, Graphviz.DOT_LAYOUT, OUTPUT_FORMAT);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private static void clonePackageStructure(Graph orig, Graph clone) {
        for (Graph sg : orig.getDirectlyOwnedSubgraphs()) {
            Graph next = null;
            if ((sg.getDirectlyOwnedNodes().size() > 0) || SHOW_EMPTY_PACKAGES) {
                next = clone.createSubgraph("cluster_" + sg.getIdString());
                next.data().set("PACKAGE", sg.getId());
                next.setGVAttributes((Properties) PACKAGE_ATTRIBS.clone());
                next.setGVAttribute("label", sg.getIdString());
            }
            else {
                next = clone;
            }
            clonePackageStructure(sg, next);
        }
        for (Node n : orig.getDirectlyOwnedNodes()) {
            Node classNode = n.createClone(clone);
            Class<?> clazz = (Class<?>) classNode.data().get("CLASS");
            boolean stop = classNode.data().getBoolean("STOP");
            if (clazz.isInterface()) {
                classNode.setGVAttributes((Properties) INTERFACE_ATTRIBS.clone());
                classNode.setGVAttribute("label", getLabel(clazz, clone, stop));
            }
            else {
                classNode.setGVAttributes((Properties) CLASS_ATTRIBS.clone());
                classNode.setGVAttribute("label", getLabel(clazz, clone, stop));
            }
        }
        for (Edge e : orig.getDirectlyOwnedEdges()) {
            e.createClone(clone);
        }
    }

    private static List<Class<?>> findClasses(List<String> startPaths) {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        for (String path : startPaths) {
            doFindClasses(classes, path);
        }
        return classes;
    }

    private static void doFindClasses(List<Class<?>> classes, String name) {
        try {
            Class<?> clazz = Class.forName(name);
            classes.add(clazz);
        }
        catch (ClassNotFoundException ex) {
            getClassesFromFile(classes, new File(name));
            String dirName = name.replaceAll("\\.", File.separator);
            getClassesFromFile(classes, new File(dirName));
        }
    }

    private static void getClassesFromFile(List<Class<?>> classes, File f) {
        String className = null;
        if (f.exists()) {
            try {
                String path = f.getCanonicalPath();
                if (f.isDirectory() && RECURSE) {
                    for (String entry : f.list()) {
                        doFindClasses(classes, new File(path, entry).getCanonicalPath());
                    }
                }
                else {
                    className = getClassNameFromPath(path);
                }
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (className != null) {
            try {
                classes.add(Class.forName(className));
            }
            catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static String getClassNameFromPath(String name) {
        String className = null;
        if (name.endsWith(".class") || name.endsWith(".java")) {
            className = name;
            for (String path : CLASSPATH) {
                if (name.startsWith(path)) {
                    className = name.substring(path.length() + 1);
                    break;
                }
            }
            className = className.replaceAll("\\" + File.separator, ".");
            className = className.substring(0, className.lastIndexOf("."));
        }
        return className;
    }

    private static Node assertClassNode(Graph diagram, Class<?> clazz) {
        Node classNode = null;
        if (!clazz.isAnonymousClass() && ((clazz != Object.class) || SHOW_OBJECT) &&
            ((clazz.getDeclaringClass() == null) || SHOW_INNER_CLASSES)) {
            boolean stop = isStopName(clazz.getName(), STOP_CLASSES);
            Graph pg = getPackageGraph(diagram, getSuperName(clazz.getName()));
            classNode = pg.getNode(clazz.getName());
            if (classNode == null) {
                classNode = pg.createNode(clazz.getName());
                classNode.data().set("CLASS", clazz);
                classNode.data().set("STOP", stop);
            }
            Class<?> superClazz = clazz.getSuperclass();
            if (superClazz != null) {
                boolean superStop = isStopName(superClazz.getName(), STOP_CLASSES);
                if (!superStop || (!stop && superStop && SHOW_STOP_CLASSES)) {
                    Node superClassNode = assertClassNode(diagram, superClazz);
                    if (superClassNode != null) {
                        if (diagram.getEdge(classNode, superClassNode) == null) {
                            Edge e = diagram.createEdge(classNode, superClassNode);
                            e.setGVAttributes((Properties) INHERITANCE_ATTRIBS.clone());
                        }
                    }
                }
            }
            for (Class<?> interf : clazz.getInterfaces()) {
                boolean superStop = isStopName(interf.getName(), STOP_CLASSES);
                if (!superStop || (!stop && superStop && SHOW_STOP_INTERFACES)) {
                    Node interfNode = assertClassNode(diagram, interf);
                    if (classNode != null) {
                        if (diagram.getEdge(classNode, interfNode) == null) {
                            Edge e = diagram.createEdge(classNode, interfNode);
                            if (clazz.isInterface()) {
                                e.setGVAttributes((Properties) INHERITANCE_ATTRIBS.clone());
                            }
                            else {
                                e.setGVAttributes((Properties) IMPLEMENTATION_ATTRIBS.clone());
                            }
                        }
                    }
                }
            }
        }
        return classNode;
    }

    private static String getLabel(Class<?> clazz, Graph pg, boolean stop) {
        StringBuffer label = new StringBuffer("{");
        if (clazz.isInterface()) {
            label.append("\\<\\<interface\\>\\>\n");

        }
        else if ((clazz.getModifiers() & Modifier.ABSTRACT) != 0) {
            label.append("\\<\\<abstract\\>\\>\n");
        }
        if (!SHOW_PACKAGES || "".equals(pg.getIdString())) {
            if (QUALIFIED_OUTSIDE_PACKAGES) {
                label.append(clazz.getCanonicalName());
            }
            else {
                label.append(getUnqualifiedName(clazz.getCanonicalName()));
            }

        }
        else {
            if (QUALIFIED_INSIDE_PACKAGES) {
                label.append(clazz.getCanonicalName());
            }
            else {
                label.append(getUnqualifiedName(clazz.getCanonicalName()));
            }

        }

        if (!stop || SHOW_STOP_FIELDS) {
            if (SHOW_PROTECTED_FIELDS) {
                appendFieldDescriptions(clazz, Modifier.PROTECTED, pg, "protected fields:",
                    label);
            }
            if (SHOW_PUBLIC_FIELDS) {
                appendFieldDescriptions(clazz, Modifier.PUBLIC, pg, "public fields:", label);
            }
        }

        if (!stop || SHOW_STOP_METHODS) {
            if (SHOW_PROTECTED_METHODS) {
                appendMethodsDescriptions(clazz, Modifier.PROTECTED, pg, "protected methods:",
                    label);
            }
            if (SHOW_PUBLIC_METHODS) {
                appendMethodsDescriptions(clazz, Modifier.PUBLIC, pg, "public methods:", label);
            }
        }

        label.append("}");
        return label.toString();
    }

    private static void appendFieldDescriptions(Class<?> clazz, int modifiers, Graph pg,
                                                String descr, StringBuffer s) {
        s.append("|" + descr + "\n");
        for (Field field : getFields(clazz, modifiers)) {
            s.append(field.getName() + ": " +
                     getBriefName(getClassName(field.getType(), pg), BRIEF_CLASSES) + "\\l");
        }
    }

    private static void appendMethodsDescriptions(Class<?> clazz, int modifiers, Graph pg,
                                                  String descr, StringBuffer s) {
        s.append("|" + descr + "\n");
        for (Method method : getMethods(clazz, modifiers)) {
            s.append(method.getName() + "(");
            List<String> params = new ArrayList<String>();
            for (Class<?> param : method.getParameterTypes()) {
                params.add(getBriefName(getClassName(param, pg), BRIEF_CLASSES));
            }
            s.append(StringUtils.join(", ", params) + ")");
            if (method.getReturnType() != null) {
                s.append(": " +
                         getBriefName(getClassName(method.getReturnType(), pg), BRIEF_CLASSES));
            }
            if (!clazz.isInterface() && ((method.getModifiers() & Modifier.ABSTRACT) != 0)) {
                s.append(" \\<abstract\\>");
            }
            s.append("\\l");
        }
    }

    private static List<Field> getFields(Class<?> clazz, int modifiers) {
        Field[] decl = clazz.getDeclaredFields();
        if (SORT_FIELDS) {
            Arrays.sort(decl, new Comparator<Field>() {

                public int compare(Field one, Field other) {
                    return one.getName().compareTo(other.getName());
                }
            });
        }
        List<Field> fields = new ArrayList<Field>();
        for (Field field : decl) {
            if ((field.getModifiers() & modifiers) != 0) {
                fields.add(field);
            }
        }
        return fields;
    }

    private static List<Method> getMethods(Class<?> clazz, int modifiers) {
        Method[] decl = clazz.getDeclaredMethods();
        if (SORT_METHODS) {
            Arrays.sort(decl, new Comparator<Method>() {

                public int compare(Method one, Method other) {
                    return one.getName().compareTo(other.getName());
                }
            });
        }
        List<Method> methods = new ArrayList<Method>();
        for (Method method : decl) {
            if ((method.getModifiers() & modifiers) != 0) {
                methods.add(method);
            }
        }
        return methods;
    }

    private static Graph getPackageGraph(Graph diagram, String packageName) {
        if (!SHOW_PACKAGES || packageName.equals("") || isStopName(packageName, STOP_PACKAGES)) {
            return diagram;
        }
        else {
            Graph packageGraph = diagram.getSubgraph(packageName);
            if (packageGraph == null) {
                String superPackage = getSuperName(packageName);
                Graph superPackageGraph = getPackageGraph(diagram, superPackage);
                packageGraph = superPackageGraph.createSubgraph(packageName);
                packageGraph.setGVAttributes((Properties) PACKAGE_ATTRIBS.clone());
                packageGraph.setGVAttribute("label", packageName);
            }
            return packageGraph;
        }
    }

    private static String getSuperName(String name) {
        String superName = "";
        if (name.lastIndexOf(".") > 0) {
            superName = name.substring(0, name.lastIndexOf("."));
        }
        return superName;
    }

    private static boolean isStopName(String name, List<String> stopNames) {
        boolean stop = false;
        for (String regex : stopNames) {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(name);
            if (m.find()) {
                stop = true;
                break;
            }
        }
        return stop;
    }

    private static String getClassName(Class<?> clazz, Graph pg) {
        String name = clazz.getCanonicalName();

        if (getSuperName(clazz.getName()).equals(pg.data().getString("PACKAGE"))) {
            if (!QUALIFIED_INSIDE_PACKAGES) {
                name = getUnqualifiedName(name);
            }
        }
        return name;
    }

    private static String getUnqualifiedName(String name) {
        String unqName = "";
        if (name.lastIndexOf(".") > 0) {
            unqName = name.substring(name.lastIndexOf(".") + 1);
        }
        return unqName;
    }

    private static String getBriefName(String name, List<String> briefNames) {
        String briefName = name;
        for (String regex : briefNames) {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(briefName);
            if (m.find()) {
                m.reset();
                briefName = m.replaceFirst("");
            }
        }
        return briefName;
    }

    private static void printUsage() {
        System.out.println("\ndrawInheritanceDiagram -- create inheritance diagram of Java classes");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Creates an UML-style graphical representation of the inheritance and");
        System.out.println("implementation relationships between Java classes and interfaces.");
        System.out.println("");
        System.out.println("Usage:\n");
        System.out.println("java org.comlink.home.tcrass.util.tools.drawInheritanceDiagram");
        System.out.println("[<options>] <path> [<path>...]");
        System.out.println("\nPossible options:");
        System.out.println("--help,-?:                   Show this message and abort.");
        System.out.println("(+|-)r:                      Whether or not to recurse into sub-");
        System.out.println("                             directories/sub-packages. [" +
                           RECURSE + "]");
        System.out.println("--output-format,-f <format>: Graphviz output format. [<output-file-ext>]");
        System.out.println("--output-file,-o <file>:     Diagram output file. [" + OUTPUT_FILE +
                           "]");
        System.out.println("--stop-classes,-C <regex>:   Stop inheritance reconstruction when a ");
        System.out.println("                             class's name matches <regex>. May be ");
        System.out.println("                             specified multiple times.");
        System.out.println("                             " +
                           CollectionUtils.listToString(STOP_CLASSES));
        System.out.println("--stop-interfaces,-I <regex>:Stop inheritance reconstruction when an");
        System.out.println("                             interface's name matches <regex>. May be ");
        System.out.println("                             specified multiple times.");
        System.out.println("                             " +
                           CollectionUtils.listToString(STOP_INTERFACES));
        System.out.println("--stop-packages,-P <regex>:  If a package name matches <regex>, don't");
        System.out.println("                             explicitly draw a package box. May be ");
        System.out.println("                             specified multiple times.");
        System.out.println("                             " +
                           CollectionUtils.listToString(STOP_PACKAGES));
        System.out.println("--brief-classes,-B <regex>:  If in a field or method description a");
        System.out.println("                             qualified class name matches <regex>, just ");
        System.out.println("                             show the plain, non-qualified class name.");
        System.out.println("                             May be specified multiple times.");
        System.out.println("                             " +
                           CollectionUtils.listToString(BRIEF_CLASSES));
        System.out.println("(+|-)x: Whether or not to clear regex lists before adding items. [" +
                           CLEAR_REGEX_LISTS + "]");
        System.out.println("(+|-)t: Whether or not to sort fields alphabetically. [" +
                           SORT_FIELDS + "]");
        System.out.println("(+|-)T: Whether or not to sort methods alphabetically. [" +
                           SORT_METHODS + "]");
        System.out.println("(+|-)a: Whether or not to show public fields. [" +
                           SHOW_PUBLIC_FIELDS + "]");
        System.out.println("(+|-)A: Whether or not to show protected fields. [" +
                           SHOW_PROTECTED_FIELDS + "]");
        System.out.println("(+|-)m: Whether or not to show public methods. [" +
                           SHOW_PUBLIC_METHODS + "]");
        System.out.println("(+|-)M: Whether or not to show protected methods. [" +
                           SHOW_PROTECTED_METHODS + "]");
        System.out.println("(+|-)c: Whether or not to show stop classes. [" +
                           SHOW_STOP_CLASSES + "]");
        System.out.println("(+|-)i: Whether or not to show stop interfaces. [" +
                           SHOW_STOP_INTERFACES + "]");
        System.out.println("(+|-)p: Whether or not to show stop packages. [" +
                           SHOW_STOP_PACKAGES + "]");
        System.out.println("(+|-)n: Whether or not to show inner classes. [" +
                           SHOW_INNER_CLASSES + "]");
        System.out.println("(+|-)e: Whether or not to show packages. [" + SHOW_PACKAGES + "]");
        System.out.println("(+|-)E: Whether or not to show empty packages. [" +
                           SHOW_EMPTY_PACKAGES + "]");
        System.out.println("(+|-)s: Whether or not to show stop classes' fields. [" +
                           SHOW_STOP_FIELDS + "]");
        System.out.println("(+|-)S: Whether or not to show stop classes' methods. [" +
                           SHOW_STOP_METHODS + "]");
        System.out.println("(+|-)O: Whether or not to show the Object class. [" + SHOW_OBJECT +
                           "]");
        System.out.println("(+|-)q: Whether or not to show fully qualified names inside");
        System.out.println("        packages. [" + QUALIFIED_INSIDE_PACKAGES + "]");
        System.out.println("(+|-)Q: Whether or not to show fully qualified names outside");
        System.out.println("        packages. [" + QUALIFIED_OUTSIDE_PACKAGES + "]");
        System.out.println("\n<path> may be a directory (requires +r), a path to a .java/.class file, a");
        System.out.println("qualified class name or a package name (requires the actual class files to");
        System.out.println("reside in a corresponding sub-directory of some class path entry). In any");
        System.out.println("case, all classes that are to show up in the diagram MUST be accessible ");
        System.out.println("through the class path.");
        System.out.println("Requires Graphviz dot to be installed and in PATH.\n");
    }

}

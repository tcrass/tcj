package de.tcrass.util;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public static String chomp(String s) {
        if (s.endsWith("\n")) {
            return s.substring(0, s.length() - 1);
        }
        else {
            return s;
        }
    }

    public static String replaceAll(String str, String pattern, String replace) {
        StringBuffer s = new StringBuffer();
        if (str != null) {
            int last = 0;
            int i = str.indexOf(pattern);
            while ((i != -1) && (i < str.length())) {
                s = s.append(str.substring(last, i)).append(replace);
                last = i + pattern.length();
                i = str.indexOf(pattern, last);
            }

            s.append(str.substring(last));
        }
        return s.toString();
    }

    public static String replaceAllMatches(String str, String regex, String replace) {
        StringBuffer s = new StringBuffer();
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        while (m.find()) {
            m.appendReplacement(s, replace);
        }
        m.appendTail(s);
        return s.toString();
    }

    public static String toHTMLCaption(String str) {
        String html = str;
        if (!html.startsWith("<HTML>")) {
            html = "<HTML>" + html + "</HTML>";
        }
        html = replaceAll(html, "\n", "<BR>");
        return html;
    }

    public static String escapeHTMLStyle(String s) {

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                case '&':
                    sb.append("&amp;");
                    break;
                case '"':
                    sb.append("&quot;");
                    break;
                case 'à':
                    sb.append("&agrave;");
                    break;
                case 'À':
                    sb.append("&Agrave;");
                    break;
                case 'â':
                    sb.append("&acirc;");
                    break;
                case 'Â':
                    sb.append("&Acirc;");
                    break;
                case 'ä':
                    sb.append("&auml;");
                    break;
                case 'Ä':
                    sb.append("&Auml;");
                    break;
                case 'å':
                    sb.append("&aring;");
                    break;
                case 'Å':
                    sb.append("&Aring;");
                    break;
                case 'æ':
                    sb.append("&aelig;");
                    break;
                case 'Æ':
                    sb.append("&AElig;");
                    break;
                case 'ç':
                    sb.append("&ccedil;");
                    break;
                case 'Ç':
                    sb.append("&Ccedil;");
                    break;
                case 'é':
                    sb.append("&eacute;");
                    break;
                case 'É':
                    sb.append("&Eacute;");
                    break;
                case 'è':
                    sb.append("&egrave;");
                    break;
                case 'È':
                    sb.append("&Egrave;");
                    break;
                case 'ê':
                    sb.append("&ecirc;");
                    break;
                case 'Ê':
                    sb.append("&Ecirc;");
                    break;
                case 'ë':
                    sb.append("&euml;");
                    break;
                case 'Ë':
                    sb.append("&Euml;");
                    break;
                case 'ï':
                    sb.append("&iuml;");
                    break;
                case 'Ï':
                    sb.append("&Iuml;");
                    break;
                case 'ô':
                    sb.append("&ocirc;");
                    break;
                case 'Ô':
                    sb.append("&Ocirc;");
                    break;
                case 'ö':
                    sb.append("&ouml;");
                    break;
                case 'Ö':
                    sb.append("&Ouml;");
                    break;
                case 'ø':
                    sb.append("&oslash;");
                    break;
                case 'Ø':
                    sb.append("&Oslash;");
                    break;
                case 'ß':
                    sb.append("&szlig;");
                    break;
                case 'ù':
                    sb.append("&ugrave;");
                    break;
                case 'Ù':
                    sb.append("&Ugrave;");
                    break;
                case 'û':
                    sb.append("&ucirc;");
                    break;
                case 'Û':
                    sb.append("&Ucirc;");
                    break;
                case 'ü':
                    sb.append("&uuml;");
                    break;
                case 'Ü':
                    sb.append("&Uuml;");
                    break;
                case '®':
                    sb.append("&reg;");
                    break;
                case '©':
                    sb.append("&copy;");
                    break;
                case '€':
                    sb.append("&euro;");
                    break;
                default:
                    int code = 0xffff & c;
                    if (code < 160) {
                        sb.append(c);
                    }
                    else {
                        sb.append("&#");
                        sb.append(new Integer(code).toString());
                        sb.append(';');
                    }
            }
        }
        return sb.toString();
    }

    public static String polymerize(String mono, long n) {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < n; i++) {
            s.append(mono);
        }
        return s.toString();
    }

    public static String join(String glue, String[] items) {
        return join(glue, Arrays.asList(items));
    }

    public static String join(String glue, List<String> items) {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < items.size(); i++) {
            s.append(items.get(i));
            if (i < (items.size() - 1)) {
                s.append(glue);
            }
        }
        return s.toString();
    }

    public static String firstToUpperCase(String s) {
        String result;
        if (s.length() < 1) {
            result = new String(s);
        }
        else {
            result = s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return result;
    }

    public static String firstToLowerCase(String s) {
        String result;
        if (s.length() < 1) {
            result = new String(s);
        }
        else {
            result = s.substring(0, 1).toLowerCase() + s.substring(1);
        }
        return result;
    }

    public static String insertLinebreaks(String line, int minLen, String separators,
                                          String linebreak) {
        StringBuffer bl = new StringBuffer();
        if (line != null) {
            Pattern p;
            p = Pattern.compile("(.{" + minLen + ",}?)( +|[" + separators + "])");
            Matcher m = p.matcher(line);
            while (m.find()) {
                String lb;
                if (m.group(2).matches(" +")) {
                    lb = linebreak;
                }
                else if (m.group(2).equals("-")) {
                    lb = "-" + linebreak;
                }
                else {
                    lb = m.group(2) + "-" + linebreak;
                }
                m.appendReplacement(bl, m.group(1) + lb);

            }
            m.appendTail(bl);
        }
        return bl.toString();
    }

    public static String insertHTMLStyleLinebreaks(String line, int minLen) {
        return insertLinebreaks(line, minLen, "-", "<br>\n");
    }

    public static String insertSystemLinebreaks(String line, int minLen) {
        return insertLinebreaks(line, minLen, "-", "\n");
    }

    public static String insertEscapedLinebreaks(String line, int minLen) {
        return insertLinebreaks(line, minLen, "-", "\\n");
    }

    public static boolean matchesAnywhere(String str, String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.find();
    }

    public static String toCamelBack(String line) {
        return toCamelBack(line, false, false);
    }
    
    public static String toCamelBack(String line, boolean firstUpper, boolean forceLower) {
        String s = "";

        Pattern p;
        Matcher m;
        StringBuffer b;

        p = Pattern.compile("[^a-zA-Z0-9]");
        m = p.matcher(line);
        b = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(b, " ");
        }
        m.appendTail(b);
        s = b.toString();
        if (forceLower) {
            s = s.toLowerCase();
        }
        if (firstUpper) {
            s = firstToUpperCase(s);
        }

        p = Pattern.compile("\\s([a-z])");
        m = p.matcher(s);
        b = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(b, m.group(1).toUpperCase());
        }
        m.appendTail(b);
        s = b.toString();

        p = Pattern.compile("\\s");
        m = p.matcher(s);
        b = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(b, "");
        }
        m.appendTail(b);
        s = b.toString();

        return s;
    }

}

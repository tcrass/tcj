package de.tcrass.util;

import java.util.HashMap;
import java.util.Timer;

public class ApplicationTimer extends Timer {

    public static final String            DEFAULT_TIMER_NAME = "__DEFAULT__";

    private static HashMap<String, Timer> timers             = new HashMap<String, Timer>();

    private ApplicationTimer() {
    }

    public static void schedule(TimerEvent e, long delay) {
        schedule(DEFAULT_TIMER_NAME, e, delay);
    }

    public static void schedule(String name, TimerEvent e, long delay) {
        do {
            if (timers.get(name) == null) {
                timers.put(name, new Timer());
            }
            try {
                timers.get(name).schedule(e, delay);
            }
            catch (IllegalStateException ex) {
                ex.printStackTrace();
                if (ex.getMessage().startsWith("Timer already cancelled.")) {
                    timers.get(name).purge();
                    timers.put(name, null);
                }
            }

        } while (timers.get(name) == null);
    }

}

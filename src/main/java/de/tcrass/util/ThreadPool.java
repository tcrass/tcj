/*
 * Created on 08.11.2004 According to Spell B. (2000), "Professional
 * Java Programming" Worx Press, Birmingham
 */
package de.tcrass.util;

import java.util.Vector;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class ThreadPool implements Runnable {

    public final static int    DEFAULT_MINIMUM_SIZE  = 0;
    public final static int    DEFAULT_MAXIMUM_SIZE  = Integer.MAX_VALUE;
    public final static long   DEFAULT_RELEASE_DELAY = 60 * 1000;

    private static ThreadPool  defaultThreadPool;

    private int                maximumSize;
    private int                minimumSize;
    protected long             releaseDelay;

    private int                currentSize;
    private int                availableThreads;
    protected Vector<Runnable> taskList;

    // --- Default singleton access ---

    public static ThreadPool getDefaultThreadPool() {
        if (defaultThreadPool == null) {
            defaultThreadPool = new ThreadPool();
        }
        return defaultThreadPool;
    }

    // --- Constructors ---

    public ThreadPool() {
        this(DEFAULT_MINIMUM_SIZE, DEFAULT_MAXIMUM_SIZE, DEFAULT_RELEASE_DELAY);
    }

    public ThreadPool(int minSize, int maxSize, long delay) {
        minimumSize = minSize;
        maximumSize = maxSize;
        releaseDelay = delay;
        taskList = new Vector<Runnable>(100);
        availableThreads = 0;
    }

    // --- Attribute access ---

    public synchronized int getMaximumSize() {
        return maximumSize;
    }

    public synchronized void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public synchronized int getMinimumSize() {
        return minimumSize;
    }

    public synchronized void setMinimumSize(int minimumSize) {
        this.minimumSize = minimumSize;
    }

    public synchronized long getReleaseDelay() {
        return releaseDelay;
    }

    public synchronized void setReleaseDelay(long releaseDelay) {
        this.releaseDelay = releaseDelay;
    }

    // --- Private methods

    private synchronized Runnable getNextTask() {
        Runnable task = null;
        if (taskList.size() > 0) {
            task = taskList.get(0);
            taskList.removeElementAt(0);
        }
        return task;
    }

    // --- Public methods ---

    public synchronized void addTask(Runnable runnable) {
        taskList.addElement(runnable);
        if (availableThreads > 0) {
            notifyAll();
        }
        else {
            if (currentSize < maximumSize) {
                Thread t = new Thread(this);
                currentSize++;
                t.start();
            }
        }
    }

    // --- Implementation of Runnable

    public void run() {
        Runnable task = null;
        while (true) {
            synchronized (this) {
                if (currentSize > maximumSize) {
                    currentSize--;
                    break;
                }
                task = getNextTask();
                if (task == null) {
                    try {
                        availableThreads++;
                        wait(releaseDelay);
                        availableThreads--;
                    }
                    catch (InterruptedException ie) {
                        SysUtils.doNothing();
                    }
                    task = getNextTask();
                    if (task == null) {
                        if (currentSize <= minimumSize) {
                            continue;
                        }
                        currentSize--;
                        break;
                    }
                }
            }
            if (task != null) {
                try {
                    task.run();
                }
                catch (Exception e) {
                    SysUtils.reportException(e);
                }
            }

        }
    }

}

package de.tcrass.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.tcrass.xml.XMLUtils;

/**
 * This class implements a general container for storing multiple data
 * items of (almost) arbitrary kind, with each data item being
 * identified by a unique String as key. Generally, this class is not
 * intended to be used stand-alone, but rather as a helper class for
 * other Objects which need to provide means for being annotated, marked
 * or labelled in any kind. Examples for such classes encompass various
 * {@link de.tcrass.graph.GraphObject}s, in particular the
 * {@link de.tcrass.graph.Graph}, {@link de.tcrass.graph.Node} and
 * {@link de.tcrass.graph.Edge} classes.
 * <p>
 * DataStore features an XML-based serialization mechanism which is
 * capable of representing a vast variety of different data types. In
 * particular
 * <ul>
 * <li>All primitive types can be stored and retrieved without loss -
 * altough they obviously get boxed into their corresponding classes for
 * storage
 * <li>Collections (Sets/Lists) of (almost) arbitrary objects can be
 * stored, but the exact list type (both its class and its element type)
 * will be lost when restoring a DataStore from XML
 * <li>Maps can have keys and values of (almost) arbitrary kind, but, as
 * for Lists, their exact class as well as their key and value type
 * definitions will be lost after XML serialization
 * <li>Collections and Maps may be be used as Collection elements and/or
 * Map keys/values in an arbitrarily nested fashion; the aforementioned
 * limitations for class/type retrieval after XML storage do, however,
 * apply for thus used containers, too
 * <li>Objects of all other classes (including Arrays) will be converted
 * to their {@link java.lang.String} representation, as provided by
 * their corresponding {@link java.lang.Object#toString()}
 * implementation
 * </ul>
 * <p>
 * The fact that the DataStore class has been designed during
 * implementation of the above mentioned graph-related classes accounts
 * for the used notation of data types within the generated XML. GraphML
 * datatypes have been used wherever applicable; other data types still
 * await proper treatment by a yet to be written XML schema definition.
 * <p>
 * A DataStore supports three different kinds of action to be performed
 * on its stored data items when cloned: the may be copied by value, by
 * reference or not at all. A default cloning action can be defined, but
 * each items's cloning action can also be set individually.
 */
public class DataStore implements Cloneable {

    /**
     * The name that will used for XML tags representing a DataStore's
     * individual data items.
     */
    public static final String           DEFAULT_TAG_NAME    = "data";

    /**
     * Constant representing the copy action "don't copy this data
     * item".
     */
    public static final int              DO_NOT_COPY         = 0;
    /**
     * Constant representing the copy action "copy a reference to this
     * data item".
     */
    public static final int              COPY_BY_REFERENCE   = 1;
    /**
     * Constant representing the copy action "copy this data item's
     * actual value".
     */
    public static final int              COPY_BY_VALUE       = 2;

    /**
     * A list of human-readable names for the three supported copy
     * actions.
     */
    public static final String[]         COPY_ACTION_NAMES   = new String[] {
        "none", "by_reference", "by_value"
                                                             };

    /**
     * The copy action that will by default be used as a DataStore's
     * default copy action (yes, this is exactly the intended meaning!).
     */
    public static final int              DEFAULT_COPY_ACTION = COPY_BY_VALUE;

    /**
     * The default XML namespace prefix that will be used for
     * DataStore-specific XML attributes.
     */
    public static final String           DEFAULT_NS_PREFIX   = "ds";

    /**
     * DataStore's namespace and XMLS definition URL.
     */
    public static final String           SCHEMA_NAME         = "http://www.sybig.de/xml/schema/dataStore/1.0/dataStore.xsd";

    /**
     * The actual namespace prefix to be used.
     */
    private static String                nsPrefix;
    /**
     * A mapping between "primitive" classes and corresponding type
     * names.
     */
    private static Map<Class<?>, String> TYPE_STRING_MAPPING;

    /**
     * The DataStore's actual data container.
     */
    private HashMap<String, Object>      data;
    /**
     * If copy actions are specified for individual data items, this is
     * where they go.
     */
    private Hashtable<String, Integer>   copyActions;
    /**
     * This DataStore's default copy action.
     */
    private int                          defaultCopyAction;

    // --- Constructors ---

    /**
     * Registers the default XML namespace prefix as the current
     * namespace prefix.
     */
    static {
        setNSPrefix(DEFAULT_NS_PREFIX);
    }

    /**
     * Creates a new DataStore with {@link DataStore#COPY_BY_VALUE} as
     * default copy action.
     */
    public DataStore() {
        data = new HashMap<String, Object>();
        copyActions = new Hashtable<String, Integer>();
        defaultCopyAction = DEFAULT_COPY_ACTION;
    }

    // --- Static methods ---

    /**
     * Returns the XML namespace prefix for DataStore-specific XML
     * attributes.
     * 
     * @return the current XML namespace prefix
     */
    public static String getNSPrefix() {
        return nsPrefix;
    }

    /**
     * Sets the XML namespace prefix for DataStore-specific XML
     * attributes.
     * 
     * @param prefix
     *            the new XML namespace prefix
     */
    public static void setNSPrefix(String prefix) {
        nsPrefix = prefix;
        TYPE_STRING_MAPPING = new Hashtable<Class<?>, String>();
        TYPE_STRING_MAPPING.put(Boolean.class, "boolean");
        TYPE_STRING_MAPPING.put(Character.class, nsPrefix + ":char");
        TYPE_STRING_MAPPING.put(Byte.class, nsPrefix + ":byte");
        TYPE_STRING_MAPPING.put(Short.class, nsPrefix + ":short");
        TYPE_STRING_MAPPING.put(Integer.class, "int");
        TYPE_STRING_MAPPING.put(Long.class, "long");
        TYPE_STRING_MAPPING.put(Float.class, "float");
        TYPE_STRING_MAPPING.put(Double.class, "double");
        TYPE_STRING_MAPPING.put(String.class, "string");
    }

    // --- Private methods ---

    /**
     * Clones an Object according to the given copy action.
     */
    private Object cloneDataStoreEntry(Object o, int action, int depth)
    throws InstantiationException, IllegalAccessException, InvocationTargetException {

        Object c = null;
        if (action == COPY_BY_REFERENCE) {
            c = o;
        }
        else if (action == COPY_BY_VALUE) {
            if (o != null) {
                if (o instanceof Boolean) {
                    c = Boolean.valueOf(((Boolean) o).booleanValue());
                }
                else if (o instanceof Character) {
                    c = Character.valueOf(((Character) o).charValue());
                }
                else if (o instanceof Byte) {
                    c = Byte.valueOf(((Byte) o).byteValue());
                }
                else if (o instanceof Short) {
                    c = Short.valueOf(((Short) o).shortValue());
                }
                else if (o instanceof Integer) {
                    c = Integer.valueOf(((Integer) o).intValue());
                }
                else if (o instanceof Long) {
                    c = Long.valueOf(((Long) o).longValue());
                }
                else if (o instanceof Float) {
                    c = Float.valueOf(((Float) o).floatValue());
                }
                else if (o instanceof Double) {
                    c = Double.valueOf(((Double) o).doubleValue());
                }
                else if (o instanceof String) {
                    c = new String((String) o);
                }
                else if (o instanceof Map) {
                    c = o.getClass().newInstance();
                    Map om = (Map) o;
                    Map cm = (Map) c;
                    for (Object key : om.keySet()) {
                        cm.put(key, cloneDataStoreEntry(om.get(key), action, depth + 1));
                    }
                }
                else if (o instanceof Collection) {
                    c = o.getClass().newInstance();
                    Collection oc = (Collection) o;
                    Collection cc = (Collection) c;
                    for (Object val : oc) {
                        cc.add(cloneDataStoreEntry(val, action, depth + 1));
                    }
                }
                else {
                    c = Clonator.clone(o);
                    if (c == null) {
                        Clonator.createShallowClone(o);
                    }
                }
            }
        }
        return c;
    }

    /**
     * Returns the type string for the given Object.
     */
    private String getTypeStr(Object o) {
        String type = null;
        if (o != null) {
            type = TYPE_STRING_MAPPING.get(o.getClass());
            if (type == null) {
                if (o instanceof Properties) {
                    type = nsPrefix + ":properties";
                }
                else if (o instanceof Map) {
                    type = nsPrefix + ":map";
                }
                else if (o instanceof Set) {
                    type = nsPrefix + ":set";
                }
                else if (o instanceof List) {
                    type = nsPrefix + ":list";
                }
                else {
                    type = nsPrefix + ":object:" + o.getClass().getName();
                }
            }
        }
        return type;
    }

    /**
     * Creates an XML element representing the given Object.
     */
    private Element dataToXML(Document d, String tagName, String keyStr, String typeStr,
                              Object o) {
        Element e = d.createElement(tagName);
        if (keyStr != null) {
            e.setAttribute("key", keyStr);
        }
        if (o != null) {
            if (o instanceof Boolean) {
                XMLUtils.addText(e, ((Boolean) o).toString());
            }
            else if (o instanceof Character) {
                XMLUtils.addText(e, ((Character) o).toString());
            }
            else if (o instanceof Byte) {
                XMLUtils.addText(e, ((Byte) o).toString());
            }
            else if (o instanceof Short) {
                XMLUtils.addText(e, ((Short) o).toString());
            }
            else if (o instanceof Integer) {
                XMLUtils.addText(e, ((Integer) o).toString());
            }
            else if (o instanceof Long) {
                XMLUtils.addText(e, ((Long) o).toString());
            }
            else if (o instanceof Float) {
                XMLUtils.addText(e, ((Float) o).toString());
            }
            else if (o instanceof Double) {
                XMLUtils.addText(e, ((Double) o).toString());
            }
            else if (o instanceof String) {
                XMLUtils.addText(e, (String) o);
            }
            else if (o instanceof Properties) {
                Properties p = (Properties) o;
                for (Object key : p.keySet()) {
                    Element itemElement = d.createElement("property");
                    itemElement.setAttribute("name", (String) key);
                    XMLUtils.addText(itemElement, (String) p.get(key));
                    e.appendChild(itemElement);
                }
            }
            else if (o instanceof Map) {
                Map<?, ?> m = (Map<?, ?>) o;
                for (Object key : m.keySet()) {
                    Element itemElement = d.createElement("entry");
                    Element keyElement = dataToXML(d, "key", null, getTypeStr(key), key);
                    itemElement.appendChild(keyElement);
                    Object val = m.get(key);
                    Element valElement = dataToXML(d, "val", null, getTypeStr(val), val);
                    itemElement.appendChild(valElement);
                    e.appendChild(itemElement);
                }
            }
            else if (o instanceof List) {
                List<?> l = (List<?>) o;
                for (Object item : l) {
                    Element itemElement = dataToXML(d, "item", null, getTypeStr(item), item);
                    e.appendChild(itemElement);
                }
            }
            else if (o instanceof Set) {
                Set<?> s = (Set<?>) o;
                for (Object item : s) {
                    Element itemElement = dataToXML(d, "element", null, getTypeStr(item), item);
                    e.appendChild(itemElement);
                }
            }
            else {
                XMLUtils.addText(e, o.toString());
            }
        }
        if (typeStr != null) {
            e.setAttribute("type", typeStr);
        }
        return e;
    }

    /**
     * Rtrieves the type string for the Object represented by the given
     * XML element, either from its "type" attribute, or from the given
     * mapping between data keys and corresponding types.
     */
    private static String getTypeString(Element e, Map<String, String> keyTypeMapping) {
        String type = XMLUtils.getStringAttribute(e, "type");
        if ((keyTypeMapping != null) && ((type == null) || type.equals(""))) {
            type = keyTypeMapping.get(e.getAttribute("key"));
        }
        return type;
    }

    /**
     * Returns the Object described by the given XML element, parsed
     * according to the given type.
     */
    private static Object dataFromXML(Element e, String typeString) {
        Object o = null;
        String val = XMLUtils.getText(e);
        if (typeString.equals("boolean")) {
            o = Boolean.parseBoolean(val);
        }
        else if (typeString.equals(nsPrefix + ":char")) {
            o = val.charAt(0);
        }
        else if (typeString.equals(nsPrefix + ":byte")) {
            o = Byte.parseByte(val);
        }
        else if (typeString.equals(nsPrefix + ":short")) {
            o = Short.parseShort(val);
        }
        else if (typeString.equals("int")) {
            o = Integer.parseInt(val);
        }
        else if (typeString.equals("long")) {
            o = Long.parseLong(val);
        }
        else if (typeString.equals("float")) {
            o = Float.parseFloat(val);
        }
        else if (typeString.equals("double")) {
            o = Double.parseDouble(val);
        }
        else if (typeString.equals("string")) {
            o = val;
        }
        else if (typeString.equals(nsPrefix + ":properties")) {
            Properties p = new Properties();
            for (Element itemElement : XMLUtils.getChildrenByName(e, "property")) {
                String key = itemElement.getAttribute("name");
                String prop = XMLUtils.getText(itemElement);
                p.setProperty(key, prop);
            }
            o = p;
        }
        else if (typeString.equals(nsPrefix + ":map")) {
            Map<Object, Object> m = new HashMap<Object, Object>();
            for (Element mappingElement : XMLUtils.getChildrenByName(e, "entry")) {
                Element keyElement = XMLUtils.getChildByName(mappingElement, "key");
                Object key = dataFromXML(keyElement, getTypeString(keyElement, null));
                Element valElement = XMLUtils.getChildByName(mappingElement, "val");
                Object mval = dataFromXML(valElement, getTypeString(valElement, null));
                m.put(key, mval);
            }
            o = m;
        }
        else if (typeString.equals(nsPrefix + ":list")) {
            List<Object> l = new ArrayList<Object>();
            for (Element itemElement : XMLUtils.getChildrenByName(e, "item")) {
                Object item = dataFromXML(itemElement, getTypeString(itemElement, null));
                l.add(item);
            }
            o = l;
        }
        else if (typeString.equals(nsPrefix + ":set")) {
            Set<Object> s = new HashSet<Object>();
            for (Element itemElement : XMLUtils.getChildrenByName(e, "element")) {
                Object item = dataFromXML(itemElement, getTypeString(itemElement, null));
                s.add(item);
            }
            o = s;
        }
        return o;
    }

    // --- Public methods ---

    /**
     * Returns a set of all keys used in this DataStore instance.
     * 
     * @return the used keys
     */
    public Set<String> getKeys() {
        return new HashSet<String>(data.keySet());
    }

    /**
     * Tells whether or not the given key is already in use.
     * 
     * @param key
     *            the key under question
     * @return true, if the given key is already in use
     */
    public boolean existsKey(String key) {
        return data.keySet().contains(key);
    }

    /**
     * Returns the copy action specified for the given key (or the
     * DataStore's default copy action, if none has been specified).
     * 
     * @param key
     *            the key whose copy action is requested
     * @return the copy action that will be applied to this key's data
     */
    public int getCopyAction(String key) {
        if (copyActions.get(key) == null) {
            copyActions.put(key, Integer.valueOf(defaultCopyAction));
        }
        return (copyActions.get(key)).intValue();
    }

    /**
     * Specifies a copy action for a key.
     * 
     * @param key
     *            the key for which a copy action is to be specified
     * @param action
     *            the copy action to be applied to this key's data
     */
    public void setCopyAction(String key, int action) {
        copyActions.put(key, Integer.valueOf(action));
    }

    /**
     * Returns this DataStore's default copy action.
     * 
     * @return the default copy action
     */
    public int getDefaultCopyAction() {
        return defaultCopyAction;
    }

    /**
     * Sets this DataStore's default copy action
     * 
     * @param action
     *            the default copy action
     */
    public void setDefaultCopyAction(int action) {
        defaultCopyAction = action;
    }

    /**
     * Removes the data item with the given key from the DataStore.
     * 
     * @param key
     *            the key of the data item to be removed
     */
    public void remove(String key) {
        data.remove(key);
    }

    /**
     * Clears the data item with the given key. The meaning of this
     * differs depending on the referenced data type:
     * <ul>
     * <li>Numerical data will be set to 0. <li>Boolean data will be set
     * to <code>false</code>. <li>String data will be set to the empty
     * String "". <li>Collections and Maps will be emptied.
     * </ul>
     * 
     * @param key
     */
    public void clear(String key) {
        Object o = get(key);
        if (o instanceof Boolean) {
            set(key, false);
        }
        else if (o instanceof Character) {
            set(key, Character.valueOf((char) 0));
        }
        else if (o instanceof Byte) {
            set(key, Byte.valueOf((byte) 0));
        }
        else if (o instanceof Short) {
            set(key, Short.valueOf((short) 0));
        }
        else if (o instanceof Integer) {
            set(key, Integer.valueOf(0));
        }
        else if (o instanceof Long) {
            set(key, Long.valueOf(0));
        }
        else if (o instanceof Float) {
            set(key, Float.valueOf(0));
        }
        else if (o instanceof Double) {
            set(key, Double.valueOf(0));
        }
        else if (o instanceof String) {
            set(key, "");
        }
        else if (o instanceof Collection) {
            ((Collection<?>) o).clear();
        }
        else if (o instanceof Map) {
            ((Map<?, ?>) o).clear();
        }
    }

    // --- Singlular values

    /**
     * Stores the given Object under the given key.
     * 
     * @param key
     *            the key under which to store the given data
     * @param o
     *            the data to be stored under the given key
     */
    public void set(String key, Object o) {
        data.put(key, o);
    }

    /**
     * Retrieves the data Object stored under the given key.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key
     */
    public Object get(String key) {
        Object o = null;
        if (data.containsKey(key)) {
            o = data.get(key);
        }
        return o;
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a String using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a String
     */
    public String getString(String key) {
        return DataTypeConverter.toString(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a boolean value using the corresponding methods provided by
     * the {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a boolean value
     */
    public boolean getBoolean(String key) {
        return DataTypeConverter.toBoolean(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a char value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a char value
     */
    public char getChar(String key) {
        return DataTypeConverter.toChar(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a byte value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a byte value
     */
    public byte getByte(String key) {
        return DataTypeConverter.toByte(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a short value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a short value
     */
    public short getShort(String key) {
        return DataTypeConverter.toShort(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to an int value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a int value
     */
    public int getInt(String key) {
        return DataTypeConverter.toInt(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a long value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a long value
     */
    public long getLong(String key) {
        return DataTypeConverter.toLong(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a float value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a float value
     */
    public float getFloat(String key) {
        return DataTypeConverter.toFloat(data.get(key));
    }

    /**
     * Retrieves the data Object stored under the given key, converted
     * to a double value using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the data to be read
     * @return the data Object stored under the given key, converted to
     *         a double value
     */
    public double getDouble(String key) {
        return DataTypeConverter.toDouble(data.get(key));
    }

    // --- Bit operations

    /**
     * Sets within the long value data item specified by the given key
     * those bits which are also set in the given long value bit mask.
     * 
     * @param key
     *            the key of the long value data item to be manipulated
     * @param val
     *            a bit mask indicating which bits are to be set within
     *            the referenced data item
     */
    public void setBits(String key, long val) {
        set(key, getLong(key) | val);
    }

    /**
     * Clears within the long value data item specified by the given key
     * those bits which are set in the given long value bit mask.
     * 
     * @param key
     *            the key of the long value data item to be manipulated
     * @param val
     *            a bit mask indicating which bits are to be cleared
     *            within the referenced data item
     */
    public void clearBits(String key, long val) {
        set(key, getLong(key) & ~val);
    }

    /**
     * Flips within the long value data item specified by the given key
     * those bits which are set in the given long value bit mask.
     * 
     * @param key
     *            the key of the long value data item to be manipulated
     * @param val
     *            a bit mask indicating which bits are to be flipped
     *            within the referenced data item
     */
    public void flipBits(String key, long val) {
        long bits = getLong(key);
        set(key, (bits & ~val) | (~bits & val));
    }

    /**
     * Returns a long value having those bits set which are also set in
     * both the given long value bit mask and the long value data item
     * specified by the given key.
     * 
     * @param key
     *            the key of the long value data item to be tested
     * @param val
     *            a bit mask indicating which bits are to be tested
     *            within the referenced data item
     * @return a long value with all bits set which are also set in both
     *         the supplied bit mask and the data item
     */
    public long testBits(String key, long val) {
        return getLong(key) & val;
    }

    /**
     * Tells whether or not <i>all</i> bits set in the given long value
     * bit mask are also set in the long value data item specified by
     * the given key
     * 
     * @param key
     *            the key of the long value data item to be tested
     * @param val
     *            a bit mask indicating which bits are to be tested
     *            within the referenced data item
     * @return true, if all bits set which are set in the supplied bit
     *         mask are also set within the data item
     */
    public boolean allSet(String key, long val) {
        return testBits(key, val) == val;
    }

    /**
     * Tells whether or not <i>all</i> bits set in the given long value
     * bit mask are cleared in the long value data item specified by the
     * given key
     * 
     * @param key
     *            the key of the long value data item to be tested
     * @param val
     *            a bit mask indicating which bits are to be tested
     *            within the referenced data item
     * @return true, if all bits set which are set in the supplied bit
     *         mask are cleared within the data item
     */
    public boolean noneSet(String key, long val) {
        return testBits(key, val) == 0;
    }

    // --- Sets ---

    /**
     * Retrieves from this DataStore the Set stored under the given key,
     * or creates a new set if the key is still unused. Throws a
     * DataType exception if an Object found to be already stored under
     * the given key isn't a Set instance.
     * 
     * @param key
     *            the key of the Set to be retrieved
     */
    public Set getSet(String key) {
        Set<?> s = null;
        Object o = get(key);
        if (o == null) {
            o = new HashSet<Object>();
            set(key, o);
        }
        if (o instanceof Set) {
            s = (Set<?>) o;
        }
        else {
            throw new DataStoreException(key + " does not referr to Set data.", this);
        }
        return s;
    }

    /**
     * Adds all elements of the given Collection to the Set stored under
     * the given key.
     * 
     * @param key
     *            the key of the Set the elements are to be added to
     * @param elems
     *            the Collection of elements to be added to the Set
     */
    public void addElems(String key, Collection<?> elems) {
        getSet(key).addAll(elems);
    }

    /**
     * Replaces the elements currently belonging to the Set stored under
     * the given key by the elements of the given Collection.
     * 
     * @param key
     *            the key of the Set the elements of which are to be
     *            replaced
     * @param elems
     *            the Collection of elements to replace the Set's
     *            current elements
     */
    public void replaceElemsWith(String key, Collection<?> elems) {
        Set s = getSet(key);
        s.clear();
        s.addAll(elems);
    }

    /**
     * Adds the given Object to the Set stored under the given key.
     * 
     * @param key
     *            the key of the Set to add the Object to
     * @param val
     *            the Object to be added to the Set
     */
    public void addElem(String key, Object val) {
        getSet(key).add(val);
    }

    /**
     * Tells whether or not the given Object is a member of the Set
     * stored under the given key.
     * 
     * @param key
     *            the key of the Set under question
     * @param val
     *            the Object whose Set membership is to be checked
     * @return true, if the given Object is a member of the specified
     *         Set
     */
    public boolean containsElem(String key, Object val) {
        return getSet(key).contains(val);
    }

    /**
     * Removes the given Object from the Set stored under the given key
     * and returns it.
     * 
     * @param key
     *            the key of the Set from which to remove the Object
     * @param val
     *            the Object to remove from the Set
     * @return the removed Object (or null, if the Object hasn't
     *         previously been a member of the Set)
     */
    public Object removeElem(String key, Object val) {
        return getSet(key).remove(val);
    }

    // --- Lists ---

    /**
     * Retrieves from this DataStore the Set stored under the given key,
     * or creates a new set if the key is still unused. Throws a
     * DataType exception if an Object found to be already stored under
     * the given key isn't a Set instance.
     * 
     * @param key
     *            the key of the Set to be retrieved
     */
    public List getList(String key) {
        List<?> l = null;
        Object o = get(key);
        if (o == null) {
            o = new ArrayList<Object>();
            set(key, o);
        }
        if (o instanceof List) {
            l = (List<?>) o;
        }
        else {
            throw new DataStoreException(key + " does not referr to List data.", this);
        }
        return l;
    }

    /**
     * Adds all elements of the given Collection to the List stored
     * under the given key.
     * 
     * @param key
     *            the key of the List the elements are to be added to
     * @param elems
     *            the Collection of elements to be added to the List
     */
    public void addItems(String key, Collection<?> elems) {
        getList(key).addAll(elems);
    }

    /**
     * Replaces the items currently stored in the List referenced by the
     * given key by the elements of the given Collection.
     * 
     * @param key
     *            the key of the List the items of which are to be
     *            replaced
     * @param elems
     *            the Collection of elements to replace the Lists's
     *            current items
     */
    public void replaceItemsWith(String key, Collection<?> elems) {
        List l = getList(key);
        l.clear();
        l.addAll(elems);
    }

    /**
     * Adds the given Object to the List specified by the given key.
     * 
     * @param key
     *            the key of the List to add the Object to
     * @param val
     *            the Object to be added to the List
     */
    public void addItem(String key, Object val) {
        getList(key).add(val);
    }

    /**
     * Tells whether or not the given Object is among the items stored
     * within the List specified by the given key.
     * 
     * @param key
     *            the key of the List under question
     * @param val
     *            the Object whose List membership is to be checked
     * @return true, if the given Object is among the items stored
     *         within the specified List
     */
    public boolean containsItem(String key, Object val) {
        return getList(key).contains(val);
    }

    /**
     * Removes the given Object from the List specified by the given key
     * and returns it.
     * 
     * @param key
     *            the key of the List from which to remove the Object
     * @param val
     *            the Object to remove from the List
     * @return the removed Object (or null, if the Object hasn't
     *         previously been among the List's items)
     */
    public Object removeItem(String key, Object val) {
        return getList(key).remove(val);
    }

    /**
     * Stores the given Object at the given position within the List
     * specified by the given key. If the List currently has too few
     * items so that the given index is out of bounds, an appropriate
     * number of null items is added.
     * 
     * @param key
     *            the key of the List to which to add the data
     * @param i
     *            the position within the List at which to store the
     *            data
     * @param val
     *            the data to be stored at the given position
     */
    public void set(String key, int i, Object val) {
        List l = getList(key);
        for (int j = l.size(); j <= i; j++) {
            l.add(null);
        }
        l.set(i, val);
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List (or
     *         null, if the index is out of bounds)
     */
    public Object get(String key, int i) {
        try {
            return getList(key).get(i);
        }
        catch (IndexOutOfBoundsException ex) {
            return null;
        }
    }

    /**
     * Removes the Object stored at the given index within the List
     * specified by the given key.
     * 
     * @param key
     *            the key of the List from which to remove an item
     * @param i
     *            the index of the item to be removed
     * @return the removed item (or null, if the index is out of bounds)
     */
    public Object remove(String key, int i) {
        try {
            return getList(key).remove(i);
        }
        catch (IndexOutOfBoundsException ex) {
            return null;
        }
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to an String using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a String (or an empty String, if the index
     *         is out of bounds)
     */
    public String getString(String key, int i) {
        return DataTypeConverter.toString(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a char value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a char value (or a null character, if the
     *         index is out of bounds)
     */
    public char getChar(String key, int i) {
        return DataTypeConverter.toChar(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a boolean value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a boolean value (or false, if the index is
     *         out of bounds)
     */
    public boolean getBoolean(String key, int i) {
        return DataTypeConverter.toBoolean(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a byte value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a byte value (or 0, if the index is out of
     *         bounds)
     */
    public byte getByte(String key, int i) {
        return DataTypeConverter.toByte(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a short value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a short value (or 0, if the index is out of
     *         bounds)
     */
    public short getShort(String key, int i) {
        return DataTypeConverter.toShort(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to an int value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to an int value (or 0, if the index is out of
     *         bounds)
     */
    public int getInt(String key, int i) {
        return DataTypeConverter.toInt(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a long value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a long value (or 0, if the index is out of
     *         bounds)
     */
    public long getLong(String key, int i) {
        return DataTypeConverter.toLong(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a float value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a float value (or 0, if the index is out of
     *         bounds)
     */
    public float getFloat(String key, int i) {
        return DataTypeConverter.toFloat(get(key, i));
    }

    /**
     * Returns the Object stored in the List specified by the given key
     * at the given index, converted to a double value using the
     * corresponding methods provided by the {@link DataTypeConverter}
     * class.
     * 
     * @param key
     *            the key of the List from which to retrieve an item
     * @param i
     *            the index of the List item to be retrieved
     * @return the item stored at the given index within the List,
     *         converted to a double value (or 0, if the index is out of
     *         bounds)
     */
    public double getDouble(String key, int i) {
        return DataTypeConverter.toDouble(get(key, i));
    }

    // --- Maps ---

    /**
     * Retrieves from this DataStore the Map stored under the given key,
     * or creates a new Map if the key is still unused. Throws a
     * DataType exception if an Object found to be already stored under
     * the given key isn't a Map instance.
     * 
     * @param key
     *            the key of the Map to be retrieved
     */
    public Map getMap(String key) {
        Map<?, ?> m = null;
        Object o = get(key);
        if (o == null) {
            o = new LinkedHashMap<Object, Object>();
            set(key, o);
        }
        if (o instanceof Map) {
            m = (Map<?, ?>) o;
        }
        else {
            throw new DataStoreException(key + " does not referr to Map data.", this);
        }
        return m;
    }

    /**
     * Replaces the entries currently stored in the Map referenced by
     * the given key by the elements of the given Map.
     * 
     * @param key
     *            the key of the Map the entries of which are to be
     *            replaced
     * @param entries
     *            the Map the entries of which are to replace the Map's
     *            current entries
     */
    public void replaceEntries(String key, Map<?, ?> entries) {
        Map m = getMap(key);
        m.clear();
        for (Object mapKey : entries.keySet()) {
            m.put(mapKey, entries.get(mapKey));
        }
    }

    /**
     * Tells whether or not the given Object is a member of the key set
     * of the Map specified by the given key.
     * 
     * @param key
     *            the key of the Map under question
     * @param mapKey
     *            the Object whose membership in the Map's key set is to
     *            be checked
     * @return true, if the given Object is among the items stored
     *         within the specified Map's key set
     */
    public boolean containsKey(String key, Object mapKey) {
        return getMap(key).containsKey(mapKey);
    }

    /**
     * Tells whether or not the given Object is among the items stored
     * within the value Collestion of the Map specified by the given
     * key.
     * 
     * @param key
     *            the key of the Map under question
     * @param val
     *            the Object of which to check whether it is among the
     *            items stored within in the Map's value collection
     * @return true, if the given Object is among the items stored
     *         within the Map's value collection
     */
    public boolean containsValue(String key, Object val) {
        return getMap(key).containsValue(val);
    }

    /**
     * Removes the Entry with the given map key from the Map specified
     * by the given DataStore key
     * 
     * @param key
     *            the key of the Map from which to remove an entry
     * @param mapKey
     *            the key of the enry to be removed from the Map
     * @return the removed entries's value (or null, if no entry with
     *         the given map key has previously existed within Map)
     */
    public Object removeEntry(String key, Object mapKey) {
        return getMap(key).remove(mapKey);
    }

    /**
     * Stores the given Object under the given map key within the Map
     * specified by the given DataStore key.
     * 
     * @param key
     *            the key of the Map in which to store an entry
     * @param mapKey
     *            the key under which to store the given Object within
     *            the Map
     * @param val
     *            the Object to be stored under the given map key
     */
    public void put(String key, Object mapKey, Object val) {
        getMap(key).put(mapKey, val);
    }

    /**
     * Returns the Object stored under the given map key within the Map
     * specified by the given DataStore key.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key (or null, if
     *         nothing has been previously been stored within the Map
     *         under the given map key)
     */
    public Object get(String key, Object mapKey) {
        return getMap(key).get(mapKey);
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a String using
     * the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         String (or an empty String, if nothing has been
     *         previously been stored within the Map under the given map
     *         key)
     */
    public String getString(String key, Object mapKey) {
        return DataTypeConverter.toString(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a char value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         char value (or a null character, if nothing has been
     *         previously been stored within the Map under the given map
     *         key)
     */
    public char getChar(String key, Object mapKey) {
        return DataTypeConverter.toChar(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a byte value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         byte value (or 0, if nothing has been previously been
     *         stored within the Map under the given map key)
     */
    public byte getByte(String key, Object mapKey) {
        return DataTypeConverter.toByte(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a short value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         short value (or 0, if nothing has been previously been
     *         stored within the Map under the given map key)
     */
    public short getShort(String key, Object mapKey) {
        return DataTypeConverter.toShort(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to an int value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to
     *         an int value (or 0, if nothing has been previously been
     *         stored within the Map under the given map key)
     */
    public int getInt(String key, Object mapKey) {
        return DataTypeConverter.toInt(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a long value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         long value (or 0, if nothing has been previously been
     *         stored within the Map under the given map key)
     */
    public long getLong(String key, Object mapKey) {
        return DataTypeConverter.toLong(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a float value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         float value (or 0, if nothing has been previously been
     *         stored within the Map under the given map key)
     */
    public float getFloat(String key, Object mapKey) {
        return DataTypeConverter.toFloat(getMap(key).get(mapKey));
    }

    /**
     * Returns the data stored under the given map key within the Map
     * specified by the given DataStore key, converted to a double value
     * using the corresponding methods provided by the
     * {@link DataTypeConverter} class.
     * 
     * @param key
     *            the key of the Map from which to retrieve an entry
     * @param mapKey
     *            the key of the Object to be retrieved from the Map
     * @return the Object stored under the given map key, converted to a
     *         double value (or 0, if nothing has been previously been
     *         stored within the Map under the given map key)
     */
    public double getDouble(String key, Object mapKey) {
        return DataTypeConverter.toDouble(getMap(key).get(mapKey));
    }

    // --- Properties ---

    /**
     * Retrieves from this DataStore the Properties map stored under the
     * given key, or creates a new Properties instance if the key is
     * still unused. Throws a DataType exception if an Object found to
     * be already stored under the given key isn't a Properties
     * instance.
     * 
     * @param key
     *            the key of the Properties instance to be retrieved
     */
    public Properties getProperties(String key) {
        Properties p = null;
        Object o = get(key);
        if (o == null) {
            o = new Properties();
            set(key, o);
        }
        if (o instanceof Properties) {
            p = (Properties) o;
        }
        else {
            throw new DataStoreException(key + " does not referr to Properties data.", this);
        }
        return p;
    }

    /**
     * Replaces the entries currently stored in the Properties instance
     * referenced by the given key by the elements of the given
     * Properties instance.
     * 
     * @param key
     *            the key of the Properties instance the entries of
     *            which are to be replaced
     * @param entries
     *            the Properties instance the entries of which are to
     *            replace the Properties instance's current entries
     */
    public void replaceProperties(String key, Properties entries) {
        Properties p = getProperties(key);
        p.clear();
        for (Object mapKey : entries.keySet()) {
            p.put(mapKey, entries.get(mapKey));
        }
    }

    /**
     * Tells whether or not there is a property named after the given
     * String stored within the Properties instance specified by the
     * given key.
     * 
     * @param key
     *            the key of the Properties instance under question
     * @param name
     *            the String whose membership within the set of property
     *            names is to be checked
     * @return true, if the given String is a member of the Properties'
     *         name set.
     */
    public boolean containsPropertyName(String key, String name) {
        return getProperties(key).containsKey(name);
    }

    /**
     * Tells whether or not the given String is among the values stored
     * within the value Collestion of the Properties instance specified
     * by the given key.
     * 
     * @param key
     *            the key of the Properties instance under question
     * @param val
     *            the String of which to check whether it is among the
     *            items stored within in the Properties' value
     *            collection
     * @return true, if the given String is among the items stored
     *         within the Properties' value collection
     */
    public boolean containsPropertyValue(String key, String val) {
        return getProperties(key).containsValue(val);
    }

    /**
     * Removes the entry with the given name from the Properties
     * instance specified by the given DataStore key
     * 
     * @param key
     *            the key of the Properties instance from which to
     *            remove an entry
     * @param name
     *            the name of the enry to be removed from the Properties
     *            instance
     * @return the removed entries's value (or null, if no entry with
     *         the given name has previously existed within Properties
     *         instance)
     */
    public String removeProperty(String key, String name) {
        return (String) getProperties(key).remove(name);
    }

    /**
     * Returns from the Properties instance referenced by the given key
     * the property value specified by the given name.
     * 
     * @param key
     *            the key of the Properies instance to retrieve a value
     *            from
     * @param name
     *            the name of the Properties entry to retrieve
     * @return the value of the named properties entry (or null, if no
     *         such entry exists)
     */
    public String getProperty(String key, String name) {
        return getProperties(key).getProperty(name);
    }

    /**
     * Stores the given String under the given name within the
     * Properties instance referenced by the given key.
     * 
     * @param key
     *            the key of the Properties instance to store a value in
     * @param name
     *            the name under which to store the given value within
     *            the referenced Properties instance
     * @param val
     *            the value to be stored
     */
    public void setProperty(String key, String name, String val) {
        getProperties(key).setProperty(name, val);
    }

    // --- Cloning stuff ---

    /**
     * Creates a clone of this DataStore instance, thereby appropriately
     * applying the necessary copy actions to all stored data.
     * 
     * @return the new clone
     */
    @Override
    public Object clone() {
        DataStore c = new DataStore();
        try {
            for (String key : data.keySet()) {
                Object val = data.get(key);
                int action = getCopyAction(key);
                c.data.put(key, cloneDataStoreEntry(val, action, 0));
            }
        }
        catch (Exception e) {
            SysUtils.reportException(e);
        }
        return c;
    }

    /**
     * Returns a clone of this DataStore instance.
     * 
     * @return the new clone
     */
    public DataStore createClone() {
        return (DataStore) clone();
    }

    // --- XML stuff ---

    /**
     * Returns a Map containing a data type description for each stored
     * data item.
     * 
     * @return the mapping of data item keys towards their corresponding
     *         data types
     */
    public Map<String, String> getKeyTypeMapping() {
        Map<String, String> mapping = new HashMap<String, String>();
        for (String key : data.keySet()) {
            Object val = data.get(key);
            mapping.put(key, getTypeStr(val));
        }
        return mapping;
    }

    /**
     * Serializes this DataStore by attaching sub-elements to the given
     * XML element which describe all data items selected for
     * serialization. Selection can be performed either by supplying a
     * white list (only the data items whose keys are on the white list
     * will be serialized) or a black list (data items whose keys are on
     * the black list are excluded from serialization; blacklisting
     * overrides whitelisting); if a null value is supplied for both
     * lists, all data items will be serialized.
     * 
     * @param e
     *            the XML element to which to attach the serialized data
     * @param whiteList
     *            a list of keys of data items to be serialized; all
     *            data items are selected if a null value is supplied
     * @param blackList
     *            a list of keys of data items to be excluded form
     *            serialization (<i>after</i> application of a possible
     *            white list); no data items are excluded if a null
     *            value is supplied
     * @param suppressRootTypes
     *            whether or not to suppres the inclusion of the
     *            serialized data items' type description as XML
     *            attributes; required e.g. for GraphML serialization
     *            where the data items' type descriptions are stored
     *            elsewhere
     */
    public void toXML(Element e, Set<String> whiteList, Set<String> blackList,
                      boolean suppressRootTypes) {
        for (String key : data.keySet()) {
            Object val = data.get(key);
            if ((whiteList == null) || whiteList.contains(key)) {
                if ((blackList == null) || !blackList.contains(key)) {
                    Element dataElement = dataToXML(e.getOwnerDocument(), DEFAULT_TAG_NAME,
                        key, (suppressRootTypes ? null : getTypeStr(val)), val);
                    e.appendChild(dataElement);
                }
            }
        }
    }

    /**
     * Serializes this DataStore by attaching sub-elements to the given
     * XML element which describe all data items selected for
     * serialization. Selection can be performed either by supplying a
     * white list (only the data items whose keys are on the white list
     * will be serialized) or a black list (data items whose keys are on
     * the black list are excluded from serialization; blacklisting
     * overrides whitelisting); if a null value is supplied for both
     * lists, all data items will be serialized.
     * 
     * @param e
     *            the XML element to which to attach the serialized data
     * @param whiteList
     *            a list of keys of data items to be serialized; all
     *            data items are selected if a null value is supplied
     * @param blackList
     *            a list of keys of data items to be excluded form
     *            serialization (<i>after</i> application of a possible
     *            white list); no data items are excluded if a null
     *            value is supplied
     */
    public void toXML(Element e, Set<String> whiteList, Set<String> blackList) {
        toXML(e, whiteList, blackList, false);
    }

    /**
     * Serializes this DataStore by attaching sub-elements to the given
     * XML element describing the data items.
     * 
     * @param e
     *            the XML element to which to attach the serialized data
     */
    public void toXML(Element e) {
        toXML(e, null, null, false);
    }

    /**
     * Creates a new DataStore according to the information found in the
     * given XML element, using the given Map as a description of which
     * data key represents data of which type.
     * 
     * @param e
     *            the XML element to create a DataStore from
     * @param keyTypeMapping
     *            the mapping of data keys to data types
     * @return the newly created DataStore instance
     */
    public static DataStore fromXML(Element e, Map<String, String> keyTypeMapping) {
        DataStore ds = new DataStore();
        for (Element dataElement : XMLUtils.getChildrenByName(e, DEFAULT_TAG_NAME)) {
            String key = dataElement.getAttribute("key");
            Object o = dataFromXML(dataElement, getTypeString(dataElement, keyTypeMapping));
            ds.data.put(key, o);
        }
        return ds;
    }

    /**
     * Creates a new DataStore according to the information found in the
     * given XML element.
     * 
     * @param e
     *            the XML element to create a DataStore from
     * @return the newly created DataStore instance
     */
    public static DataStore fromXML(Element e) {
        return fromXML(e, null);
    }

}

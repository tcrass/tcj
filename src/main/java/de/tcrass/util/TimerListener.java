package de.tcrass.util;

public interface TimerListener {

    void timerAlarm(TimerEvent t);

}

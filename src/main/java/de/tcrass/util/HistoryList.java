package de.tcrass.util;

import java.util.ArrayList;

public class HistoryList<E> extends ArrayList<E> {

    private static final long serialVersionUID = 1L;
    private int               current          = -1;

    public void addItem(E o) {
        if (!o.equals(getCurrentItem())) {
            for (int i = size() - 1; i > current; i--) {
                remove(i);
            }
            add(o);
            current = size() - 1;
        }
    }

    public E getCurrentItem() {
        E item = null;
        if (current >= 0) {
            item = get(current);
        }
        return item;
    }

    public boolean hasPrevious() {
        return (current > 0);
    }

    public boolean hasNext() {
        return (current < (size() - 1));
    }

    public E getNextItem() {
        E o = null;
        if (hasNext()) {
            current++;
            o = get(current);
        }
        return o;
    }

    public E getPreviousItem() {
        E o = null;
        if (hasPrevious()) {
            current--;
            o = get(current);
        }
        return o;
    }

    public void removeItems(E o) {
        for (int i = size() - 1; i >= 0; i--) {
            if (get(i) == o) {
                remove(i);
                if (i <= current) {
                    current--;
                }
            }
        }
    }

}

package de.tcrass.util;

import java.util.HashMap;
import java.util.Map;

public class MapOfMaps<K, L, V> extends HashMap<K, Map<L, V>> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Map<L, V> getMap(K key) {
        return get(key);
    }

    public V get(K key1, L key2) {
        V result = null;
        Map<L, V> m = getMap(key1);
        if (m != null) {
            result = m.get(key2);
        }
        return result;
    }

}

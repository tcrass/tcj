package de.tcrass.util;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Clonator {

    // --- Private methods ---

    private static void equalizePrimitiveAttributesTo(Class<?> c, Object o, Object template) {
        try {
            Field[] fields = c.getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);
            for (Field f : fields) {
                Class<?> type = f.getType();
                if (type.isPrimitive() && !Modifier.isStatic(f.getModifiers()) &&
                    !Modifier.isFinal(f.getModifiers()) &&
                    !Modifier.isAbstract(f.getModifiers())) {
                    f.set(o, f.get(template));
                }
            }
        }
        catch (IllegalAccessException e) {
            SysUtils.reportException(e);
        }
        Class<?> superclass = c.getSuperclass();
        if (superclass != null) {
            equalizePrimitiveAttributesTo(superclass, o, template);
        }
    }

    private static void equalizeObjectAttributesTo(Class<?> c, Object o, Object template) {
        try {
            Field[] fields = c.getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);
            for (Field f : fields) {
                Class<?> type = f.getType();
                if (!type.isPrimitive() && !Modifier.isStatic(f.getModifiers()) &&
                    !Modifier.isFinal(f.getModifiers()) &&
                    !Modifier.isAbstract(f.getModifiers())) {
                    f.set(o, f.get(template));
                }
            }
        }
        catch (IllegalAccessException e) {
            SysUtils.reportException(e);
        }
        Class<?> superclass = c.getSuperclass();
        if (superclass != null) {
            equalizeObjectAttributesTo(superclass, o, template);
        }
    }

    // --- Public methods ---

    public static Object clone(Object o) throws InvocationTargetException,
    IllegalAccessException {
        Object c = null;
        if (o != null) {
            if (o instanceof Class) {
                c = o;
            }
            else {
                Method[] methods = o.getClass().getMethods();
                for (Method m : methods) {
                    if ((m.getParameterTypes().length == 0) && m.getName().equals("clone")) {
                        c = m.invoke(o, new Object[] {});
                        break;
                    }
                }
            }
        }
        return c;
    }

    public static Object createPrimitiveClone(Object o) {
        Class<?> c = o.getClass();
        Object clone = null;
        try {
            clone = c.newInstance();
            equalizePrimitiveAttributesTo(clone, o);
        }
        catch (IllegalAccessException e) {
            SysUtils.reportException(e);
        }
        catch (InstantiationException e) {
            SysUtils.reportException(e);
        }
        return clone;
    }

    public static void equalizePrimitiveAttributesTo(Object o, Object template) {
        Class<?> c = o.getClass();
        if (c.isInstance(template)) {
            equalizePrimitiveAttributesTo(c, o, template);
        }
    }

    public static Object createShallowClone(Object o) {
        Class<?> c = o.getClass();
        Object clone = null;
        try {
            clone = c.newInstance();
            equalizePrimitiveAttributesTo(clone, o);
            equalizeObjectAttributesTo(clone, o);
        }
        catch (IllegalAccessException e) {
            SysUtils.reportException(e);
        }
        catch (InstantiationException e) {
            SysUtils.reportException(e);
        }
        return clone;
    }

    public static void equalizeObjectAttributesTo(Object o, Object template) {
        Class<?> c = o.getClass();
        if (c.isInstance(template)) {
            equalizeObjectAttributesTo(c, o, template);
        }
    }

}

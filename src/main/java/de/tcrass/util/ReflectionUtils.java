package de.tcrass.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import de.tcrass.io.FileUtils;

public class ReflectionUtils {

    // --- Private methods

    private static void addClassesInDirectory(List<Class<?>> classes, File dir,
            File root) {
        if (dir.isDirectory()) {
            try {
                String prefix = FileUtils.createRelativeFile(root, dir)
                        .getPath();
                prefix = StringUtils.replaceAll(prefix, File.separator, ".");
                if (!prefix.equals("")) {
                    prefix = prefix + ".";
                }

                File[] files = dir.listFiles();
                for (File f : files) {
                    if (f.isDirectory()) {
                        addClassesInDirectory(classes, f, root);
                    } else if (f.getName().toLowerCase().endsWith(".class")) {
                        String className = prefix
                                + f.getName().substring(0,
                                        f.getName().length() - 6);
                        Class<?> c = Class.forName(className);
                        classes.add(c);
                    }
                }
            } catch (IOException e) {
                SysUtils.reportException(e);
            } catch (ClassNotFoundException e) {
                SysUtils.reportException(e);
            }
        }
    }

    // --- Public methods

    public static boolean implementsInterface(Class<?> clazz,
            String interfaceName) {
        boolean impl = false;
        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> element : interfaces) {
            if (element.getName().equals(interfaceName)) {
                impl = true;
                break;
            }
        }
        if (!impl) {
            Class<?> superclass = clazz.getSuperclass();
            if (superclass != null) {
                impl = implementsInterface(superclass, interfaceName);
            }
        }
        return impl;
    }

    public static boolean isSubclassOf(Class<?> clazz, Class<?> superclazz) {
        boolean sub = false;
        Class<?> directSuperclass = clazz.getSuperclass();
        if (directSuperclass != null) {
            if (directSuperclass.equals(superclazz)) {
                sub = true;
            } else {
                sub = isSubclassOf(directSuperclass, superclazz);
            }
        }
        return sub;
    }

    public static ArrayList<Class<?>> getClassesInPackage(String jarfile,
            String packageName) {
        ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
        try {
            String path = new String(packageName);
            List<String> resources = FileUtils.getResourceNames(jarfile, path);
            for (String resource : resources) {
                if (resource.toLowerCase().endsWith(".class")) {
                    String className = path + "."
                            + resource.substring(0, resource.length() - 6);
                    Class<?> c = Class.forName(className);
                    classes.add(c);
                }
            }
        } catch (IOException e) {
            SysUtils.reportException(e);
        } catch (ClassNotFoundException e) {
            SysUtils.reportException(e);
        }
        return classes;
    }

    public static List<Class<?>> getClassesInDirectory(File dir) {
        ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
        addClassesInDirectory(classes, dir, dir);
        return classes;
    }

    public static Object getFieldValue(Object o, String fieldName) {
        Object value = null;
        try {
            Field f = o.getClass().getField(fieldName);
            value = f.get(o);
        } catch (NoSuchFieldException e) {
            SysUtils.reportException(e);
        } catch (IllegalAccessException e) {
            SysUtils.reportException(e);
        }
        return value;
    }

    public static Class getDeclaringClass(Class<?> clazz, String name,
            Class<?>[] sign) {
        Class decl = null;
        try {
            clazz.getDeclaredMethod(name, sign);
            decl = clazz;
        } catch (NoSuchMethodException ex) {
        }
        if (decl == null) {
            if (clazz.getSuperclass() != null) {
                decl = getDeclaringClass(clazz.getSuperclass(), name, sign);
            }
        }
        if (decl == null) {
            for (Class<?> interf : clazz.getInterfaces()) {
                decl = getDeclaringClass(interf, name, sign);
                if (decl != null) {
                    break;
                }
            }
        }
        return decl;
    }

    public static Object callMethod(Object o, String name, Class<?>[] sign, Object[] args)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Object val = null;
        Class<?> decl = getDeclaringClass(o.getClass(), name, sign);
        if (decl != null) {
            Method m = decl.getDeclaredMethod(name, sign);
            AccessibleObject.setAccessible(new AccessibleObject[] { m }, true);
            val = m.invoke(o, args);
        }
        else {
            throw new NoSuchMethodException(o.getClass().getCanonicalName() + "." + name + CollectionUtils.arrayToString(sign));
        }
        return val;
    }

}

package de.tcrass.util;

public class DataStoreException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private DataStore         ds;

    public DataStoreException(String msg, DataStore ds) {
        super(msg);
        this.ds = ds;
    }

    public DataStore getDataStore() {
        return ds;
    }

}

package de.tcrass.util;

import java.util.ArrayList;
import java.util.List;

public class Trie<E> {

    static class TrieNode<E> {

        protected TrieNode<E>               parent;
        protected Character                 c;
        protected PositionList<TrieNode<E>> children   = new PositionList<TrieNode<E>>();
        protected boolean                   isTerminal = true;
        protected int                       level      = 0;
        protected E                         data;

        protected TrieNode(TrieNode<E> parent, Character c) {
            this.parent = parent;
            this.c = c;
        }

        protected String getString() {
            if (parent == null) {
                return "";
            }
            else {
                return parent.getString() + c;
            }
        }

    }

    TrieNode<E> root;

    public Trie() {
        root = new TrieNode<E>(null, null);
        root.level = 0;
    }

    protected TrieNode<E> findChild(TrieNode<E> n, char c) {
        TrieNode<E> child = null;
        for (TrieNode<E> m : n.children.getElements()) {
            if (Character.valueOf(c).equals(m.c)) {
                child = m;
                break;
            }
        }
        return child;
    }

    protected TrieNode<E> assertNodeForKey(TrieNode<E> n, String s, int level) {
        if ("".equals(s)) {
            n.isTerminal = true;
            return n;
        }
        else {
            char c = s.charAt(0);
            TrieNode<E> child = findChild(n, c);
            if (child == null) {
                child = new TrieNode<E>(n, c);
                child.level = level + 1;
                n.children.add(child, c);
            }
            return assertNodeForKey(child, s.substring(1), level + 1);
        }
    }

    protected void add(String s) {
        assertNodeForKey(root, s, 0);
    }

    public void put(String s, E data) {
        TrieNode<E> n = assertNodeForKey(root, s, 0);
        n.data = data;
    }

    protected void doGetKeys(TrieNode<E> n, List<String> result) {
        for (TrieNode<E> child : n.children.getElements()) {
            if (child.isTerminal) {
                result.add(child.getString());
            }
            doGetKeys(child, result);
        }
    }

    public List<String> getKeys() {
        List<String> result = new ArrayList<String>();
        doGetKeys(root, result);
        return result;
    }

    // protected TrieNode<E> getNode(TrieNode<E> n, String s) {
    // if ("".equals(s)) {
    // return n;
    // }
    // else {
    // TrieNode<E> child = findChild(n, s.charAt(0));
    // if (child == null) {
    // return null;
    // }
    // else {
    // return getNode(child, s.substring(1));
    // }
    // }
    // }

    public E get(String key) {
        TrieNode<E> n = assertNodeForKey(root, key, 0);
        if (n == null) {
            return null;
        }
        else {
            return n.data;
        }
    }

}

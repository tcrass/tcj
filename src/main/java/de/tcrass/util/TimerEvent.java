package de.tcrass.util;

import java.util.TimerTask;

public class TimerEvent extends TimerTask {

    private Object        source;
    private TimerListener listener;
    private String        command;
    private Object        data = null;

    public TimerEvent(Object source, TimerListener listener, String command) {
        this.source = source;
        this.listener = listener;
        this.command = command;
    }

    public TimerEvent(Object source, TimerListener listener, String command, Object data) {
        this.source = source;
        this.listener = listener;
        this.command = command;
        this.data = data;
    }

    @Override
    public void run() {
        listener.timerAlarm(this);
    }

    public Object getSource() {
        return source;
    }

    public String getCommand() {
        return command;
    }

    public Object getData() {
        return data;
    }

}

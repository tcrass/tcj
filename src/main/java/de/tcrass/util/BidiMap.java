package de.tcrass.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BidiMap<L, R> {

    private HashMap<L, R> left;
    private HashMap<R, L> right;

    public BidiMap() {
        left = new HashMap<L, R>();
        right = new HashMap<R, L>();
    }

    public int size() {
        return left.size();
    }

    public void put(L l, R r) {
        if (left.containsKey(l)) {
            left.remove(l);
        }
        if (right.containsKey(r)) {
            right.remove(r);
        }
        left.put(l, r);
        right.put(r, l);
    }

    public R getForward(L l) {
        return left.get(l);
    }

    public L getBackward(R r) {
        return right.get(r);
    }

    public Iterator<L> getLeftIterator() {
        return left.keySet().iterator();
    }

    public Iterator<R> getRightIterator() {
        return right.keySet().iterator();
    }

    public boolean containsLeft(L l) {
        return left.keySet().contains(l);
    }

    public boolean containsRight(R r) {
        return right.keySet().contains(r);
    }

    public void removeLeft(L o) {
        if (left.containsKey(o)) {
            Object r = left.get(o);
            left.remove(o);
            right.remove(r);
        }
    }

    public void removeRight(R o) {
        if (right.containsKey(o)) {
            Object l = right.get(o);
            right.remove(o);
            left.remove(l);
        }
    }

    public Set<L> getLeftElements() {
        return new HashSet<L>(left.keySet());
    }

    public Set<R> getRightElements() {
        return new HashSet<R>(right.keySet());
    }

    public String toString() {
        StringBuffer s = new StringBuffer("BidiMap{");
        for (Iterator<L> i = left.keySet().iterator(); i.hasNext();) {
            L o = i.next();
            s.append(o.toString() + " <=> " + getForward(o).toString());
            if (i.hasNext()) {
                s.append(", ");
            }
        }
        s.append("}");
        return s.toString();
    }
    
}

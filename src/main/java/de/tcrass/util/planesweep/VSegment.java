/*
 * Created on 21.06.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.util.planesweep;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class VSegment extends PlaneSegment {

    private long x;

    public VSegment(long a, long b, long x, Object data) {
        super(a, b, data);
        this.x = x;
    }

    public long getXPos() {
        return x;
    }

    @Override
    public long getOrthoPos() {
        return getXPos();
    }

    @Override
    public long getX1() {
        return getOrthoPos();
    }

    @Override
    public long getY1() {
        return getStartPos();
    }

    @Override
    public long getX2() {
        return getOrthoPos();
    }

    @Override
    public long getY2() {
        return getEndPos();
    }

}

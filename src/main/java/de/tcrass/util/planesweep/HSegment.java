/*
 * Created on 21.06.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.util.planesweep;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class HSegment extends PlaneSegment {

    private long y;

    public HSegment(long a, long b, long y, Object data) {
        super(a, b, data);
        this.y = y;
    }

    public long getYPos() {
        return y;
    }

    @Override
    public long getOrthoPos() {
        return getYPos();
    }

    @Override
    public long getX1() {
        return getStartPos();
    }

    @Override
    public long getY1() {
        return getOrthoPos();
    }

    @Override
    public long getX2() {
        return getEndPos();
    }

    @Override
    public long getY2() {
        return getOrthoPos();
    }

}

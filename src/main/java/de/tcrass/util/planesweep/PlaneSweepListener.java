/*
 * Created on 21.06.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.util.planesweep;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public interface PlaneSweepListener {

    public void segmentStarted(PlaneSweepEvent e);

    public void segmentEnded(PlaneSweepEvent e);

    public void readout(PlaneSweepEvent e);

}

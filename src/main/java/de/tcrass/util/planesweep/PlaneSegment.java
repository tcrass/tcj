/*
 * Created on 21.06.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.util.planesweep;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public abstract class PlaneSegment {

    private long   startPos, endPos;
    private Object data;

    public PlaneSegment(long a, long b, Object data) {
        if (a > b) {
            startPos = b;
            endPos = a;
        }
        else {
            startPos = a;
            endPos = b;
        }
        this.data = data;
    }

    public long getStartPos() {
        return startPos;
    }

    public long getEndPos() {
        return endPos;
    }

    public Object getData() {
        return data;
    }

    public abstract long getOrthoPos();

    public abstract long getX1();

    public abstract long getY1();

    public abstract long getX2();

    public abstract long getY2();

    @Override
    public String toString() {
        return super.toString() + " [a = " + startPos + ", b = " + endPos + ", ortho = " +
               getOrthoPos() + "]";
    }

}

/*
 * Created on 21.06.2005 TODO To change the template for this generated
 * file go to Window - Preferences - Java - Code Style - Code Templates
 */
package de.tcrass.util.planesweep;

import java.util.ArrayList;

import de.tcrass.util.PositionList;

/**
 * @author tcrass TODO To change the template for this generated type
 *         comment go to Window - Preferences - Java - Code Style - Code
 *         Templates
 */
public class Plane {

    public static final int            LEFT_TO_RIGHT    = 1;
    public static final int            RIGHT_TO_LEFT    = 2;
    public static final int            TOP_TO_BOTTOM    = 4;
    public static final int            BOTTOM_TO_TOP    = 8;

    public static final int            HORIZONTAL_SWEEP = LEFT_TO_RIGHT | RIGHT_TO_LEFT;
    public static final int            VERTICAL_SWEEP   = TOP_TO_BOTTOM | BOTTOM_TO_TOP;

    private PositionList<PlaneSegment> leftPoints;
    private PositionList<PlaneSegment> rightPoints;
    private PositionList<PlaneSegment> topPoints;
    private PositionList<PlaneSegment> bottomPoints;

    private boolean                    cancelled;

    public Plane() {
        leftPoints = new PositionList<PlaneSegment>();
        rightPoints = new PositionList<PlaneSegment>();
        topPoints = new PositionList<PlaneSegment>();
        bottomPoints = new PositionList<PlaneSegment>();
    }

    public HSegment addHSegment(int a, int b, int y, Object o) {
        HSegment s = new HSegment(a, b, y, o);
        leftPoints.add(s, a);
        rightPoints.add(s, b);
        topPoints.add(s, y);
        bottomPoints.add(s, y);
        return s;
    }

    public VSegment addVSegment(int a, int b, int x, Object o) {
        VSegment s = new VSegment(a, b, x, o);
        topPoints.add(s, a);
        bottomPoints.add(s, b);
        leftPoints.add(s, x);
        rightPoints.add(s, x);
        return s;
    }

    public PlaneSegment addSegment(int x1, int y1, int x2, int y2, Object o) {
        PlaneSegment s = null;
        if (y1 == y2) {
            s = addHSegment(x1, x2, y1, o);
        }
        else if (x1 == x2) {
            s = addVSegment(y1, y2, x1, o);
        }
        return s;
    }

    public void removeSegment(PlaneSegment s) {
        topPoints.remove(s);
        bottomPoints.remove(s);
        leftPoints.remove(s);
        rightPoints.remove(s);
    }

    public void cancel() {
        cancelled = true;
    }

    public void sweep(int direction, PlaneSweepListener l) {
        int sgn = 1;
        int start = 0;
        int stop = 0;
        PositionList<PlaneSegment> startPoints = null;
        PositionList<PlaneSegment> endPoints = null;
        Class<?> readoutTrigger = null;
        switch (direction) {
            case LEFT_TO_RIGHT:
                startPoints = leftPoints;
                endPoints = rightPoints;
                start = 0;
                stop = startPoints.size();
                sgn = 1;
                readoutTrigger = VSegment.class;
                break;
            case RIGHT_TO_LEFT:
                startPoints = rightPoints;
                endPoints = leftPoints;
                start = startPoints.size() - 1;
                stop = -1;
                sgn = -1;
                readoutTrigger = VSegment.class;
                break;
            case TOP_TO_BOTTOM:
                startPoints = topPoints;
                endPoints = bottomPoints;
                start = 0;
                stop = startPoints.size();
                sgn = 1;
                readoutTrigger = HSegment.class;
                break;
            case BOTTOM_TO_TOP:
                startPoints = bottomPoints;
                endPoints = topPoints;
                start = startPoints.size() - 1;
                stop = -1;
                sgn = -1;
                readoutTrigger = HSegment.class;
                break;
        }
        int is = start;
        int ie = start;
        ArrayList<PlaneSweepEvent> startEvents = new ArrayList<PlaneSweepEvent>();
        ArrayList<PlaneSweepEvent> readoutEvents = new ArrayList<PlaneSweepEvent>();
        PositionList<PlaneSegment> current = new PositionList<PlaneSegment>();
        cancelled = false;
        while (!cancelled && (ie != stop)) {
            while ((is != stop) &&
                   (sgn * startPoints.positionAtIndex(is) <= sgn *
                                                             endPoints.positionAtIndex(ie))) {
                PlaneSegment s = startPoints.elementAtIndex(is);
                if (readoutTrigger.isInstance(s)) {
                    PlaneSweepEvent e = new PlaneSweepEvent(this, s,
                        startPoints.positionAtIndex(is), current);
                    readoutEvents.add(e);
                }
                else {
                    current.add(s, s.getOrthoPos());
                    PlaneSweepEvent e = new PlaneSweepEvent(this, s,
                        startPoints.positionAtIndex(is), current);
                    startEvents.add(e);
                }
                is += sgn;
            }
            int i = 0;
            while (!cancelled && (i < startEvents.size())) {
                // System.out.println("Starting " +
                // ((PlaneSweepEvent)startEvents.get(i)).getSegment().
                // toString()
                // + ": " + current.size());
                l.segmentStarted(startEvents.get(i));
                i++;
            }
            startEvents.clear();
            i = 0;
            while (!cancelled && (i < readoutEvents.size())) {
                // System.out.println("*** Readout " +
                // ((PlaneSweepEvent)readoutEvents.get(i)).getSegment().
                // toString()
                // + ": " + current.size());
                l.readout(readoutEvents.get(i));
                i++;
            }
            readoutEvents.clear();
            PlaneSegment s = endPoints.elementAtIndex(ie);
            if ((ie != stop)) {
                // System.out.println("Ending " + s.toString() + ": " +
                // current.size());
                if (!cancelled && !readoutTrigger.isInstance(s)) {
                    current.remove(s);
                    PlaneSweepEvent e = new PlaneSweepEvent(this, s,
                        endPoints.positionAtIndex(ie), current);
                    l.segmentEnded(e);
                }
                ie += sgn;
            }
        }
    }

}

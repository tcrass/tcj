package de.tcrass.util.planesweep;

import java.util.EventObject;

import de.tcrass.util.PositionList;

public class PlaneSweepEvent extends EventObject {

    private static final long          serialVersionUID = 1L;
    public static final int            SEGMENT_STARTED  = 1;
    public static final int            SEGMENT_ENDED    = -1;
    public static final int            READOUT          = 0;

    private PositionList<PlaneSegment> elements;
    private PlaneSegment               segm;
    private long                       pos;

    public PlaneSweepEvent(Plane src,
                           PlaneSegment s,
                           long pos,
                           PositionList<PlaneSegment> elements) {
        super(src);
        segm = s;
        this.pos = pos;
        this.elements = elements;
    }

    public PlaneSegment getSegment() {
        return segm;
    }

    public long getPos() {
        return pos;
    }

    public PositionList<PlaneSegment> getCurrentElements() {
        return elements;
    }

    public Plane getPlane() {
        return (Plane) getSource();
    }

}

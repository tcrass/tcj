package de.tcrass.javadoc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import com.sun.javadoc.Tag;
import com.sun.tools.doclets.Taglet;

/**
 * @include foo bar baz
 * 
 * @author tcrass
 *
 */
public class IncludeFileTaglet implements Taglet {

    public static final String NAME = "include.file";
    
    public static void register(Map tagletMap) {
        IncludeFileTaglet t = new IncludeFileTaglet();
        if (tagletMap.containsKey(t.getName())) {
            tagletMap.remove(t.getName());
        }
        tagletMap.put(t.getName(), t);
    }
    
    public String getName() {
        return NAME;
    }

    public boolean inConstructor() {
        return false;
    }

    public boolean inField() {
        return false;
    }

    public boolean inMethod() {
        return false;
    }

    public boolean inOverview() {
        return false;
    }

    public boolean inPackage() {
        return false;
    }

    public boolean inType() {
        return false;
    }

    public boolean isInlineTag() {
        return true;
    }

    public String toString(Tag tag) {
        return toString(new Tag[] {tag});
    }
    
    public String toString(Tag[] tags) {
        StringBuffer s = new StringBuffer();
        try {
            for (Tag t : tags) {
                String path = t.text().trim();
                BufferedReader r = new BufferedReader(new FileReader(path));
                String line;
                while ((line = r.readLine()) != null) {
                    s.append(line + "\n");
                }
            }
        }
        catch (IOException ex) {
            s.append("<br>\n<b style=\"color:#ff0040\">ERROR: " + ex.getMessage() + "</b><br>\n");
        }
        return s.toString();
    }

    
    
}
